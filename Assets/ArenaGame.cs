﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaGame : MonoBehaviour
{
    public int killsNeeded;
    public int kills;
    public Text killPrint;


    public GameObject gameOverText;
    public GameObject winText;

    public Health playerHealth;

    // Reference to our save script so we can save data before advancing to next level
    public SaveGame saveScript;

    // Reference to Player Stats to keep track of kills
    public PlayerStats playerStats;
    public PlayerManager playerManager;
    int expWas;

    bool alreadyWon;

    // Update is called once per frame
    void Update()
    {
	if(expWas < playerStats.exp)
	    kills ++;
	
	expWas = playerStats.exp;
	
        killPrint.text = "Kills " + kills + "/" +killsNeeded;

	if(!alreadyWon && kills >= killsNeeded)
	{
	    alreadyWon = true;
	    
	    winText.SetActive(true);

	    saveScript.SaveZombsOnly(playerManager.playerUnits);
	}
	
	if(playerHealth.currHealth <= 0f)
	{
	    gameOverText.SetActive(true);
	    winText.SetActive(false);
	}
    }
}
