﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    // How far in front of the player should the attack takes place
    public float attackDistance;

    // How much damage each attack deals
    public int damageMax;
    public int damageMin;

    // Reference to character animator
    public Animator anim;

    // Tag to make sure we only attack once per animation
    private bool alreadyAttacked;

    // If a script is added here, damage dealt is sent to the script for processing
    public Weapon feedBackScript;

    
    // Reference to Fire Master script for spawning fires if fire archer
    public FireRoot fireRoot;


    // AudioSource to play sounds
    public AudioSource audioSource;
    
    // Audio clip to play when firing the bow
    public AudioClip fireSound;
    public AudioClip drawSound;
    
    // For shooting arrows
    public GameObject arrowPrefab;


    // Arrow speed
    public float arrowSpeed;

    // Base arrow pitch, angle to shoot it at
    float arrowPitch;

    // Amount to adjust pitch to help tweak accuracy of shots
    public float pitchAdjustment;

    public float maxDistance;

    // Distance the arrow's target is from the player
    public float targetDistance;
    
    // Variable to store raycast hits
    private RaycastHit hit;


    Vector3 oldPosition;

    public Wind windScript;
    
 
    void Update()
    {
	AnimatorStateInfo animatorInfo = anim.GetCurrentAnimatorStateInfo(0);

	// Is attack animation playing?
	if(animatorInfo.tagHash == Animator.StringToHash("Attacking"))
	{
	    
	    if(animatorInfo.normalizedTime < .3f)
	    {
		alreadyAttacked = false;
	    }
	    // Make sure we did not already determine hit and apply damage
	    if(!alreadyAttacked)
	    {
		if(animatorInfo.normalizedTime > .5f)
		{
		    // Raycast again to make sure target didn't move out of range before attack finished
		    // Create a raycast out the front of the zombie
		    Ray ray = new Ray(oldPosition - transform.right * .5f, transform.forward);
		    Ray ray2 = new Ray(oldPosition, transform.forward);
		    Ray ray3 = new Ray(oldPosition + transform.right * .5f, transform.forward);

		    Debug.DrawRay(oldPosition - transform.right * .5f, transform.forward * attackDistance, Color.green, 1f);
		    Debug.DrawRay(oldPosition, transform.forward * attackDistance, Color.green, 1f);
		    Debug.DrawRay(oldPosition + transform.right * .5f, transform.forward * attackDistance, Color.green, 1f);		    
		    // Calculate if there was a hit, within the attack distance
		    if (Physics.Raycast(ray, out hit, attackDistance) || Physics.Raycast(ray2, out hit, attackDistance) || Physics.Raycast(ray3, out hit, attackDistance))
		    {				
			// Our raycast hit something, therefor apply damage to it			
			// Random damage
			float damageDealt = Random.Range(damageMin, damageMax);

			// Make a reference to the hit objects health script (if any)
			Health theirHealth = hit.transform.GetComponent<Health>();
			
			// If hit has health component and they aren't dead already
			if(theirHealth != null && theirHealth.currHealth > 0)
			{
			    // Keep track of exactly how much damage we dealt
			    float oldHealth = theirHealth.currHealth;
			    
			    // Send damage and relative direction to enemy
			    theirHealth.TakeDamage(damageDealt, transform);

			    // Calculate how much damage was actually dealt
			    float realDmg = oldHealth - theirHealth.currHealth;
			    
			    if(feedBackScript)
			    {
				bool killingBlow = false;

				// If they are now dead
				if(theirHealth.currHealth < 0f)
				    killingBlow = true;
				
				feedBackScript.FeedBack(realDmg, killingBlow);
			    }
			}
		    }
		    // Set boolean to make sure we don't attack again this attack
		    alreadyAttacked = true;
		}
	    }	    
	}
	else if(animatorInfo.tagHash == Animator.StringToHash("Shooting"))
	{
	    if(animatorInfo.normalizedTime < .3f)
	    {
		alreadyAttacked = false;

		audioSource.clip = drawSound;
		audioSource.Play();
	    }
	    // Make sure we did not already determine hit and apply damage
	    if(!alreadyAttacked)
	    {
		if(animatorInfo.normalizedTime > .5f)
		{
		    // Instantiate arrow
		    GameObject arrow = Instantiate(arrowPrefab, transform.position + transform.forward + transform.up * .75f, Quaternion.identity);

		    arrow.GetComponent<WindPush>().windScript = windScript;

		    Arrow arrowScript = arrow.GetComponent<Arrow>();
		    arrowScript.player = transform.gameObject;

		    
		    FireArrow fireArrowScript = arrow.GetComponent<FireArrow>();
		    
		    if(fireArrowScript)
		    {
			fireArrowScript.fireRoot = fireRoot;
		    }
		    
		    //Play firing sound effect
		    audioSource.clip = fireSound;
		    audioSource.Play();

		    if(targetDistance > maxDistance)
			targetDistance = maxDistance;
		    
		    // Aim up more when target is farther away to arch arrow through air and adjust for gravity
		    arrowPitch = targetDistance * pitchAdjustment;

		    // Flip arrow pitch if facing other direction (negative is up positive is down when backwards)
		    if(transform.rotation.eulerAngles.y > 180f)
		    {
			arrowPitch = -arrowPitch;
		    }
		    
		    arrow.GetComponent<Rigidbody>().velocity = Quaternion.AngleAxis(arrowPitch, Vector3.forward) * transform.forward * (arrowSpeed * Random.Range(.85f, 1.15f));

		    Debug.Log(arrow.transform.rotation);

		    GameObject.Destroy(arrow, 3f);

		    // Set boolean to make sure we don't attack again this attack
		    alreadyAttacked = true;
		}
	    }
	}

	// Remember previous position
	oldPosition = transform.position;
    }
}
