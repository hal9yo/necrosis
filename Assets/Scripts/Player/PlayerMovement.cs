﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    // Our stats (to get current stats.runSpeed from)
    public Stats stats;

    // How fast the walking animation moves
    float baseAnimSpeed = .7f;
    float animSpeed = .7f;
    
    // Reference to player animator
    public Animator[] anim;

    // Whether to do melee or archer attack
    public bool archery;

    public PlayerAttack attackScript;

    // Boolean to register when player attacks
    bool attack;

    float moveSpeed;
    
    AnimatorStateInfo animState;

    // Start is called before the first frame update
    void Start()
    {
	moveSpeed = stats.runSpeed;	
    }


    
    // Update is called once per frame
    void Update()
    {

	if(Input.GetKey(KeyCode.LeftShift))
	{
	    animSpeed = baseAnimSpeed;
	    moveSpeed = stats.walkSpeed;
	}
	else
	{
	    animSpeed = baseAnimSpeed + .3f;
	    moveSpeed = stats.runSpeed;
	}
	
        // Store movement input in a vector3 variable
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

	// Normalize movement vector so diagonal movement is not faster
	movement.Normalize();

	// Unless game is paused or menu is open
	if(Time.timeScale > 0f)
	    // If attack pressed, set the boolean to tell the script
	    attack = Input.GetKey(KeyCode.LeftControl);

	// Grab current animator state to see if attacking or something
	animState = anim[0].GetCurrentAnimatorStateInfo(0);

	// Adjust player position by the momentum variable just made

	//Remember position
	Vector3 position = transform.position;


	transform.position += movement * moveSpeed * Time.deltaTime;	


	// Update animations to reflect current state, unless attacking
	if (!animState.IsTag("Attacking") && !animState.IsTag("Shooting"))
	{
	    //Update direction if moved more than distance
	    if(Mathf.Abs(transform.position.x-position.x) > .1f || Mathf.Abs(transform.position.z-position.z) > .1f)
	    {
		//Look in direction you have moved
		transform.rotation = Quaternion.LookRotation(transform.position - position);
	    }
	    
	    foreach (Animator thisAnim in anim)
	    {
		if(thisAnim.gameObject.activeSelf)
		{
		    if (movement.magnitude != 0)
		    {
			thisAnim.SetFloat("Horizontal", movement.x);
			thisAnim.SetFloat("Vertical", movement.z);
			thisAnim.SetBool("Walking", true);

			//thisAnim.SetFloat("Speed", animSpeed);
		    }
		    else
		    {
			thisAnim.SetBool("Walking", false);
		    }
		}
	    }
	}

	
	if (attack)
	{
	    RaycastHit hit;
	    
	    //Constantly check mouse position for hover over usable objects
	    //Create a ray from the camera to our space
	    Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

	    //If the raycast hit something
	    if(Physics.Raycast(camRay, out hit))
	    {
		// Make character look at where the mouse was clicked
		transform.LookAt(hit.point);
	    }
	    
	    // Debug.Log(mousePos);

	    // Debug.Log(lookPoint);
	    // Set attacking animation
	    if(!archery)
	    {
		// Do melee attack animation
		if (!animState.IsTag("Attacking"))
		{
		    foreach(Animator thisAnim in anim)
		    {
			thisAnim.SetTrigger("Attack");
			thisAnim.SetFloat("Horizontal", transform.forward.x);
			thisAnim.SetFloat("Vertical", transform.forward.z);
		    }
		}
	    }
	    else
	    {
		// Get distance to mouse click spot
		float targetDistance = Vector3.Distance(transform.position, hit.point);

		attackScript.targetDistance = targetDistance;
		
		// Do Archery attack animation
		if (!animState.IsTag("Shooting"))
		{
		    foreach(Animator thisAnim in anim)
		    {
			thisAnim.SetTrigger("Shoot");
		    }
		}
		foreach(Animator thisAnim in anim)
		{
		    thisAnim.SetFloat("Horizontal", transform.forward.x);
		    thisAnim.SetFloat("Vertical", transform.forward.z);
		}
	    }
	}
    }
}
