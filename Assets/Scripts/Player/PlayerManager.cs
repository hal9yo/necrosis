﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    //Reference to PlayerStats for will
    public PlayerStats playerStat;
    
    //Reusable raycast hit
    RaycastHit hit;

    //Raycast variable
    Ray camRay;

    // List to hold all player units
    public GameObject[] playerUnits;

    // Which spell, if any, is currently selected
    public int spell;

    // Which spells are currently unlocked
    public bool[] spells;
    private int spellCount = 0;

    public SpellIcons spellIcons;

    //The list of selected units
    public List<Transform> selectedUnits = new List<Transform>();

    
    //The lists of units stored under number hotkeys
    public List<Transform> group1 = new List<Transform>();
    public List<Transform> group2 = new List<Transform>();
    public List<Transform> group3 = new List<Transform>();
    public List<Transform> group4 = new List<Transform>();


    //Boolean to facilitate dragging
    bool isDragging = false;

    //Boolean to remember if we are invoking Fire
    bool fireStart = false;
    //Boolean to remember if we are banishing Fire
    bool fireStop = false;

    //Boolean to remember if we are invoking Water
    bool waterStart = false;
    //Boolean to remember if we are banishing Water
    bool waterStop = false;

    //Remember initial mouse position for dragging
    Vector3 mousePosition;

    //Remember what unit we hovered over, so we can turn off icons when mouse no longer hovers
    private Transform hoveredUnit = null;
        
    //A prefab to make Waypoints from
    public GameObject movePointPrefab;

    //A prefab of the Fire object 
    public GameObject firePrefab;

    
    //A prefab of the Water object
    public GameObject waterPrefab;
    

    //A reference to the fire root script to remove fireobjects if we disable them
    public FireRoot fireRoot;

    //A reference to the water root script to add water objects if we create them
    public WaterRoot waterRoot;

    
    //The location of the last Water the mouse was touching
    Vector3 lastFire;
    //The location of the last Water the mouse was touching
    Vector3 lastWater;

    
    // Text display object in UI
    public GameObject textDisplay;
    // Text box of text display
    public Text textBox;

    
    // reference to the wind script in this level
    public Wind windScript;

    public Inventory invScript;

    // Whether or not zombies can go rogue if left unattended too long
    public bool zombiesCantRogue;

    // How much subtracted from will drain of each zombie
    public float drainBonus;

    // Hotspot for mouse Icons
    Vector2 hotSpot = new Vector2(16f, 16f);
    
    // Mouse icons for various actions
    public Texture2D pickUpIcon;
    public Texture2D lookAtIcon;
    
    // Distance player can pick up objects
    public float interactDist = 4f;
    // Distance to interact with objects
    public float normalInteractDist = 4f;
    
    // The PauseGame script accesses the inventory menu, we will use this to access the equiped items on NPCs
    public PauseGame pauseScript;
    
    // Reference to the spell animator
    //public Animator spellAnim;
    
    // Reference to character animator
    public Animator anim;

    public LayerMask layerMask;
    
    private void OnGUI()
    {
	if(isDragging && spell == 0)
	{
	    //Make a rectangle between the mouse position stored when mouse first clicked and the current mouse position
	    var rect = ScreenHelper.GetScreenRect(mousePosition, Input.mousePosition);

	    //Draw on the screen the rectangle made
	    ScreenHelper.DrawScreenRect(rect, new Color(0.4f, 0.7f, 0f, 0.1f));

	    ScreenHelper.DrawScreenRectBorder(rect, 1, new Color(0.4f, 0.7f, 0f, 0.7f));
	}
    }


    // Update called at start
    void Start()
    {	
	// Initialize list of Player Units
	playerUnits = GameObject.FindGameObjectsWithTag("PlayerUnit");

	for(int i = 0; i < spells.Length; i++)
	{
	    if(spells[i])
		spellCount++;
	}

	// If the we have not already hooked up the firs master script to this script go find it
	if(!fireRoot)
	    fireRoot = GameObject.Find("FireRoot").GetComponent<FireRoot>();

	// If we haven't hooked up wind script to this one go find it
	if(!windScript)
	    windScript = GameObject.Find("Wind").GetComponent<Wind>();
    }
    
    // Update is called once per frame
    void Update()
    {
	if(Time.timeScale > 0f)
	{
	    // Reset will drain
	    playerStat.willDrain = 0f;

	    // Did we run out of will?
	    if(playerStat.will <=0)
	    {
		Debug.Log("Out of will");
	    
		// Deselect units
		DeselectUnits();

		//Make every PlayerUnit go rogue
		foreach(GameObject obj in playerUnits)
		{
		    Zombie zombScript = obj.transform.GetComponent<Zombie>();
		    // Set the unit to rogue
		    zombScript.isRogue = true;

		    // Clear the unit's waypoints
		    zombScript.targets.Clear();
		}
	    }
	    // If will is not negative
	    else
	    {
		// Check every player unit
		foreach(GameObject obj in playerUnits)
		{
		    // Make sure is not dead or something
		    if(obj.CompareTag("PlayerUnit"))
		    {
			// Grab this units zombie script
			Zombie zombieScript = obj.transform.GetComponent<Zombie>();

			// If the player unit is not rogue
			if(!zombieScript.isRogue)
			{
			    if(zombieScript.willDrain - drainBonus >= 0f)
				//Increase will drain by the amount this unit drains
				playerStat.willDrain += zombieScript.willDrain - drainBonus;
			    else
				playerStat.willDrain = 0f;
			}
		    }
		}
	    }
	
	    //Constantly check mouse position for hover over usable objects
	    //Create a ray from the camera to our space
	    camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

	    //If the raycast hit something
	    if(Physics.Raycast(camRay, out hit, 300f, ~layerMask))
	    {
		//Debug.Log(hit.transform.gameObject);
		
		// Run hover method on hit object
		OnHover(hit);

		AnimatorStateInfo animatorInfo = anim.GetCurrentAnimatorStateInfo(0);

		if(animatorInfo.tagHash == Animator.StringToHash("Shooting"))
		{
		}
		else if(isDragging)
		    // Make character look at where the mouse was clicked
		    transform.LookAt(hit.point);
	    }
	 
	    //Are we dragging the mouse?
	    if(isDragging && spell == 0)
	    {
		//Make a list of PlayerUnits
		GameObject[] playerUnits = GameObject.FindGameObjectsWithTag("PlayerUnit");

		//Check every unit in list to see if it is within the bounds of the selection rectangle
		foreach(GameObject obj in playerUnits)
		{
		    // Store reference to currently hovered object's health
		    Health theirHealth = obj.transform.GetComponent<Health>();
		    
		    //If this object is within the selection rectangle
		    if (IsWithinSelectionBounds(obj.transform))
		    {
			//Show hover selection, indicating unit will be selected when mouse unclicked
			theirHealth.hoverSelect = true;
			
			//Show health meter
			theirHealth.showLife = true;
		    }
		    else
		    {
			//Don't show selection brackets
			theirHealth.hoverSelect = false;
			//Don't show health meter
			theirHealth.showLife = false;
		    }
		}
	    }
	
	
	    //Detect when mouse is pressed down
	    if(Input.GetMouseButtonDown(0))
	    {
		MouseDown();
	    }

	
	    //Detect when mouse is released
	    if(Input.GetMouseButtonUp(0))
	    {
		MouseUp();
	    }


	    //When player right-clicks and zombie(s) is currently selected
	    if(Input.GetMouseButtonDown(1))
	    {
		RightMouseDown();
	    }
	

	    // When player holds Shift and clicks a number, assigning units to hotkey
	    if(Input.GetKey(KeyCode.LeftShift) && selectedUnits.Count > 0)
	    {
		// Pressed number 1 key
		if(Input.GetKey(KeyCode.Alpha1))
		{
		    PopulateGroup(1);
		}
		// Pressed number 2 key
		if(Input.GetKey(KeyCode.Alpha2))
		{
		    PopulateGroup(2);
		}
		// Pressed number 3 key
		if(Input.GetKey(KeyCode.Alpha3))
		{
		    PopulateGroup(3);
		}
		// Pressed number 4 key
		if(Input.GetKey(KeyCode.Alpha4))
		{
		    PopulateGroup(4);
		}

	    }

	    // Player is not holding Ctrl and presses 1 key
	    if(!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha1))
	    {
		SelectGroup(1);
	    }

	    // Player is not holding Ctrl and presses 2 key
	    if(!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha2))
	    {
		SelectGroup(2);
	    }

	    // Player is not holding Ctrl and presses 3 key
	    if(!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha3))
	    {
		SelectGroup(3);
	    }

	    // Player is not holding Ctrl and presses 4 key
	    if(!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha4))
	    {
		SelectGroup(4);
	    }

	    // Player presses 'Q' key
	    if(Input.GetKeyDown(KeyCode.Q))
	    {
		SelectArchers();
	    }

	    // Player presses 'E' key
	    if(Input.GetKeyDown(KeyCode.E))
	    {
		SelectMelee();
	    }

	    // Player presses 'F' key
	    if(Input.GetKeyDown(KeyCode.F))
	    {
		SelectAll();
	    }
	}
    }



    private void OnHover(RaycastHit hit)
    {
	//Debug.Log(hit.transform.gameObject.name);

	if(hit.transform)
	{
	    // If we are hovering a permadead character
	    if(hit.transform.gameObject.CompareTag("PermaDead"))
	    {
		// Make a layermask for items
		LayerMask mask = (1 << 18);
	    
		// Get a new raycast just for items
		if(Physics.Raycast(camRay, out hit, 300f, mask))
		{

		}
	    }
	    // If we are hovering over an item or character and  Are we close enough to pick it up/interact with their inv	    
	    else if(((hit.transform.gameObject.layer == 18 || (hit.transform.gameObject.layer == 8 && hit.transform.GetComponent<NPC1>())) && Vector3.Distance(hit.point, transform.position) < interactDist) || hit.transform.CompareTag("Interactive") && Vector3.Distance(hit.point, transform.position) < normalInteractDist)
	    {
		// Change the mouse icon to the hand, and make it fully opaque
		Cursor.SetCursor(pickUpIcon, hotSpot, CursorMode.ForceSoftware);
	    }
	    // If the mouse hovers an object with text to display
	    else if(hit.transform.CompareTag("Text"))
	    {
		// Change the mouse icon to the eye, and make it fully opaque
		Cursor.SetCursor(lookAtIcon, hotSpot, CursorMode.ForceSoftware);
	    }
	    // Not hovering over an item or text object, make mouse cursor normal
	    else
	    	Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

	

	    // SPELLS
	
	    // Have we started invoking Fire
	    if(fireStart)
	    {
		// Is the mouse over an area with no fire and no water
		if(!hit.transform.CompareTag("Fire") && !hit.transform.CompareTag("Water"))
		{
		    // Is the mouse within range of the player **not doing this right now: and the last touched fire? (Don't let player pull fire too far)
		    if(Vector3.Distance(hit.point, transform.position) < (playerStat.fireLvl + 1) * 5f)
		    {
			if(playerStat.will >= 2f)
			{
			    // Create fire here
			    fireRoot.SpawnFire(hit.point, true, null);

			    // Subtract the will it cost
			    playerStat.will -= 2f;

			    // Increase our experience with fire magic
			    playerStat.IncFireExp();
			}
			else
			{
			    // We ran out of will, stop the spell
			    fireStart = false;
			    playerStat.pauseRegen = false;
			}
		    }
		}
	    }

	    // Have we started invoking Water
	    if(waterStart)
	    {
		// Is the mouse over an area with no water
		if(!hit.transform.CompareTag("Water"))
		{
		    // Is the mouse within range of the player **not doing this right now: and the last touched fire? (Don't let player pull fire too far)
		    if(Vector3.Distance(hit.point, transform.position) < (playerStat.waterLvl + 1) * 5f)
		    {
			if(playerStat.will >= 3f)
			{
			    // Create water objects in center, north, south, east and west of mouse position
			    GameObject water1 = Instantiate(waterPrefab, new Vector3(hit.point.x, 0f, hit.point.z), Quaternion.identity);
			
			    GameObject water2 = Instantiate(waterPrefab, new Vector3(hit.point.x + 4f, 0f, hit.point.z), Quaternion.identity);
			    GameObject water3 = Instantiate(waterPrefab, new Vector3(hit.point.x, 0f, hit.point.z + 3f), Quaternion.identity);
			    GameObject water4 = Instantiate(waterPrefab, new Vector3(hit.point.x - 4f, 0f, hit.point.z), Quaternion.identity);
			    GameObject water5 = Instantiate(waterPrefab, new Vector3(hit.point.x, 0f, hit.point.z - 3f), Quaternion.identity);

			    // Add the new water objects to the water root to animate them
			    waterRoot.waterScripts.Add(water1.GetComponent<Water>());
			    waterRoot.waterScripts.Add(water2.GetComponent<Water>());
			    waterRoot.waterScripts.Add(water3.GetComponent<Water>());
			    waterRoot.waterScripts.Add(water4.GetComponent<Water>());
			    waterRoot.waterScripts.Add(water5.GetComponent<Water>());

			
			    // Subtract the will it costs to make water
			    playerStat.will -= 3f;

			    // Increase water experience so your spell level levels up over time
			    playerStat.IncWaterExp();
			}
			else
			{
			    // We ran out of will, stop the spell
			    waterStart = false;
			    playerStat.pauseRegen = false;
			}
		    }
		}
		// Mouse over water object
		else
		{
		    // Grab the water Script on the water object
		    Water thisWaterScript = hit.transform.GetComponent<Water>();
		
		    // If that water object is currently off
		    if(!thisWaterScript.on)
		    {
			// Turn it on
			thisWaterScript.TurnOn();

			// Subtract required Will for the operation
			playerStat.will -= 2f;
		    }
		}
	    }

	    // Have we started banishing fire and the mouse is on an area that currently has fire and do we have enough will?
	    if(fireStop && playerStat.will >= 1f)
	    {
		// Did we hit a fire object?
		if(hit.transform.CompareTag("Fire"))
		{
		    //Get a reference to the fire script
		    GrassFire grassFire = hit.transform.gameObject.GetComponent<GrassFire>();

		    if(grassFire)
		    {
			// Remove it from the master list so we don't try using it again
			fireRoot.grassFires.Remove(hit.transform.gameObject.GetComponent<GrassFire>());
			fireRoot.soundOn.Remove(hit.transform.gameObject.GetComponent<GrassFire>());
		    }
		
		    // Put it in the sky real quick so it will stop colliding with other colliders first (otherwise they are never notified of "OnTriggerExit" and variables depending on this object keep assuming it's there
		    hit.transform.position = new Vector3(0f, 666f, 0f);
		
		    // Destroy this fire object
		    hit.transform.gameObject.SetActive(false);

		    // Subtract the required will
		    playerStat.will -= 1f;
		}
		// Did we hit a character object?
		else if(hit.transform.gameObject.layer == 8)
		{
		    // Get a reference to the fire script on the character
		    CharacterFire characterFireScript = hit.transform.GetChild(1).GetComponent<CharacterFire>();
		
		    // If it is on fire
		    if(characterFireScript.heat > 0f)
		    {
			// Turn off the fire
			characterFireScript.heat = 0f;

			// Subtract the required will
			playerStat.will -= 1f;
		    }
		}
	    }

	    // Have we started banishing fire and the mouse is on an area that currently has fire?
	    if(waterStop)
		if(hit.transform.CompareTag("Water"))
		{
		    // Get a reference to the water Script on this water object
		    Water thisScript = hit.transform.GetComponent<Water>();

		    // If the water is currently on
		    if(thisScript.on)
		    {
			// Do we have enough will?
			if(playerStat.will >= 1f)
			{
			    // Tell it to turn off
			    thisScript.TurnOff();
		
			    // Subtract the required will
			    playerStat.will -= 1f;
			}
		    }
		}
	
	    // If we have the fire spell selected
	    if(spell == 1)
	    {
		// If the mouse cursor is close enough to the player to cast
		if(Vector3.Distance(hit.point, transform.position) < (playerStat.fireLvl + 1) * 5f)
		{
		    // Make the mouse cursor colored so we know we are in range
		    spellIcons.oor = false;
		    // Update the mouse cursor
		    spellIcons.Fire();
		
		    // If the mouse hovers over a fire object we can manipulate and we have enough will to cast
		    if(hit.transform.CompareTag("Fire") & playerStat.will >= 2f)
		    {
			// Change transparency of icon to indicate spell can be used on object
			spellIcons.hover = true;
			// Update the mouse cursor
			spellIcons.Fire();
		    }
		    // If the mouse cursor doesn't hover over a fire object or we don't have enough will
		    else
		    {
			// Turn the cursor transparency on and update the mouse cursor
			spellIcons.hover = false;
			spellIcons.Fire();	    
		    }
		}
		// If the mouse cursor is not close enough to the player to cast
		else
		{
		    // Make the mouse cursor white so we know we are Out Of Range
		    spellIcons.oor = true;
		    // Update the mouse cursor
		    spellIcons.Fire();
		}
	    }

	    // If we have the air spell selected
	    if(spell ==2)
	    {
		// Get distance of mouse from player
		float airDist = Vector3.Distance(hit.point, transform.position);
	    
		// Is the distance close enough to cast at our current Air Magic Level?
		if(airDist < (playerStat.airLvl + 1) * 5f)
		{
		    // Make the mouse cursor colored so we know we are in range
		    spellIcons.oor = false;

		    if(playerStat.will >= airDist)
			// Change transparency of icon to indicate spell can be used currently
			spellIcons.hover = true;
		    else
			// Change transparency of icon to indicate spell can not be used currently
			spellIcons.hover = false;
		
		    // Update the mouse cursor
		    spellIcons.Air();

		}
		// Out of Range to cast spell
		else
		{
		    // Make the mouse cursor white so we know we are Out Of Range
		    spellIcons.oor = true;
		    // Change transparency of icon to indicate spell can't be used
		    spellIcons.hover = false;
		    // Update the mouse cursor
		    spellIcons.Air();
		}
	    }

	    // If we have the water spell selected
	    if(spell == 3)
	    {
		// If the mouse cursor is close enough to the player to cast
		if(Vector3.Distance(hit.point, transform.position) < (playerStat.waterLvl + 1) * 5f)
		{
		    // Make the mouse cursor colored so we know we are in range
		    spellIcons.oor = false;
		    // Update the mouse cursor
		    spellIcons.Water();
		
		    // If the mouse hovers over a fire object we can manipulate and we have enough will to cast
		    if(hit.transform.CompareTag("Water") & playerStat.will >= 2f)
		    {
			// Change transparency of icon to indicate spell can be used on object
			spellIcons.hover = true;
			// Update the mouse cursor
			spellIcons.Water();
		    }
		    // If the mouse cursor doesn't hover over a fire object or we don't have enough will
		    else
		    {
			// Turn the cursor transparency on and update the mouse cursor
			spellIcons.hover = false;
			spellIcons.Water();	    
		    }
		}
		// If the mouse cursor is not close enough to the player to cast
		else
		{
		    // Make the mouse cursor white so we know we are Out Of Range
		    spellIcons.oor = true;
		    // Update the mouse cursor
		    spellIcons.Water();
		}
	    }
	
	    // If we have a previously hovered unit
	    if(hoveredUnit != null)
	    {
		//If current unit does not match previously hovered, turn off previously hovered
		if(!GameObject.ReferenceEquals(hit.transform.gameObject, hoveredUnit.gameObject))
		{
		    //Store reference to hovered items health
		    Health hoverHealth = hoveredUnit.GetComponent<Health>();
		    //Check for health component
		    if(hoverHealth)
		    {
			// Turn off any icons we turned on when hovering
			hoverHealth.hoverRaise = false;
			hoverHealth.hoverSelect = false;
			hoverHealth.showLife = false;
			hoverHealth.showBanEarthIcon = false;
		    }
		}
	    }

	    // Store reference to currently hovered object's health
	    Health thisHealth = hit.transform.GetComponent<Health>();
	    
	    // Hovering over unit with icons to turn on
	    if(thisHealth)
	    {
		//turn on hoverselect, indicating unit is selectable
		thisHealth.hoverSelect = true;
		    
		//turn on showLife, show their lifemeter
		thisHealth.showLife = true;

		// Remember we were hovering this object
		hoveredUnit = hit.transform;
		if(hit.transform.CompareTag("Dead") && Vector3.Distance(hit.point, transform.position) > interactDist)
		{
		    //Show raise icon
		    thisHealth.hoverRaise = true;
		    
		    // Remember we were hovering this object
		    hoveredUnit = hit.transform;
		}
	    }
	    // not hovering, deactivate previous, if any
	    else
	    {

		hoveredUnit = null;
	    }
	}
    }
    


    private void LateUpdate()
    {
	transform.eulerAngles = new Vector3( 0f, transform.eulerAngles.y, transform.eulerAngles.z );
    }




    


    private void MouseDown()
    {	
	//When Left Shift is not being pressed, and no spell is selected, deselect any selected units before starting
	if(!Input.GetKey(KeyCode.LeftShift) && spell == 0)
	{
	    DeselectUnits();
	}

	//Store mouse position
	mousePosition = Input.mousePosition;
	    
	//Create a ray from the camera to our space
	camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

	//Shoot that ray and get the hit data
	if(Physics.Raycast(camRay, out hit, 300f, ~layerMask))
	{
	    // Make character look at where the mouse was clicked
	    transform.LookAt(hit.point);



	    //Debug.Log(hit.transform.gameObject);

	    if(hit.transform)
	    {
	    
		// If raycast hit a character  and  Are we close enough to interact with their inventory
		if(hit.transform.gameObject && hit.transform.gameObject.layer == 8 && hit.transform != gameObject.transform && Vector3.Distance(hit.point, transform.position) < interactDist)
		{
		    // Turn mouse cursor back to normal
		    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
				
		    // Select which character the inventory is being opened for
		    pauseScript.character = hit.transform.gameObject;
		
		    // Open the inventory menu
		    pauseScript.invUp = true;		
		}


		// If we hit a piece of interactive environment (switch/ladder/bookshelf/door/etc)
		else if(hit.transform.CompareTag("Interactive") && Vector3.Distance(hit.point, transform.position) < normalInteractDist)
		{
		    // Run the Use script on the piece of environment
		    hit.transform.GetComponent<InteractiveEnvironment>().Use();
		}
		

		// If raycast hit an item  and  Are we close enough to pick it up
		else if(hit.transform.gameObject && hit.transform.gameObject.layer == 18 && Vector3.Distance(hit.point, transform.position) < interactDist)
		{				
		    // Parent the item to the player
		    hit.transform.parent = transform;

		    // Add the object to our Inventory list
		    invScript.inventory.Add(hit.transform.gameObject);

		    // Turn off gameobject of item so it is invisible and not effected by physics
		    hit.transform.gameObject.SetActive(false);
		}

	    

		// If the mouse hovers an object with text to display
		else if(hit.transform.CompareTag("Text"))
		{
		    // Turn on the text display
		    textDisplay.SetActive(true);

		    // Give the text display the text from the hovered object
		    textBox.text = hit.transform.GetComponent<DisplayText>().text.text;
	    		
		    Debug.Log(LayoutUtility.GetPreferredHeight(textDisplay.GetComponent<RectTransform>()));
		
		    // Update text box size
		    Canvas.ForceUpdateCanvases();

		    // Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
		    textDisplay.SetActive(false);

		    // Turn on the text display
		    textDisplay.SetActive(true);
		}

	    
		// If we have the Fire spell
		else if(spell == 1)
		{
		    // If we are invoking
		    if(spellIcons.invoke)
		    {
			// If the unit hit with the raycast is fire, and the mouse is close enough to the player
			if(hit.transform.CompareTag("Fire") && Vector3.Distance(hit.point, transform.position) < (playerStat.fireLvl + 1) * 5f)
			{
			    // Do we have enough will?
			    if(playerStat.will >= 2f)
			    {
				fireStart = true;
				playerStat.pauseRegen = true;

				//spellAnim.transform.position = transform.forward * 2f;
				//spellAnim.transform.rotation = transform.rotation;
				//spellAnim.SetTrigger("InvFire");

				lastFire = hit.transform.position;
			    }
			}
		    }
		    // If we are banishing
		    else
		    {
			// Do we have enough will?
			if(playerStat.will >= 1f)
			{
			    fireStop = true;
			    playerStat.pauseRegen = true;
			    //spellAnim.transform.position = hit.transform.position;
			    //spellAnim.SetTrigger("BanFire");
			}
		    }
		}
		//If we have the Air spell chosen
		else if(spell == 2)
		{
		    // Get distance of mouse from player
		    float airDist = Vector3.Distance(hit.point, transform.position);

		    // Do we have enough Will to cast at this distance?
		    if(playerStat.will >= airDist && airDist < (playerStat.airLvl + 1) * 5f)
		    {		    
			// Set the direction of the wind to the direction the mouse is from the player
			windScript.direction = new Vector3(hit.point.x - transform.position.x, 0f, transform.position.z - hit.point.z).normalized;

			// Set the strength of the wind to the distance from the player to the mouse
			windScript.strength = airDist;

			// Subtract the amount of will equal to the strength we just set it to
			playerStat.will -= airDist;

			// Increase experience with air magic
			playerStat.IncAirExp();
		    }
		}

		//If we have the Water spell chosen
		else if(spell == 3)
		{
		    // If we are invoking
		    if(spellIcons.invoke)
		    {
			// If the unit hit with the raycast is fire, and the mouse is close enough to the player
			if(hit.transform.CompareTag("Water") && Vector3.Distance(hit.point, transform.position) < (playerStat.waterLvl + 1) * 5f)
			{
			    // Do we have enough will?
			    if(playerStat.will >= 2f)
			    {
				waterStart = true;
				playerStat.pauseRegen = true;

				//spellAnim.transform.position = transform.forward * 2f;
				//spellAnim.transform.rotation = transform.rotation;
				//spellAnim.SetTrigger("InvFire");

				lastWater = hit.transform.position;
			    }
			}
		    }
		    // If we are banishing
		    else
		    {
			// Do we have enough will?
			if(playerStat.will >= 1f)
			{
			    waterStop = true;
			    playerStat.pauseRegen = true;
			    //spellAnim.transform.position = hit.transform.position;
			    //spellAnim.SetTrigger("BanFire");
			}
		    }
		}
	    
		//If we have the Earth spell chosen
		else if(spell == 4)
		{
		}
	    
		//If the unit hit with the raycast has the PlayerUnit tag
		else if(hit.transform.CompareTag("PlayerUnit") || hit.transform.CompareTag("Rogue"))
		{
		    //Run SelectUnit method, passing the unit hit with the raycast
		    SelectUnit(hit.transform);
		}
		// Dead unit selected
		else if(hit.transform.CompareTag("Dead"))
		{
		    // Do you have enough will?
		    if(playerStat.will >= 10f)
		    {
			// Resurrect dead unit
			hit.transform.gameObject.GetComponent<Health>().Resurrect();

			// Reference zombScript
			Zombie zombScript = hit.transform.gameObject.GetComponent<Zombie>();

			if(zombScript)
			{
			    // Deselect unit
			    zombScript.selected = false;

			    if(zombiesCantRogue)
				zombScript.canRogue = false;
			    else
				zombScript.canRogue = true;
			}
		    
			// Reinitialize list of player units
			playerUnits = GameObject.FindGameObjectsWithTag("PlayerUnit");	
			    
			// Subtract requisite will
			playerStat.will -= 10f;
		    }
		}


		//If we didn't click on a unit and a text box isn't up
		else if(!textDisplay.activeSelf)
		{
		    isDragging = true;
		}
	    }
	    
	    // Our raycast hit nothing (just start dragging anyway)
	    else
		isDragging = true;
	}
    }




    private void RightMouseDown()
    {
	// If not holding Ctrl
	if(!Input.GetKey(KeyCode.LeftShift))
	{
	    // If we have selected units
	    if(selectedUnits.Count > 0)
	    {
		// Tell units to go somewhere or attack something
		RightClickSelectedUnits();
	    }
	}
	// If we are holding Ctrl
	else
	{
	    //Store mouse position
	    mousePosition = Input.mousePosition;
	    
	    //Create a ray from the camera to our space
	    camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

	    //Shoot that ray and get the hit data
	    if(Physics.Raycast(camRay, out hit, 300f, ~layerMask))
	    {
		//If the unit hit with the raycast has the PlayerUnit tag
		if(hit.transform.CompareTag("PlayerUnit"))
		{
		    //Kill unit
		    hit.transform.gameObject.GetComponent<Health>().currHealth = 0f;
		}
	    }
	}
    }

    
    

    private void MouseUp()
    {
	//Check every PlayerUnit to see if it is selectable
	foreach(GameObject obj in playerUnits)
	{
	    //If this object is within the selection rectangle
	    if (IsWithinSelectionBounds(obj.transform))
	    {
		//Select the object as it is selectable and within selection rectangle
		SelectUnit(obj.transform);

		//Remove hover indicator
		obj.transform.GetComponent<Health>().hoverSelect = false;
		obj.transform.GetComponent<Health>().showLife = false;
	    }
	}
		    
	//Stop dragging because mouse is released now and we selected any units that needed to be
	isDragging = false;

	// Stop invoking Fire, if we were
	fireStart = false;
	// Stop banishing Fire, if we were
	fireStop = false;

	// Stop invoking Water, if we were
	waterStart = false;
	// Stop banishing Water, if we were
	waterStop = false;
	
	//Begin regenerating will again, if paused due to spell casting
	playerStat.pauseRegen = false;
    }


    


    private void RightClickSelectedUnits()
    {
	RaycastHit hit;

	//If mouseclick raycast hits something
	//Create a ray from the camera to our space
	camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

	//Shoot that ray and get the hit data
	if(Physics.Raycast(camRay, out hit, 300f, ~layerMask))
	{
	    //Debug.Log(hit.transform.gameObject);
	    
	    //When Left Shift is not being pressed remove previous waypoints if any
	    if(!Input.GetKey(KeyCode.LeftShift))
	    {
		for(int i = selectedUnits.Count - 1; i >= 0; i--)
		{
		    if(selectedUnits[i] == null)
		    {
			selectedUnits.RemoveAt(i);
		    }
		    else
		    {
			selectedUnits[i].GetComponent<Zombie>().DestroyAllNavPoints();
		    }
		}
	    }

	    if(hit.transform)
	    {
		// Did we click on an enemy?
		if(hit.transform.CompareTag("EnemyUnit") || hit.transform.CompareTag("Rogue"))
		{
		    //Highlight unit
		    hit.transform.GetComponent<Health>().selected = true;

		    for(int i = selectedUnits.Count - 1; i >= 0; i--)
		    {
			if(selectedUnits[i] == null)
			{
			    selectedUnits.RemoveAt(i);
			}
			else
			{
			    //Add enemy object to waypoint list
			    selectedUnits[i].GetComponent<Zombie>().targets.Add(hit.transform.gameObject);
			}
		    }
		}
		// If not enemy, make waypoint at point
		else
		{
		    for(int i = selectedUnits.Count - 1; i >= 0; i--)
		    {
			if(selectedUnits[i] == null)
			{
			    selectedUnits.RemoveAt(i);
			}
			else
			{
			    //Create a new waypoint
			    GameObject movePoint = Instantiate(movePointPrefab, new Vector3 (hit.point.x, 1f, hit.point.z), Quaternion.identity);
			    
			    //Add the new waypoint to the list
			    selectedUnits[i].GetComponent<Zombie>().targets.Add(movePoint);
			}
		    }
		}
	    }
	}
    }


    
    



    

    private void SelectUnit(Transform unit)
    {
	
	//Make reference to zombies script on object
	Zombie zomb = unit.GetComponent<Zombie>();

	//Do we have enough will to select the unit?
	if(playerStat.will >= zomb.willDrain)
	{
	    //Clear any navpoints unit previously had
	    zomb.DestroyAllNavPoints();
	    
	    //Add the clicked on unit to the list
	    selectedUnits.Add(unit);

	    //Turn on selected boolean in health script
	    unit.GetComponent<Health>().selected = true;

	    //Turn on selected boolean in zombie AI script
	    zomb.selected = true;
	}
    }


    
    private void DeselectUnits()
    {
	//Unhighlight each unit in list & turn off selected boolean in Zombie script
	for(int i = selectedUnits.Count - 1; i >= 0; i--)
	{
	    // If there is an entry at this point in the list
	    if(selectedUnits[i])
	    {
		Zombie zombScript = selectedUnits[i].GetComponent<Zombie>();
		Health healthScript = selectedUnits[i].GetComponent<Health>();
		// Turn off selected tag in Healh and Zombie scripts (if found)
		if(healthScript)
		    selectedUnits[i].GetComponent<Health>().selected = false;
		if(zombScript)
		{
		    zombScript.selected = false;


		    if(zombiesCantRogue)
			zombScript.canRogue = false;
		    else
			zombScript.canRogue = true;
		}
	    }
	}
	
	//After unhighlighting each unit in list clear list
	selectedUnits.Clear();
    }



    private void SelectAll()
    {
	// Go through every gameobject in our playerunits list
	foreach(GameObject obj in playerUnits)
	{
	    //Select the object
	    SelectUnit(obj.transform);
	}
    }


    private void SelectArchers()
    {
	// If player is not holding shift, deselect currently selected
	if(!Input.GetKey(KeyCode.LeftShift))
	{
	    DeselectUnits();
	}

	// Go through every gameobject in our playerunits list
	foreach(GameObject obj in playerUnits)
	{
	    // If unit does have archer script
	    if(obj.GetComponent<ArcherAttack>())
		// Select the unit
		SelectUnit(obj.transform);
	}
    }


        private void SelectMelee()
    {
	// If player is not holding shift
	if(!Input.GetKey(KeyCode.LeftShift))
	{
	    // Deselect currently selected units
	    DeselectUnits();
	}

	// Go through every gameobject in our playerunits list
	foreach(GameObject obj in playerUnits)
	{
	    // If unit doesn't have archer script
	    if(!obj.GetComponent<ArcherAttack>())
		// Select the unit
		SelectUnit(obj.transform);
	}
    }

    

    private void SelectGroup(int groupNumber)
    {
	// If player is not holding shift, deselect currently selected
	if(!Input.GetKey(KeyCode.LeftShift))
	{
	    DeselectUnits();
	}

	switch(groupNumber)
	{
	    case 1:
		// Go through all units in this group
		for(int i = group1.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(group1[i] == null)
		    {
			// Remove the null entry
			group1.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			// Select the unit
			SelectUnit(group1[i]);
		    }
		}
		break;
		
	    case 2:
		// Go through all units in this group
		for(int i = group2.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(group2[i] == null)
		    {
			// Remove the null entry
			group2.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			// Select the unit
			SelectUnit(group2[i]);
		    }
		}
		break;

	    case 3:
		// Go through all units in this group
		for(int i = group3.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(group3[i] == null)
		    {
			// Remove the null entry
			group3.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			// Select the unit
			SelectUnit(group3[i]);
		    }
		}
		break;

	    case 4:
		// Go through all units in this group
		for(int i = group4.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(group4[i] == null)
		    {
			// Remove the null entry
			group4.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			// Select the unit
			SelectUnit(group4[i]);
		    }
		}
		break;
	}
    }


    private void PopulateGroup(int groupNumber)
    {
	switch(groupNumber)
	{
	    case 1:
		//First clear the list
		group1.Clear();
		
		// Go through all currently selected units
		for(int i = selectedUnits.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(selectedUnits[i] == null)
		    {
			// Remove the null entry
			selectedUnits.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			//Add the unit to the group list
			group1.Add(selectedUnits[i]);
		    }
		}
		break;

	    case 2:
		//First clear the list
		group2.Clear();
		
		// Go through all currently selected units
		for(int i = selectedUnits.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(selectedUnits[i] == null)
		    {
			// Remove the null entry
			selectedUnits.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			//Add the unit to the group list
			group2.Add(selectedUnits[i]);
		    }
		}
		break;		

	    case 3:
		//First clear the list
		group3.Clear();
		
		// Go through all currently selected units
		for(int i = selectedUnits.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(selectedUnits[i] == null)
		    {
			// Remove the null entry
			selectedUnits.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			//Add the unit to the group list
			group3.Add(selectedUnits[i]);
		    }
		}
		break;

	    case 4:
		//First clear the list
		group4.Clear();
		
		// Go through all currently selected units
		for(int i = selectedUnits.Count - 1; i >= 0; i--)
		{
		    // If it is a null entry
		    if(selectedUnits[i] == null)
		    {
			// Remove the null entry
			selectedUnits.RemoveAt(i);
		    }
		    // If it is not a null entry
		    else
		    {
			//Add the unit to the group list
			group4.Add(selectedUnits[i]);
		    }
		}
		break;
	}
    }

    
    
    //Return what is within selection rectangle
    private bool IsWithinSelectionBounds(Transform transform)
    {
	if(!isDragging)
	{
	    return false;
	}	
	    var camera = Camera.main;
	    var viewportBounds = ScreenHelper.GetViewportBounds(camera, mousePosition, Input.mousePosition);
	    return viewportBounds.Contains(camera.WorldToViewportPoint(transform.position));	
    }


    
    //When disabled
    void OnDisable()
    {
	//Deselect all units
	DeselectUnits();
    }
}
