﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerStats : MonoBehaviour
{
    // amount will returns to
    public int maxWill;

    // The base amount of the max will (before bonuses added and multiplied
    public int baseMaxWill;
    
    // Add to Max Will
    public float maxWillAdd;
    // Multiply Max Will by
    public float maxWillMultiplier = 1f;

    
    //Current will amount
    public float will;

    
    //How much will regenerates each second
    public int willRegen;
    public int baseWillRegen;

    //Bonuses to Will Regen
    public float willRegenAdd;
    public float willRegenMultiplier = 1f;

    
    
    //Reference to UI text element that prints will level on screen
    public Text willPrint;

    //How much will is draining each second
    public float willDrain;

    //Player level
    public int level = 1;

    //Player experience
    public int exp = 1;

    int fireExp = 0;
    int waterExp = 0;
    int airExp = 0;
    int earthExp = 0;

    public int fireLvl = 1;
    public int waterLvl = 1;
    public int airLvl = 1;
    public int earthLvl = 1;

    
    //The area the player is currently in
    public int area = 0;

    //The entrance the player currently enters the area from
    public int entrance = 0;

    //While casting spells pause the regen until you have finished so you can't just cast forever
    public bool pauseRegen = false;




    void Start()
    {
	CalculateWillBonuses();
    }

    
    
    void Update()
    {	
	// Unless we paused the regeneration of will
	if(!pauseRegen)
	    // Apply will regeneration
	    will += willRegen * Time.deltaTime;

	// Apply will drain
	will -= willDrain * Time.deltaTime;

	// Keep will from going over maximum
	if(will > maxWill)
	    will = maxWill;

	//Print the values for the player
	willPrint.text = "Will " + Mathf.Round(will).ToString() + " + " + willRegen.ToString() + " - " + willDrain;

	//Enough experience to level up?
	if(exp >= level + 1)
	{
	    // Reset experience (subtract exp to level up, don't set to 0, so if we got more exp than required to level up, extra will be applied to next level)
	    exp -= level + 1;
	    
	    // Increase level
	    level++;

	    // Increase stats for new level
	    baseMaxWill += (int)(level * .75f);
	    baseWillRegen++;

	    //Recalculate bonuses
	    CalculateWillBonuses();
	}
    }



    // Run this to increase fire experience/level
    public void IncFireExp()
    {
	// Increase fire experience
	fireExp ++;

	
	if(fireExp > fireLvl + 1 * 50)
	{
	    // Reset experience
	    fireExp = 0;

	    // Increase level
	    fireLvl ++;
	}
    }


        // Run this to increase air experience/level
    public void IncAirExp()
    {
	// Increase air experience
	airExp ++;

	
	if(airExp > airLvl + 1 * 20)
	{
	    // Reset experience
	    airExp = 0;

	    // Increase level
	    airLvl ++;
	}
    }


        // Run this to increase water experience/level
    public void IncWaterExp()
    {
	// Increase water experience
	waterExp ++;

	
	if(waterExp > waterLvl + 1 * 25)
	{
	    // Reset experience
	    waterExp = 0;

	    // Increase level
	    waterLvl ++;
	}
    }


        // Run this to increase earth experience/level
    public void IncEarthExp()
    {
	// Increase earth experience
	earthExp ++;

	
	if(earthExp > earthLvl + 1 * 20)
	{
	    // Reset experience
	    earthExp = 0;

	    // Increase level
	    earthLvl ++;
	}
    }



    
    void CalculateWillBonuses()
    {
    	// Add bonuses and multipliers
	maxWill = (int)(baseMaxWill * maxWillMultiplier + maxWillAdd);
	
	willRegen = (int)(baseWillRegen * willRegenMultiplier + willRegenAdd);
    }




    public void ChangeMaxWillMultiplier(float amount)
    {
	// Change multiplier
	maxWillMultiplier += amount;

	// Recalculate
	CalculateWillBonuses();
    }


    public void ChangeMaxWillAdd(float amount)
    {
	// Change multiplier
	maxWillAdd += amount;

	// Recalculate
	CalculateWillBonuses();
    }



        public void ChangeWillRegenMultiplier(float amount)
    {
	// Change multiplier
	willRegenMultiplier += amount;

	// Recalculate
	CalculateWillBonuses();
    }


    public void ChangeWillRegenAdd(float amount)
    {
	// Change multiplier
	willRegenAdd += amount;

	// Recalculate
	CalculateWillBonuses();
    }
}
