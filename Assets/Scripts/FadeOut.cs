﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public float Ttl;
    float maxTtl;

    void Start()
    {
	maxTtl = Ttl;
    }

    // Update is called once per frame
    void Update()
    {
        if(Ttl < 0f)
	{
	    Destroy(gameObject);
	}
	else
	{
	    Ttl -= Time.deltaTime;

	    Color newColor = new Color(1, 1, 1, Ttl / maxTtl);
	    transform.GetComponent<Renderer>().material.color = newColor;
	}
    }
}
