﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTargets : MonoBehaviour
{
    // A list to hold temporary waypoints and sighted enemies
    public List<GameObject> targets = new List<GameObject>();

    // Do we see the necromancer as a threat?
    public bool isThreat = false;

    // Variable to remember the list number of the closest
    public int closestEntry = 0;
	
    // Variable to remember the list number of the furthest
    int furthestEntry = 0;

    // Variable to remember distance to furthest target
    float furthestDistance = 0;

    // Variable to remember distance to closest target
    public float closestDistance = 0;

    // Variable to track how long since we last told allies enemy locations
    float tellTimer = 0f;

    // How long between calling out enemy locations?
    public float tellTime = 5f;

    // How long to ignore being told about an enemy that we just stopped searching for
    public float ignoreTime = 5f;
    
    // Reference to our NPC sound script
    public NPCSounds npcSound;

    // Reference to our attack script
    public Attack attackScript;

    // Reference to archer attack script
    public ArcherAttack archerAttackScript;

    // Current velocity of our closest target
    float targetVelocity;


    
    // If we run from fire easily
    public bool afraidOfFire;

    // Distance we want to keep from fire
    public float distToKeepFromFire;


    // Tells NPC1, ai master-script, to stop what doing and run from fire
    public bool fireAlert;
    
    // Location of fire
    public Vector3 fireLocation;

    // Distance we need to move to feel safe from fire
    public float fireRunDist;

    // List of ladders we can see
    public List<Ladder> ladders;

    // If Player disappears from sight within reach of a ladder, suspect they went down/up that ladder
    public Ladder suspectLadder;
    public Ladder returnLadder;
    
    // Turn on debug messages
    public bool debugOn;

    
    


    void Update()
    {
	// Check our targets
	foreach(GameObject obj in targets)
	{
	    // Put a reference to this target object's RememberUnit script
	    RememberUnit unitMemory = obj.GetComponent<RememberUnit>();

	    // Make sure this target object is referencing a target before trying to access target scripts/variables
	    if(unitMemory.unit)
	    {
		if(unitMemory.unit.GetComponent<Health>().currHealth > 0f)
		{
		    // If it's been longer than a second since we've seen the unit
		    if(Time.time - unitMemory.lastSeen > 1f)
		    {
			// Turn off line of sight flag
			unitMemory.lineOfSight = false;
		    }

		    // If the memory waypoint is pointing to no unit
		    if(!unitMemory.unit)
		    {
			//Reparent it to this game object so it isn't wasting space in the hierarchy
			obj.transform.parent = transform;
			    
			// Deactivate this entry in the list
			obj.SetActive(false);
		    }
		}
		else
		{
		    //Reparent it to this game object so it isn't wasting space in the hierarchy
		    obj.transform.parent = transform;
		
		    // Deactivate this entry in the list, as this game object is now dead
		    obj.SetActive(false);
		}
	    }
	}
    }

    
    void OnEnable()
    {
	// Reattach targets if they are active for no good reason
	foreach(GameObject obj in targets)
	{
	    //Reparent it to this game object
	    obj.transform.parent = gameObject.transform;
	    
	    //Deactivate this target entry
	    obj.SetActive(false);
	}
    }
    


    void OnDisable()
    {
	// Reattach targets if they are active for no good reason
	foreach(GameObject obj in targets)
	{
	    //Reparent it to this game object
	    //obj.transform.parent = gameObject.transform;
	    
	    //Deactivate this target entry
	    if(obj)
		obj.SetActive(false);
	}
    }


    // Function that finds the closest target and remembers which one and the distance,
    // and the furthest target and remembers which one and distanec (stored as variables)
    public void FindClosestFurthest()
    {
	//Vector to remember distance of closest yet found
	closestDistance = 9999f;

	//Vector to remember distance of furthest yet found
	furthestDistance = 0f;

	// Variable to hold calculated distances for testing
	float thisDistance = 9999f;
	
	//Iterate through targets list starting at second entry, first already calculated
	for(int i = 0; i < targets.Count; i++)
	{
	    // Is this entry in the list active?
	    if(targets[i].activeSelf)
	    {
		// Calculate distance to the object held in the currently iterated entry of the array
		thisDistance = Vector3.Distance(transform.position, targets[i].transform.position);

		if(debugOn)
		    Debug.Log(Vector3.Distance(transform.position, targets[i].transform.position));
	    }
	    // This entry in the list is not active, so set distance to 9999 for closeness purposes
	    else
	    {
		// Set calculated distance for unused entries to 9999 so we don't accidentally calculate them as the closest, which should only ever be active entries
		thisDistance = 9999f;
	    }

	    //If it is closer than the current closest unit
	    if(thisDistance < closestDistance)
	    {
		//Set the closest distance yet to this distance
		closestDistance = thisDistance;
		
		//Set closest entry to this entry
		closestEntry = i;
	    }
	    // If it is not closest than the closest distance, then check if it is further than the furthest distance yet found
	    else if(thisDistance > furthestDistance)
	    {
		//Set the furthest distance yet to this distance
		furthestDistance = thisDistance;

		//Set furthest entry yet to this entry
		furthestEntry = i;
	    }
	}
    }





    // Find the first entry in targets list that is open, if any, if not return -1
    int OpenTarget()
    {
	// Iterate through the targets list
	for(int i = 0; i < targets.Count; i++)
	{
	    // If this entry is not being used (determined if not active)
	    if(!targets[i].activeSelf)
	    {
		// Return this entry number, it is an open entry we can use
		return i;
	    }
	}
	// We have gone through the entire list without returning, so no open entry was found
	// so return -1 to indicate no open entry exists
	return -1;
    }

    


    // Find the first entry in targets list that is open, if any, if not return -1
    public int NumberOfTargets()
    {
	// Variable to hold count of active targets
	int count = 0;
	
	// Iterate through the targets list
	for(int i = 0; i < targets.Count; i++)
	{
	    // If this entry is being used (determined if active)
	    if(targets[i].activeSelf)
	    {
		// Iterate count variable
		count++;
	    }
	}
	// We have gone through the entire list now return count
	return count;
    }

    



    void OnTriggerEnter(Collider other)
    {
	if(other.gameObject.tag == "Interactive")
	{
	    Ladder ladderScript = other.transform.GetComponent<Ladder>();
	    if(ladderScript)
	    {
		ladders.Add(ladderScript);
	    }
	}

	//Is the object in our field of view a player unit
	else if(other.gameObject.tag == "PlayerUnit" || other.gameObject.tag == "Rogue")
	{
	    if(Random.Range(0,2) == 1)
		//Play zombie spotted sound effect
		npcSound.PlayRandom(1);
	}

	// If object in our field of view is the player
	else if(other.gameObject.tag == "Player")
	{
	    if(isThreat)
	    {	
		if(Random.Range(0,3) == 1)
		    //Play NecroIsEnemy spotted sound effect
		    npcSound.PlayRandom(2);
	    }
	    else
	    {
		if(Random.Range(0,3) == 1)
		    //Play NecroUnknown spotted sound effect
		    npcSound.PlayRandom(10);
	    }
	}

	//Is the object in our field of view an ally?
	else if(other.gameObject.tag == "EnemyUnit")
	{
	    if(Random.Range(0, 2) == 1)
	    {
		//If we know the player is a threat tell them
		if(isThreat)
		{
		    //Debug.Log(other.gameObject.transform.GetComponent<Attack>().isThreat);
		    
		    if(!other.gameObject.transform.GetComponent<Attack>().isThreat)
		    {
			//Play recruit ally sound effect
			npcSound.PlayRandom(9);
		    }
		}
	    }
	} 
    }

    
    // Anytime a collider is in our field of view
    void OnTriggerStay(Collider other)
    {
	if(debugOn)
	    Debug.Log("Spotted: " + other);
	
	//Ignore level geometry and arrows
	if(other.gameObject.tag == "Ground")
	{

	}
	// If we saw an arrow
	else if(other.gameObject.tag == "Arrow")
	{
	    // Do we have line of sight on the arrow?
	    // A raycast hit variable to contain the first object hit by the linecast
	    RaycastHit hit;

	    // The layers we want to be able to hit with the linecast
	    LayerMask mask = LayerMask.GetMask("Walls");
	    
	    // Use linecast to check if we have direct line of sight to the object, ignoring enemy if linecast hits a wall
	    if(!Physics.Linecast(transform.position, other.transform.position, out hit, mask))
	    {
		// Get a reference to the arrow's script
		Arrow arrowScript = other.transform.GetComponent<Arrow>();

		// Check which character shot the arrow
		GameObject character = arrowScript.player;

		// Is the character who fired the arrow an enemy?
		if(character.tag == "PlayerUnit" || (character.tag == "Player") || character.tag == "Rogue")
		{
		    // Consider player a threat now
		    if(!isThreat)
			isThreat = true;
		    
		    // Add the character's position to our targets
		    CheckArea(character);
		}
	    }
	}
	// Is it a trigger collider?
	else if(other.isTrigger)
	{
	    //Is the object in our field of view fire?
	    if(other.gameObject.tag == "Fire")
	    {
		SpottedFire(other);
	    } 
	}
	
	//Is the object in our field of view a player unit or, if we see the player as a threat, the player themself
	else if(other.gameObject.tag == "PlayerUnit" || (isThreat && other.gameObject.tag == "Player") || other.gameObject.tag == "Rogue" || other.gameObject.tag == "WolfUnit")
	{
	    SpottedEnemy(other);
	}

	// If object in our field of view is the player and not a threat
	else if(other.gameObject.tag == "Player")
	{
	    SpottedPlayer(other);
	}

	//Is the object in our field of view an ally?
	else if(other.gameObject.tag == "EnemyUnit")
	{
	    SpottedAlly(other);
	} 



	//Is the object a dead unit?
	else if(other.gameObject.tag == "Dead" || other.gameObject.tag == "PermaDead")
	{
	    SpottedDead(other);
	}
    }


    void OnTriggerExit(Collider other)
    {
	if(other.gameObject.tag == "PlayerUnit" || other.gameObject.tag == "WolfUnit" || (isThreat && other.gameObject.tag == "Player") || other.gameObject.tag == "Rogue")
	{
	    //Check if this unit is targeted
	    foreach(GameObject obj in targets)
	    {
		if(obj.activeSelf)
		{
		    // Put a reference to this target object's RememberUnit script
		    RememberUnit unitMemory = obj.GetComponent<RememberUnit>();
			
		    // If the gameobject exiting sight cone is the one at this point in the list
		    if(GameObject.ReferenceEquals(unitMemory.unit, other.gameObject))
		    {
			// Turn off line of sight flag
			unitMemory.lineOfSight = false;

			// if we just lost sight of the player and are threatened by them
			if(other.gameObject.tag == "Player")
			{
			    // See if there is a nearby ladder, if so suspect the player went down/up that ladder
			    foreach(Ladder ladderScript in ladders)
			    {
				// Make sure it's not the return ladder
				if(ladderScript != returnLadder)
				{
				    if(Vector3.Distance(obj.transform.position, ladderScript.transform.position) < 5f)
				    {
					suspectLadder = ladderScript;
					return;
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	
	else if(other.gameObject.tag == "Interactive")
	{
	    // if this object has a ladder script
	    Ladder ladderScript = other.transform.GetComponent<Ladder>();
	    
	    if(ladderScript)
	    {
		// Remove it from the list of ladders
		ladders.Remove(ladderScript);
	    }
	}
    }


    void SpottedFire(Collider other)
    {
	// How far is the fire object
	float distFromFire = Vector3.Distance(transform.position, other.transform.position);

	// Is it close enough to worry us?
	if(distFromFire < distToKeepFromFire)
	{
	    // Fire doesn't scare us, only move away if no enemy is closer
	    if(!afraidOfFire)
	    {
		// Is the fire closer than the nearest enemy?
		if(distFromFire < closestDistance)
		{
		    // Turn on flag to make AI avoid fire
		    fireAlert = true;

		    // Pass location of fire for AI to avoid
		    fireLocation = other.transform.position;

		    // Pass distance we want to move to AI to move away from fire
		    fireRunDist = distToKeepFromFire - distFromFire;
		}
	    }
	    // Are afraid of fire, just run
	    else
	    {
		// Turn on flag to make AI avoid fire
		fireAlert = true;

		// Pass location of fire for AI to avoid
		fireLocation = other.transform.position;
	    }
	}
    }


    
    void CheckArea(GameObject other)
    {
	//First figure out which of our targets is currently closest and which furthest
	FindClosestFurthest();

	//Temp variable if found in list
	bool found = false;
		   
	//Make sure this enemy is not already targeted
	foreach(GameObject obj in targets)
	{
	    if(obj.activeSelf)
	    {
		// Put a reference to this target object's RememberUnit script
		RememberUnit unitMemory = obj.GetComponent<RememberUnit>();
			
		// If the gameobject sighted is the one at this point in the list
		if(GameObject.ReferenceEquals(unitMemory.unit, other))
		{
		    // Update location of memory unit to current location of gameobject
		    obj.transform.position = other.transform.position;

		    // Set to face the same direction
		    obj.transform.rotation = other.transform.rotation;

		    // Note that we have line of sight
		    unitMemory.lineOfSight = true;

		    // Note the time that we had line of sight
		    unitMemory.lastSeen = Time.time;
			    
		    // Mark unit as found
		    found = true;
		}
	    }
	}
		
	//Not found in list already
	if(!found)
	{
	    //Find first open target entry, if any
	    int open = OpenTarget();

	    //Do we have an open target entry?
	    if(open > -1)
	    {
		//Activate this target entry
		targets[open].SetActive(true);

		//Unparent it from this game object so it won't follow our movement
		targets[open].transform.parent = null;

		//Set the target entry to the location of the object
		targets[open].transform.position = other.transform.position;

		// Put a reference to this target object's RememberUnit script
		RememberUnit unitMemory = targets[open].GetComponent<RememberUnit>();
			
		//Set the target associated unit to the object
		unitMemory.unit = other;

		//Trip tag that we have line of sight
		unitMemory.lineOfSight = true;
	    }
	    //Is this enemy closer than the furthest enemy?
	    else if(Vector3.Distance(transform.position, other.transform.position) < furthestDistance)
	    {
		//Set the furthest target entry to the location of this object instead
		targets[furthestEntry].transform.position = other.transform.position;

			    
		//Set to face the same direction
		targets[furthestEntry].transform.forward = other.transform.forward;

		// Put a reference to this target object's RememberUnit script
		RememberUnit unitMemory = targets[furthestEntry].GetComponent<RememberUnit>();
			
		//Set the target associated unit to the object
		unitMemory.unit = other;

		//Trip tag that we have line of sight
		unitMemory.lineOfSight = true;
	    }
	}
    }





	
    void SpottedEnemy(Collider other)
    {
	// A raycast hit variable to contain the first object hit by the linecast
	RaycastHit hit;

	// The layers we want to be able to hit with the linecast
	LayerMask mask = LayerMask.GetMask("Walls");
	    
	// Use linecast to check if we have direct line of sight to the object, ignoring enemy if linecast hits a wall
	if(!Physics.Linecast(transform.position, other.transform.position, out hit, mask))
	{

	    //First figure out which of our targets is currently closest and which furthest
	    FindClosestFurthest();

	    //Temp variable if found in list
	    bool found = false;
		   
	    //Make sure this enemy is not already targeted
	    foreach(GameObject obj in targets)
	    {
		if(obj.activeSelf)
		{
		    // Put a reference to this target object's RememberUnit script
		    RememberUnit unitMemory = obj.GetComponent<RememberUnit>();
			
		    // If the gameobject sighted is the one at this point in the list
		    if(GameObject.ReferenceEquals(unitMemory.unit, other.gameObject))
		    {
			// Update location of memory unit to current location of gameobject
			obj.transform.position = other.transform.position;

			// Set to face the same direction
			obj.transform.rotation = other.transform.rotation;

			// Note that we have line of sight
			unitMemory.lineOfSight = true;

			// Note the time that we had line of sight
			unitMemory.lastSeen = Time.time;
			    
			// Mark unit as found
			found = true;
		    }
		}
	    }
		
	    //Not found in list already
	    if(!found)
	    {
		//Find first open target entry, if any
		int open = OpenTarget();

		//Do we have an open target entry?
		if(open > -1)
		{
		    //Activate this target entry
		    targets[open].SetActive(true);

		    //Unparent it from this game object so it won't follow our movement
		    targets[open].transform.parent = null;

		    //Set the target entry to the location of the object
		    targets[open].transform.position = other.transform.position;

		    // Put a reference to this target object's RememberUnit script
		    RememberUnit unitMemory = targets[open].GetComponent<RememberUnit>();
			
		    //Set the target associated unit to the object
		    unitMemory.unit = other.gameObject;

		    //Trip tag that we have line of sight
		    unitMemory.lineOfSight = true;
		}
		//Is this enemy closer than the furthest enemy?
		else if(Vector3.Distance(transform.position, other.transform.position) < furthestDistance)
		{
		    //Set the furthest target entry to the location of this object instead
		    targets[furthestEntry].transform.position = other.transform.position;

			    
		    //Set to face the same direction
		    targets[furthestEntry].transform.forward = other.transform.forward;

		    // Put a reference to this target object's RememberUnit script
		    RememberUnit unitMemory = targets[furthestEntry].GetComponent<RememberUnit>();
			
		    //Set the target associated unit to the object
		    unitMemory.unit = other.gameObject;

		    //Trip tag that we have line of sight
		    unitMemory.lineOfSight = true;
		}
	    }

	}
	    
	// Our line of sight raycast hit something other than the object in question
	else
	{
	    //Debug.Log(hit.transform.gameObject);
	    
	    //Check if this enemy is targeted
	    foreach(GameObject obj in targets)
	    {
		// Only check active target objects
		if(obj.activeSelf)
		{
		    // Put a reference to this target object's RememberUnit script
		    RememberUnit unitMemory = obj.GetComponent<RememberUnit>();
			
		    // If the gameobject sighted is the one at this point in the list
		    if(GameObject.ReferenceEquals(unitMemory.unit, other.gameObject))
		    {
			// Turn off tag of having line of sight (we don't)
			unitMemory.lineOfSight = false;
		    }
		}
	    }
	}
    }

    

    void SpottedPlayer(Collider other)
    {
	// A raycast hit variable to contain the first object hit by the linecast
	RaycastHit hit;

	// The layers we want to be able to hit with the linecast
	LayerMask mask = LayerMask.GetMask("Walls");
		
	// Use linecast to check if we have direct line of sight to the object
	if(!Physics.Linecast(transform.position, other.transform.position, out hit, mask))
	{
	    // Get the player's current animation
	    int currentAnimation = other.gameObject.GetComponent<Health>().animators[0].GetCurrentAnimatorStateInfo(0).tagHash;
		
	    // Is player doing something suspicious?
	    if(!isThreat && currentAnimation == Animator.StringToHash("Attacking") || currentAnimation == Animator.StringToHash("Shooting"))
	    {
		// Remember player is a threat
		isThreat = true;

		// Let attack scripts know
		attackScript.isThreat = true;

		// If archer
		if(archerAttackScript != null)
		    archerAttackScript.isThreat = true;

		
		if(Random.Range(0,2) == 1)
		    //Play NecroIsThreat spotted sound effect
		    npcSound.PlayRandom(2);
	    }
	}
    }
    


    void SpottedAlly(Collider other)
    {
	// Has it been long enough since we last told enemy locations?
	if(tellTimer <= 0)
	{
	    // Reset tellTimer
	    tellTimer = Random.Range(1f, tellTime);
		
	    // A raycast hit variable to contain the first object hit by the linecast
	    RaycastHit hit;

	    // The layers we want to be able to hit with the linecast
	    LayerMask mask = LayerMask.GetMask("Walls");
	
	    // Use linecast to check if we have direct line of sight to the object
	    if(!Physics.Linecast(transform.position, other.transform.position, out hit, mask))
	    {
		//If we know the player is a threat tell them
		if(isThreat)
		{
		    //Debug.Log(other.gameObject.transform.GetComponent<Attack>().isThreat);
		    
		    if(!other.gameObject.transform.GetComponent<Attack>().isThreat)
		    {
			//Set isthreat tag in NPC, attack, and archer attack(if present) scripts
			other.gameObject.transform.GetComponentInChildren<NPCTargets>().isThreat = true;
			other.gameObject.transform.GetComponent<Attack>().isThreat = true;
			if(other.gameObject.transform.GetComponent<ArcherAttack>())
			    other.gameObject.transform.GetComponent<ArcherAttack>().isThreat = true;
		    }
		}
	    
		//Tell them our closest target, if it is an enemy
		if(targets[closestEntry].activeSelf)
		{
		    other.gameObject.transform.Find("Vision").GetComponent<NPCTargets>().AddNavPoint(targets[closestEntry]);
		}
	    }
	}
	// hasn't been long enough to tell again, so increment timer
	else
	{
	    tellTimer -= Time.deltaTime;
	}
    }


    

    void SpottedDead(Collider other)
    {
	if(debugOn)
	    Debug.Log(other.gameObject.name);
	
	//Check if this unit is targeted
	foreach(GameObject obj in targets)
	{
	    if(obj.activeSelf)
	    {
		//If the gameobject sighted is the one at this point in the list
		if(GameObject.ReferenceEquals(obj.GetComponent<RememberUnit>().unit, other.gameObject))
		{
		    //Reparent it to this game object so it isn't wasting space in the hierarchy
		    obj.transform.parent = transform;
			    
		    // Deactivate this entry in the list, as this game object is now dead
		    obj.SetActive(false);
		}
	    }
	}
    }

    

    // Add a target we were told about by another NPC
    void AddNavPoint(GameObject toAdd)
    {
	// Put a reference to this target object's RememberUnit script
	RememberUnit theirUnitMemory = toAdd.GetComponent<RememberUnit>();
		
	if(debugOn)
	    Debug.Log(toAdd);
	
	//First figure out which of our targets is currently closest and which furthest
	FindClosestFurthest();

	//Temp variable if found in list
	bool found = false;
		   
	// Search for this unit in our targets list
	foreach(GameObject obj in targets)
	{
	    // Put a reference to this target object's RememberUnit script
	    RememberUnit ourUnitMemory = obj.GetComponent<RememberUnit>();

	    if(debugOn)
		Debug.Log(ourUnitMemory.unit + " " + theirUnitMemory.unit);
			
	    // Make sure we aren't adding a null entry
	    if(theirUnitMemory.unit)
	    {
		// If the gameobject sighted is the one at this point in the list
		if(GameObject.ReferenceEquals(ourUnitMemory.unit, theirUnitMemory.unit))
		{
		    			    
		    // Mark unit as found
		    found = true;
		    
		    // If they saw the unit more recently than we have, and more recently than we have searched for it
		    if(theirUnitMemory.lastSeen > ourUnitMemory.lastSeen && theirUnitMemory.lastSeen > ourUnitMemory.searchedFor)
		    {
			// Set this memory object as active
			obj.SetActive(true);
			
			// Update location of memory unit to current location of gameobject
			obj.transform.position = toAdd.transform.position;

			// Set to face the same direction
			obj.transform.forward = toAdd.transform.forward;

			// Set last seen time to time last seen by person telling us
			ourUnitMemory.lastSeen = theirUnitMemory.lastSeen;
		    }
		}
	    }
	}

	if(debugOn)
	    Debug.Log("Found = " + found);
	
	//Not found in list already
	if(!found)
	{
	    //Find first open target entry, if any
	    int open = OpenTarget();

	    //Do we have an open target entry?
	    if(open > -1)
	    {
		//Activate this target entry
		targets[open].SetActive(true);

		//Unparent it from this game object so it won't follow our movement
		targets[open].transform.parent = null;

		//Set the target entry to the location of the object
		targets[open].transform.position = toAdd.transform.position;

		// Put a reference to this target object's RememberUnit script
		RememberUnit ourUnitMemory = targets[open].GetComponent<RememberUnit>();
			
		//Set the target associated unit to the object
		ourUnitMemory.unit = theirUnitMemory.unit;

		//Set the time last seen
		ourUnitMemory.lastSeen = theirUnitMemory.lastSeen;
	    }
	    //Is this enemy closer than the furthest enemy?
	    else if(Vector3.Distance(transform.position, toAdd.transform.position) < furthestDistance)
	    {
		//Set the furthest target entry to the location of this object instead
		targets[furthestEntry].transform.position = toAdd.transform.position;

			    
		//Set to face the same direction
		targets[furthestEntry].transform.forward = toAdd.transform.forward;

		// Put a reference to this target object's RememberUnit script
		RememberUnit ourUnitMemory = targets[furthestEntry].GetComponent<RememberUnit>();
			
		//Set the target associated unit to the object
		ourUnitMemory.unit = theirUnitMemory.unit;

		//Set the time last seen
		ourUnitMemory.lastSeen = theirUnitMemory.lastSeen;
	    }
	}
    }
}


