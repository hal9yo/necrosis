﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC1 : MonoBehaviour
{
    // Health script reference
    public Health ourHealth;

    // Targets script reference
    public NPCTargets targets;

    // Fight script reference
    public NPCFighting fight;

    // Search script reference
    public NPCSearch search;

    // Patrol script reference
    public NPCPatrol patrol;

    // Retreat script
    public NPCRetreat retreat;

    // Avoid Fire scipt (To make us move away from fire)
    public NPCAvoidFire avoidFire;

    // Character Fire script (to tell when we are burning)
    public CharacterFire charFire;
    
    // stats script reference
    public Stats stats;
    
    // Nav component reference
    public UnityEngine.AI.NavMeshAgent navComponent;
    
    
    // Compare health to previous
    private float healthWas;

    // Keep track of whether or not we are retreating
    bool retreating = false;

    // Keep track of whether or not we are searching
    bool searching = false;

    // Reference to player's stats script, for adding experience when killed
    public PlayerStats playerStat;
    
    // Bool that makes them not go searching for enemies when they lose sight
    public bool dontSearch;


    // Turn on for some debug messages from the unit's AI
    public bool debugOn;


    // Timer of how often to check for retreat conditions so not to bog down game as much with constant checks
    float retreatTimer;

    
    // Start is called before the first frame update
    void Start()
    {
	//Initialize movement speed, with random difference, also subtract however much health we have currently lost
	navComponent.speed = stats.walkSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);

	// Randomize navigation priority
	navComponent.avoidancePriority += Random.Range(-5, 5);

	// Attach reference to player's stats to inc exp when dying
	if(playerStat == null)
	    if(GameObject.Find("Necromancer"))
		playerStat = GameObject.Find("Necromancer").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
	// If we are using a ladder
	if (navComponent.isOnOffMeshLink)
	{
	    // Just warp to the end of the ladder
	    navComponent.Warp(navComponent.currentOffMeshLinkData.endPos);
	}
	
	// Do we see fire
	if(targets.fireAlert)
	{
	    if(debugOn)
		Debug.Log("Move away from fire seen at: " + targets.fireLocation);

	    // Start running! It's a fire!
	    navComponent.speed = stats.runSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);

	    // Move away from the fire
	    avoidFire.AvoidFire(targets.fireLocation, targets.fireRunDist);

	    // Turn off the fire alarm as we already moved away
	    targets.fireAlert = false;
	}
	// Do we feel fire
	else if(charFire.heat > .4f)
	{
	    if(debugOn)
		Debug.Log("Move away from fire felt at: " + charFire.sourceOfFire);

	    // Start running! It's a fire!
	    navComponent.speed = stats.runSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);
		    
	    // Try to move away from the source of the fire
	    avoidFire.AvoidFire(charFire.sourceOfFire, 5f);
	}
	// Do we feel too much fire
	else if(charFire.heat > .6f)
	{
	    if(debugOn)
		Debug.Log("On fire! Run aimlessly!");

	    // Start running! We're on fire!
	    navComponent.speed = stats.runSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);

	    // Run aimlessly
	    retreat.RunAway();
	}
	else
	{
	    // Do we have any targets to attack?
	    if(targets.NumberOfTargets() > 0)
	    {
		// Accelerate to attack speed
		navComponent.speed = stats.runSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);
	    
		// Are we already retreating?
		if(retreating)
		{
		    if(debugOn)
			Debug.Log("allies: " + retreat.CheckAllies() + "  allies needed: " + retreat.powerNeeded);
		    
		    // Have we acquired enough allies?
		    if(retreat.CheckAllies() >= retreat.powerNeeded)
			// Stop retreating
			retreating = false;
		    // Don't have enough allies yet,
		    else
			// keep retreating
			retreat.Retreat();
		}
		// Not currently retreating
		else
		{
		    // bool to return true if we find we are currently outnumbered
		    bool threatened = false;

		    // Is it time for us to check if we are outnumbered?
		    if(retreatTimer < 0f)
		    {
			// Check if we are outnumbered
			threatened = retreat.AssessThreat();

			// Reset the timer to count down to next time we check
			retreatTimer = Random.Range(.5f, 3f);
		    }
		    // Otherwise just countdown the timer
		    else
			retreatTimer -= Time.deltaTime;
		    
		    // Are we outnumbered?
		    if(threatened)
		    {
			if(debugOn)
			    Debug.Log("Retreat");
			// Retreat
			retreating = true;
		    }
		    // We are not outnumbered
		    else
		    {
			// Do we have line of sight on the nearest target?
			if(targets.targets[targets.closestEntry].GetComponent<RememberUnit>().lineOfSight)
			{
			    if(debugOn)
				Debug.Log("Engage");

			    // If we were searching
			    if(searching)
				// reset searching values as no longer searching
				search.Reset();
			
			    // Engage the target in combat
			    fight.EngageTargets();
			}
			// We don't have line of sight
			else
			{
			    // Look at closest (this is weird, makes them keep looking at the last known position while searching, just weird)
			    //transform.LookAt(targets.targets[targets.closestEntry].transform.position);
			
			    if(debugOn)
				Debug.Log("Search");

			    // Don't search if told not to
			    if(!dontSearch)
			    {
				// Note that we are searching
				searching = true;
			
				// Search around an area that was in the last known direction of this enemy, ahead half our search radius
				search.Search();
			    }
			}
		    }
		}
	    }
	    // No targets to attack, so patrol
	    else
	    {
		if(debugOn)
		    Debug.Log("Patrol");

		// If we are following another and are too far behind
		if(patrol.followOther && patrol.currentDist > patrol.runDist)
		{
		    // Accelerate to attack speed
		    navComponent.speed = stats.runSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);
		}
		else
		{
		    // Decelerate to walk speed
		    navComponent.speed = stats.walkSpeed * ourHealth.currHealth / stats.maxHealth + Random.Range(0f, 1f);
		}
	    
		// Continue on patrol or wander if no patrol points
		patrol.WalkRoute();
	    }
	}
    }

    // Called when behavior disabled (should be when npc killed)
    void OnDisable()
    {
	// Make sure we just died
	if(ourHealth.currHealth <= 0)
	{
	    // Add experience to player stat
	    if(playerStat != null)
		playerStat.exp += stats.power;		
	}
    }
}

