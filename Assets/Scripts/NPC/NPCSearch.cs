﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class NPCSearch : MonoBehaviour
{
    // Reference to our targets script
    public NPCTargets targets;

    // Reference to our navmeshagent
    public UnityEngine.AI.NavMeshAgent navComponent;

    // Reference to our NPC sound script
    public NPCSounds npcSound;
    
    // A timer to track how long we have been searching    
    public float timer = 0f;


    
    // Max amount of time to spend searching for targets when have lost sight of them (if no other threats)
    // The time we search will be reduced if we have other pressing threats in the area
    public float maxSearchTime;

    public float searchTime;

    // How close to get to search waypoints
    public float distDesired;

    // Maximum radius this unit will search
    public float maxSearchRadius;

    // Current working searchradius (cut in half when more targets to worry)
    public float searchRadius;


    // Bool to tell the timer to start counting, don't start until you reach the area to search
    bool startTimer = false;


    public bool debugOn;
    
    // Start is called before the first frame update
    void Start()
    {
	//Initialize variables
	searchTime = maxSearchTime;
	searchRadius = maxSearchRadius;
    }


    
    public void Search()
    {
	// Update target distance
	float distanceToTarget = Vector3.Distance(transform.position, targets.targets[targets.closestEntry].transform.position);
	
	// Do we only have one target?
	if(targets.NumberOfTargets() < 2)
	{
	    // Set searchRadius to max
	    searchRadius = maxSearchRadius;

	    // Set searchTime to max
	    searchTime = maxSearchTime;
	}
	else
	{
	    // Cut search radius in half, to do quick search, and get back to other targets sooner
	    searchRadius = maxSearchRadius * 0.5f;

	    // Limit search time to 3
	    searchTime = 3f;
	}

	if(debugOn)
	{
	    Debug.Log("closest enemy: " + targets.targets[targets.closestEntry].transform.position);
	    Debug.Log(timer + " / " + searchTime);
	}

	// If we haven't searched long enough
	if(timer <= searchTime)
	{
	    // Do we suspect the player of having gone up/down a ladder?
	    if(targets.suspectLadder)
	    {
		Debug.Log("suspect ladder");
		// Are we close enough to the ladder to use it?
		if(Vector3.Distance(transform.position, targets.suspectLadder.transform.position) < 5f)
		{
		    // Set a search point on the other end of the ladder		    
		    targets.targets[targets.closestEntry].transform.position = new Vector3(targets.suspectLadder.exitPoint.position.x, 1.5f, targets.suspectLadder.exitPoint.position.z);

		    // Set navmesh destination to this new defined point
		    if(navComponent.enabled)
			navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);

		    // Remember return ladder so we don't suspect it immediately
		    targets.returnLadder = targets.suspectLadder.exitPoint.GetComponentInParent<Ladder>();
			
		    // Stop wanting to use the ladder
		    targets.suspectLadder = null;
		}
		// If we aren't close enough to the ladder to use it yet
		else
		    // Set our destination to the position of the ladder
		    if(navComponent.enabled)
			navComponent.SetDestination(targets.suspectLadder.transform.position);		
	    }
	    
	    // Are we at the memory waypoint for the target?
	    else if(distanceToTarget <= distDesired)
	    {
		// Start the timer for how long to search, now we are in the area
		if(!startTimer)
		    startTimer = true;
	    
		Debug.Log("close enough");
		Vector3 point = Vector3.zero;

		// Keep searching for a random navmesh location until we find one
		while(point == Vector3.zero)
		{
		    point = RandomNavmeshLocation(searchRadius, targets.targets[targets.closestEntry].transform.position);
		    if(debugOn)
			Debug.Log(point);
		}
		// Set the random point as our memory waypoint (speculating where the target may be)
		targets.targets[targets.closestEntry].transform.position = point;

		// Set our destination to the position of the waypoint
		if(navComponent.enabled)
		    navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
	    }
	    // We are not at the memory waypoint yet
	    else
	    {
		// Is the destination unreachable?
		
		if(debugOn)
		    Debug.Log("not close enough  " + targets.closestDistance + " > " + distDesired);
		
		// Set destination for the memory waypoint
		if(navComponent.enabled)
		    navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
	    }

	    // If the timer is running
	    if(startTimer)
		//Increment our timer
		timer += Time.deltaTime;
	}
	// Time has run out
	else
	{
	    if(debugOn)
		Debug.Log("Time up!");
	    
	    //Reparent to our gameobject
	    targets.targets[targets.closestEntry].transform.parent = transform;

	    // Set last time we searched for this unit
	    targets.targets[targets.closestEntry].GetComponent<RememberUnit>().searchedFor = Time.time;
	    
	    // Must not have found target by now, or else would not be searching so
	    // Deactivate this target, we can't find the associated enemy
	    targets.targets[targets.closestEntry].SetActive(false);

	    // Reset timer
	    timer = 0f;

	    // Reset startTimer boolean
	    startTimer = false;

	    // Increase max search time so we search longer each time
	    maxSearchTime = Mathf.Round(maxSearchTime * 1.5f);

	    	// Forget any return ladder that we were not suspecting
	    targets.returnLadder = null;
	}
    }

    Vector3 RandomNavmeshLocation(float radius, Vector3 area)
    {
         Vector3 randomDirection = Random.insideUnitSphere * radius;
         randomDirection += area;
         UnityEngine.AI.NavMeshHit hit;
         Vector3 finalPosition = Vector3.zero;
         if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
	 {
             finalPosition = hit.position;            
         }
         return finalPosition;
     }

    public void Reset()
    {
	// Reset timer
	timer = 0f;

	// Reset startTimer boolean
	startTimer = false;

	// Forget any return ladder that we were not suspecting
	targets.returnLadder = null;
    }
}
