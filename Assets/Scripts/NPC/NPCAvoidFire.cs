﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCAvoidFire : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent navComponent;

    public bool debugOn;
    
    public void AvoidFire(Vector3 location, float runDist)
    {
	if(debugOn)
	    Debug.Log("move away from fire to: " + transform.position + (transform.position - location).normalized * runDist);
	if(navComponent.enabled)
	    // Set nav destination in opposite direction of fire
	    navComponent.SetDestination(transform.position + (transform.position - location).normalized * runDist);


	transform.LookAt(location);
    }
}
