﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCPatrol : MonoBehaviour
{
    // A flag to make this character not patrol
    public bool standStill;

    // A flag to make this character start wandering after following patrol path one time
    public bool oneTimePatrol;

    // A flag to make this character travel to random patrol points, instead of in order
    public bool randomPatrol;

    // Distance from patrol point to consider point reached
    public float distDesired = 3f;
    

    // Another character for this charcter to follow on patrols, not get separated from
    public GameObject followOther = null;

    // Distance to keep from unit we are following
    public float followDist = 1f;

    // Distance to start running to catch up
    public float runDist = 5f;

    // Current distance from unit we follow
    public float currentDist;

    // Whether to follow on the left side rather than the right side
    public bool followLeft;

    
    // Reference to unit we follow's health
    public Health otherHealth;

    // Reference to the patrol script on unit we follow so we can remember which points we've been to
    public NPCPatrol otherPatrol;
    
    // A list to hold permanent waypoints, a patrol path
    public List<GameObject> patrolPoints = new List<GameObject>();

    public List<GameObject> pauseAtPoints = new List<GameObject>();

    public List<float> pauseHowLong = new List<float>();
    
    // Maximum amount of time to wait at a patrol point
    public float maxWait;

    // Minimum amount of time to wait at a patrol point
    public float minWait;

    // Maximum distance to set wander points from self when wandering aimlessly
    public float wanderDist;


   

    
    // Reference to the navmeshagent
    public UnityEngine.AI.NavMeshAgent navComponent;
    
    // Keep track of current patrol point
    public int patrol = 0;

    // Timer for waiting at patrol points
    float timer = 0f;



    
    void Start()
    {
	// If we have a unit to follow
	if(followOther)
	{
	    // Set up reference to other unit's health script
	    otherHealth = followOther.GetComponent<Health>();

	    // Set up reference to other's patrol script
	    otherPatrol = followOther.transform.GetComponent<NPCPatrol>();
	}

	// Is it a random patrol?
	if(randomPatrol)
	{
	    // Set starting patrol point to a random entry in the list
	    patrol = Random.Range(0, patrolPoints.Count);

	    // Set a random wait time to make the npc wait a bit before starting random patrol
	    timer = Random.Range(minWait, maxWait);
	}
    }



    
    public void WalkRoute()
    {
	// Are we set to just stay in one place?
	if(!standStill)
	{
	    if(!followOther)
	    {
		// Do we have any patrol points to walk between?
		if(patrolPoints.Count > 0)
		{
		    // Have we not reached out destination?
		    if(Vector3.Distance(transform.position, patrolPoints[patrol].transform.position) > distDesired)
		    {
			if(navComponent.enabled)
			    // Set destination
			    navComponent.SetDestination(patrolPoints[patrol].transform.position);

			// Reset wait timer if not already
			if(timer <=0)
			{
			    if(pauseAtPoints.Contains(patrolPoints[patrol]))
				timer = Random.Range(maxWait, pauseHowLong[pauseAtPoints.IndexOf(patrolPoints[patrol])]);
			    else
				timer = Random.Range(minWait, maxWait);
			}
		    }
		    // We are at our destination
		    else
		    {
			// Have we not waited long enough?
			if(timer > 0)
			    // Increment timer
			    timer -= Time.deltaTime;
			// We have waited long enough
			else
			{
			    // If not random patrol
			    if(!randomPatrol)
			    {
				// Increment patrol point we are heading to
				patrol++;
			
				// If patrol incremented past last patrol point
				if(patrol >= patrolPoints.Count)
				{
				    // If not set to patrol once
				    if(!oneTimePatrol)
					// Reset patrol count to 0 and start over with patrol
					patrol = 0;
				    // Set to patrol just once
				    else
					// Remove all patrol points and start wandering
					patrolPoints.Clear();
				}
			    }
			    // Random patrol, choose next patrol point randomly
			    else
				// Set current patrol point to a random entry in the list
				patrol = Random.Range(0, patrolPoints.Count);
			}
		    }
		}
		// We have no patrol points, wander aimlessly
		else
		{
		    //Debug.Log("Wander");
		    // Have we not reached our current destination?
		    if(navComponent.enabled && navComponent.remainingDistance > distDesired)
		    {	    // Reset wait timer if not already
			//Debug.Log("Not reached destination");
			if(timer <= 0)
			    timer = Random.Range(minWait, maxWait);
		    }
		    // We are at our destination
		    else
		    {
			// Have we not waited long enough?
			if(timer > 0)
			    // Increment timer
			    timer -= Time.deltaTime;
			// We have waited long enough
			else
			{
			    // Set a new random destination
			    Vector3 point = Vector3.zero;

			    // Keep looking until we find a good point
			    while(point == Vector3.zero)
			    {
				point = RandomNavmeshLocation(wanderDist, transform.position);
			    }

			    if(navComponent.enabled)
				navComponent.SetDestination(point);
			}
		    }
		}
	    }


	    
	    // We are set to follow another
	    else
	    {
		// If other unit is dead
		if(otherHealth.currHealth <= 0)
		{
		    // Remove follow unit reference
		    followOther = null;
		}
		else
		{
		    // Set our patrol point to the same, so if they die we continue from same point
		    patrol = otherPatrol.patrol;
			
		    // Calculate distance to unit we follow
		    currentDist = Vector3.Distance(transform.position, followOther.transform.position);
			
		    // Is the other further from us than they should be?
		    if(currentDist > followDist)
		    {
			if(navComponent.enabled)
			{
			    // follow a point either slightly right of or left of the unit we are following
			    if(!followLeft)
				navComponent.SetDestination(followOther.transform.position + followOther.transform.right * 2f);
			    else
				navComponent.SetDestination(followOther.transform.position - followOther.transform.right * 2f);
			}
		    }
		}
	    }
	}
    }


    
    Vector3 RandomNavmeshLocation(float radius, Vector3 location)
    {
	UnityEngine.AI.NavMeshHit hit;
	Vector3 finalPosition = Vector3.zero;
	while(finalPosition == Vector3.zero)
	{
	    Vector3 randomDirection = Random.insideUnitSphere * radius;
	    randomDirection += location;

	    if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
	    {
		finalPosition = hit.position;            
	    }
	}
	return finalPosition;
    }
}
