﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RememberUnit : MonoBehaviour
{
    // A variable that holds a reference to the gameobject this waypoint is associated with, when used for targetting purposes
    public GameObject unit;

    // A tag to remember if the NPC had line of sight on this unit this frame
    public bool lineOfSight;

    // When did we last search for this unit
    public float searchedFor;

    // When was the last time we had line of sight on this unit
    public float lastSeen;
}
