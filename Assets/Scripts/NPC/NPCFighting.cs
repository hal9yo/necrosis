﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCFighting : MonoBehaviour
{
    // Reference to our targets script
    public NPCTargets targets;

    // Reference to our navmeshagent
    public NavMeshAgent navComponent;

    // Reference to our attack script
    public Attack attackScript;

    // Reference to our NPC sound script
    public NPCSounds npcSound;

    // Reference to our Stats
    public Stats stats;

    // Reference to our search script
    public NPCSearch npcSearch;

    // Remember the last place the target was, to guesstimate future position
    Vector3 oldPosition;

    // Target velocity
    Vector3 targetVelocity;
    
    //Desired distance to keep away from targets
    public float minDistance;

    //Desired distance to stay near to targets
    public float maxDistance;

    // Distance archers will abandon ranged and just go for melee
    public float meleeDistance;

    //Bool to prevent character from chasing after enemies
    public bool holdGround;
    
    //Bool to turn on debug messages
    public bool debugOn = false;

    // How far to run from enemies when they get to close
    public float runDist;

    

    // Main method that makes the gameobject move towards the closest enemy they are aware of
    public void EngageTargets()
    {
	// Run the method to calculate closest target and its distance to us
	targets.FindClosestFurthest();

	//If we are further than we want to be
	if(targets.closestDistance > maxDistance)
	{
	    if(!holdGround)	    
	    //Setnavmesh destination to within range of the target
		if(navComponent.enabled)
		{

		    //sc.center = transform.InverseTransformPoint(targets.targets[targets.closestEntry].transform.position);
		    //sc.radius = maxDistance;

		    //navComponent.SetDestination(sc.ClosestPoint(transform.position));
		    
		    navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
		}
	}
	// If target is too close for ranged, go for melee
	else if(targets.closestDistance < meleeDistance)
	{
	    // Rush target
	    if(navComponent.enabled)
		navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
	    // Look at target
	    transform.LookAt(new Vector3(targets.targets[targets.closestEntry].transform.position.x, 1.5f, targets.targets[targets.closestEntry].transform.position.z));
	}
	//Target is too close for comfort
	else if(targets.closestDistance < minDistance)
	{
	    Vector3 runTo = transform.position + (transform.position - targets.targets[targets.closestEntry].transform.position).normalized * runDist;
	    //Set navmesh destination in opposite direction from target
	    if(navComponent.enabled)
		navComponent.SetDestination(runTo);

	    Debug.Log("Run to " + runTo);

	    
	    //Look at target so we can attack
	    transform.LookAt(new Vector3(targets.targets[targets.closestEntry].transform.position.x, 1.5f, targets.targets[targets.closestEntry].transform.position.z));
	}
	//We are at desired firing distance
	else
	{
	    if(debugOn)
		Debug.Log("look at target");

	    // Find closest target
	    targets.FindClosestFurthest();
		
	    //Look at target so we can attack
	    transform.LookAt(new Vector3(targets.targets[targets.closestEntry].transform.position.x, 1.5f, targets.targets[targets.closestEntry].transform.position.z));

	    // Raycast hit variables to contain the first object hit by the linecasts
	    RaycastHit hit1;
	    RaycastHit hit2;
	    RaycastHit hit3;
	    
	    // Draw the lines they use to check for allies in the way so we can debug this
	    Debug.DrawLine(transform.position, targets.targets[targets.closestEntry].transform.position);
	    Debug.DrawLine(transform.position + transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position);
	    Debug.DrawLine(transform.position - transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position);

	    
	    // Use linecast to check if we have direct line of sight to the object from our center
	    if(Physics.Linecast(transform.position, targets.targets[targets.closestEntry].transform.position, out hit1) && Physics.Linecast(transform.position + transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position, out hit2) && Physics.Linecast(transform.position - transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position, out hit3))
	    {
		//Did any of the linecasts hit an ally?
		if(hit1.collider.tag == "EnemyUnit" || hit2.collider.tag == "EnemyUnit" || hit3.collider.tag == "EnemyUnit")
		{
		    //Create navpoint to move left or right, depending on where the unit obscuring our line of sight is
		    Vector3 moveDirection;

		    float dir1 = 0f;
		    float dir2 = 0f;
		    float dir3 = 0f;
			    
		    if(hit1.collider.tag == "EnemyUnit")
		    {
			dir1 = LeftOrRight(hit1.transform.position);

			if(debugOn)
			    Debug.Log("hit1: " + dir1);
		    }
		    if(hit2.collider.tag =="EnemyUnit")
		    {
			dir2 = LeftOrRight(hit2.transform.position);

			if(debugOn)
			    Debug.Log("hit2: " + dir2);
		    }
		    if(hit3.collider.tag =="EnemyUnit")
		    {
			dir3 = LeftOrRight(hit3.transform.position);

			if(debugOn)
			    Debug.Log("hit3: " + dir3);
		    }
			    
		    if(debugOn)
			Debug.Log("hit1+hit2+hit3 = " + (dir1+dir2+dir3));

		    // hit player/playerunits are more to the right of us
		    if(dir1 + dir2 + dir3 > 0)
			// Move to the left
			moveDirection = transform.position + transform.right * -10f + transform.forward * 5f;
		    // hit player/playerunits are more to the left of us
		    else if(dir1 + dir2 + dir3 < 0)
			// Move to the right
			moveDirection = transform.position + transform.right * 10f + transform.forward * 5f;
		    // hit player/playerunits are directly in front of us
		    else
		    {
			//Randomly choose left or right to move to
			if(Random.Range(0, 2) > 0)
			    moveDirection = transform.position + transform.right * 10f + transform.forward * 5f;
			else
			    moveDirection = transform.position + transform.right * -10f + transform.forward * 5f;
		    }

		    // Move to the strafe location
		    if(navComponent.enabled)
			navComponent.SetDestination(moveDirection);
		}
		//Do we have line of sight on target?
		else
		{
		    if(debugOn)
			Debug.Log("In range, line of sight, stop moving");
		    //Stop moving
		    if(navComponent.enabled)
		    {
			navComponent.isStopped = true;
			navComponent.ResetPath();
		    }
		}
	    }

	    //No line of sight
	    else
	    {
		if(debugOn)
		    Debug.Log("In range, no line of sight, stop moving");
		//move closer
		if(navComponent.enabled)
		    navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
	    }
	}
    }



    //Return -1 if object is left of us, 1 if right of us, 0 if straight ahead
    float LeftOrRight(Vector3 targetObj)
    {
	//Get direction to the target Object
	Vector3 targetDir = targetObj - transform.position;
	
	//Get crossproduct between our forward and the direction to the object
	Vector3 perp = Vector3.Cross(transform.forward, targetDir);

	//Get dot between the crossproduct and our up
	float dir = Vector3.Dot(perp, transform.up);
		
	if (dir > 0f)
	    return 1f;
	else if (dir < 0f) 
	    return -1f;
	else 
	    return 0f;
    }
}
