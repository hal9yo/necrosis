﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSounds : MonoBehaviour
{

    // Audio Source to play sound effects
    public AudioSource audioSource;

    
    // Array of audio clips when zombie spotted
    public AudioClip[] zombieSpotted;

    // Array of audio clips when necromancer spotted
    public AudioClip[] necroSpotted;   

    // Array of audio clips when running
    public AudioClip[] runAndYell;

    // Array of audio clips when hurt
    public AudioClip[] hurtSound;
    
    // Array of audio clips when attacking
    public AudioClip[] attackSound;

    // Array of audio clips when burning
    public AudioClip[] burningSound;

    // Array of audio clips when dying
    public AudioClip[] dyingSound;

    // Array of audio clips telling allies about Necromancer
    public AudioClip[] necroTelling;

    // Array of audio clips when recruiting allies to fight
    public AudioClip[] recruitAllies;

    // Array of audio clips when necromancer spotted but character doesn't know
    public AudioClip[] necroUnknown;

	
    // How often to play certain sounds
    public float soundFrequency;

    // Timer to track how long since playing certain sound last
    private float idleTimer = 0f;

    // Tag to avoid playing sound again
    public bool burningPlaying;


    public void OnEnable()
    {
	// Randomize the pitch of the sounds played
	audioSource.pitch = Random.Range(.9f, 1.1f);
    }

    //Play a random audioclip from one of several groups
    public void PlayRandom(int option)
    {
	//If not already playing sound effect
	if(!audioSource.isPlaying)
	{
	    // Reset volume if lowered
	    if(audioSource.volume < .5f)
		audioSource.volume = .5f;
	    
	    // Set clip to null in case it isn't time to play again
	    audioSource.clip = null;
	    
	    //select which type of sound to play
	    switch(option)
	    {
		//Zombie has been spotted sound effect
		case 1:
		    if(idleTimer <= 0 && zombieSpotted.Length > 0)
		    {
			audioSource.clip = zombieSpotted[Random.Range(0, zombieSpotted.Length)];
			idleTimer = Random.Range(1f, soundFrequency);
		    }
		    break;
		    
		    //Necromancer spotted
		case 2:
		    if(idleTimer <= 0 && necroSpotted.Length > 0)
		    {
			audioSource.clip = necroSpotted[Random.Range(0, necroSpotted.Length)];
			idleTimer = Random.Range(1f, soundFrequency);
		    }
		    break;
		    
		    //Run Away
		case 3:
		    if(idleTimer <= 0 && runAndYell.Length > 0)
		    {
			audioSource.clip = runAndYell[Random.Range(0, runAndYell.Length)];
			idleTimer = Random.Range(0f, soundFrequency);
		    }
		    break;
		    
		    //Getting hurt
		case 4:
		    if(hurtSound.Length > 0)
			audioSource.clip = hurtSound[Random.Range(0, hurtSound.Length)];
		    break;
		    
		    //Attacking
		case 5:
		    if(attackSound.Length > 0)
			audioSource.clip = attackSound[Random.Range(0, attackSound.Length)];
		    break;

		    //Burning
		case 6:
		    if(burningSound.Length > 0)
			audioSource.volume = 1f;
			burningPlaying = true;
			audioSource.clip = burningSound[Random.Range(0, burningSound.Length)];
		    break;
		    //Dying
		case 7:
		    if(dyingSound.Length > 0)
			audioSource.volume = 1f;
			audioSource.clip = dyingSound[Random.Range(0, dyingSound.Length)];
		    break;
		    
		case 8:
		    if(idleTimer <= 0 && necroTelling.Length > 0)
		    {
			audioSource.clip = necroTelling[Random.Range(0, necroTelling.Length)];
			idleTimer = Random.Range(0f, soundFrequency);
		    }
		    break;
		case 9:
		    if(idleTimer <= 0 && recruitAllies.Length > 0)
		    {
			audioSource.clip = recruitAllies[Random.Range(0, recruitAllies.Length)];
			idleTimer = Random.Range(0f, soundFrequency);
		    }
		    break;
		case 10:
		    if(idleTimer <= 0 && necroUnknown.Length > 0)
		    {
			audioSource.clip = necroUnknown[Random.Range(0, necroUnknown.Length)];
			idleTimer = Random.Range(0f, soundFrequency);
		    }
		    break;
	    }

	    // Make sure not a null audio.clip
	    if(GetComponent<AudioSource>().clip)
	    {
		//Play the sound
		audioSource.Play();
	    }
	}
    }

    public void FadeScream()
    {
	if(burningPlaying)
	{
	    if(audioSource.volume > 0f)
	    {
		audioSource.volume -= Time.deltaTime;
	    }
	    else
	    {
		audioSource.Stop();
		audioSource.volume = .5f;
		burningPlaying = false;
	    }
	}
    }
}
