﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : InteractiveEnvironment
{
    public Transform exitPoint;
    public GameObject player;
    
    // Code to run when environment object is used
    public override void Use()
    {
        player.transform.position = new Vector3(exitPoint.position.x, 1.5f, exitPoint.position.z);
    }
}
