﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour
{
    //Reference to the loading text
    public GameObject loadingText;


    //Function that restarts the current level
    public void Restart()
    {
	//Display "Loading..." message
	loadingText.SetActive(true);
	
	//Restart this "scene"
	SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void NewGame()
    {
	//Display "Loading..." message
	loadingText.SetActive(true);

	//Load the first level
	SceneManager.LoadSceneAsync("hamlet");
    }

    public void NewArenaGame()
    {
	//Display "Loading..." message
	loadingText.SetActive(true);

	//Load the first level
	SceneManager.LoadSceneAsync("mini1");	
    }

    public void ExitToTitle()
    {
	//Display "Loading..." message
	loadingText.SetActive(true);

	//Load the first level
	SceneManager.LoadSceneAsync("TitleScreen");	
    }

    public void NextArenaLevel(int nextLevel)
    {
	//Display "Loading..." message
	loadingText.SetActive(true);
	
	//Load the first level
	SceneManager.LoadSceneAsync("mini" + nextLevel.ToString());	
    }
}
