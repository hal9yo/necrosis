﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicOff : MonoBehaviour
{
    public PlayBackgroundMusic musicScript;
    public Text buttonText;
    
    //Function that turns off music
    public void MusicToggle()
    {
	//Switch music from off to on and vice versa
	musicScript.enabled = !musicScript.enabled;

	//Debug message to let us know
	Debug.Log("Music toggled");

	//Change the text on the button to reflect the current state
	if(musicScript.enabled)
	{
	    buttonText.text = "Music On";
	}
	else
	{
	    buttonText.text = "Music Off";
	}
    }
}
