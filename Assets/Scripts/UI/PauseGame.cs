﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PauseGame : MonoBehaviour
{
    private bool isPaused = false;
    public GameObject pauseMenu;
    public bool stopTime = true;
    public bool invUp = false;
    public GameObject invMenu;
    public Inventory invScript;

    // If text is displayed to screen use this flag to pause the game while it is read
    public bool textUp = false;
    
    public Scrollbar scrollbar;

    // Images to display the sprites of inventory items
    public Image invImage1;
    public Image invImage2;
    public Image invImage3;
    public Image invImage4;

    // Texts to display the names of inventory items
    public Text invText1;
    public Text invText2;
    public Text invText3;
    public Text invText4;

    // Inventory buttons, so we can turn them off if no item in that slot
    public GameObject invDrop1;
    public GameObject invDrop2;
    public GameObject invDrop3;
    public GameObject invDrop4;
    
    public GameObject invEquip1;
    public GameObject invEquip2;
    public GameObject invEquip3;
    public GameObject invEquip4;
    
    public GameObject invLookAt1;
    public GameObject invLookAt2;
    public GameObject invLookAt3;
    public GameObject invLookAt4;
	    
    public GameObject weapDrop;
    public GameObject weapUnequip;
    public GameObject weapLookAt;
    
    public GameObject armDrop;
    public GameObject armUnequip;
    public GameObject armLookAt;
    
    public GameObject talDrop;
    public GameObject talUnequip;
    public GameObject talLookAt;

    public GameObject shieldDrop;
    public GameObject shieldUnequip;
    public GameObject shieldLookAt;
    

    // Images to display the sprites of character's inventory items
    public Image charInvImage1;
    public Image charInvImage2;

    // Texts to display the names of character's inventory items
    public Text charInvText1;
    public Text charInvText2;

    // character's Inventory buttons, so we can turn them off if no item in that slot
    public GameObject charInvDrop1;
    public GameObject charInvDrop2;
    
    public GameObject charInvEquip1;
    public GameObject charInvEquip2;
    
    public GameObject charInvLookAt1;
    public GameObject charInvLookAt2;
    
    
    // Images to display the current look of the character
    public Image charHead;
    public Image charBody;
    public Image charWeap;
    public Image charShield;

    public GameObject leftArrow;
    public GameObject rightArrow;
    
    // Image displays for weapon, armor, talisman Equips
    public Image equipedWeapImg;
    public Image equipedArmImg;
    public Image equipedTalImg;
    public Image equipedShieldImg;

    // Text displays for weapon, armor, talisman Equips
    public Text equipedWeapText;
    public Text equipedArmText;
    public Text equipedTalText;
    public Text equipedShieldText;

    // Text that displays the name of the character we are currently manipulating the Equipment of
    public Text currentCharacter;

    // Text that displays the current stats of the character
    public Text characterStats;

    // Keeps track of where in the inventory we are currently displaying/manipulating (when inventory is longer than the 4 slots we have in the menu)
    public int index = 0;

    public float defaultMinFightDist = 2f;
    public float defaultMaxFightDist = 2.5f;
    public float defaultMeleeFightDist = 2.5f;
	
    // Character whose equipment is being opened next to the inventory
    public GameObject character;

    // The Inventory Script of the character we are currently Equipping
    public Inventory charInvScript;

    // Reference to our player to make their equipment open when no other character selected
    public GameObject player;

    
    // Text display object in UI
    public GameObject textDisplay;
    // Text box of text display
    public Text textBox;

    public List<GameObject> playerUnits;
    public int characterIndex;

    string npcMeleeArmSpriteSheet = "NPC-Clothes";
    string npcArcherArmSpriteSheet = "NPC-Archer-Clothes";
    string npcFireArcherArmSpriteSheet = "NPC-FireArcher";
    
    void Start()
    {
	scrollbar.onValueChanged.AddListener((float val) => ScrollbarCallback(val));
    }



    
    // Update is called once per frame
    void Update()
    {
	//If the escape key is pressed
        if (Input.GetKeyDown("escape"))
        {
	    //If inventory is up
	    if(invUp && !isPaused)
		// Turn off inventory menu
		invUp = false;
	    else
		//Change state to and from paused
		isPaused = !isPaused;
        }

	//If the state is paused
	if(isPaused)
	{
	    //Show pause menu
	    pauseMenu.SetActive(true);
	}
	//If the state is not paused
	else
	{
	    //Hide pause menu
	    pauseMenu.SetActive(false);
	}


	//If the 'I' key is pressed
        if (Input.GetKeyDown("i") && !isPaused)
        {
	    // Select which character the inventory is being opened for
	    character = player;
	    
	    //Change state to and from Inventory Menu being up
            invUp = !invUp;
        }

	//If the state is paused
	if(invUp)
	{
	    // Get a list of all player units
	    playerUnits = new List<GameObject>(GameObject.FindGameObjectsWithTag("PlayerUnit"));

	    // Default index to the last entry (player)
	    characterIndex = playerUnits.Count;
	    // Set index to the character we are editing first
	    for(int i = 0; i < playerUnits.Count; i++)
	    {
		// If the character we're editing is this entry in the index
		if(playerUnits[i] == character)
		    // Set the index to this entry
		    characterIndex = i;
	    }
	    
	    // Get inventory script of current character
	    charInvScript = character.GetComponentInChildren<Inventory>();

	    // Display the name of the character whose Equipment is being manipulated
	    currentCharacter.text = character.name;

	    // Update inv menu
	    UpdateInventory();

	    // Update equiped items menu
	    UpdateEquipment();
	    
	    //Show inv menu
	    invMenu.SetActive(true);
	}
	//If the state is not paused
	else
	{
	    //Hide inv menu
	    invMenu.SetActive(false);
	}

	// Pause the game if either the menu or inventory is up
	if(stopTime && (isPaused || invUp || textUp))
	{
	    //Set time to 0, time stops
	    Time.timeScale = 0f;
	}
	else
	    Time.timeScale = 1f;
    }




    public void IncreaseCharacterIndex()
    {
	// Increase the index
	characterIndex++;

	Debug.Log(characterIndex);
	Debug.Log(playerUnits.Count);
	
	// Loop back to start after the end
	if(characterIndex > playerUnits.Count)
	    characterIndex = 0;

	// If the last entry is selected set character to player
	if(characterIndex == playerUnits.Count)
	    character = player;
	else
	    character = playerUnits[characterIndex];
	
	// Get inventory script of current character
	charInvScript = character.GetComponentInChildren<Inventory>();

	UpdateEquipment();
    }

    public void DecreaseCharacterIndex()
    {
	// Decrease the index
	characterIndex--;

	Debug.Log(characterIndex);
	Debug.Log(playerUnits.Count);
		
	// Loop back to end after the beginning
	if(characterIndex < 0)
	    characterIndex = playerUnits.Count;

	// If the last entry is selected set character to player
	if(characterIndex == playerUnits.Count)
	    character = player;
	else
	    character = playerUnits[characterIndex];

	Debug.Log(characterIndex);
	Debug.Log(playerUnits.Count);

	
	// Get inventory script of current character
	charInvScript = character.GetComponentInChildren<Inventory>();
	
	UpdateEquipment();
    }
    

    
    

    // Update the menu displays to the currently equiped items
    void UpdateEquipment()
    {
	// Reference character's health script to change armor amount
	Health healthScript = character.GetComponent<Health>();

	// If character has no inventor
	if(!charInvScript)
	{
	    // This character cannot equip anything so turn everything off
	    
	    // No item equiped here so don't show anything
	    equipedWeapImg.enabled = false;
	    equipedWeapText.text = null;

	    // Turn off buttons
	    weapDrop.SetActive(false);
	    weapUnequip.SetActive(false);
	    weapLookAt.SetActive(false);

	    // No item equiped here so don't show anything
	    equipedArmImg.enabled = false;
	    equipedArmText.text = null;

	    // Turn off buttons
	    armDrop.SetActive(false);
	    armUnequip.SetActive(false);
	    armLookAt.SetActive(false);

	    // No item equiped here so don't show anything
	    equipedTalImg.enabled = false;
	    equipedTalText.text = null;

	    // Turn off buttons
	    talDrop.SetActive(false);
	    talUnequip.SetActive(false);
	    talLookAt.SetActive(false);

	    // No item equiped here so don't show anything
	    equipedShieldImg.enabled = false;
	    equipedShieldText.text = null;

	    // Turn off buttons
	    shieldDrop.SetActive(false);
	    shieldUnequip.SetActive(false);
	    shieldLookAt.SetActive(false);



	    // Images to display the sprites of character's inventory items
	    charInvImage1.enabled = false;
	    charInvImage2.enabled = false;

	    // Texts to display the names of character's inventory items
	    charInvText1.text = null;
	    charInvText2.text = null;

	    // character's Inventory buttons, so we can turn them off if no item in that slot
	    charInvDrop1.SetActive(false);
	    charInvDrop2.SetActive(false);
    
	    charInvEquip1.SetActive(false);
	    charInvEquip2.SetActive(false);
	    
	    charInvLookAt1.SetActive(false);
	    charInvLookAt2.SetActive(false);


	    // Turn off equip buttons on player inventory since character can't equip items
	    invEquip1.SetActive(false);
	    invEquip2.SetActive(false);
	    invEquip3.SetActive(false);
	    invEquip4.SetActive(false);

	    
	    
	    // Put character sprite in the portrait

	    charHead.sprite = healthScript.animators[0].transform.GetComponent<SpriteRenderer>().sprite;

	    // Turn on one portrait sprite and the rest off
	    charHead.enabled = true;
	    charBody.enabled = false;
	    charWeap.enabled = false;
	    charShield.enabled = false;
	}


	// Character has an inventory    
	else
	{  
	    if(!charInvScript.weapon)
	    {
		// No item equiped here so don't show anything
		equipedWeapImg.enabled = false;
		equipedWeapText.text = null;

		// Turn off buttons
		weapDrop.SetActive(false);
		weapUnequip.SetActive(false);
		weapLookAt.SetActive(false);
	    }
	    else
	    {
		// Update image and text for equiped weapon
		equipedWeapImg.sprite = charInvScript.weapon.GetComponentInChildren<SpriteRenderer>().sprite;
		equipedWeapText.text = charInvScript.weapon.name;

		// Make image turned on
		equipedWeapImg.enabled = true;

		// Turn on buttons
		weapDrop.SetActive(true);
		weapUnequip.SetActive(true);
		weapLookAt.SetActive(true);
	    }

	    if(!charInvScript.armor)
	    {
		// No item equiped here so don't show anything
		equipedArmImg.enabled = false;
		equipedArmText.text = null;

		// Turn off buttons
		armDrop.SetActive(false);
		armUnequip.SetActive(false);
		armLookAt.SetActive(false);
	    }
	    else
	    {
		// Update image and text for equiped weapon
		equipedArmImg.sprite = charInvScript.armor.GetComponentInChildren<SpriteRenderer>().sprite;
		equipedArmText.text = charInvScript.armor.name;

		// Make image turned on
		equipedArmImg.enabled = true;

		// Turn on buttons
		armDrop.SetActive(true);
		armUnequip.SetActive(true);
		armLookAt.SetActive(true);
	    }

	    if(!charInvScript.talisman)
	    {
		// No item equiped here so don't show anything
		equipedTalImg.enabled = false;
		equipedTalText.text = null;

		// Turn off buttons
		talDrop.SetActive(false);
		talUnequip.SetActive(false);
		talLookAt.SetActive(false);
	    }
	    else
	    {
		// Update image and text for equiped weapon
		equipedTalImg.sprite = charInvScript.talisman.GetComponentInChildren<SpriteRenderer>().sprite;
		equipedTalText.text = charInvScript.talisman.name;

		// Make image turned on
		equipedTalImg.enabled = true;

		// Turn on buttons
		talDrop.SetActive(true);
		talUnequip.SetActive(true);
		talLookAt.SetActive(true);
	    }

	    if(!charInvScript.shield)
	    {
		// No item equiped here so don't show anything
		equipedShieldImg.enabled = false;
		equipedShieldText.text = null;

		// Turn off buttons
		shieldDrop.SetActive(false);
		shieldUnequip.SetActive(false);
		shieldLookAt.SetActive(false);
	    }
	    else
	    {
		// Update image and text for equiped weapon
		equipedShieldImg.sprite = charInvScript.shield.GetComponentInChildren<SpriteRenderer>().sprite;
		equipedShieldText.text = charInvScript.shield.name;

		// Make image turned on
		equipedShieldImg.enabled = true;

		// Turn on buttons
		shieldDrop.SetActive(true);
		shieldUnequip.SetActive(true);
		shieldLookAt.SetActive(true);
	    }


	    // If there is an item in the character's first inventory slot
	    if(charInvScript.inventory.Count > 0 && character != player)
	    {
		charInvImage1.enabled = true;
		charInvImage1.sprite = charInvScript.inventory[0].GetComponentInChildren<SpriteRenderer>().sprite;
		charInvText1.text = charInvScript.inventory[0].name;
		charInvDrop1.SetActive(true);
		charInvEquip1.SetActive(true);
		charInvLookAt1.SetActive(true);
	    }
	    else
	    {
		charInvImage1.enabled = false;
		charInvText1.text = null;
		charInvDrop1.SetActive(false);
		charInvEquip1.SetActive(false);
		charInvLookAt1.SetActive(false);
	    }

	    // If character has an object in second inventory slot
	    if(charInvScript.inventory.Count > 1 && character != player)
	    {
		charInvImage1.enabled = true;
		charInvImage2.sprite = charInvScript.inventory[1].GetComponentInChildren<SpriteRenderer>().sprite;
		charInvText2.text = charInvScript.inventory[1].name;
		charInvDrop2.SetActive(true);
		charInvEquip2.SetActive(true);
		charInvLookAt2.SetActive(true);

	    }
	    else
	    {
		charInvImage2.enabled = false;
		charInvText2.text = null;
		charInvDrop2.SetActive(false);
		charInvEquip2.SetActive(false);
		charInvLookAt2.SetActive(false);
	    }



	    
	    //Update character portrait


	    if(playerUnits.Count > 0)
	    {
		leftArrow.SetActive(true);
		rightArrow.SetActive(true);
	    }
	    else
	    {
		leftArrow.SetActive(false);
		rightArrow.SetActive(false);		
	    }
	    
	    foreach(Animator anim in healthScript.animators)
	    {
		if(anim.gameObject.activeSelf)
		{
		    anim.transform.GetComponent<SpriteSwap>().ForceUpdate();
		}
	    }
	
	    SpriteRenderer[] renderers = new SpriteRenderer[4];

	    renderers[0] = healthScript.animators[0].transform.GetComponent<SpriteRenderer>();
	    renderers[1] = healthScript.animators[1].transform.GetComponent<SpriteRenderer>();
	    renderers[2] = healthScript.animators[2].transform.GetComponent<SpriteRenderer>();
	    renderers[3] = healthScript.animators[3].transform.GetComponent<SpriteRenderer>();

	    if(renderers[0].gameObject.activeSelf)
	    {
		charHead.sprite = renderers[0].sprite;
		charHead.enabled = true;
	    }
	    else
		charHead.enabled = false;

	    if(renderers[1].gameObject.activeSelf)
	    {
		charBody.sprite = renderers[1].sprite;
		charBody.enabled = true;
	    }
	    else
		charBody.enabled = false;

	    if(renderers[2].gameObject.activeSelf)
	    {
		charWeap.sprite = renderers[2].sprite;
		charWeap.enabled = true;
	    }
	    else
		charWeap.enabled = false;
	
	    if(renderers[3].gameObject.activeSelf)
	    {
		charShield.sprite = renderers[3].sprite;
		charShield.enabled = true;
	    }
	    else
		charShield.enabled = false;
	}

	    
	// Get Current Character Stats
	
	Attack charAttack = character.GetComponent<Attack>();
	PlayerAttack playAttack = character.GetComponent<PlayerAttack>();
	ArcherAttack charArchAttack = character.GetComponent<ArcherAttack>();
	Stats statScript = character.GetComponent<Stats>();

	List<Animator> animators = healthScript.animators;

	float minDmg = 0;
	float maxDmg = 1;
	float attkSpd;
	float attkRng = 3;
	float blkChnc;
	float armor;
	float bowSpd;
	float bowRng;
	float arrwMinDmg;
	float arrwMaxDmg;
	float runSpd;
	float wlkSpd;
	float currHealth;
	float maxHealth;
	float healthRegen;
	
	currHealth = (int)(healthScript.currHealth * 10f) * .1f;
	maxHealth = statScript.maxHealth;
	healthRegen = statScript.healthRegen;
	
	if(charAttack)
	{
	    minDmg = charAttack.damageMin;
	    maxDmg = charAttack.damageMax;
	    attkRng = charAttack.attackDistance;
	}
	
	if(playAttack)
	{
	    minDmg = playAttack.damageMin;
	    maxDmg = playAttack.damageMax;

	    if(charInvScript.weapon)
	    {
		Weapon weapScript = charInvScript.weapon.GetComponent<Weapon>();
		if(weapScript.type == "melee")
		    attkRng = playAttack.attackDistance;
		else if(weapScript.type == "bow")
		    bowRng = playAttack.attackDistance;
	    }
	}

	attkSpd = animators[0].GetFloat("AttackSpeed");
	blkChnc = statScript.block;
	armor = healthScript.armor;

	if(charArchAttack)
	{
	    arrwMinDmg = charArchAttack.arrowPrefab.GetComponent<Arrow>().damageMin;
	    arrwMaxDmg = charArchAttack.arrowPrefab.GetComponent<Arrow>().damageMax;
	    bowRng = charArchAttack.shootDistance;
	}
	else
	{
	    arrwMinDmg = 0;
	    arrwMaxDmg = 0;
	    bowRng = 0;
	}
	
	bowSpd = animators[0].GetFloat("BowSpeed");
	runSpd = statScript.runSpeed;
	wlkSpd = statScript.walkSpeed;

	if(healthRegen != 0)
	    characterStats.text = "Health: " + currHealth + " / " + maxHealth + "\nHealth Regen: " + healthRegen + "\n\n___Melee___\nMin Dmg: " + minDmg + "\nMax Dmg: " + maxDmg + "\nAttack Speed: " + attkSpd + "\nAttack Range: " + attkRng + "\nBlock Chance: " + blkChnc + "%\nArmor: " + armor + "\n\n___Range___\nArrow Min Dmg: " + arrwMinDmg + "\nArrow Max Dmg: " + arrwMaxDmg + "\nBow Speed: " + bowSpd + "\nBow Range: " + bowRng + "\n\n___Movement Speed___\nRun Speed: " + runSpd + "\nWalk Speed: " + wlkSpd;
	else
	    characterStats.text = "Health: " + currHealth + " / " + maxHealth + "\n\n___Melee___\nMin Dmg: " + minDmg + "\nMax Dmg: " + maxDmg + "\nAttack Speed: " + attkSpd + "\nAttack Range: " + attkRng + "\nBlock Chance: " + blkChnc + "%\nArmor: " + armor + "\n\n___Range___\nArrow Min Dmg: " + arrwMinDmg + "\nArrow Max Dmg: " + arrwMaxDmg + "\nBow Speed: " + bowSpd + "\nBow Range: " + bowRng + "\n\n___Movement Speed___\nRun Speed: " + runSpd + "\nWalk Speed: " + wlkSpd;
    }
    



    
    
    void UpdateInventory()
    {
	// If we have more than 4 items
	if(invScript.inventory.Count > 4)
	    // Set scrollbar size by the number of items in the inventory
	    scrollbar.size = 4f / (float)invScript.inventory.Count;
	else
	    // Cap size to 1
	    scrollbar.size = 1;

	// Don't fucking divide by zero
	if(invScript.inventory.Count > 1)
	    // Set scrollbar position by how far through the inventory count the index is
	    scrollbar.value = (float)index / (float)(invScript.inventory.Count - 1);
	// Instead of dividing by zero just set the value to 0
	else
	    scrollbar.value = 0;
	    
	// Go through the items in the inventory starting with the index, ending at index + 2 (three items total)
	// Assign the image of the inventory item to the correlating menu slot, as well as the name to the text slot
	if(invScript.inventory.Count > index)
	{	    
	    invImage1.sprite = invScript.inventory[index].GetComponentInChildren<SpriteRenderer>().sprite;
	    invText1.text = invScript.inventory[index].name;

	    // Make sure image turned on
	    invImage1.enabled = true;

	    // Turn on buttons
	    invDrop1.SetActive(true);
	    invEquip1.SetActive(true);
	    invLookAt1.SetActive(true);
	}
	else
	{
	    // No item at this point in inventory so don't show anything
	    invImage1.enabled = false;
	    invText1.text = null;

	    // Turn off buttons for this item slot in the UI
	    invDrop1.SetActive(false);
	    invEquip1.SetActive(false);
	    invLookAt1.SetActive(false);
	}

	
	if(invScript.inventory.Count > index + 1)
	{
	    // Put image and name of object in the UI Canvas at this slot
	    invImage2.sprite = invScript.inventory[index + 1].GetComponentInChildren<SpriteRenderer>().sprite;
	    invText2.text = invScript.inventory[index + 1].name;

	    // Turn on image display for this item slot in the UI
	    invImage2.enabled = true;

	    // Turn on buttons
	    invDrop2.SetActive(true);
	    invEquip2.SetActive(true);
	    invLookAt2.SetActive(true);
	}
	else
	{
	    invImage2.enabled = false;
	    invText2.text = null;

	    // Turn off buttons for this item slot in the UI
	    invDrop2.SetActive(false);
	    invEquip2.SetActive(false);
	    invLookAt2.SetActive(false);
	}

	
	if(invScript.inventory.Count > index + 2)
	{
	    invImage3.sprite = invScript.inventory[index + 2].GetComponentInChildren<SpriteRenderer>().sprite;
	    invText3.text = invScript.inventory[index + 2].name;

	    invImage3.enabled = true;

	    // Turn on buttons
	    invDrop3.SetActive(true);
	    invEquip3.SetActive(true);
	    invLookAt3.SetActive(true);
	}
	else
	{
	    invImage3.enabled = false;
	    invText3.text = null;

	    	    // Turn off buttons for this item slot in the UI
	    invDrop3.SetActive(false);
	    invEquip3.SetActive(false);
	    invLookAt3.SetActive(false);
	}

	
	if(invScript.inventory.Count > index + 3)
	{
	    invImage4.sprite = invScript.inventory[index + 3].GetComponentInChildren<SpriteRenderer>().sprite;
	    invText4.text = invScript.inventory[index + 3].name;

	    invImage4.enabled = true;

	    // Turn on buttons
	    invDrop4.SetActive(true);
	    invEquip4.SetActive(true);
	    invLookAt4.SetActive(true);
	}
	else
	{
	    invImage4.enabled = false;
	    invText4.text = null;

	    // Turn off buttons for this item slot in the UI
	    invDrop4.SetActive(false);
	    invEquip4.SetActive(false);
	    invLookAt4.SetActive(false);
	}

    }


    // When the scroll bar is moved by the player
    void ScrollbarCallback(float newValue)
    {
	// Set the index to the value of the scroll bar * the size of the inventory less three because 3 slots
	index = (int)(newValue * (invScript.inventory.Count - 1));
	
	// Update the inventory to show from the new index value
	UpdateInventory();
    }

    public void IncreaseIndex()
    {	
	// Increase the index
	index++;

	// If the index is further than the entries in the inventory
	if(index > invScript.inventory.Count - 1)
	    index = invScript.inventory.Count - 1;

	// Update the inventory to show from the new index
	UpdateInventory();
    }

    public void DecreaseIndex()
    {
	// Decrease the index
	index--;

	// If the index is less than 0
	if(index < 0)
	    index = 0;

	// Update the inventory to show from the new index
	UpdateInventory();
    }



    

    public void EquipThis(int slot)
    {
	// If item is a Weapon
	if(invScript.inventory[index + slot].tag == "Weapon")
	{
	    Weapon weapScript;
	    Attack charAttack;
	    PlayerAttack playAttack;
	    ArcherAttack charArchAttack;
	    Stats statScript;
	    List<Animator> animators;
	    Armor armScript;
	    NPCFighting fightScript;
	    Zombie zombScript;
	    
	    // Try to unequip anything that may be currently equiped
	    // Make sure a weapon is currently equiped
	    if(charInvScript.weapon)
	    {
		// Add the currently equiped weapon to the inventory list
		invScript.inventory.Add(charInvScript.weapon);

		// Get reference to item's Weapon script
		weapScript = charInvScript.weapon.GetComponent<Weapon>();
	    
		// Get reference to charater's stats
		charAttack = character.GetComponent<Attack>();
		playAttack = character.GetComponent<PlayerAttack>();
		charArchAttack = character.GetComponent<ArcherAttack>();
		statScript = character.GetComponent<Stats>();
	    
		animators = character.GetComponent<Health>().animators;

	    
		// Set the block chance to that of the weapon
		statScript.block -= weapScript.block;
		// Adjust the character's power level
		statScript.power -= weapScript.power;
		// Set walk and run speeds less amount this weapon hinders movement
		statScript.walkSpeed += weapScript.walkSpeedLess;
		statScript.runSpeed += weapScript.runSpeedLess;

		// Run any special script this weapon may have
		weapScript.UnEquip(character);
		
		// Turn off character's weapon sprite
		animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";

		// Divide animators speed by the attackspeed of the weapon
		foreach(Animator anim in animators)
		{
		    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / weapScript.attackSpeed);
		}

		if(charArchAttack)
		    charArchAttack.shootDistance = defaultMaxFightDist;
		
		//Change the character's Stats to those of the weapon
		if(charAttack)
		{
		    // Set player's attack stats to those of the weapon
		    charAttack.attackDistance = 3f;
		    charAttack.damageMin = 1;
		    charAttack.damageMax = 2;

		    // Change the distances the character keeps during fights to those of the weapon
		    fightScript = character.GetComponent<NPCFighting>();
		    if(fightScript)
		    {
			fightScript.maxDistance = defaultMaxFightDist;
			fightScript.minDistance = defaultMinFightDist;
			fightScript.meleeDistance = defaultMeleeFightDist;
		    }
		    zombScript = character.GetComponent<Zombie>();
		    if(zombScript)
		    {
			zombScript.maxDistance = defaultMaxFightDist;
			zombScript.meleeDistance = defaultMinFightDist;
		    }
		

		    // If there is an armor equipped
		    if(charInvScript.armor)
		    {
			armScript = charInvScript.armor.GetComponent<Armor>();

			// Set character's armor sprite to that of the armor, based on No weapon (melee)
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
		    }
		    // No armor equiped
		    else
		    {
			// Set character's armor sprite to the default clothes, based on NO Weapon (melee)
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		    }
		}
		if(playAttack)
		{
		    // Set player's attack stats to those of the weapon
		    playAttack.attackDistance = 2.5f;
		    playAttack.damageMin = 0;
		    playAttack.damageMax = 1;
		}
	    }



	    // Change the name of the character, unless it is the Necromancer
	    if(character != player)
		character.name = "NPC w " + invScript.inventory[index + slot].name;
	    
	    // Add the Weapon to the Equiped Weapon slot of the character
	    charInvScript.weapon = invScript.inventory[index + slot];

	    // Get reference to item's Weapon script
	    weapScript = invScript.inventory[index + slot].GetComponent<Weapon>();

	    Debug.Log(invScript.inventory[index + slot].GetComponent<Weapon>());
	    
	    // Get reference to charater's stats
	    charAttack = character.GetComponent<Attack>();
	    playAttack = character.GetComponent<PlayerAttack>();
	    charArchAttack = character.GetComponent<ArcherAttack>();
	    statScript = character.GetComponent<Stats>();
	    
	    animators = character.GetComponent<Health>().animators;

	    // Set the block chance to that of the weapon
	    statScript.block += weapScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= weapScript.walkSpeedLess;
	    statScript.runSpeed -= weapScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power += weapScript.power;
	    
	    // Run any special script, this weapon may have, on the character
	    weapScript.Equip(character);

	    // Set archer attack distance to the max distance of the weapon
	    if(charArchAttack)
	    {
		charArchAttack.shootDistance = weapScript.maxDistance;

		// If weapon is a bow, adjust the pitch of the archer attack script to the pitch for a bow of that power
		//if(weapScript.type == "bow")
		//{
		//   Bow bowScript = invScript.inventory[index + slot].GetComponent<Bow>();
		//   charArchAttack.pitchAdjustment = bowScript.pitchAdjustment;
		//}
	    }
	    // Multiply character's attack animation speed by that of the weapon
	    // Set animator controller's to those of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * weapScript.attackSpeed);
	    }

	    
	    //Change the character's Stats to those of the weapon
	    if(charAttack)
	    {
		// Set player's attack stats to those of the weapon
		charAttack.attackDistance = weapScript.attackDistance;
		charAttack.damageMin = weapScript.damageMin;
		charAttack.damageMax = weapScript.damageMax;

		// Set character's weapon sprite to that of the weapon
		animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = weapScript.npcWeapSpriteSheet;
		

		// Change the distances the character keeps during fights to those of the weapon
		fightScript = character.GetComponent<NPCFighting>();
		if(fightScript)
		{
		    fightScript.maxDistance = weapScript.maxDistance;
		    fightScript.minDistance = weapScript.minDistance;
		    fightScript.meleeDistance = weapScript.meleeDistance;
		}

		// Change the distances the character keeps during fights to those of the weapon
		zombScript = character.GetComponent<Zombie>();
		if(zombScript)
		{
		    zombScript.maxDistance = weapScript.maxDistance;
		    zombScript.meleeDistance = weapScript.meleeDistance;
		}

		
		// If there is an armor equiped
		if(charInvScript.armor)
		{
		    // Reference the armor script on the item
		    armScript = charInvScript.armor.GetComponent<Armor>();

		    // Set character's armor sprite to that of the armor, based on weapon
		    if(weapScript.type == "melee")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombMeleeArmSpriteSheet;
		    }
		    else if(weapScript.type == "bow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombArcherArmSpriteSheet;
		    }
		    else if(weapScript.type == "firebow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcFireArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombFireArcherArmSpriteSheet;
		    }
		}
		// No armor equiped
		else
		{
		    // Set character's armor sprite to the default clothes, based on weapon
		    if(weapScript.type == "melee")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		    else if(weapScript.type == "bow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcArcherArmSpriteSheet;
		    else if(weapScript.type == "firebow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcFireArcherArmSpriteSheet;		    
		}
	    }
	    if(playAttack)
	    {
		// Set player's attack stats to those of the weapon
		playAttack.attackDistance = weapScript.attackDistance;
		playAttack.damageMin = weapScript.damageMin;
		playAttack.damageMax = weapScript.damageMax;
		
		// Set player's weapon sprite to that of the weapon
		animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = weapScript.playerWeapSpriteSheet;
	    }
	}



	// If item is shield
	else if(invScript.inventory[index + slot].tag == "Shield")
	{
	    Shield shieldScript;
	    Stats statScript;
	    List<Animator> animators;
	    
	    // Unequip current shield
	    // Make sure a shield is currently equiped
	    if(charInvScript.shield)
	    {
		// Add the currently equiped weapon to the inventory list
		charInvScript.inventory.Add(charInvScript.shield);

		// Get reference to item's Talisman script
		shieldScript = charInvScript.shield.GetComponent<Shield>();

		// Get reference to charater's stats
		statScript = character.GetComponent<Stats>();
	    
		animators = character.GetComponent<Health>().animators;

		// Multiply character's attack animation speed by that of the weapon
		foreach(Animator anim in animators)
		{
		    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / shieldScript.attackSpeed);
		    anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") / shieldScript.attackSpeed);
		}

	    
		// Set the block chance to that of the weapon
		statScript.block -= shieldScript.block;
		// Set walk and run speeds less amount this weapon hinders movement
		statScript.walkSpeed += shieldScript.walkSpeed;
		statScript.runSpeed += shieldScript.runSpeed;
		// Adjust the character's power level
		statScript.power -= shieldScript.power;
		
		// Run any special script, this weapon may have, on the character
		shieldScript.UnEquip(character);

		// Turn off weapon sprite for player
		animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";
	    }


	    // Equip new Shield
	    
	    // Add the Shield to the Equiped Shield slot of the character
	    charInvScript.shield = invScript.inventory[index + slot];

	    // Get reference to item's Shield script
	    shieldScript = invScript.inventory[index + slot].GetComponent<Shield>();

	    Debug.Log(invScript.inventory[index + slot].GetComponent<Shield>());
	    
	    // Get reference to charater's stats
	    statScript = character.GetComponent<Stats>();
	    
	    animators = character.GetComponent<Health>().animators;

	    // Multiply character's attack animation speed by that of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * shieldScript.attackSpeed);
		anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") * shieldScript.attackSpeed);
	    }

	    
	    // Set the block chance to that of the weapon
	    statScript.block += shieldScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= shieldScript.walkSpeed;
	    statScript.runSpeed -= shieldScript.runSpeed;
	    // Adjust the character's power level
	    statScript.power += shieldScript.power;
		
	    // Run any special script, this weapon may have, on the character
	    shieldScript.Equip(character);

	    // Turn on weapon sprite for player or NPC based on script they have
	    Attack charAttack = character.GetComponent<Attack>();
	    PlayerAttack playAttack = character.GetComponent<PlayerAttack>();

	    if(charAttack)
		animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = shieldScript.npcShieldSprite;		
	    if(playAttack)
		animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = shieldScript.playerShieldSprite;
	}

	
	
	
	// If item is Armor
	else if(invScript.inventory[index + slot].tag == "Armor")
	{
	    Armor armScript;
	    Attack charAttack;
	    PlayerAttack playAttack;
	    Health healthScript;
	    Stats statScript;
	    List<Animator> animators;
	    Weapon weapScript;
		    
	    // Try to unequip anything that may be currently equiped
	    // Make sure a weapon is currently equiped
	    if(charInvScript.armor)
	    {
		// Reference the armor script on the item
		armScript = charInvScript.armor.GetComponent<Armor>();
	    
		// Reference attack script on character (just to check if player or npc)
		charAttack = character.GetComponent<Attack>();
		playAttack = character.GetComponent<PlayerAttack>();

		// Reference character's health script to change armor amount
		healthScript = character.GetComponent<Health>();

		// Refernce character's Stats script to change walk and run speeds
		statScript = character.GetComponent<Stats>();
		    
		// Reference character's animators to 
		animators = healthScript.animators;


		// Multiply character's attack animation speed by that of the weapon
		// Set animator controller's to those of the weapon
		foreach(Animator anim in animators)
		{
		    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / armScript.attackSpeedMultiplier);
		    anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") / armScript.attackSpeedMultiplier);
		}
	    

		// Set player's Health Armor Stat to normal
		healthScript.armor -= armScript.armorAmount;
		
		// Set walk and run speeds less amount this weapon hinders movement
		statScript.walkSpeed += armScript.walkSpeedLess;
		statScript.runSpeed += armScript.runSpeedLess;
		// Adjust the character's power level
		statScript.power -= armScript.power;

		// Run any special unequip script on the armor being unequipped
		armScript.UnEquip(character);
		
		if(charAttack)
		{
		    // Get reference to item's Weapon script
		    weapScript = charInvScript.weapon.GetComponent<Weapon>();

		    // Set character's armor sprite to the default, based on weapon
		    if(weapScript.type == "melee")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		    else if(weapScript.type == "bow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcArcherArmSpriteSheet;
		    else if(weapScript.type == "firebow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcFireArcherArmSpriteSheet;
		}
		if(playAttack)
		{
		    // Set player's weapon sprite to that of the weapon
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "clothes";
		}
	    
		// Add the currently equiped armor to the inventory list
		invScript.inventory.Add(charInvScript.armor);
	    }
	    
	    // Add the Armor to the Equiped Armor slot of the character
	    charInvScript.armor = invScript.inventory[index + slot];


	    // Reference the armor script on the item
	    armScript = invScript.inventory[index + slot].GetComponent<Armor>();
	    
	    // Reference attack script on character (just to check if player or npc)
	    charAttack = character.GetComponent<Attack>();
	    playAttack = character.GetComponent<PlayerAttack>();

	    // Reference character's health script to change armor amount
	    healthScript = character.GetComponent<Health>();

	    // Refernce character's Stats script to change walk and run speeds
	    statScript = character.GetComponent<Stats>();
		    
	    // Reference character's animators to 
	    animators = healthScript.animators;


	    // Multiply character's attack animation speed by that of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * armScript.attackSpeedMultiplier);
		anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") * armScript.attackSpeedMultiplier);
	    }

	    // Set character's Health Armor Stat to that of this Armor
	    healthScript.armor += armScript.armorAmount;
		
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= armScript.walkSpeedLess;
	    statScript.runSpeed -= armScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power += armScript.power;

	    // Run any special script, this armor may have, on the character
	    armScript.Equip(character);
	    
	    //Change the character's Stats to those of the armor
	    if(charAttack)
	    {		
		// If a weapon is equipped to character
		if(charInvScript.weapon)
		{
		    // Get reference to character's Weapon script
		    weapScript = charInvScript.weapon.GetComponent<Weapon>();

		    Debug.Log(weapScript.type);
		    
		    // Set character's armor sprite to that of the armor, based on weapon
		    if(weapScript.type == "melee")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombMeleeArmSpriteSheet;
		    }
		    else if(weapScript.type == "bow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombArcherArmSpriteSheet;
		    }
		    else if(weapScript.type == "firebow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcFireArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombFireArcherArmSpriteSheet;
		    }
		    Debug.Log(animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName);
		}
		// If character has no weapon equipped
		else
		{
		    // Use melee version of armor
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
		    Debug.Log(animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName);
		}
	    }
	    if(playAttack)
	    {		
		// Set player's weapon sprite to that of the weapon
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.playerArmSpriteSheet;
	    }
	}



	// If item is a Talisman
	else if(invScript.inventory[index + slot].tag == "Talisman")
	{
	    Talisman talScript;
	    
	    // Try to unequip anything that may be currently equiped
	    // Make sure a weapon is currently equiped
	    if(charInvScript.talisman)
	    {
		// Add the currently equiped weapon to the inventory list
		charInvScript.inventory.Add(charInvScript.talisman);

		// Get reference to item's Talisman script
		talScript = charInvScript.talisman.GetComponent<Talisman>();
	    
		// Run the unequip method from the Talisman Script
		talScript.UnEquip(character);
	    }
	    
	    // Add the Talisman to the Equiped Talisman slot of the character
	    charInvScript.talisman = invScript.inventory[index + slot];

	    // Get reference to item's Talisman script
	    talScript = invScript.inventory[index + slot].GetComponent<Talisman>();

	    Debug.Log(talScript);
	    
	    // Run the equip method from the Talisman Script
	    talScript.Equip(character);
	}

	// If it is consumable
	if(invScript.inventory[index + slot].tag == "Consumable")
	{
	    Consumable itemScript = invScript.inventory[index + slot].GetComponent<Consumable>();
	    
	    // Use the item on the current character
	    itemScript.Use(character);
	    
	    // If it is a consumable and was just used up
	    if(itemScript.uses < 1)
	    {
		// Remove item from inventory
		invScript.inventory.Remove(invScript.inventory[index + slot]);

		// Destroy the item
		Destroy(itemScript.gameObject);
	    }
	}
	// If it is not consumable
	else
	{
	    // Remove the item from the inventory list
	    invScript.inventory.Remove(invScript.inventory[index + slot]);

	}

	
	// Update inventory and equipment displays to reflect changes
	UpdateInventory();
	UpdateEquipment();
    }




    
    public void DropThis(int slot)
    {
	// Turn the item's object back on (make it visible and effected by physics again
	invScript.inventory[index + slot].SetActive(true);
	
	// Deparent the item from the character
	invScript.inventory[index + slot].transform.parent = null;
	
	// Put the item in front of the character
	invScript.inventory[index + slot].transform.position = character.transform.position + character.transform.forward * 1.5f;
	
	// Remove the item from the inventory list
	invScript.inventory.Remove(invScript.inventory[slot]);

	// Update inventory and equipment displays to reflect changes
	UpdateInventory();
	UpdateEquipment();
    }



    

    public void LookAtThis(int slot)
    {
	// Turn on the text display
	textDisplay.SetActive(true);
	
	// Give the text display the text from the hovered object
	textBox.text = invScript.inventory[index + slot].GetComponentInChildren<Text>().text;
		
	// Update text box size
	Canvas.ForceUpdateCanvases();

	// Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
	textDisplay.SetActive(false);

	// Turn on the text display
	textDisplay.SetActive(true);
    }





    
    public void UnEquipWeapon()
    {
	Debug.Log(character);
	
	// Make sure a weapon is currently equiped
	if(charInvScript.weapon)
	{
	    // Add the currently equiped weapon to the inventory list
	    invScript.inventory.Add(charInvScript.weapon);

	    // Get reference to item's Weapon script
	    Weapon weapScript = charInvScript.weapon.GetComponent<Weapon>();
	    
	    // Get reference to charater's stats
	    Attack charAttack = character.GetComponent<Attack>();
	    PlayerAttack playAttack = character.GetComponent<PlayerAttack>();
	    ArcherAttack charArchAttack = character.GetComponent<ArcherAttack>();
	    Stats statScript = character.GetComponent<Stats>();
	    
	    List<Animator> animators = character.GetComponent<Health>().animators;

	    // Change name of character, unless it is Necromancer
	    if(character != player)
		character.name = "NPC";
	    
	    // Set the block chance to that of the weapon
	    statScript.block -= weapScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed += weapScript.walkSpeedLess;
	    statScript.runSpeed += weapScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power -= weapScript.power;

	    
	    // Run any special script this weapon may have
	    weapScript.UnEquip(character);
		

	    Debug.Log(weapScript.attackSpeed);
	    
	    // Divide animators speed by the attackspeed of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / weapScript.attackSpeed);
	    }
	    
	    // Set character's weapon sprite as empty
	    animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";
	    
	    //Change the character's Stats to those of the weapon
	    if(charAttack)
	    {
		// Set player's attack stats to those of the weapon
		charAttack.attackDistance = 2.5f;
		charAttack.damageMin = 0;
		charAttack.damageMax = 1;

		// Change the distances the character keeps during fights to those of the weapon
		NPCFighting fightScript = character.GetComponent<NPCFighting>();
		if(fightScript)
		{
		    fightScript.maxDistance = defaultMaxFightDist;
		    fightScript.minDistance = defaultMinFightDist;
		    fightScript.meleeDistance = defaultMeleeFightDist;
		}
		Zombie zombScript = character.GetComponent<Zombie>();
		if(zombScript)
		{
		    zombScript.maxDistance = defaultMaxFightDist;
		    zombScript.meleeDistance = defaultMinFightDist;
		}
		
		Armor armScript;
		
		// If there is an armor script on the character
		if(charInvScript.armor)
		{
		    armScript = charInvScript.armor.GetComponent<Armor>();

		    // Set character's armor sprite to that of the armor, based on No weapon (melee)
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
		}
		// No armor equiped
		else
		{
		    // Set character's armor sprite to the default clothes, based on NO Weapon (melee)
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		}
	    }
	    if(playAttack)
	    {
		// Set player's attack stats to those of the weapon
		playAttack.attackDistance = 2.5f;
		playAttack.damageMin = 0;
		playAttack.damageMax = 1;
	    }
	    
	    // Remove the currently equiped weapon from the equiped weapon slot
	    charInvScript.weapon = null;

	    // Update inventory and equipment displays to reflect changes
	    UpdateInventory();
	    UpdateEquipment();
	}
    }


    
    public void UnEquipArmor()
    {
	// Make sure a weapon is currently equiped
	if(charInvScript.armor)
	{
	    // Reference the armor script on the item
	    Armor armScript = charInvScript.armor.GetComponent<Armor>();
	    
	    // Reference attack script on character (just to check if player or npc)
	    Attack charAttack = character.GetComponent<Attack>();
	    PlayerAttack playAttack = character.GetComponent<PlayerAttack>();

	    // Reference character's health script to change armor amount
	    Health healthScript = character.GetComponent<Health>();

	    // Refernce character's Stats script to change walk and run speeds
	    Stats statScript = character.GetComponent<Stats>();
		    
	    // Reference character's animators to 
	    List<Animator> animators = healthScript.animators;


	    // Multiply character's attack animation speed by that of the weapon
	    // Set animator controller's to those of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / armScript.attackSpeedMultiplier);
	    }
	    

	    // Set player's Health Armor Stat to normal
	    healthScript.armor -= armScript.armorAmount;
		
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed += armScript.walkSpeedLess;
	    statScript.runSpeed += armScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power -= armScript.power;

	    
	    if(charAttack)
	    {
		// Get reference to item's Weapon script
		Weapon weapScript = charInvScript.weapon.GetComponent<Weapon>();

		// Set character's armor sprite to the default, based on weapon
		if(weapScript.type == "melee")
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		else if(weapScript.type == "bow")
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcArcherArmSpriteSheet;
		else if(weapScript.type == "firebow")
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcFireArcherArmSpriteSheet;
	    }
	    if(playAttack)
	    {
		// Set player's weapon sprite to that of the weapon
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "clothes";
	    }
	    
	    // Add the currently equiped weapon to the inventory list
	    invScript.inventory.Add(charInvScript.armor);
	    
	    // Remove the currently equiped weapon from the equiped weapon slot
	    charInvScript.armor = null;

	    // Update inventory and equipment displays to reflect changes
	    UpdateInventory();
	    UpdateEquipment();
	}
    }


    
    public void UnEquipTalisman()
    {
	// Make sure a weapon is currently equiped
	if(charInvScript.talisman)
	{
	    // Add the currently equiped weapon to the inventory list
	    charInvScript.inventory.Add(charInvScript.talisman);

	    // Get reference to item's Talisman script
	    Talisman talScript = charInvScript.talisman.GetComponent<Talisman>();
	    
	    // Run the unequip method from the Talisman Script
	    talScript.UnEquip(character);

	    // Remove the currently equiped weapon from the equiped weapon slot
	    charInvScript.talisman = null;

	    // Update inventory and equipment displays to reflect changes
	    UpdateInventory();
	    UpdateEquipment();
	}
    }




    public void UnEquipShield()
    {
	// Make sure a weapon is currently equiped
	if(charInvScript.shield)
	{
	    // Add the currently equiped weapon to the inventory list
	    charInvScript.inventory.Add(charInvScript.shield);

	    // Get reference to item's Talisman script
	    Shield shieldScript = charInvScript.shield.GetComponent<Shield>();

	    // Get reference to charater's stats
	    Stats statScript = character.GetComponent<Stats>();
	    
	    List<Animator> animators = character.GetComponent<Health>().animators;

	    // Multiply character's attack animation speed by that of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / shieldScript.attackSpeed);
	    }

	    
	    // Set the block chance to that of the weapon
	    statScript.block -= shieldScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed += shieldScript.walkSpeed;
	    statScript.runSpeed += shieldScript.runSpeed;
	    // Adjust the character's power level
	    statScript.power -= shieldScript.power;

	    
	    // Run any special script, this weapon may have, on the character
	    shieldScript.UnEquip(character);

	    // Turn off shield sprite for player
	    animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";
	    
	    // Remove the currently equiped weapon from the equiped weapon slot
	    charInvScript.shield = null;

	    // Update inventory and equipment displays to reflect changes
	    UpdateInventory();
	    UpdateEquipment();
	}
    }


    


    
    public void DropWeapon()
    {
	// Get reference to item's Weapon script
	Weapon weapScript = charInvScript.weapon.GetComponent<Weapon>();
	    
	// Get reference to charater's stats
	Attack charAttack = character.GetComponent<Attack>();
	PlayerAttack playAttack = character.GetComponent<PlayerAttack>();
	ArcherAttack charArchAttack = character.GetComponent<ArcherAttack>();
	Stats statScript = character.GetComponent<Stats>();
	
	List<Animator> animators = character.GetComponent<Health>().animators;

	// Change name of character, unless it is Necromancer
	if(character != player)
	    character.name = "NPC";
    
	// Set the block chance to not that of the weapon
	statScript.block -= weapScript.block;
	// Set walk and run speeds less amount this weapon hinders movement
	statScript.walkSpeed += weapScript.walkSpeedLess;
	statScript.runSpeed += weapScript.runSpeedLess;
	// Adjust the character's power level
	statScript.power -= weapScript.power;

	
	// Run any special script this weapon may have
	weapScript.UnEquip(character);
		
	// Turn off character's weapon sprite
	animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";

	// Divide animators speed by the attackspeed of the weapon
	foreach(Animator anim in animators)
	{
	    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / weapScript.attackSpeed);
	}

	//Change the character's Stats to the defaults
	if(charAttack)
	{
	    // Set player's attack stats to those of the weapon
	    charAttack.attackDistance = 3f;
	    charAttack.damageMin = 1;
	    charAttack.damageMax = 2;
	    
	    // Change the distances the character keeps during fights to those of the weapon
	    NPCFighting fightScript = character.GetComponent<NPCFighting>();
	    if(fightScript)
	    {
		fightScript.maxDistance = defaultMaxFightDist;
		fightScript.minDistance = defaultMinFightDist;
		fightScript.meleeDistance = defaultMeleeFightDist;
	    }
	    Zombie zombScript = character.GetComponent<Zombie>();
	    if(zombScript)
	    {
		zombScript.maxDistance = defaultMaxFightDist;
		zombScript.meleeDistance = defaultMinFightDist;
	    }

	    Armor armScript;
	    
	    // Reference the armor script on the character
	    if(charInvScript.armor)
	    {
		armScript = charInvScript.armor.GetComponent<Armor>();

		// Set character's armor sprite to that of the armor, based on No weapon (melee)
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
	    }
	    // No armor equiped
	    else
	    {
		// Set character's armor sprite to the default clothes, based on NO Weapon (melee)
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
	    }
	}
	if(playAttack)
	{
	    // Set player's attack stats to those of the weapon
	    playAttack.attackDistance = 2.5f;
	    playAttack.damageMin = 0;
	    playAttack.damageMax = 1;
	}

	charInvScript.weapon.SetActive(true);
	
	// Deparent the item from the character
	charInvScript.weapon.transform.parent = null;

	// Put the item in front of the character
	charInvScript.weapon.transform.position = character.transform.position + character.transform.forward * 1.5f;

		    
	// Remove the item from the weapon slot
	charInvScript.weapon = null;
	    
	// Update inventory and equipment displays to reflect changes
	UpdateInventory();
	UpdateEquipment();
    }

    public void DropArmor()
    {
	charInvScript.armor.SetActive(true);
	
	// Deparent the item from the character
	charInvScript.armor.transform.parent = null;

	// Put the item in front of the character
	charInvScript.armor.transform.position = character.transform.position + character.transform.forward * 1.5f;    


	// Reference the armor script on the item
	Armor armScript = charInvScript.armor.GetComponent<Armor>();
	    
	// Reference attack script on character (just to check if player or npc)
	Attack charAttack = character.GetComponent<Attack>();
	PlayerAttack playAttack = character.GetComponent<PlayerAttack>();

	// Reference character's health script to change armor amount
	Health healthScript = character.GetComponent<Health>();

	// Refernce character's Stats script to change walk and run speeds
	Stats statScript = character.GetComponent<Stats>();
		    
	// Reference character's animators to 
	List<Animator> animators = healthScript.animators;


	// Multiply character's attack animation speed by that of the weapon
	// Set animator controller's to those of the weapon
	foreach(Animator anim in animators)
	{
	    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / armScript.attackSpeedMultiplier);
	}
	    

	// Set player's Health Armor Stat to normal
	healthScript.armor -= armScript.armorAmount;
		
	// Set walk and run speeds less amount this weapon hinders movement
	statScript.walkSpeed += armScript.walkSpeedLess;
	statScript.runSpeed += armScript.runSpeedLess;
	statScript.power -= armScript.power;

	if(charAttack)
	{
	    // Get reference to item's Weapon script
	    Weapon weapScript = charInvScript.weapon.GetComponent<Weapon>();

	    // Set character's armor sprite to the default, based on weapon
	    if(weapScript.type == "melee")
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
	    else if(weapScript.type == "bow")
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcArcherArmSpriteSheet;
	    else if(weapScript.type == "firebow")
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcFireArcherArmSpriteSheet;
	}
	if(playAttack)
	{
	    // Set player's weapon sprite to that of the weapon
	    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "clothes";
	}
	    	    	
	// Remove the item from the armor slot
	charInvScript.armor = null;
	
	// Update inventory and equipment displays to reflect changes
	UpdateInventory();
	UpdateEquipment();
    }

    public void DropTalisman()
    {
	charInvScript.talisman.SetActive(true);
	
	// Deparent the item from the character
	charInvScript.talisman.transform.parent = null;

	// Put the item in front of the character
	charInvScript.talisman.transform.position = character.transform.position + character.transform.forward * 1.5f;

	// Turn on items rigidbody
	charInvScript.talisman.GetComponent<Rigidbody>().detectCollisions = true;
	charInvScript.talisman.GetComponent<Rigidbody>().useGravity = true;

	// Get reference to item's Talisman script
	Talisman talScript = charInvScript.talisman.GetComponent<Talisman>();
	    
	// Run the unequip method from the Talisman Script
	talScript.UnEquip(character);

	// Remove the item from the talisman slot
	charInvScript.talisman = null;

	// Update inventory and equipment displays to reflect changes
	UpdateInventory();
	UpdateEquipment();
    }

    public void DropShield()
    {
	charInvScript.shield.SetActive(true);
	
	// Deparent the item from the character
	charInvScript.shield.transform.parent = null;

	// Put the item in front of the character
	charInvScript.shield.transform.position = character.transform.position + character.transform.forward * 1.5f;

	// Turn on items rigidbody
	charInvScript.shield.GetComponent<Rigidbody>().detectCollisions = true;
	charInvScript.shield.GetComponent<Rigidbody>().useGravity = true;

	
	// Get reference to item's Talisman script
	Shield shieldScript = charInvScript.talisman.GetComponent<Shield>();

	// Get reference to charater's stats
	Stats statScript = character.GetComponent<Stats>();
	    
	List<Animator> animators = character.GetComponent<Health>().animators;

	// Multiply character's attack animation speed by that of the weapon
	foreach(Animator anim in animators)
	{
	    anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / shieldScript.attackSpeed);
	}

	    
	// Set the block chance to that of the weapon
	statScript.block -= shieldScript.block;
	// Set walk and run speeds less amount this weapon hinders movement
	statScript.walkSpeed += shieldScript.walkSpeed;
	statScript.runSpeed += shieldScript.runSpeed;
	statScript.power -= shieldScript.power;
	
	// Run any special script, this weapon may have, on the character
	shieldScript.UnEquip(character);

	// Turn off shield sprite for player
	animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";
    }




    


    
    public void LookAtWeapon()
    {
	// Turn on the text display
	textDisplay.SetActive(true);
	
	// Give the text display the text from the hovered object
	textBox.text = charInvScript.weapon.GetComponentInChildren<Text>().text;
		
	// Update text box size
	Canvas.ForceUpdateCanvases();

	// Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
	textDisplay.SetActive(false);

	// Turn on the text display
	textDisplay.SetActive(true);
    }

    public void LookAtArmor()
    {
	// Turn on the text display
	textDisplay.SetActive(true);
	
	// Give the text display the text from the hovered object
	textBox.text = charInvScript.armor.GetComponentInChildren<Text>().text;
		
	// Update text box size
	Canvas.ForceUpdateCanvases();

	// Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
	textDisplay.SetActive(false);

	// Turn on the text display
	textDisplay.SetActive(true);
    }


    public void LookAtTalisman()
    {
	// Turn on the text display
	textDisplay.SetActive(true);
	
	// Give the text display the text from the hovered object
	textBox.text = charInvScript.talisman.GetComponentInChildren<Text>().text;
		
	// Update text box size
	Canvas.ForceUpdateCanvases();

	// Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
	textDisplay.SetActive(false);

	// Turn on the text display
	textDisplay.SetActive(true);
    }

    public void LookAtShield()
    {
	// Turn on the text display
	textDisplay.SetActive(true);
	
	// Give the text display the text from the hovered object
	textBox.text = charInvScript.shield.GetComponentInChildren<Text>().text;
		
	// Update text box size
	Canvas.ForceUpdateCanvases();

	// Turn off the text display (The UI system is really dumb, have to turn it off and on again to update the text box to the size of the new text...)
	textDisplay.SetActive(false);

	// Turn on the text display
	textDisplay.SetActive(true);
    }
}
