﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Reference to gameobject to follow
    public Transform follow;

    // How close to sceen edge makes screen start to move
    public int screenEdge;
    
    // Camera move speed
    public float camSpeed;

    // Adjust screen position speed
    public float adjustSpeed;
    
    // Adjust screen position speed
    public float scrollSpeed;

    //Camera Y height
    public float camYHeight;

    //Amount beyond the follow x coordinate to follow
    public float plusX = 0f;

    //Amount beyond the follow z coordinate to follow
    public float plusZ = 0f;
    
    //Dimensions of screen resolution
    private int screenWidth;
    private int screenHeight;

    //Minimum and maximum x movement
    public float minX;
    public float maxX;

    //Minimum and maximum z movement
    public float minZ;
    public float maxZ;

    //Minimum and maximum y movement
    public int minY;
    public int maxY;

    // Min and max values for Orthographic zoom levels
    public float maxZoom;
    public float minZoom;
    
    // Reference to our Camera
    public Camera cam;


    // Called once at start
    void Start()
    {
	screenWidth = Screen.width;
	screenHeight = Screen.height;
    }


    
    // Update is called once per frame
    void Update()
    {		  
	// If mouse is moved to right edge of screen
	if(Input.mousePosition.x > screenWidth - screenEdge)
	    plusX += Time.deltaTime * adjustSpeed;

	// If mouse is moved to left edge of screen
	if(Input.mousePosition.x < screenEdge)
	    plusX -= Time.deltaTime * adjustSpeed;
	
	// If mouse is moved to top edge of screen
	if(Input.mousePosition.y > screenHeight - screenEdge)
	    plusZ += Time.deltaTime * adjustSpeed;

	// If mouse is moved to bottom edge of screen
	if(Input.mousePosition.y < screenEdge)
	    plusZ -= Time.deltaTime * adjustSpeed;

	// When holding shift
	if(Input.GetKey(KeyCode.LeftShift))
	{
	    // If the camera is in Perspective mode
	    if(!cam.orthographic)
	    {
		// If Mouse Wheel scrolled down and camera not too far move Camera away
		if(camYHeight < maxY && Input.mouseScrollDelta.y < 0)
		{
		    camYHeight -= Input.mouseScrollDelta.y * scrollSpeed;
		    plusZ += Input.mouseScrollDelta.y * scrollSpeed;
		}
		// If Mouse Wheel scrolled up and camera not too close move Camera toward
		if(camYHeight > minY && Input.mouseScrollDelta.y > 0)
		{
		    camYHeight -= Input.mouseScrollDelta.y * scrollSpeed;
		    plusZ += Input.mouseScrollDelta.y * scrollSpeed;
		}
	    }
	    // If camera is in Orthographic mode
	    else
	    {
		// If Mouse Wheel scrolled Down, increase Camera size (move further away)
		if(cam.orthographicSize < maxZoom && Input.mouseScrollDelta.y < 0)
		    cam.orthographicSize -= Input.mouseScrollDelta.y * scrollSpeed;

		// If Mouse Wheel scrolled Up, decrease Camera size (move closer)
		if(cam.orthographicSize > minZoom && Input.mouseScrollDelta.y > 0)
		    cam.orthographicSize -= Input.mouseScrollDelta.y * scrollSpeed;
	    }
	}

	// If key 'P' pressed, switch between Orthographic and Perspective and back
	if(Input.GetKeyDown(KeyCode.P))
	    cam.orthographic = !cam.orthographic;

	// Clip all positions to their assigned limits
	if(plusX < minX * camYHeight)
	    plusX = minX * camYHeight;
	if(plusX > maxX * camYHeight)
	    plusX = maxX * camYHeight;
	if(plusZ < minZ * camYHeight)
	    plusZ = minZ * camYHeight;
	if(plusZ > maxZ * camYHeight)
	    plusZ = maxZ * camYHeight;
	if(camYHeight < minY)
	    camYHeight = minY;
	if(camYHeight > maxY)
	    camYHeight = maxY;

	// If we have a character to floow
	if(follow)
	{
	    // Move camera to the position it should be at
	    transform.position = Vector3.Lerp(transform.position, new Vector3(follow.position.x + plusX, camYHeight, follow.position.z + plusZ), Time.deltaTime * camSpeed);
	}
    }
}
