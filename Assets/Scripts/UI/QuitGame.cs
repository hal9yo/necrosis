﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    //Function that quits the game
    public void Quit()
    {
	//Quit the application
	Application.Quit();
	//Debug message tells us it worked
	Debug.Log("Game is exiting");
    }
}
