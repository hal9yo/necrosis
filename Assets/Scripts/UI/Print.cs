﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Print : MonoBehaviour
{
    public float ttl;
    
    // Gameobjects containing sprites of the following:
    public GameObject zero;
    public GameObject one;
    public GameObject two;
    public GameObject three;
    public GameObject four;
    public GameObject five;
    public GameObject six;
    public GameObject seven;
    public GameObject eight;
    public GameObject nine;
    public GameObject block;
    public GameObject crit;
    public GameObject lvlUp;
    
    // Update is called once per frame
    void Update()
    {
 
    }
    
    public void PrintBlock()
    {
	Vector3 printPos = transform.position + new Vector3(0f, 1f, 0f);
	GameObject blockPrint = Instantiate(block, printPos, transform.rotation);
	blockPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
	Destroy(blockPrint, ttl);
    }

    public void PrintCrit()
    {
	Vector3 printPos = transform.position + new Vector3(0f, 1f, 0f);
	GameObject critPrint = Instantiate(crit, transform.position, transform.rotation);
	critPrint.GetComponent<Rigidbody>().AddForce(transform.up * 150);
	Destroy(critPrint, ttl + .3f);
    }

    public void PrintLvlUp()
    {
	Vector3 printPos = transform.position + new Vector3(0f, 4f, 0f);
	GameObject lvlPrint = Instantiate(lvlUp, transform.position, transform.rotation);
	lvlPrint.GetComponent<Rigidbody>().AddForce(transform.up * 50);
	Destroy(lvlPrint, ttl + .5f);
    }

    public void PrintDmg(int dmg)
    {
	int place = 1;

	// if we have a 3 digit number
	if(dmg > 99)
	{
	    // get hundreds place by dividing by 100
	    int hundreds = dmg/100;
	    
	    // print number from hundreds place in first place
	    PrintDigit(hundreds, 1);

	    // increase place so next printed digit is in right place
	    place = 2;

	    // remove hundreds place from number for tens and ones check
	    dmg -= hundreds * 100;
	}

	// if we have a 2 digit number (whether or not it was initially 3 digit)
	if(dmg > 9)
	{
	    // get tens place
	    int tens = dmg/10;

	    // print tens place
	    PrintDigit(tens, place);

	    // increase place so ones is printed over tens
	    place++;

	    // remove tens place
	    dmg -= tens * 10;

	    // if no number left, print 0 in ones place
	    if (dmg == 0)
	    {	       
		PrintDigit(0, place);
		return;
	    }
	}
	else
	{
	    // place is > 1 so we must have printed a hundreds place, if we aren't printing a digit in the tens place print a 0
	    if (place > 1)
	    {
		PrintDigit(0, 2);
		place ++;
	    }
	}
	// Always print ones place, if it is a single digit number print it, if it was a 2 or 3 digit number they would already have been printed, if it is zero print it
	    PrintDigit(dmg, place);
    }

    private void PrintDigit(int digit, int place)
    {
	Vector3 printPos = transform.position + new Vector3(0f, 1f, 0f);
	switch (place)
	{
	    case 1:
		printPos = transform.position + new Vector3(0f, 1f, 0f);
		break;
	    case 2:
		printPos = transform.position + new Vector3(.5f, 1f, 0f);
		break;
	    case 3:
		printPos = transform.position + new Vector3(1f, 1f, 0f);
		break;
	}
	switch (digit)
	{
	    case 0:
		GameObject zeroPrint = Instantiate(zero, printPos, transform.rotation);
		zeroPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(zeroPrint, ttl);
		break;
	    case 1:
		GameObject onePrint = Instantiate(one, printPos, transform.rotation);
		onePrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(onePrint, ttl);
		break;
	    case 2:
		GameObject twoPrint = Instantiate(two, printPos, transform.rotation);
		twoPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(twoPrint, ttl);
		break;
	    case 3:
		GameObject threePrint = Instantiate(three, printPos, transform.rotation);
		threePrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(threePrint, ttl);
		break;
	    case 4:
		GameObject fourPrint = Instantiate(four, printPos, transform.rotation);
		fourPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(fourPrint, ttl);
		break;
	    case 5:
		GameObject fivePrint = Instantiate(five, printPos, transform.rotation);
		fivePrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(fivePrint, ttl);
		break;
	    case 6:
		GameObject sixPrint = Instantiate(six, printPos, transform.rotation);
		sixPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(sixPrint, ttl);
		break;
	    case 7:
		GameObject sevenPrint = Instantiate(seven, printPos, transform.rotation);
		sevenPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(sevenPrint, ttl);
		break;
	    case 8:
		GameObject eightPrint = Instantiate(eight, printPos, transform.rotation);
		eightPrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(eightPrint, ttl);
		break;
	    case 9:
		GameObject ninePrint = Instantiate(nine, printPos, transform.rotation);
		ninePrint.GetComponent<Rigidbody>().AddForce(transform.up * 100);
		Destroy(ninePrint, ttl);
		break;
	}
    }
}
