﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    public float ttl = 3f;

    public GameObject image1;
    public GameObject image2;

    public Color color;
    public Camera cam;

    // Update is called once per frame
    void Update()
    {
	ttl -= Time.deltaTime;
	if(ttl <= 0f)
	    SceneManager.LoadScene("TitleScreen");
	if(ttl <= 1f)
	{
	    image1.SetActive(false);
	    cam.backgroundColor = color;
	    image2.SetActive(true);
	}
    }
}
