﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellIcons : MonoBehaviour
{
    public PlayerManager manager;

    public Texture2D earthOOR;
    public Texture2D banEarth;
    public Texture2D invEarth;
    public Texture2D banEarthTrans;
    public Texture2D invEarthTrans;

    public Texture2D waterOOR;
    public Texture2D banWater;
    public Texture2D invWater;
    public Texture2D banWaterTrans;
    public Texture2D invWaterTrans;

    public Texture2D airOOR;
    public Texture2D banAir;
    public Texture2D invAir;
    public Texture2D banAirTrans;
    public Texture2D invAirTrans;

    public Texture2D fireOOR;
    public Texture2D banFire;
    public Texture2D invFire;
    public Texture2D banFireTrans;
    public Texture2D invFireTrans;

    Vector2 hotSpot = new Vector2(16f, 16f);

    // The gui object for selecting spells
    public GameObject spellWheel;

    // The gui parts for each spell on the spell wheel
    public GameObject fireIcon;
    public GameObject airIcon;
    public GameObject waterIcon;
    public GameObject earthIcon;
    public Graphic fireRender;
    public Graphic airRender;
    public Graphic waterRender;
    public Graphic earthRender;

    // A material that makes the spell icons in the gui fully visible
    public Material visibleSprite;

    // A material that makes the spell icons in the gui transparent
    public Material transparentSprite;

    // This boolean remembers if we are invoking or banishing the given element we have selected
    public bool invoke;
    // Boolean that is true when the spell hovers over a target and can be used
    public bool hover;
    // Boolean that is true when the spell is out of range (too far from player to cast)
    public bool oor;

    float leftRight = 0;
    float upDown = 0;    
    
    void start()
    {
	Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
	Cursor.visible = true;
    }



    void Update()
    {
	// Turn on pentagram menu to select spell when middle mouse pressed
	if(Input.GetMouseButtonDown(2))
	{
	    // Reset the cursor to normal (if you middle click and don't move mouse go to normal cursor)
	    NormalCursor();

	    // Turn off the cursor while selecting
	    Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
	    Cursor.visible = false;

	    // Make the spell wheel visible
	    spellWheel.SetActive(true);

	    // Reset mouse movement variables
	    leftRight = 0f;
	    upDown = 0f;

	    // Make all spell selection sprites transparent
	    if(fireIcon.activeSelf)
		fireRender.material = transparentSprite;

	    if(airIcon.activeSelf)
		airRender.material = transparentSprite;

	    if(waterIcon.activeSelf)
		waterRender.material = transparentSprite;

	    if(earthIcon.activeSelf)
		earthRender.material = transparentSprite;
	}

	// Select spells by moving the mouse when middle mouse held down
	if(Input.GetMouseButton(2))
	{

	    leftRight = Input.GetAxisRaw("Mouse X");
	    upDown = Input.GetAxisRaw("Mouse Y");


	    if(leftRight > 0 && upDown < 0)
	    {
		if(fireIcon.activeSelf)
		    fireRender.material = visibleSprite;

		if(airIcon.activeSelf)
		    airRender.material = transparentSprite;

		if(waterIcon.activeSelf)
		    waterRender.material = transparentSprite;

		if(earthIcon.activeSelf)
		    earthRender.material = transparentSprite;

		Fire();
	    }
		    
	    if(leftRight < 0 && upDown > 0)
	    {
		if(fireIcon.activeSelf)
		    fireRender.material = transparentSprite;

		if(airIcon.activeSelf)
		    airRender.material = visibleSprite;

		if(waterIcon.activeSelf)
		    waterRender.material = transparentSprite;

		if(earthIcon.activeSelf)
		    earthRender.material = transparentSprite;

		Air();
	    }
	    
	    if(leftRight > 0 && upDown > 0)
	    {
		if(fireIcon.activeSelf)
		    fireRender.material = transparentSprite;

		if(airIcon.activeSelf)
		    airRender.material = transparentSprite;

		if(waterIcon.activeSelf)
		    waterRender.material = visibleSprite;

		if(earthIcon.activeSelf)
		    earthRender.material = transparentSprite;

		Water();
	    }

	    if(leftRight < 0 && upDown < 0)
	    {
		if(fireIcon.activeSelf)
		    fireRender.material = transparentSprite;

		if(airIcon.activeSelf)
		    airRender.material = transparentSprite;

		if(waterIcon.activeSelf)
		    waterRender.material = transparentSprite;

		if(earthIcon.activeSelf)
		    earthRender.material = visibleSprite;

		Earth();
	    }
	}

	// Turn off pentagram menu to select spell when middle mouse released
	if(Input.GetMouseButtonUp(2))
	{
	    Cursor.visible = true;
	    spellWheel.SetActive(false);
	}

	// If we have a spell selected and scroll the mousewheel, change invoke to banish or vice versa
	if(manager.spell > 0 && Input.mouseScrollDelta.y != 0)
	{
	    invoke = !invoke;

	    // Depending on which spell we have selected change the appropriate icon
	    if(manager.spell == 1)
	    {
		Fire();
	    }
	    if(manager.spell == 2)
	    {
		Air();
	    }
	    if(manager.spell == 3)
	    {
		Water();
	    }
	    if(manager.spell == 4)
	    {
		Earth();
	    }
	}
    }



    public void NormalCursor()
    {
	Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
	
	manager.spell = 0;
    }

    

    public void Earth()
    {
	if(oor)
	    EarthOOR();
	else
	{
	    if(invoke)
		InvEarth();
	    else
		BanEarth();
	}
	
	manager.spell = 4;
    }
    
    public void BanEarth()
    {
	// If we are hovering set fullbright icon, if not transparent icon
	if(hover)
	    Cursor.SetCursor(banEarth, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(banEarthTrans, hotSpot, CursorMode.ForceSoftware);
    }

    public void InvEarth()
    {
	if(hover)
	    Cursor.SetCursor(invEarth, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(invEarthTrans, hotSpot, CursorMode.ForceSoftware);
    }


    
    public void Water()
    {	if(oor)
	    WaterOOR();
	else
	{
	    if(invoke)
		InvWater();
	    else
		BanWater();
	}
	
	manager.spell = 3;
    }
    
    public void BanWater()
    {
	if(hover)
	    Cursor.SetCursor(banWater, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(banWaterTrans, hotSpot, CursorMode.ForceSoftware);
    }

    public void InvWater()
    {
	if(hover)
	    Cursor.SetCursor(invWater, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(invWaterTrans, hotSpot, CursorMode.ForceSoftware);
    }



    public void Air()
    {
	if(oor)
	    AirOOR();
	else
	{
	    if(invoke)
		InvAir();
	    else
		BanAir();
	}
	
	manager.spell = 2;
    }
	
    public void BanAir()
    {
	if(hover)
	    Cursor.SetCursor(banAir, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(banAirTrans, hotSpot, CursorMode.ForceSoftware);
    }

    public void InvAir()
    {
	if(hover)
	    Cursor.SetCursor(invAir, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(invAirTrans, hotSpot, CursorMode.ForceSoftware);
    }



    public void Fire()
    {
	if(oor)
	    FireOOR();
	else
	{
	    if(invoke)
		InvFire();
	    else
		BanFire();
	}
	
	manager.spell = 1;
    }
	
    public void BanFire()
    {
	if(hover)
	    Cursor.SetCursor(banFire, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(banFireTrans, hotSpot, CursorMode.ForceSoftware);
    }

    public void InvFire()
    {
	if(hover)
	    Cursor.SetCursor(invFire, hotSpot, CursorMode.ForceSoftware);
	else
	    Cursor.SetCursor(invFireTrans, hotSpot, CursorMode.ForceSoftware);
    }

    public void FireOOR()
    {
	Cursor.SetCursor(fireOOR, hotSpot, CursorMode.ForceSoftware);
    }
    public void AirOOR()
    {
	Cursor.SetCursor(airOOR, hotSpot, CursorMode.ForceSoftware);
    }
    public void WaterOOR()
    {
	Cursor.SetCursor(waterOOR, hotSpot, CursorMode.ForceSoftware);
    }
    public void EarthOOR()
    {
	Cursor.SetCursor(earthOOR, hotSpot, CursorMode.ForceSoftware);
    }
}

