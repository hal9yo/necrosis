﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBackgroundMusic : MonoBehaviour
{
    //Audio Source to play music from
    public AudioSource audioSource;
    
    //Array of music clips for this level
    public AudioClip[] musicFiles;


    
    // Start is called before the first frame update
    void Start()
    {
	//Get the audio source
	audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
	//If not already playing sound effect
	if(!audioSource.isPlaying)
	{
	    
	    //select a random music clip to play
	    audioSource.clip = musicFiles[Random.Range(0, musicFiles.Length)];
	    
	    //Play the sound
	    audioSource.Play();
	}
    }

    // When script is disabled
    void OnDisable()
    {
	//Stop the sound
	audioSource.Stop();
    }
}
