﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDisplay : MonoBehaviour
{
    // Reference to display text object to turn off when done
    public GameObject displayText;

    // Pause script used to pause the game while we are reading text
    public PauseGame pauseScript;

    // Wait a certain amount of time before deactiving script so it doesn't just turn off as soon as it is turned on
    float Ttl = 1f;

    
    
    
    // Update is called once per frame
    void Update()
    {
	//If text is displayed 
        if (displayText.activeSelf)
	{
	    //Stop time
	    pauseScript.textUp = true;

	    
	    // If mouse is clicked or space or enter
	    if(Ttl > 1f && (Input.GetKeyDown("escape") || Input.GetKeyDown("space") || Input.GetKeyDown("enter")))
	    {
		//Stop showing the display text box
		displayText.SetActive(false);

		//Set time to 1, time is normal speed
		Time.timeScale = 1f;

		// Reset the timer for next time
		Ttl = 0f;
	    }
	    else
		Ttl += Time.unscaledDeltaTime;
	}
    }
	    
    public void CloseText()
    {
	//Stop showing the display text box, keep time paused because we are going to another menu
	displayText.SetActive(false);

	//Set time to 1, time is normal speed
	pauseScript.textUp = false;

	// Reset the timer for next time
	Ttl = 0f;
    }
}
