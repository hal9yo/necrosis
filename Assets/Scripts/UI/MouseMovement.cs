﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    // Mouse sensitivity
    public float sensitivity;

    // How far the aimer can be from the player
    public float range;

    void Start()
    {
	 Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(Input.GetAxis("Mouse X"), 0f, Input.GetAxis("Mouse Y")) * sensitivity;
    }
}
