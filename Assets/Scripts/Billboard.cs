﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    // Allows sprite to rotate around the z axis
    [SerializeField]
    bool canRoll = false;
    
    // Update is called once per frame
    void LateUpdate()
    {
	
	if(!canRoll)
	{
	    transform.LookAt(Camera.main.transform);
	    transform.eulerAngles = new Vector3(45f, transform.rotation.y, 0f);
	}
	else
	    transform.eulerAngles = new Vector3(45f, transform.rotation.y, transform.rotation.z);
    }
}
