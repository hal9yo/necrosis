﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOffset : MonoBehaviour
{
    public AudioClip otherClip;
    AudioSource audioSource;
    float timer;

    public float timeToStart;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
	audioSource.pitch += Random.Range(-.2f, .2f);
    }

    void Update()
    {
        if (!audioSource.isPlaying)
        {
	    if(timer <= 0f)
	    {
		audioSource.clip = otherClip;
		audioSource.Play();
	    }
	    else
	    {
		timer -= Time.deltaTime;
	    }
        }
    }
}
