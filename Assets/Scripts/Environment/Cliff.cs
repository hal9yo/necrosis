﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cliff : MonoBehaviour
{
    // Keep a list of rigidbodies that are falling off the cliff (so we don't have to GetComponent the rigidbody every frame)
    public List<Rigidbody> fallingObjects;

    // How fast units fall down this cliff
    public float fallSpeed = 500f;



    
    // When object enters cliff face
    void OnTriggerEnter(Collider col)
    {
	// Add object's rigidbody to the list of falling rigidbodies
	fallingObjects.Add(col.GetComponent<Rigidbody>());
    }

    // When object leaves the cliff face
    void OnTriggerExit(Collider col)
    {
	// Remove rigidbody from the list of falling objects
	fallingObjects.Remove(col.GetComponent<Rigidbody>());
    }

    // Every frame
    void Update()
    {
	if(fallingObjects.Count > 0)
	{
	    // Go through the list of falling rigidbodies, if any
	    foreach(Rigidbody body in fallingObjects)
	    {
		// Increase the rigidbody's velocity down the cliff
		body.velocity += transform.forward * Time.deltaTime * fallSpeed;

		body.velocity += new Vector3(0f, 0f, -Time.deltaTime * fallSpeed);
	    }
	}
    }
}
