﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasmFall : MonoBehaviour
{
    // Keep a list of rigidbodies that are falling off the cliff (so we don't have to GetComponent the rigidbody every frame)
    public List<Rigidbody> fallingObjects;

    public List<Health> healthScripts;

    public List<float> fallTime;


    public float fallSpeed = 500f;


    // Every frame
    void Update()
    {
	if(fallingObjects.Count > 0)
	{
	    
	    // Go through the list of falling rigidbodies, if any
	    for(int i = 0; i < fallingObjects.Count; i++)
	    {
		// Make sure we don't have an empty entry
		if(!fallingObjects[i])
		{
		    fallingObjects.RemoveAt(i);
		    healthScripts.RemoveAt(i);
		    fallTime.RemoveAt(i);
		}
		else
		{
		    // Increase the rigidbody's velocity down the chasm
		    fallingObjects[i].velocity += new Vector3(0f, 0f, -50f);
		
		    
		    // Check how long the object has been falling
		    if(Time.time - fallTime[i] > 1f)
		    {
			// If there is a health script add 666 damage
			if(healthScripts[i] != null)
			    healthScripts[i].currHealth -= 666f;
			else
			    Destroy(fallingObjects[i]);
		    
			// Remove from all lists
			fallingObjects.RemoveAt(i);
			healthScripts.RemoveAt(i);
			fallTime.RemoveAt(i);
		    }
		}
	    }
	}
    }
}
