﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Water : MonoBehaviour
{
    // Reference to this level's tilemap (water level)
    public Tilemap tileMap;
    // Reference to this level's grid (water level)
    public GridLayout gridLayout;

    // The tilemap position of our water object
    Vector3Int cellPosition;

    // All the tiles we want to use for shallow water
    [SerializeField]
    Tile[] shallowTiles;
    // All the tiles we want to use for deeper water
    [SerializeField]
    Tile[] deepTiles;
    
    // All the tiles we want to use for deepest water
    [SerializeField]
    Tile[] deeperTiles;
    // All the tiles we want to use for deepest water
    [SerializeField]
    Tile[] deepestTiles;
    

    // Water tiles for edges
    [SerializeField]
    Tile[] waterLeft;
    [SerializeField]
    Tile[] waterRight;
    [SerializeField]
    Tile[] waterUp;
    [SerializeField]
    Tile[] waterDown;

    // Raycast to check for nearby water (tiles touching ours)
    RaycastHit hit;
    
    // LayerMask for Raycasts, so it only hits the Water layer (4)
    LayerMask waterMask;

    // LayerMask for Raycasts, so it only hits the Bridge layer (16)
    LayerMask bridgeMask;


    
    // Distance between water objects (distance between tiles on Tilemap)
    public float xOffset;
    public float zOffset;

    public MeshRenderer mesh;

    public BoxCollider boxCol;

    // Values for the depth at various points of the object
    public int centerDepth;
    public int neDepth;
    public int nDepth;
    public int nwDepth;
    public int wDepth;
    public int swDepth;
    public int sDepth;
    public int seDepth;
    public int eDepth;


    // Boolean that says when this water is created, have it spread to the tiles it touches
    public bool spread;

    // How big to make the water when generating
    public float size;

    // Prefab of water for when the water spreads it instantiates this prefab
    public GameObject waterPrefab;

    public UnityEngine.AI.NavMeshObstacle navMeshObstacle;

    // These bools true if water is found north, east, west, or south of this water object
    public bool n = false;
    public bool e = false;
    public bool w = false;
    public bool s = false;

    // These bools true if water is found NorthEast, NorthWest, SouthEast, SouthWest of this object
    public bool nE = false;
    public bool nW = false;
    public bool sE = false;
    public bool sW = false;

    // Tag to note when this water object is turned off or on
    public bool on;

    // Tag that denotes this water is part of the original level architecture (still make sprites "fall" into space even when off)
    public bool permanentWater;

    public bool debugOn = false;

    // Turn this tag on to update next frame
    public bool updateNextFrame = false;
	

    // Turn on for smaller water objects that can fill out small corners
    public bool smallWater;
	
    void OnEnable()
    {
       
	on = true;
	
	mesh.enabled = false;
	
	waterMask = LayerMask.GetMask("Water");

	bridgeMask = LayerMask.GetMask("Bridge");
 	    
	// Get references to the tilemaps we will manipulate
	if(!gridLayout)
	{
	    gridLayout = GameObject.Find("WaterTiles").GetComponent<GridLayout>();
	}
	if(!tileMap)
	{
	    tileMap = GameObject.Find("WaterTilemap").GetComponent<Tilemap>();
	}

	// Get the tilemap position of where this water object is
	cellPosition = gridLayout.WorldToCell(transform.position);

	// Snap to the grid
	transform.position = gridLayout.CellToWorld(cellPosition);


	// Run method that checks nearby water objects and fills in tiles to match
	UpdateWater();

	// If not permanent water update all water touching this water
	if(!permanentWater)
	{
	    Collider[] hitColliders = Physics.OverlapBox(transform.position, transform.localScale * .6f, Quaternion.identity, waterMask);
	    foreach(Collider col in hitColliders)
	    {
		col.transform.GetComponent<Water>().UpdateWater();
	    }
	}
    }

    void Update()
    {
	if(updateNextFrame)
	{
	    UpdateWater();
	    updateNextFrame = false;
	}
    }

    
    

    // Check nearby water objects, and update the depth of each side and corner (then update tiles and check bridges)
    
    void UpdateWater()
    {
	if(on)
	{
	    // Ray for raycasts to check for water on each side
	    Ray ray;

	
	    // check to our North
	    ray = new Ray(transform.position + new Vector3(0f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(0f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		n = true;
	    }

	    // check to our East
	    ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, 0f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, 0f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		e = true;
	    }
	
	    // check to our West
	    ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, 0f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, 0f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		w = true;
	    }

	    // check to our South
	    ray = new Ray(transform.position + new Vector3(0f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(0f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		s = true;
	    }

	    // check to our NorthEast
	    ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		nE = true;
	    }

	    // check to our NorthWest
	    ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, transform.localScale.z * .5f + .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		nW = true;
	    }

	    // check to our SouthEast
	    ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f + .3f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		sE = true;
	    }

	    // check to our SouthWest
	    ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up);

	    if(debugOn)
	    {
		Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f - .3f, 3f, -transform.localScale.z * .5f - .3f), -Vector3.up * 5f, Color.green, 5f);
	    }
	
	    // If physics raycast checking only Water layer hit something at this position
	    if(Physics.Raycast(ray, out hit, 5f, waterMask))
	    {
		if(debugOn)
		    Debug.Log(hit.transform.gameObject);
	    
		sW = true;
	    }

	    if(debugOn)
		Debug.Log(n + " " + s + " " + e + " " + w + " " + nE + " " + nW + " " + sE + " " + sW);
	    
	    // Determine depth at various points based on surrounding water
	    centerDepth = (int)(CountTrue(new[] { n, s, e, w, nE, nW, sE, sW }) - 1);
	
	    nDepth = CountTrue(new[] { n, nE, nW }) * 3 - 2;
	    neDepth = CountTrue(new[] {n, nE, e}) * 3 - 2;
	    nwDepth = CountTrue(new[] {n, nW, w}) * 3 - 2;

	    sDepth = CountTrue(new[] {s, sE, sW}) * 3 - 2;
	    seDepth = CountTrue(new[] {s, sE, e}) * 3 - 2;
	    swDepth = CountTrue(new[] {s, sW, w}) * 3 - 2;

	    eDepth = CountTrue(new[] {e, nE, sE}) * 3 - 2;

	    wDepth = CountTrue(new[] {w, nW, sW}) * 3 - 2;

	    if(debugOn)
		Debug.Log(centerDepth);
	    if(debugOn)
		Debug.Log(nDepth);
	    if(debugOn)
		Debug.Log(neDepth);
	    if(debugOn)
		Debug.Log(nwDepth);
	    if(debugOn)
		Debug.Log(sDepth);
	    if(debugOn)
		Debug.Log(seDepth);
	    if(debugOn)
		Debug.Log(swDepth);
	    if(debugOn)
		Debug.Log(eDepth);
	    if(debugOn)
		Debug.Log(wDepth);

	    // Set Navigation obstacle by depth
	    if(centerDepth > 5)
		navMeshObstacle.enabled = true;
	    else
		navMeshObstacle.enabled = false;

	    // adjust navmesh obstacle if one side deep and other not (make smaller and move to the deep side)
	    if(nDepth > 5 && sDepth <= 5)
	    {
		navMeshObstacle.enabled = true;
		navMeshObstacle.size = new Vector3(navMeshObstacle.size.x, 1f, .5f);
		navMeshObstacle.center = new Vector3(navMeshObstacle.center.x, 1f, -.25f);
	    }
	    else if(nDepth <= 5 && sDepth > 5)
	    {
		navMeshObstacle.enabled = true;
		navMeshObstacle.size = new Vector3(navMeshObstacle.size.x, 1f, .5f);
		navMeshObstacle.center = new Vector3(navMeshObstacle.center.x, 1f, .25f);
	    }
	    if(eDepth > 5 && wDepth <= 5)
	    {
		navMeshObstacle.enabled = true;
		navMeshObstacle.size = new Vector3(.5f, 1f, navMeshObstacle.size.z);
		navMeshObstacle.center = new Vector3(.25f, 1f, navMeshObstacle.center.z);
	    }
	    else if(eDepth <= 5 && wDepth > 5)
	    {
		navMeshObstacle.enabled = true;
		navMeshObstacle.size = new Vector3(.5f, 1f, navMeshObstacle.size.z);
		navMeshObstacle.center = new Vector3(-.25f, 1f, navMeshObstacle.center.z);
	    }


	    // Update the tiles based on these new depths
	    UpdateTiles();

	    // Check for a bridge
	    BridgeCheck();

	}
    }







    // Update which tile is placed where depending on the depth in that area and whether or not it connects to another water object
    
    void UpdateTiles()
    {
	    
	// Set the center tile by the depth of our center
	if(centerDepth <= 2)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	}
	else if(centerDepth <= 3)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	}
	else if(centerDepth <= 5)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	}
	else
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	}

	
	// Set the East tile by the depth of our east
	if(eDepth <= 2)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	}
	else if(eDepth <= 3)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	}
	else if(eDepth <= 5)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	}
	else
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	}
	
	// Set the West tile by the depth of our west
	if(wDepth <= 2)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	}
	else if(wDepth <= 3)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	}
	else if(wDepth <= 5)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	}
	else
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	}
	



	// Draw extra water around water if not half size water
	if(!smallWater)
	{
		
	    // Set the NorthEast tile by the depth of our northeast
	    if(neDepth <= 2)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	    }
	    else if(neDepth <= 3)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	    }
	    else if(neDepth <= 5)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	    }
	    else
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	    }
	
	    // Set the NorthWest tile by the depth of our northwest
	    if(nwDepth <= 2)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	    }
	    else if (nwDepth <= 3)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	    }
	    else if(nwDepth <= 56)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	    }
	    else
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	    }
	
	    // Set the SouthWest tile by the depth of our southwest
	    if(swDepth <= 2)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	    }
	    else if(swDepth <= 3)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	    }
	    else if(swDepth <= 5)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	    }
	    else
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	    }
	
	    // Set the SouthEast tile by the depth of our southeast
	    if (seDepth <= 2)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
	    }
	    else if(seDepth <= 3)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
	    }
	    else if(seDepth <= 5)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
	    }
	    else
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
	    }


	
	
	    // If there is no water West of us, Half Water Tile left
	    if(!w)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-1,  1, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
		
		tileMap.SetTile(cellPosition + new Vector3Int(-2,  0, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
	    }
	    else
	    {
		if(wDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(wDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);

		    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(wDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	 
	    // If there is no water East of us, Half Water Tile right
	    if(!e)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int( 1, -1, 0), waterRight[Random.Range(0, waterRight.Length)]);
		tileMap.SetTile(cellPosition + new Vector3Int( 0, -2, 0), waterRight[Random.Range(0, waterRight.Length)]);
	    }
	    else
	    {
		if(eDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(e && eDepth <= 4)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(eDepth > 4 && eDepth <= 6)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	
	    // If there is no water South of us, Half Water Tile below
	    if(!s)
		tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), waterDown[Random.Range(0, waterDown.Length)]);
	    else
	    {
	    
		if(sDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(s && sDepth <= 4)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(sDepth > 4 && sDepth <= 6)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    
		if(swDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(swDepth <= 4)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(swDepth > 4 && swDepth <= 6)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    
		if(!w && !sW)
		    tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
		else
		{
		    if(swDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(swDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(swDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	    
		if(!e && !sE)
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), waterRight[Random.Range(0, waterRight.Length)]);
		else
		{
		    if(seDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(seDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(seDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	    
	    }
	
	    // If there is no water North
	    if(!n)
	    {
		if (!nE && !nW)
		    tileMap.SetTile(cellPosition + new Vector3Int( 1,  1, 0), waterUp[Random.Range(0, waterUp.Length)]);
		else
		{
		    if(nDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(nDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(nDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	    }
	    // If there is water North
	    else
	    {
		if(nDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(nDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(nDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    
		if(neDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(neDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(neDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}

		if(!w && !nW)
		    tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
		else
		{
		    if(nwDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(nwDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(nwDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	    
		if(!e && !nE)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), waterRight[Random.Range(0, waterRight.Length)]);
		}
		else
		{
		    if(neDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(neDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(neDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	    }
	}
    }






    


    // Check for bridges and turn off nav mesh agent if we are overlapping one
    void BridgeCheck()
    {
	// Ray for raycasts to check for bridges on each side
	Ray ray;
	
	// Check our center
	ray = new Ray(transform.position + new Vector3(0f, 0f, 0f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(0f, 3f, 0f), -Vector3.up * 5f, Color.green, 5f);
	}
		
	// If physics raycast checking only Bridge layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;
	}

	// check to our North
	ray = new Ray(transform.position + new Vector3(0f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(0f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our East
	ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, 0f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, 0f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}
	
	// check to our West
	ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, 0f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, 0f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our South
	ray = new Ray(transform.position + new Vector3(0f, 3f, -transform.localScale.z * .5f + 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(0f, 3f, -transform.localScale.z * .5f + 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our NorthEast
	ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our NorthWest
	ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, transform.localScale.z * .5f - 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our SouthEast
	ray = new Ray(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, -transform.localScale.z * .5f - 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(transform.localScale.x *.5f - 1f, 3f, -transform.localScale.z * .5f - 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}

	// check to our SouthWest
	ray = new Ray(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, -transform.localScale.z * .5f + 1f), -Vector3.up);

	if(debugOn)
	{
	    Debug.DrawRay(transform.position + new Vector3(-transform.localScale.x *.5f + 1f, 3f, -transform.localScale.z * .5f + 1f), -Vector3.up * 5f, Color.green, 5f);
	}
	
	// If physics raycast checking only Water layer hit something at this position
	if(Physics.Raycast(ray, out hit, 5f, bridgeMask))
	{
	    if(debugOn)
		Debug.Log(hit.transform.gameObject);
	    
	    navMeshObstacle.enabled = false;

	    return;

	}
    }





    

    public float GetDepth(Vector3 location)
    {	
	// If Location is on West side of Water Object
	if(location.x < transform.position.x - 2f)
	{
	    // If Location is on South side of Water Object
	    if(location.z < transform.position.z - 2f)
	    {
		return swDepth;
	    }
	    // If Location is in Horizontal Middle of Water Object
	    else if(location.z < transform.position.z + 2f)
	    {
		return wDepth;
	    }
	    // If Location is on North side of Object
	    else
	    {
		return nwDepth;
	    }
	}
	// If Location is in Vertical Middle side of Water Object
	else if(location.x < transform.position.x + 2f)
	{
	    // If Location is on South side of Water Object
	    if(location.z < transform.position.z - 2f)
	    {
		return sDepth;
	    }
	    // If Location is in Horizontal Middle of Water Object
	    else if(location.z < transform.position.z + 2f)
	    {
		return centerDepth;
	    }
	    // If Location is on North side of Object
	    else
	    {
		return nDepth;
	    }
	}
	// If Location is on East side of Water Object
	else
	{
	    // If Location is on South side of Water Object
	    if(location.z < transform.position.z - 2f)
	    {
		return seDepth;
	    }
	    // If Location is in Horizontal Middle of Water Object
	    else if(location.z < transform.position.z + 2f)
	    {
		return eDepth;
	    }
	    // If Location is on North side of Object
	    else
	    {
		return neDepth;
	    }
	}
    }





    public void TurnOff()
    {
	// If we have a nav mesh agent turn it off
	navMeshObstacle.enabled = false;

	// Turn off all the water tiles associated with this object

	// Reset center tiles
	tileMap.SetTile(cellPosition, null);
	tileMap.SetTile(cellPosition + new Vector3Int(-1,  -1, 0), null);
	// Reset West tile
	tileMap.SetTile(cellPosition + new Vector3Int(-1,  0, 0), null);
	// Reset East tile
	tileMap.SetTile(cellPosition + new Vector3Int(0,  -1, 0), null);

	if(!smallWater)
	{
	    tileMap.SetTile(cellPosition + new Vector3Int(1,  0, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(0,  1, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1,  -2, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), null);

	    // Replace tiles south and north
	    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), null);
	    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), null);
	    
	    // If there was water South of us, do southEast and southWest
	    if(s)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), null);
		tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), null);
		tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), null);
	    }
	    
	    // If there was water North, turn off northEast and northWest
	    if(n)
	    {
		tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), null);
		tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), null);
		tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), null);
	    }
	}

	// Turn off tag that says this water is on
	on = false;
	    
	// Update water touching this water
	Collider[] hitColliders = Physics.OverlapBox(transform.position, transform.localScale * .6f, Quaternion.identity, waterMask);

	// First move this water so it doesn't effect their update
	//transform.position = new Vector3(transform.position.x, 666f, transform.position.z);
	
	// Update all the water objects we found to be touching us
	foreach(Collider col in hitColliders)
	{
	    if(col.transform.gameObject != gameObject)
		col.transform.GetComponent<Water>().updateNextFrame = true;
	}
	
	// If this isn't permanent water
	if(!permanentWater)
	{
	    // Set object into the sky so it triggers collider exit on characters touching it
	    transform.position = new Vector3(transform.position.x, 666f, transform.position.z);
	}
	
	// Turn off navmesh obstacle so npcs can walk through this area again
	navMeshObstacle.enabled = false;

	if(!permanentWater)
	{
	    // Just destroy this waterobject as we don't need it anymore
	    Destroy(gameObject, .5f);
	}
    }




    // Turn on previously turned off water
    public void TurnOn()
    {
	// Note the water is now on
	on = true;

	// Turn off navmesh obstacle so npcs can walk through this area again
	navMeshObstacle.enabled = true;
	    
	// Update the water's graphics and depths based on water surrounding it
	UpdateWater();

	// Update water touching this water
	Collider[] hitColliders = Physics.OverlapBox(transform.position, transform.localScale * .6f, Quaternion.identity, waterMask);
	foreach(Collider col in hitColliders)
	{
	    if(col.transform.gameObject != gameObject)
		col.transform.GetComponent<Water>().UpdateWater();
	}
    }


    
    
    
    // Return how number of true booleans in a set
    int CountTrue(bool[] array)
    {
       int value = 0;
 
       for(int i = 0; i < array.Length; i++)
       {
            if(array[i] == true) value++;
       }
 
       return value;
    }








    // Update some tiles randomly, to make them animate
    
    public void RandomUpdateTiles(int percent)
    {
	if(on)
	{
	    // Set the center tiles by the depth of our center
	    if(Random.Range(0, 100) < percent)
	    {
		if(centerDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(centerDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(centerDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	    if(Random.Range(0, 100) < percent)
	    {
		if(centerDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(centerDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(centerDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	
	    // Set the East tile by the depth of our east
	    if(Random.Range(0, 100) < percent)
	    {
		if(eDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(eDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(eDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(0, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	
	    // Set the West tile by the depth of our west
	    if(Random.Range(0, 100) < percent)
	    {
		if(wDepth <= 2)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		}
		else if(wDepth <= 3)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		}
		else if(wDepth <= 5)
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		}
		else
		{
		    tileMap.SetTile(cellPosition + new Vector3Int(-1, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		}
	    }
	



	    // Draw extra water around water if not half size water
	    if(!smallWater)
	    {
		// Set the NorthEast tile by the depth of our northeast
		if(Random.Range(0, 100) < percent)
		{
		    if(neDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(neDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(neDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(1, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
		// Set the NorthWest tile by the depth of our northwest
		if(Random.Range(0, 100) < percent)
		{
		    if(nwDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if (nwDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(nwDepth <= 56)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(0, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	
		// Set the SouthWest tile by the depth of our southwest
		if(Random.Range(0, 100) < percent)
		{
		    if(swDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(swDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(swDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-2, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}
	
		// Set the SouthEast tile by the depth of our southeast
		if(Random.Range(0, 100) < percent)
		{
		    if (seDepth <= 2)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
		    }
		    else if(seDepth <= 3)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
		    }
		    else if(seDepth <= 5)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
		    }
		    else
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
		    }
		}


	
	
		// If there is no water West of us, Half Water Tile left
		if(Random.Range(0, 100) < percent)
		{
		    if(!w)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-1,  1, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
		    }
		    else
		    {
			if(wDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);

			}
			else if(wDepth <= 3)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);

			}
			else if(wDepth <= 5)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}

		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(!w)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int(-2,  0, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
		    }
		    else
		    {
			if(wDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(wDepth <= 3)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(wDepth <= 5)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}
	 
		// If there is no water East of us, Half Water Tile right
		if(Random.Range(0, 100) < percent)
		{
		    if(!e)
		    {
			tileMap.SetTile(cellPosition + new Vector3Int( 1, -1, 0), waterRight[Random.Range(0, waterRight.Length)]);
		    }
		    else
		    {
			if(eDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);

			}
			else if(eDepth <= 4)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(eDepth > 4 && eDepth <= 6)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);

			}
		    }

		}
		if(Random.Range(0, 100) < percent)
		{
		    if(!e)
			tileMap.SetTile(cellPosition + new Vector3Int( 0, -2, 0), waterRight[Random.Range(0, waterRight.Length)]);

		    else
		    {
			if(eDepth <= 2)
			{

			    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(eDepth <= 4)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(eDepth > 4 && eDepth <= 6)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(0, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}
	
		// If there is no water South of us, Half Water Tile below
		if(Random.Range(0, 100) < percent)
		{
		    if(!s)
			tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), waterDown[Random.Range(0, waterDown.Length)]);
		    else
		    {
	    
			if(sDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(s && sDepth <= 4)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(sDepth > 4 && sDepth <= 6)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-2, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(s)
		    {
			if(swDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(swDepth <= 4)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(swDepth > 4 && swDepth <= 6)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(-3, -2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(s)
		    {		
			if(!w && !sW)
			    tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
			else
			{
			    if(swDepth <= 2)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			    }
			    else if(swDepth <= 3)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			    }
			    else if(swDepth <= 5)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			    }
			    else
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-3, -1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			    }
			}
		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(s)
		    {
			if(!e && !sE)
			    tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), waterRight[Random.Range(0, waterRight.Length)]);
			else
			{
			    if(seDepth <= 2)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			    }
			    else if(seDepth <= 3)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			    }
			    else if(seDepth <= 5)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			    }
			    else
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(-1, -3, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			    }
			}
		    }
		}
	
		// If there is no water North
		if(Random.Range(0, 100) < percent)
		{
		    if(!n)
		    {
			if (!nE && !nW)
			    tileMap.SetTile(cellPosition + new Vector3Int( 1,  1, 0), waterUp[Random.Range(0, waterUp.Length)]);
			else
			{
			    if(nDepth <= 2)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			    }
			    else if(nDepth <= 3)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			    }
			    else if(nDepth <= 5)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			    }
			    else
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			    }
			}
		    }
		    // If there is water North
		    else
		    {
			if(nDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(nDepth <= 3)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(nDepth <= 5)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(1, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}

		if(Random.Range(0, 100) < percent)
		{
		    if(n)
		    {
			if(neDepth <= 2)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			}
			else if(neDepth <= 3)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			}
			else if(neDepth <= 5)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			}
			else
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(2, 1, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			}
		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(n)
		    {
			if(!w && !nW)
			    tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), waterLeft[Random.Range(0, waterLeft.Length)]);
			else
			{
			    if(nwDepth <= 2)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			    }
			    else if(nwDepth <= 3)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			    }
			    else if(nwDepth <= 5)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			    }
			    else
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(0, 2, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			    }
			}
		    }
		}
		if(Random.Range(0, 100) < percent)
		{
		    if(n)
		    {
			if(!e && !nE)
			{
			    tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), waterRight[Random.Range(0, waterRight.Length)]);
			}
			else
			{
			    if(neDepth <= 2)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), shallowTiles[Random.Range(0, shallowTiles.Length)]);
			    }
			    else if(neDepth <= 3)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deepTiles[Random.Range(0, deepTiles.Length)]);
			    }
			    else if(neDepth <= 5)
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deeperTiles[Random.Range(0, deeperTiles.Length)]);
			    }
			    else
			    {
				tileMap.SetTile(cellPosition + new Vector3Int(2, 0, 0), deepestTiles[Random.Range(0, deepestTiles.Length)]);
			    }
			}
		    }
		}
	    }
	}
    }
}
