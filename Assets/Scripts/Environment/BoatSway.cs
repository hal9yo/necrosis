﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatSway : MonoBehaviour
{
    float xPos;
    float yPos;
    float zPos;
    
    float xRot;
    float yRot;
    float zRot;
    

    float sway;

    float timer = 0f;

    // Keeps track if we are touching water that is currently on
    bool waterOn;
    
    // Start is called before the first frame update
    void Start()
    {	
	xPos = transform.position.x;
	yPos = transform.position.y;
	zPos = transform.position.z;
	
	xRot = transform.rotation.eulerAngles.x;
	yRot = transform.rotation.eulerAngles.y;
	zRot = transform.rotation.eulerAngles.z;
    }

    // Update is called once per frame
    void Update()
    {
	if(waterOn)
	{
	    if(timer <= 0f)
	    {
		transform.position = new Vector3(xPos + Random.Range(-2f, 2f) * Time.deltaTime, yPos, zPos);
		transform.rotation = Quaternion.Euler(new Vector3(xRot, yRot, zRot + Random.Range(-50f, 50f) * Time.deltaTime));
		timer = Random.Range(.01f, 1f);
	    }
	    else
	    {
		timer -= Time.deltaTime;
	    }
	}
	
	waterOn = false;
    }

    // If we are currently touching water areas
    void OnTriggerStay(Collider col)
    {
	if(!waterOn && col.transform.GetComponent<Water>().on)
	    waterOn = true;
    }

}
