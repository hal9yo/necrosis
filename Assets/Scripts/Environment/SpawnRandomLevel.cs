﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomLevel : MonoBehaviour
{
    
    public Transform[] spawnPoints;
    public GameObject[] wayPoints;
    public GameObject retreatPoint;

    public GameObject[] npcObjects;
    public GameObject[] wolfObjects;

    public float timer;

    public float minTime;
    public float maxTime;

    public PlayerStats playerStats;

    public Wind windScript;
    
    public FireRoot fireRoot;


    
    // Update is called once per frame
    void Update()
    {
        if(timer <=0)
	{
	    timer = Random.Range(minTime, maxTime);

	    
	    // Randomly choose a type of npc to spawn
	    int which = Random.Range(0, npcObjects.Length);

	    int where = Random.Range(0, spawnPoints.Length);
	    
	    // Spawn the npc at a random point around the spawn point
	    GameObject npc = Instantiate(npcObjects[which], RandomNavmeshLocation(10f, spawnPoints[where].position), Quaternion.identity);

	    // Get the patrol script and throw some waypoints in there so they wander the area
	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();

	    foreach(GameObject point in wayPoints)
	    {
		patrolScript.patrolPoints.Add(point);
	    }
	    patrolScript.randomPatrol = true;
		
	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = false;

	    npc.GetComponent<WindPush>().windScript = windScript;

	    //Add a homebase to retreat to if things get hairy
	    npc.GetComponent<NPCRetreat>().homeBases.Add(retreatPoint);

	    npc.GetComponent<NPC1>().playerStat = playerStats;

	    ArcherAttack archerScript = npc.GetComponent<ArcherAttack>();
	    archerScript.fireRoot = fireRoot;
	    archerScript.windScript = windScript;
	   
	}
	else
	{
	    timer -= Time.deltaTime;
	}
    }	

    Vector3 RandomNavmeshLocation(float radius, Vector3 location)
    {
	UnityEngine.AI.NavMeshHit hit;
	Vector3 finalPosition = Vector3.zero;
	while(finalPosition == Vector3.zero)
	{
	    Vector3 randomDirection = Random.insideUnitSphere * radius;
	    randomDirection += location;

	    randomDirection = new Vector3(randomDirection.x, 1f, randomDirection.y);

	    if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
	    {
		finalPosition = hit.position;            
	    }
	}
	return finalPosition;
    }
}
