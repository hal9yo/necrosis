﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandom : MonoBehaviour
{
    
    public Transform spawnPointA;
    public Transform spawnPointB;
    public Transform spawnPointC;

    public GameObject[] npcObjects;
    public GameObject[] zombieObjects;
    public GameObject[] wolfObjects;

    private float timer;

    public float minTime;
    public float maxTime;

    public float percentNPC;
    

    public float decayTime;


    public Wind windScript;
    
    public FireRoot fireRoot;
    
    // Update is called once per frame
    void Update()
    {
        if(timer <=0)
	{
	    float random = Random.Range(0f, 1f);

	    if(random < percentNPC)
	    {


		// Randomly choose a type of npc to spawn
		int which = Random.Range(0, npcObjects.Length);

		// Spawn the npc at a random point around the spawn point
		GameObject npc = Instantiate(npcObjects[which], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);

		// Get the patrol script and throw some waypoints in there so they wander the area
		NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
		patrolScript.patrolPoints.Add(spawnPointA.gameObject);
		patrolScript.patrolPoints.Add(spawnPointB.gameObject);
		patrolScript.patrolPoints.Add(gameObject);
		patrolScript.randomPatrol = true;
		
		Health healthScript = npc.GetComponent<Health>();
		healthScript.decay = true;;
		healthScript.decayTime = decayTime;

		npc.GetComponent<WindPush>().windScript = windScript;

		//Add a homebase to retreat to if things get hairy
		npc.GetComponent<NPCRetreat>().homeBases.Add(spawnPointA.gameObject);

		// If it is a fire archer
		if(which == 5)
		{
		    npc.GetComponent<ArcherAttack>().fireRoot = fireRoot;
		}
	    }
	    else
	    {
		// Usually spawn zombie
		if(Random.Range(0, 15) > 0)
		{
		    int which2 = Random.Range(0, zombieObjects.Length);
		    GameObject zombie = Instantiate(zombieObjects[which2], RandomNavmeshLocation(10f, spawnPointB.position), Quaternion.identity);
		    zombie.gameObject.tag = "PlayerUnit";

		    Health healthScript = zombie.GetComponent<Health>();
		    healthScript.decay = true;;
		    healthScript.decayTime = decayTime;

		    zombie.GetComponent<WindPush>().windScript = windScript;
		}
		// Sometimes spawn wolf
		else
		{
		    int which2 = Random.Range(0, wolfObjects.Length);
		    GameObject wolf = Instantiate(wolfObjects[which2], RandomNavmeshLocation(10f, spawnPointC.position), Quaternion.identity);
		    wolf.gameObject.tag = "WolfUnit";

		    Health healthScript = wolf.GetComponent<Health>();
		    healthScript.decay = true;;
		    healthScript.decayTime = decayTime;

		    wolf.GetComponent<WindPush>().windScript = windScript;
		}
	    }
	    timer = Random.Range(minTime, maxTime);
	}
	else
	{
	    timer -= Time.deltaTime;
	}

	
	// If key pressed spawn archer
	if(Input.GetKeyDown(KeyCode.A))
	{
	    GameObject npc = Instantiate(npcObjects[0], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);

	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;
	}

	// If key pressed spawn dagger npc
	if(Input.GetKeyDown(KeyCode.D))
	{		
	    GameObject npc = Instantiate(npcObjects[1], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);
	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;
	}

	// If key pressed spawn longswordsman
	if(Input.GetKeyDown(KeyCode.L))
	{
	    GameObject npc = Instantiate(npcObjects[3], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);

	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;
	}

	// If key pressed spawn longbow archer
	if(Input.GetKeyDown(KeyCode.B))
	{
	    GameObject npc = Instantiate(npcObjects[2], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);
	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;
	}

	// If key pressed spawn swordsman
	if(Input.GetKeyDown(KeyCode.S))
	{
	    GameObject npc = Instantiate(npcObjects[4], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);
	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;
	}

	// If key pressed spawn longswordsman
	if(Input.GetKeyDown(KeyCode.F))
	{
	    GameObject npc = Instantiate(npcObjects[5], RandomNavmeshLocation(10f, spawnPointA.position), Quaternion.identity);
	    NPCPatrol patrolScript = npc.GetComponent<NPCPatrol>();
	    patrolScript.patrolPoints.Add(spawnPointA.gameObject);
	    patrolScript.patrolPoints.Add(spawnPointB.gameObject);
	    patrolScript.patrolPoints.Add(gameObject);
	    patrolScript.randomPatrol = true;

	    Health healthScript = npc.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    npc.GetComponent<WindPush>().windScript = windScript;

	    npc.GetComponent<ArcherAttack>().fireRoot = fireRoot;
	}

	
	// If key pressed spawn zombie
	if(Input.GetKeyDown(KeyCode.Z))
	{
	    GameObject zombie = Instantiate(zombieObjects[0], RandomNavmeshLocation(10f, spawnPointB.position), Quaternion.identity);
	    zombie.gameObject.tag = "PlayerUnit";

	    Health healthScript = zombie.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    zombie.GetComponent<WindPush>().windScript = windScript;
	}
	


    
	// If key pressed spawn Werewolf or wolf (depending if shift is held
	if(Input.GetKeyDown(KeyCode.W))
	{
	    GameObject wolf = Instantiate(wolfObjects[0], RandomNavmeshLocation(10f, spawnPointC.position), Quaternion.identity);
	    wolf.gameObject.tag = "WolfUnit";
		
	    Health healthScript = wolf.GetComponent<Health>();
	    healthScript.decay = true;;
	    healthScript.decayTime = decayTime;

	    wolf.GetComponent<WindPush>().windScript = windScript;
	}

        Vector3 RandomNavmeshLocation(float radius, Vector3 location)
	{
	    UnityEngine.AI.NavMeshHit hit;
	    Vector3 finalPosition = Vector3.zero;
	    while(finalPosition == Vector3.zero)
	    {
		Vector3 randomDirection = Random.insideUnitSphere * radius;
		randomDirection += location;

		if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
		{
		    finalPosition = hit.position;            
		}
	    }
	    return finalPosition;
	}
     
    }
}
