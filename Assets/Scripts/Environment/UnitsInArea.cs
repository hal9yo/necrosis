﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsInArea : MonoBehaviour
{
    // The gotolevel script this unitsinarea script is associated with
    public GoToLevel gotoScript;

    // When a zombie enters our trigger collider add them to the list that brings them to the next level if we go to the next level
    void OnTriggerEnter(Collider other)
    {
	if(other.transform.tag == "PlayerUnit")
	{
	    bool found = false;
	    foreach(GameObject obj in gotoScript.zombList)
	    {
		if(obj == other.gameObject)
		    found = true;
	    }
	    if(!found)
		gotoScript.zombList.Add(other.gameObject);
	}

	if(other.transform.tag == "EnemyUnit")
	{
	    bool found = false;
	    foreach(GameObject obj in gotoScript.enemyList)
	    {
		if(obj == other.gameObject)
		    found = true;
	    }
	    if(!found)
		gotoScript.enemyList.Add(other.gameObject);
	}
    }

    void OnTriggerStay(Collider other)
    {
	// If any units in our trigger collider are dead or permadead
	if(other.transform.tag == "Dead" || other.transform.tag == "PermaDead")
	{
	    // Try to remove them from the enemy list as dead enemies shouldn't travel
	    gotoScript.enemyList.Remove(other.gameObject);
	}
    }

    // When a zombie leaves the collider remove them from that list
    void OnTriggerExit(Collider other)
    {
	if(other.transform.tag == "PlayerUnit")
	{
	    gotoScript.zombList.Remove(other.gameObject);
	}

	if(other.transform.tag == "EnemyUnit")
	{
	    gotoScript.enemyList.Remove(other.gameObject);
	}
    }
}
