﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Chasm : MonoBehaviour
{
    // Reference to the fall script
    public ChasmFall fallScript;



    
    // When object enters cliff face
    void OnTriggerStay(Collider col)
    {
	//Debug.Log("ping!");
	// Get the objects distance from the center on each dimension
	float distX = Mathf.Abs(col.transform.position.x - transform.position.x);
	float distZ = Mathf.Abs(col.transform.position.z - transform.position.z);


	// Reference the rigidbody attached to the collider that entered
	Rigidbody rigidbody = col.GetComponent<Rigidbody>();

	//Debug.Log((transform.localScale.x * .5f - distX) + "   " + (transform.localScale.z * .5f - distZ));
	// If the object is further from either edge of chasm than 1 unit
	if(transform.localScale.x * .5f - distX > 2f || transform.localScale.z * .5f - distZ > 2f)
	{
	    if(rigidbody)
	    {
		// Make sure object is not already in the fallScript's list
		if(!fallScript.fallingObjects.Contains(rigidbody))
		{
		    // Add object's rigidbody to the list of falling rigidbodies
		    fallScript.fallingObjects.Add(rigidbody);


		    // Remember what time the object fell into the chasm
		    fallScript.fallTime.Add(Time.time);

	
		    // If possible get and remember the object's health script
		    Health healthScript = null;

		    healthScript = col.GetComponent<Health>();
		    
		    fallScript.healthScripts.Add(healthScript);

		    if(healthScript)
		    {
			//Debug.Log(col.gameObject.GetComponentInChildren<SortingGroup>());
			col.gameObject.GetComponentInChildren<SortingGroup>().sortingOrder = -2;
		    }
		}
	    }
	}
	// Move the object slowly into the chasm
	else
	{
	    
	    //Debug.Log("ping!");
	    
	    if(rigidbody)
	    {
		Vector3 direction = col.transform.position - transform.position;
		direction = -direction.normalized;
		rigidbody.velocity += direction * Time.deltaTime * 100f;
	    }
	}
    }
}
