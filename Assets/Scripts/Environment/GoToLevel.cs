﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToLevel : MonoBehaviour
{
    // Reference to our save script so we can save data before advancing to next level
    public SaveGame saveScript;

    // Reference to our player stats so we can change it to the level we are going to
    public PlayerStats playerStats;

    // Variable to store which level this exit takes us to
    public string whichLevel;

    // Variable of which area number we are going to
    public int area;

    // Variable of which entrance of the area we go to
    public int entrance;

    // Zombies that are in the vicinity which will travel to the next level with the player
    public List<GameObject> zombList;

    // Enemies that are in the vicinity which will travel to the next level with the player
    public List<GameObject> enemyList;
    
    void OnTriggerEnter(Collider other)
    {	
	if(other.CompareTag("Player"))
	{
	    playerStats.area = area;
	    playerStats.entrance = entrance;
	    
	    Debug.Log("Saving...");

	    saveScript.Save(zombList, enemyList);

	    Debug.Log("Go to Cave");   

	    SceneManager.LoadSceneAsync(whichLevel);
	}
    }
}
