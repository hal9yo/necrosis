﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterRoot : MonoBehaviour
{
    public List<Water> waterScripts;

    public int percent;

    public int howMany;

    public Wind windScript;

    // Add this to wind to make water move when wind is 0
    public float baseMove = 0;

    public bool debugOn;
    
    int index = 0;
    
    // Update is called once per frame
    void Update()
    {
        if(waterScripts.Count > 0)
	{
	    // Determine how many tiles change per frame based on wind strength
	    //howMany = (int)(windScript.strength * 4f);
	    percent = (int)((windScript.strength + baseMove) * 2f);

	    // Change a random number of water objects based on howMany value
	    for(int i = 0; i < howMany; i++)
	    {
		if(debugOn)
		    Debug.Log(index);
		
		// Change a random number of tiles on that object based on percent value
		waterScripts[index].RandomUpdateTiles(percent);

		index++;

		if(index >= waterScripts.Count)
		{
		    index = 0;
		    break;
		}
	    }
	}
    }
}
