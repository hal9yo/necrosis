﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPush : MonoBehaviour
{
    public Wind windScript;
    public Rigidbody rb;
    public Health ourHealth;
    public float amount = 1f;


    // Update is called once per frame
    void Update()
    {
	if(windScript)
	{
	    if(windScript.strength > 5f)
	    {
		// If it is a character
		if(ourHealth)
		{
		    // If the character is alive
		    if(ourHealth.currHealth > 0f)
		    {
			// Make sure we have a rigidbody before trying to manipulate it
			if(rb)
			{
			    // Amount velocity will be after push from wind
			    Vector3 change = windScript.direction * windScript.strength * Time.deltaTime * amount;

			    // Only change velocity if change greater than 1 (stop slowly sliding at lower wind velocities)
			    if((rb.velocity + change).magnitude > 1f)
			    {
				rb.velocity += change;
			    }
			}
		    }
		    // Character is dead, be more resistant to moving
		    else
		    {
			if(rb)
			{
			    // Amount velocity will be after push from wind
			    Vector3 change = windScript.direction * windScript.strength * amount;

			    // Only change velocity if change greater than 2 (stop slowly sliding at lower wind velocities)
			    if((rb.velocity + change).magnitude > 10f)
			    {
				// change velocity by 1/5th the amount
				rb.velocity += change *.1f;
			    }
			}
		    }
		}
		else
		{
		    if(rb)
		    {
			rb.velocity += windScript.direction * windScript.strength * amount;
		    }
		}
	    }
	}
    }
}
