﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wind : MonoBehaviour
{
    // The direction the wind is blowing
    public Vector3 direction;
    // Remember the direction so we know when it changes
    public Vector3 oldDirection;

    // How fast the wind is blowing
    public float strength;

    // How fast the wind speed changes
    public float changeStrength;
    
    // Remember strength to note when it changes
    float oldStrength;

    // Max speed of the wind in this level
    public float maxStrength;
    
    // The linerenderers attached to the camera that shows the player the direction the wind is blowing
    public LineRenderer mainline;
    public LineRenderer mainoutline;
    public LineRenderer arrowline;
    public LineRenderer arrowoutline;
    public Text windPrint;

    public List<GameObject> windSounds;

    
    float timer;
    
    
    // Start is called before the first frame update
    void Start()
    {
	// Pick a random direction for the wind
	direction = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
	// Remember direction so we know if it changes
	oldDirection = direction;
	
	// Pick a random speed for the wind
	strength = Random.Range(0f, maxStrength);
	// Remember the strength so we know if it changes
	oldStrength = strength;

	// Draw the line showing the direction and strength of the wind
	mainline.SetPosition(0, Vector3.zero);
	mainline.SetPosition(1, direction * -strength *.5f);
	
	// Draw the outline around the other line
	mainoutline.SetPosition(0, Vector3.zero);
	mainoutline.SetPosition(1, direction * -strength * .5f);

	// Draw the arrow line showing the direction and strength of the wind
	arrowline.SetPosition(0, Vector3.zero);
	arrowline.SetPosition(1, direction * strength * .5f);
	
	// Draw the arrow outline around the other line
	arrowoutline.SetPosition(0, Vector3.zero);
	arrowoutline.SetPosition(1, direction * strength * .5f);

	// Print the wind speed, rounded to nearest tenth, on the screen below the arrow
	windPrint.text = Mathf.Round(strength) + " mps";

	// Go ahead and turn on windsounds equal to the strength
	UpdateWindSounds();
    }

    void Update()
    {
	// If the direction or strength changed since last frame
	if(oldDirection != direction || oldStrength != strength)
	{
	    // Update the wind vane graphic and speedometer
	    UpdateWindVane();

	    // Update the wind sounds
	    UpdateWindSounds();
		
	    // Remember the direction
	    oldDirection = direction;
	    oldStrength = strength;
	}
	
	if(timer <= 0f)
	{
	    // Pick a somewhat random new direction for the wind
	    direction = new Vector3(direction.x + Random.Range(-.3f, .3f), 0f, direction.y + Random.Range(-.3f, .3f)).normalized;

	    // Remember the direction of the wind
	    oldDirection = direction;
	    
	    // Pick a random speed for the wind
	    strength += Random.Range(-changeStrength, changeStrength);
	    
	    if(strength > maxStrength)
		strength -= Random.Range(0f, changeStrength);
	    else if(strength < 0f)
		strength = 0f;

	    // Remember the strength of the wind
	    oldStrength = strength;

	    UpdateWindVane();

	    
	    timer = Random.Range(.1f, 10f);
	}
	else
	{
	    timer -= Time.deltaTime;
	}
    }

    void UpdateWindVane()
    {
	// Draw the line showing the direction and strength of the wind
	mainline.SetPosition(0, Vector3.zero);
	mainline.SetPosition(1, direction * -strength *.5f);
	
	// Draw the outline around the other line
	mainoutline.SetPosition(0, Vector3.zero);
	mainoutline.SetPosition(1, direction * -strength * .5f);

	// Draw the arrow line showing the direction and strength of the wind
	arrowline.SetPosition(0, Vector3.zero);
	arrowline.SetPosition(1, direction * strength * .5f);
	
	// Draw the arrow outline around the other line
	arrowoutline.SetPosition(0, Vector3.zero);
	arrowoutline.SetPosition(1, direction * strength * .5f);

	// Print the wind speed, rounded to nearest tenth, on the screen below the arrow
	windPrint.text = Mathf.Round(strength) + " mps";
    }

    void UpdateWindSounds()
    {	
	// Cast our strength to an integer for iterating
	int strengthInt = (int)strength;
	
	// Go through every wind sound
	for(int i = 0; i <windSounds.Count; i++)
	{
	    // If the current iteration is less than the strength of the wind
	    if(i < strengthInt)
	    {
		// Turn on wind sounds to match the current stength
		windSounds[i].SetActive(true);
	    }
	    // If the current iteration is greater than or equal to the wind
	    else
	    {
		    windSounds[i].SetActive(false);
	    }
	}
    }
}
