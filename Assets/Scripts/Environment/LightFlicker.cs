﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    // Reference to this object's light component
    public Light thisLight;

    // Timer to track how long since last light adjustment
    private float timer = .2f;

    // How fast to make the light flicker
    public float flickerSpeed = .2f;

    // Called at start
    void Start()
    {
	// Get the reference to our light component
	thisLight = GetComponent<Light>();
    }
    
    // Update is called once per frame
    void Update()
    {
	// Is it time to adjust the light intensity again?
	if(timer <= 0f)
	{
	    // Set random light intensity
	    thisLight.intensity = (Random.Range (0.75f, 1f));

	    // Reset timer
	    timer = flickerSpeed;
	}
	else
	    // Decrement timer
	    timer -= Time.deltaTime;
	
    }
}
