﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.AI;

public class ObjectSubmerge : MonoBehaviour
{
    // Reference to the sprite mask that obscures the sprites
    public GameObject spriteMask;

    // Reference to the character's sprites that will be partially obscured by the water
    public List<GameObject> sprites;
    
    // The current depth the object is submerged to
    public float depth = 0;

    // A list of water objects we are currently colliding with to get depths
    public List<Water> waterScripts;
    
    // Splash animation prefab
    public GameObject splashPrefab;
    
    // Audio Source to play water sound effects
    public AudioSource audioSource;

    // Array of audio clips when splashing in water
    public AudioClip[] bigSplashSounds;
    public AudioClip[] smallSplashSounds;
    
    // The fire script of the object, to turn off fires
    public ObjectFire fireScript;

    // The fire object
    public GameObject fire;
    
    public float maskDefaultY;


    
    // When we first enter a water area
    void OnTriggerEnter(Collider col)
    {
	if(col.gameObject.tag != "Bridge")
	{
	    // Add this water object to our list of Water scripts for depths
	    waterScripts.Add(col.gameObject.GetComponent<Water>());

	    bool waterOn = false;

	    // Remove null entries from Water Script List
	    waterScripts.RemoveAll(item => item == null);
		
	    foreach(Water wScript in waterScripts)
	    {
		if(wScript.on)
		    waterOn = true;
	    }
		
	    // IF we are not on a bridge
	    if(waterOn)
	    {
		if(fireScript)
		    // Stop any burning that may be happening
		    fireScript.heat = 0f;

		if(fire && fire.activeSelf)
		    fire.SetActive(false);
		
		// Did we just enter the water?
		if(waterScripts.Count < 2)
		{
		    // Play a random small splash sound at random volume as oneshot (allows multiple sounds to play at same time)
		    audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));

		    // Create a few splash animations around the character, with random delays
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		}

		audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));
		// Create a few splash animations around the character, with random delays
		StartCoroutine(DrawSplash());
		StartCoroutine(DrawSplash());
		StartCoroutine(DrawSplash());
	    }


	}
    }




    
    // If we are currently touching water areas
    void OnTriggerStay()
    {	
	//Set Depth here
	depth = 0f;

	// Tag to see if the water we are touching is not on
	bool waterOn = false;
	
	// Remove null entries from Water Script List
	waterScripts.RemoveAll(item => item == null);

	// Cycle through every water script we are touching
	foreach(Water thisScript in waterScripts)
	{
	    // Set our depth to the value on this Script
	    float thisDepth = thisScript.GetDepth(transform.position);


	    // If this depth is less than our current depth, set it to our depth
	    if(thisDepth > depth)
	    {
		depth = thisDepth;
	    }

	    // If any of these scripts are on, set tag that we are touching water that is on
	    if(thisScript.on)
		waterOn = true;
	}

	// Only if we are touching water that is on
	if(waterOn)
	{
	    if(fireScript)
		// Stop any burning that may be happening
		fireScript.heat = 0f;
	    if(fire && fire.activeSelf)
		fire.SetActive(false);

	    // Raise the bottom of the sprite mask to obscure bottom of sprite
	    spriteMask.transform.localPosition = new Vector3(0f, maskDefaultY + .3f, 0f);
	}
	// If we aren't touching water that is on
	else
	{
	    // Raise the bottom of the sprite mask to obscure bottom of sprite
	    spriteMask.transform.localPosition = new Vector3(0f, maskDefaultY, 0f);
	}

	// If our sprites are above the depth under us
	if(sprites[0].transform.localPosition.y != -depth * .07f)
	{
	    // Lower our Sprites by time since last frame
	    foreach(GameObject obj in sprites)
	    {
		obj.transform.localPosition = new Vector3(0f, -depth * .07f, 0f);
	    }
	}
    }


    


    // When we leave a water area
    void OnTriggerExit(Collider col)
    {
	if(col.gameObject.tag != "Bridge")
	{
	    // Remove this water from the object's list of depths
	    waterScripts.Remove(col.gameObject.GetComponent<Water>());
	
	    // Play a random small splash sound at random volume as oneshot (allows multiple sounds to play at same time)
	    audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));
		
	
	    // Set our depth to 0
	    depth = 0f;

	    // Tag to see if we are touching any water that is on
	    bool waterOn = false;
		
	    // If we are no longer touching water areas
	    if(waterScripts.Count < 1)
	    {
		// Make our sprites fully visible
		foreach(GameObject obj in sprites)
		{
		    obj.transform.localPosition = Vector3.zero;
		}
		    
		// Reset position of sprite mask to stop obscuring sprite
		spriteMask.transform.localPosition = new Vector3(0f, maskDefaultY, 0f);

		// Play a random small splash sound at random volume as oneshot (allows multiple sounds to play at same time)
		audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));

		// Create a few splash animations around the character, with random delays
		StartCoroutine(DrawSplash());
		StartCoroutine(DrawSplash());
		StartCoroutine(DrawSplash());
	    }
	    // We are still in water
	    else
	    {
		// Remove null entries from list
		waterScripts.RemoveAll(item => item == null);
			
		// Cycle through every water script we are touching
		foreach(Water thisScript in waterScripts)
		{
		    if(thisScript.on)
			waterOn = true;
			    
		    // Get the depth at this location from the script
		    float thisDepth = thisScript.GetDepth(transform.position);

		    // If this depth is less than our current depth, set it to our depth
		    if(thisDepth > depth)
			depth = thisDepth;

		    // Play a random small splash sound at random volume as oneshot (allows multiple sounds to play at same time)
		    audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));

		    // Create a few splash animations around the character, with random delays
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		}		    

		// If we are touching water that is on
		if(waterOn)
		{
		    if(fireScript)
			// Stop any burning that may be happening
			fireScript.heat = 0f;
		    if(fire && fire.activeSelf)
			fire.SetActive(false);
		    

		    // Raise the bottom of the sprite mask to obscure bottom of sprite
		    spriteMask.transform.localPosition = new Vector3(0f, maskDefaultY + .3f, 0f);
		}
	    }
	}
    }
    

    


    

    // Method that draws splash sprite animations around the character
    IEnumerator DrawSplash()
    {
	yield return new WaitForSeconds(Random.Range(0f, 1f));
					
	Instantiate(splashPrefab, new Vector3(transform.position.x + Random.Range(-1f, 1f), 0f, transform.position.z + Random.Range(-1f, 1f)), Quaternion.identity);
    }
}
