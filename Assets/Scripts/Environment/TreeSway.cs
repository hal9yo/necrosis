﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSway : MonoBehaviour
{
    float xPos;
    float yPos;
    float zPos;
    
    float xRot;
    float yRot;
    float zRot;
    

    float sway;

    float timer = 0f;

    // Bool to turn script off when we want it off
    public bool on = true;

    // How much to sway (when no wind)
    public float baseSwayAmount = 15f;
    // How much to sway based on current wind speed
    float swayAmount = 35f;
    
    // How fast to sway when no wind
    public float baseSpeed = 1f;
    // How fast to sway with current wind
    public float speed = 1f;

    // Reference to wind script, to make trees sway more and faster as wind picks up
    public Wind windScript;
    
    // Start is called before the first frame update
    void Start()
    {
	xPos = transform.position.x;
	yPos = transform.position.y;
	zPos = transform.position.z;
	
	xRot = transform.rotation.eulerAngles.x;
	yRot = transform.rotation.eulerAngles.y;
	zRot = transform.rotation.eulerAngles.z;
    }

    // Update is called once per frame
    void Update()
    {
	if(on && timer <= 0f)
	{
	    speed = 1f - windScript.strength * .02f;
	    swayAmount = baseSwayAmount + (windScript.strength);

	    // Make the z axis sway in the direction and strength of the wind's x (inverted) plus a random amount to make it sway and move (and vice versa)
	    float zSway = -windScript.direction.x * windScript.strength * .5f + Random.Range(-swayAmount, swayAmount) * Time.deltaTime;
	    float xSway = -windScript.direction.z * windScript.strength * .5f + Random.Range(-swayAmount, swayAmount) * Time.deltaTime;

	    //transform.position = new Vector3(xPos + sway * Time.deltaTime, yPos, zPos);
	    transform.rotation = Quaternion.Euler(new Vector3(xRot + xSway, yRot, zRot + zSway));
	    timer = Random.Range(.01f, speed);
	}
	else
	{
	    timer -= Time.deltaTime;
	}
    }
}
