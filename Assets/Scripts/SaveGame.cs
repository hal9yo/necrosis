﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class SaveGame : MonoBehaviour
{
    // Reference to our player's stats script
    public PlayerStats stats;
    public Inventory playerInv;

    // Every zombie type we can have
    public List<GameObject> zombieTypes;
    
    // Every enemy type we can have
    public List<GameObject> enemyTypes;

    // Every item in the game we have
    public List<GameObject> itemTypes;
    
    // Reference to the transform of the player to spawn zombies in the vicinity of
    public Transform playerTrans;

    // Turn this on if we want this script to load data when the level loads (basically any level except the first)
    public bool loadOnEnable;

    public bool loadZombsOnEnable;

    public Wind windScript;

    public FireRoot fireRoot;



    public float defaultMinFightDist = 2f;
    public float defaultMaxFightDist = 2.5f;
    public float defaultMeleeFightDist = 2.5f;



    string npcMeleeArmSpriteSheet = "NPC-Clothes";
    string npcArcherArmSpriteSheet = "NPC-Archer-Clothes";
    string npcFireArcherArmSpriteSheet = "NPC-FireArcher";

    

    // On enable script that tells the script to load data if this is not the first level (by assigning a boolean to tell it so)
    void OnEnable()
    {
	if(loadOnEnable)
	    Load();
	
	if(loadZombsOnEnable)
	    LoadZombsOnly();
    }




    
    public void Save(List<GameObject> zombList, List<GameObject> enemyList)
    {	
	// Create a binary formatter to convert variable to file data type
	BinaryFormatter binaryForm = new BinaryFormatter();

	// Create the file
	FileStream file = File.Create(Application.persistentDataPath + "/savegame.dat");

	// Create a save data class file
	SaveData data = new SaveData();

	// Copy each variable to the corresponding entry in the file
	data.level = stats.level;
	data.exp = stats.exp;
	data.maxWill = stats.maxWill;
	data.baseMaxWill = stats.baseMaxWill;
	data.willRegen = stats.willRegen;
	data.baseWillRegen = stats.baseWillRegen;
	data.maxWillAdd = stats.maxWillAdd;
	data.maxWillMultiplier = stats.maxWillMultiplier;
	data.area = stats.area;
	data.entrance = stats.entrance;


	int[] playerInvToSave = new int[playerInv.inventory.Count];

	// Go through each item type
	for(int ii = 0; ii < itemTypes.Count; ii++)
	{
	    // Get length of the name of the item at this point in the list
	    int length = itemTypes[ii].name.Length;

	    // Compare length and name of this item with the weapon of the zomb
	    if(playerInv.weapon && playerInv.weapon.name.Length >= length && playerInv.weapon.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerWeap = ii;
	    }

	    if(playerInv.armor && playerInv.armor.name.Length >= length && playerInv.armor.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerArm = ii;
	    }

	    if(playerInv.talisman && playerInv.talisman.name.Length >= length && playerInv.talisman.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerTal = ii;
	    }
		
	    if(playerInv.shield && playerInv.shield.name.Length >= length && playerInv.shield.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerShield = ii;
	    }

	    for(int iii = 0; iii < playerInv.inventory.Count; iii++)
	    {
		if(playerInv.inventory[iii].name.Length >= length && playerInv.inventory[iii].name.Substring(0, length) == itemTypes[ii].name)
		{
		    // Set the entry in the list to the number of that item
		    playerInvToSave[iii] = ii;
		}
	    }
	}

	// Set the variables in the data class to the ones we just created
	data.playerInv = playerInvToSave;
	
    
	// Make an array of integers and set it to the length of the zombie list we have been passed
	int[] zombies = new int[zombList.Count];
	// Do the same for two arrays of floats to hold the distance from each zombie to the player
	float[] zombieDistX = new float[zombList.Count];
	float[] zombieDistZ = new float[zombList.Count];

	int[] zombWeap = new int[zombList.Count];
	int[] zombArm = new int[zombList.Count];
	int[] zombTal = new int[zombList.Count];
	int[] zombShield = new int[zombList.Count];
	List<int>[] zombInventory = new List<int>[zombList.Count];
	
	// Convert each zombie type in the list to an integer in an array to be saved to the file
	// Go through each entry in the zombie list
	for(int i = 0; i < zombList.Count; i++)
	{
	    // If the zombie is currently alive
	    if(zombList[i].GetComponent<Health>().currHealth > 0f)
	    {
		// If it's an NPC save type as NPC and save inventory
		if(zombList[i].name.Substring(0, 3) == "NPC")
		{
		    zombies[i] = 2;
	    
		    // Grab a reference to the zombie's inventory
		    Inventory zombInv = zombList[i].transform.Find("Inventory").gameObject.GetComponent<Inventory>();
	    
		    // Go through each item type
		    for(int ii = 0; ii < itemTypes.Count; ii++)
		    {
			// Get length of the name of the item at this point in the list
			int length = itemTypes[ii].name.Length;

			// Compare length and name of this item with the weapon of the zomb
			if(zombInv.weapon && zombInv.weapon.name.Length >= length && zombInv.weapon.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombWeap[i] = ii;
			}

			if(zombInv.armor && zombInv.armor.name.Length >= length && zombInv.armor.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombArm[i] = ii;
			}

			if(zombInv.talisman && zombInv.talisman.name.Length >= length && zombInv.talisman.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombTal[i] = ii;
			}
		
			if(zombInv.shield && zombInv.shield.name.Length >= length && zombInv.shield.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombShield[i] = ii;
			}

			zombInventory[i] = new List<int>();
		
			for(int iii = 0; iii < zombInv.inventory.Count; iii++)
			{
			    if(zombInv.inventory[iii].name.Length >= length && zombInv.inventory[iii].name.Substring(0, length) == itemTypes[ii].name)
			    {
				// Set the entry in the list to the number of that item
				zombInventory[i].Add(ii);
			    }
			}
		    }
		}
		// If it's not an NPC save the type that it is
		else
		{
		    // Go through each entry in the zombietypes list
		    for(int ii = 0; ii < zombieTypes.Count; ii++)
		    {
			// Get the length of the name of the zombie type we are checking this zombie against
			int length = zombieTypes[ii].name.Length;

			// If this entry in the zombietypes list matches the entry in the zombie list
			if(zombList[i].name.Length >= length && zombList[i].name.Substring(0, length) == zombieTypes[ii].name)
			{
			    // Set this entry of the zombies array to this zombietype
			    zombies[i] = ii;
			    zombieDistX[i] = playerTrans.position.x - zombList[i].transform.position.x;
			    zombieDistZ[i] = playerTrans.position.z - zombList[i].transform.position.z;
			}
		    }
		}
	    }
	}
	
	// Set the arrays in the data class to the arrays we just created
	data.zombies = zombies;
	data.zombieDistX = zombieDistX;
	data.zombieDistZ = zombieDistZ;
	data.zombieWeap = zombWeap;
	data.zombieArm = zombArm;
	data.zombieTal = zombTal;
	data.zombieShield = zombShield;
	data.zombieInv = zombInventory;


	
	// Make an array of integers and set it to the length of the enemy list we have been passed
	int[] enemies = new int[enemyList.Count];
	// Do the same for two arrays of floats to hold the distance from each zombie to the player
	float[] enemyDistX = new float[enemyList.Count];
	float[] enemyDistZ = new float[enemyList.Count];

	// Make an array of lists for the enemy targets
	List<float>[] enemyTargsX = new List<float>[enemyList.Count];
	List<float>[] enemyTargsZ = new List<float>[enemyList.Count];
	
	// Convert each enemy type in the list to an integer in an array to be saved to the file
	// Go through each entry in the enemy list
	for(int i = 0; i < enemyList.Count; i++)
	{
	    // Go through each entry in the zombietypes list
	    for(int ii = 0; ii < enemyTypes.Count; ii++)
	    {
		// Get the length of the name of the zombie type we are checking this zombie against
		int length = enemyTypes[ii].name.Length;

		// If this entry in the enemy types list matches the entry in the zombie list
		if(enemyList[i].name.Length >= length && enemyList[i].name.Substring(0, length) == enemyTypes[ii].name)
		{
		    // Set this entry of the zombies array to this zombietype
		    enemies[i] = ii;
		}
	    }
	    // Set the position of this enemy, relative to the player
	    enemyDistX[i] = playerTrans.position.x - enemyList[i].transform.position.x;
	    enemyDistZ[i] = playerTrans.position.z - enemyList[i].transform.position.z;

	    // Get the targets of the enemy
	    NPCTargets enemyTargets = enemyList[i].transform.Find("Vision").gameObject.GetComponent<NPCTargets>();

	    // Initialize the lists for enemy targets for this unit
	    enemyTargsX[i] = new List<float>();
	    enemyTargsZ[i] = new List<float>();
	    
	    // Go through the enemy's targets
	    for(int ii = 0; ii < enemyTargets.targets.Count; ii++)
	    {
		//Debug.Log(enemyTargets.targets[ii].activeSelf);
		//Debug.Log(enemyTargets.targets[ii].transform.position.x + "    " + enemyTargets.targets[ii].transform.position.z);
		
		if(enemyTargets.targets[ii].activeSelf)
		{
		    //Debug.Log(enemyTargsX[i]);
		    //Debug.Log(playerTrans.position.x);
		    //Debug.Log(enemyTargets.targets[ii].transform.position.x);
		    enemyTargsX[i].Add(playerTrans.position.x - enemyTargets.targets[ii].transform.position.x);
		    enemyTargsZ[i].Add(playerTrans.position.z - enemyTargets.targets[ii].transform.position.z);
		}
		else
		{
		    enemyTargsX[i].Add(666f);
		    enemyTargsZ[i].Add(666f);
		}
		
	    }
	}
	
	// Set the arrays in the data class to the arrays we just created
	data.enemies = enemies;
	data.enemyDistX = enemyDistX;
	data.enemyDistZ = enemyDistZ;
	data.enemyTargetsX = enemyTargsX;
	data.enemyTargetsZ = enemyTargsZ;
	
	// Make data file format
	binaryForm.Serialize(file, data);
	// Close the file
	file.Close();

	// Print debug info to let us know
	Debug.Log("Game saved.");
    }

	

    public void Load()
    {
	if(File.Exists(Application.persistentDataPath + "/savegame.dat"))
	{
	    BinaryFormatter binaryForm = new BinaryFormatter();
	    FileStream file = File.Open(Application.persistentDataPath + "/savegame.dat", FileMode.Open);
	    SaveData data = (SaveData)binaryForm.Deserialize(file);
	    file.Close();

	    stats.level = data.level;
	    stats.exp = data.exp;
	    stats.maxWill = data.maxWill;
	    stats.baseMaxWill = data.baseMaxWill;
	    stats.willRegen = data.willRegen;
	    stats.baseWillRegen = data.baseWillRegen;
	    stats.maxWillAdd = data.maxWillAdd;
	    stats.maxWillMultiplier = data.maxWillMultiplier;
	    stats.area = data.area;
	    stats.entrance = data.entrance;

	    int[] playerInvToLoad = new int[data.playerInv.Length];
	    Array.Copy(data.playerInv, playerInvToLoad, data.playerInv.Length);

	    // If player had a weapon
	    if(data.playerWeap > 0)
	    {
		// Create an item of that type
		GameObject weapon = Instantiate(itemTypes[data.playerWeap], playerTrans.position, playerTrans.rotation);

		// Equip the item so it changes their stats
		EquipThis(weapon, playerTrans.gameObject, playerInv);

		weapon.SetActive(false);
	    }

	    // If zombie had an armor
	    if(data.playerArm > 0)   
	    {
		GameObject armor = Instantiate(itemTypes[data.playerArm], playerTrans.position, playerTrans.rotation);

		EquipThis(armor, playerTrans.gameObject, playerInv);

		armor.SetActive(false);
	    }

	    if(data.playerTal > 0)
	    {
		GameObject talisman = Instantiate(itemTypes[data.playerTal], playerTrans.position, playerTrans.rotation);
		    
		EquipThis(talisman, playerTrans.gameObject, playerInv);

		talisman.SetActive(false);
	    }

	    if(data.playerShield > 0)
	    {
		GameObject shield = Instantiate(itemTypes[data.playerShield], playerTrans.position, playerTrans.rotation);
		    
		EquipThis(shield, playerTrans.gameObject, playerInv);

		shield.SetActive(false);
	    }
		

	    // Add items from inventory back to inventory
	    for(int ii = 0; ii < playerInvToLoad.Length; ii++)
	    {
		playerInv.inventory.Add(Instantiate(itemTypes[playerInvToLoad[ii]], playerTrans.position, playerTrans.rotation));
			
		playerInv.inventory[ii].SetActive(false);
	    }
	
	    
	    // Load the zombies that we exited the previous level with, if any, into a new array
	    int[] zombies = new int[data.zombies.Length];
	    Array.Copy(data.zombies, zombies, data.zombies.Length);

	    // Load the zombies distances from the player into an array
	    float[] zombieDistX = new float[data.zombieDistX.Length];
	    Array.Copy(data.zombieDistX, zombieDistX, data.zombieDistX.Length);
	    float[] zombieDistZ = new float[data.zombieDistZ.Length];
	    Array.Copy(data.zombieDistZ, zombieDistZ, data.zombieDistZ.Length);

	    // Load the zombies' inventories
	    int[] zombWeap = new int[zombies.Length];
	    Array.Copy(data.zombieWeap, zombWeap, data.zombieWeap.Length);

	    int[] zombArm = new int[zombies.Length];
	    Array.Copy(data.zombieArm, zombArm, data.zombieArm.Length);
	    
	    int[] zombTal = new int[zombies.Length];
	    Array.Copy(data.zombieTal, zombTal, data.zombieTal.Length);

	    int[] zombShield = new int[zombies.Length];
	    Array.Copy(data.zombieShield, zombShield, data.zombieShield.Length);
	    
	    List<int>[] zombInv = new List<int>[zombies.Length];
	    Array.Copy(data.zombieInv, zombInv, data.zombieInv.Length);

	    
	    // Cycle through the array we just made
	    for(int i = 0; i < zombies.Length; i++)
	    {
		// Create a zombie, of the given type, in the level
		GameObject zombie = Instantiate(zombieTypes[zombies[i]], new Vector3(playerTrans.position.x - zombieDistX[i], 1.5f, playerTrans.position.z - zombieDistZ[i]), playerTrans.rotation);

		zombie.GetComponent<WindPush>().windScript = windScript;

		
		if(zombie.name.Substring(0, 3) == "NPC")
		{
		    zombie.GetComponent<ArcherAttack>().fireRoot = fireRoot;

		    Inventory invScript = zombie.transform.Find("Inventory").gameObject.GetComponent<Inventory>();

		    // Equip the items that the zombie had

		    // If zombie had a weapon
		    if(zombWeap[i] > 0)
		    {
			// Create an item of that type
			GameObject weapon = Instantiate(itemTypes[data.zombieWeap[i]], zombie.transform.position, zombie.transform.rotation);

			// Change name of this character
			zombie.name = "NPC w " + weapon.name;
			
			// Equip the item so it changes their stats
			EquipThis(weapon, zombie, invScript);
		    }

		    // If zombie had an armor
		    if(zombArm[i] > 0)   
		    {
			GameObject armor = Instantiate(itemTypes[data.zombieArm[i]], zombie.transform.position, zombie.transform.rotation);

			EquipThis(armor, zombie, invScript);
		    }

		    if(zombTal[i] > 0)
		    {
			GameObject talisman = Instantiate(itemTypes[data.zombieTal[i]], zombie.transform.position, zombie.transform.rotation);
		    
			EquipThis(talisman, zombie, invScript);
		    }

		    if(zombShield[i] > 0)
		    {
			GameObject shield = Instantiate(itemTypes[data.zombieShield[i]], zombie.transform.position, zombie.transform.rotation);
		    
			EquipThis(shield, zombie, invScript);
		    }
		
		    // Turn off gameobjects of equiped items so they don't react to physics
		    if(invScript.weapon)
			invScript.weapon.SetActive(false);
		    if(invScript.armor)
			invScript.armor.SetActive(false);
		    if(invScript.talisman)
			invScript.talisman.SetActive(false);
		    if(invScript.shield)
			invScript.shield.SetActive(false);

		    // Add items from inventory back to inventory
		    for(int ii = 0; ii < zombInv[i].Count; ii++)
		    {
			invScript.inventory.Add(Instantiate(itemTypes[zombInv[i][ii]], zombie.transform.position, zombie.transform.rotation));
			
			invScript.inventory[ii].SetActive(false);
		    }
		}
	    }


	    // Load the enemies that we exited the previous level with, if any, into a new array
	    int[] enemies = new int[data.enemies.Length];
	    Array.Copy(data.enemies, enemies, data.enemies.Length);

	    // Load the enemies distances from the player into an array
	    float[] enemyDistX = new float[data.enemyDistX.Length];
	    Array.Copy(data.enemyDistX, enemyDistX, data.enemyDistX.Length);

	    float[] enemyDistZ = new float[data.enemyDistZ.Length];
	    Array.Copy(data.enemyDistZ, enemyDistZ, data.enemyDistZ.Length);

	    // Make an array of lists for the enemy targets
	    List<float>[] enemyTargsX = new List<float>[data.enemyTargetsX.Length];
	    Array.Copy(data.enemyTargetsX, enemyTargsX, data.enemyTargetsX.Length);

	    List<float>[] enemyTargsZ = new List<float>[data.enemyTargetsZ.Length];
	    Array.Copy(data.enemyTargetsZ, enemyTargsZ, data.enemyTargetsZ.Length);
	    
	    // Cycle through the array we just made
	    for(int i = 0; i < enemies.Length; i++)
	    {
		// Create a zombie, of the given type, in the level
		GameObject npc = Instantiate(enemyTypes[enemies[i]], new Vector3(playerTrans.position.x - enemyDistX[i], 1.5f, playerTrans.position.z - enemyDistZ[i]), playerTrans.rotation);

		npc.GetComponent<WindPush>().windScript = windScript;

		NPCTargets enemyTargets = npc.transform.Find("Vision").gameObject.GetComponent<NPCTargets>();

		for(int ii = 0; ii < enemyTargets.targets.Count; ii++)
		{
		    // Make sure this target was being used (666 + 666 is our code for not used)
		    if(!(enemyTargsX[i][ii] == 666f && enemyTargsZ[i][ii] == 666f))
		    {
			// Turn on target
			enemyTargets.targets[ii].SetActive(true);

			// Set targets position to saved position
			enemyTargets.targets[ii].transform.position = new Vector3(enemyTargsX[i][ii], 1f, enemyTargsZ[i][ii]);
		    }
		}
	    }
	    
	    
	    Debug.Log("Game loaded.");
	}
	else
	    Debug.Log("No save data to load!");
    }



    

    public void SaveZombsOnly(GameObject[] zombList)
    {	
	// Create a binary formatter to convert variable to file data type
	BinaryFormatter binaryForm = new BinaryFormatter();

	// Create the file
	FileStream file = File.Create(Application.persistentDataPath + "/savearenagame.dat");

	// Create a save data class file
	SaveData data = new SaveData();

	// Copy each variable to the corresponding entry in the file
	data.level = stats.level;
	data.exp = stats.exp;
	data.maxWill = stats.maxWill;
	data.baseMaxWill = stats.baseMaxWill;
	data.willRegen = stats.willRegen;
	data.baseWillRegen = stats.baseWillRegen;
	data.maxWillAdd = stats.maxWillAdd;
	data.maxWillMultiplier = stats.maxWillMultiplier;
	data.area = stats.area;

	// Save the player's inventory and equiped items
	int[] playerInvToSave = new int[playerInv.inventory.Count];

	// Go through each item type
	for(int ii = 0; ii < itemTypes.Count; ii++)
	{
	    // Get length of the name of the item at this point in the list
	    int length = itemTypes[ii].name.Length;

	    // Compare length and name of this item with the weapon of the zomb
	    if(playerInv.weapon && playerInv.weapon.name.Length >= length && playerInv.weapon.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerWeap = ii;
	    }

	    if(playerInv.armor && playerInv.armor.name.Length >= length && playerInv.armor.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerArm = ii;
	    }

	    if(playerInv.talisman && playerInv.talisman.name.Length >= length && playerInv.talisman.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerTal = ii;
	    }
		
	    if(playerInv.shield && playerInv.shield.name.Length >= length && playerInv.shield.name.Substring(0, length) == itemTypes[ii].name)
	    {
		// Set the entry in the list to the number of that item
		data.playerShield = ii;
	    }

	    for(int iii = 0; iii < playerInv.inventory.Count; iii++)
	    {
		if(playerInv.inventory[iii].name.Length >= length && playerInv.inventory[iii].name.Substring(0, length) == itemTypes[ii].name)
		{
		    // Set the entry in the list to the number of that item
		    playerInvToSave[iii] = ii;
		}
	    }
	}

	// Set the inventory array in the data class to the one we just created
	data.playerInv = playerInvToSave;

	
	// Make an array of integers and set it to the length of the zombie list we have been passed
	int[] zombies = new int[zombList.Length];
	// Do the same for two arrays of floats to hold the distance from each zombie to the player
	float[] zombieDistX = new float[zombList.Length];
	float[] zombieDistZ = new float[zombList.Length];

	int[] zombWeap = new int[zombList.Length];
	int[] zombArm = new int[zombList.Length];
	int[] zombTal = new int[zombList.Length];
	int[] zombShield = new int[zombList.Length];
	List<int>[] zombInventory = new List<int>[zombList.Length];
	
	// Convert each zombie type in the list to an integer in an array to be saved to the file
	// Go through each entry in the zombie list
	for(int i = 0; i < zombList.Length; i++)
	{
	    // If the zombie is currently alive
	    if(zombList[i].GetComponent<Health>().currHealth > 0f)
	    {
		// If it's an NPC save type as NPC and save inventory
		if(zombList[i].name.Substring(0, 3) == "NPC")
		{
		    zombies[i] = 2;
	    
		    // Grab a reference to the zombie's inventory
		    Inventory zombInv = zombList[i].transform.Find("Inventory").gameObject.GetComponent<Inventory>();
	    
		    // Go through each item this zombie is carrying
		    for(int ii = 0; ii < itemTypes.Count; ii++)
		    {
			// Get length of the name of the item at this point in the list
			int length = itemTypes[ii].name.Length;

			// Compare length and name of this item with the weapon of the zomb
			if(zombInv.weapon && zombInv.weapon.name.Length >= length && zombInv.weapon.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombWeap[i] = ii;
			}

			if(zombInv.armor && zombInv.armor.name.Length >= length && zombInv.armor.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombArm[i] = ii;
			}

			if(zombInv.talisman && zombInv.talisman.name.Length >= length && zombInv.talisman.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombTal[i] = ii;
			}
		
			if(zombInv.shield && zombInv.shield.name.Length >= length && zombInv.shield.name.Substring(0, length) == itemTypes[ii].name)
			{
			    // Set the entry in the list to the number of that item
			    zombShield[i] = ii;
			}

			zombInventory[i] = new List<int>();
		
			for(int iii = 0; iii < zombInv.inventory.Count; iii++)
			{
			    if(zombInv.inventory[iii].name.Length >= length && zombInv.inventory[iii].name.Substring(0, length) == itemTypes[ii].name)
			    {
				// Set the entry in the list to the number of that item
				zombInventory[i].Add(ii);
			    }
			}
		    }
		}
		// If it's not an NPC save the type that it is
		else
		{
		    // Go through each entry in the zombietypes list
		    for(int ii = 0; ii < zombieTypes.Count; ii++)
		    {
			// Get the length of the name of the zombie type we are checking this zombie against
			int length = zombieTypes[ii].name.Length;

			// If this entry in the zombietypes list matches the entry in the zombie list
			if(zombList[i].name.Length >= length && zombList[i].name.Substring(0, length) == zombieTypes[ii].name)
			{
			    // Set this entry of the zombies array to this zombietype
			    zombies[i] = ii;
			    zombieDistX[i] = playerTrans.position.x - zombList[i].transform.position.x;
			    zombieDistZ[i] = playerTrans.position.z - zombList[i].transform.position.z;
			}
		    }
		}
	    }
	}
	
	// Set the arrays in the data class to the arrays we just created
	data.zombies = zombies;
	data.zombieDistX = zombieDistX;
	data.zombieDistZ = zombieDistZ;
	data.zombieWeap = zombWeap;
	data.zombieArm = zombArm;
	data.zombieTal = zombTal;
	data.zombieShield = zombShield;
	data.zombieInv = zombInventory;

		
	// Make data file format
	binaryForm.Serialize(file, data);
	
	// Close the file
	file.Close();


	// Print debug info to let us know
	Debug.Log("Game saved.");
    }



    public void LoadZombsOnly()
    {	
	if(File.Exists(Application.persistentDataPath + "/savearenagame.dat"))
	{
	    
	    BinaryFormatter binaryForm = new BinaryFormatter();
	    FileStream file = File.Open(Application.persistentDataPath + "/savegame.dat", FileMode.Open);
	    SaveData data = (SaveData)binaryForm.Deserialize(file);
	    file.Close();
	
	    stats.level = data.level;
	    stats.exp = data.exp;
	    stats.maxWill = data.maxWill;
	    stats.baseMaxWill = data.baseMaxWill;
	    stats.willRegen = data.willRegen;
	    stats.baseWillRegen = data.baseWillRegen;
	    stats.maxWillAdd = data.maxWillAdd;
	    stats.maxWillMultiplier = data.maxWillMultiplier;
	    stats.area = data.area;
	    
	    // Load the zombies that we exited the previous level with, if any, into a new array
	    int[] zombies = new int[data.zombies.Length];
	    Array.Copy(data.zombies, zombies, data.zombies.Length);

	    // Load the zombies' inventories
	    int[] zombWeap = new int[zombies.Length];
	    Array.Copy(data.zombieWeap, zombWeap, data.zombieWeap.Length);

	    int[] zombArm = new int[zombies.Length];
	    Array.Copy(data.zombieArm, zombArm, data.zombieArm.Length);
	    
	    int[] zombTal = new int[zombies.Length];
	    Array.Copy(data.zombieTal, zombTal, data.zombieTal.Length);

	    int[] zombShield = new int[zombies.Length];
	    Array.Copy(data.zombieShield, zombShield, data.zombieShield.Length);
	    
	    List<int>[] zombInv = new List<int>[zombies.Length];
	    Array.Copy(data.zombieInv, zombInv, data.zombieInv.Length);

			    
	    // Cycle through the array we just made
	    for(int i = 0; i < zombies.Length; i++)
	    {
		float zombieDistX = (-3 + (int)(i/8)) * 2f;
		float zombieDistZ = (-3 + (int)(i%8)) * 2f;
		    
		// Create a zombie, of the given type, in the level
		GameObject zombie = Instantiate(zombieTypes[zombies[i]], new Vector3(playerTrans.position.x - zombieDistX, 1.5f, playerTrans.position.z - zombieDistZ), playerTrans.rotation);

		zombie.GetComponent<WindPush>().windScript = windScript;

		if(zombie.name.Substring(0, 3) == "NPC")
		{
		    zombie.GetComponent<ArcherAttack>().fireRoot = fireRoot;

		    Inventory invScript = zombie.transform.Find("Inventory").gameObject.GetComponent<Inventory>();

		    // Equip the items that the zombie had

		    // If zombie had a weapon
		    if(zombWeap[i] > 0)
		    {
			// Create an item of that type
			GameObject weapon = Instantiate(itemTypes[data.zombieWeap[i]], zombie.transform.position, zombie.transform.rotation);

			// Change name of this character
			zombie.name = "NPC w " + weapon.name;
			
			// Equip the item so it changes their stats
			EquipThis(weapon, zombie, invScript);
		    }

		    // If zombie had an armor
		    if(zombArm[i] > 0)   
		    {
			GameObject armor = Instantiate(itemTypes[data.zombieArm[i]], zombie.transform.position, zombie.transform.rotation);

			EquipThis(armor, zombie, invScript);
		    }

		    if(zombTal[i] > 0)
		    {
			GameObject talisman = Instantiate(itemTypes[data.zombieTal[i]], zombie.transform.position, zombie.transform.rotation);
		    
			EquipThis(talisman, zombie, invScript);
		    }

		    if(zombShield[i] > 0)
		    {
			GameObject shield = Instantiate(itemTypes[data.zombieShield[i]], zombie.transform.position, zombie.transform.rotation);
		    
			EquipThis(shield, zombie, invScript);
		    }
		
		    // Turn off gameobjects of equiped items so they don't react to physics
		    if(invScript.weapon)
			invScript.weapon.SetActive(false);
		    if(invScript.armor)
			invScript.armor.SetActive(false);
		    if(invScript.talisman)
			invScript.talisman.SetActive(false);
		    if(invScript.shield)
			invScript.shield.SetActive(false);

		    // Add items from inventory back to inventory
		    for(int ii = 0; ii < zombInv[i].Count; ii++)
		    {
			invScript.inventory.Add(Instantiate(itemTypes[zombInv[i][ii]], zombie.transform.position, zombie.transform.rotation));
			
			invScript.inventory[ii].SetActive(false);
		    }
		}
	    }
	    
	    
	    Debug.Log("Game loaded.");
	}
	else
	    Debug.Log("No save data to load!");
    }




    
    public void EquipThis(GameObject toEquip, GameObject character, Inventory charInvScript)
    {
	// If item is a Weapon
	if(toEquip.tag == "Weapon")
	{
	    Weapon weapScript;
	    Attack charAttack;
	    PlayerAttack playAttack;
	    ArcherAttack charArchAttack;
	    Stats statScript;
	    List<Animator> animators;
	    Armor armScript;
	    NPCFighting fightScript;
	    Zombie zombScript;



	    // Add the Weapon to the Equiped Weapon slot of the character
	    charInvScript.weapon = toEquip;

	    // Get reference to item's Weapon script
	    weapScript = toEquip.GetComponent<Weapon>();
	    
	    // Get reference to charater's stats
	    charAttack = character.GetComponent<Attack>();
	    playAttack = character.GetComponent<PlayerAttack>();
	    charArchAttack = character.GetComponent<ArcherAttack>();
	    statScript = character.GetComponent<Stats>();
	    
	    animators = character.GetComponent<Health>().animators;

	    // Set the block chance to that of the weapon
	    statScript.block += weapScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= weapScript.walkSpeedLess;
	    statScript.runSpeed -= weapScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power += weapScript.power;
	    
	    // Run any special script, this weapon may have, on the character
	    weapScript.Equip(character);

	    // Set archer attack distance to the max distance of the weapon
	    if(charArchAttack)
	    {
		charArchAttack.shootDistance = weapScript.maxDistance;
	    }
	    
	    // Multiply character's attack animation speed by that of the weapon
	    // Set animator controller's to those of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * weapScript.attackSpeed);
	    }

	    
	    //Change the character's Stats to those of the weapon
	    if(charAttack)
	    {
		// Set player's attack stats to those of the weapon
		charAttack.attackDistance = weapScript.attackDistance;
		charAttack.damageMin = weapScript.damageMin;
		charAttack.damageMax = weapScript.damageMax;

		// Set character's weapon sprite to that of the weapon
		animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = weapScript.npcWeapSpriteSheet;
		

		// Change the distances the character keeps during fights to those of the weapon
		fightScript = character.GetComponent<NPCFighting>();
		if(fightScript)
		{
		    fightScript.maxDistance = weapScript.maxDistance;
		    fightScript.minDistance = weapScript.minDistance;
		    fightScript.meleeDistance = weapScript.meleeDistance;
		}

		// Change the distances the character keeps during fights to those of the weapon
		zombScript = character.GetComponent<Zombie>();
		if(zombScript)
		{
		    zombScript.maxDistance = weapScript.maxDistance;
		    zombScript.meleeDistance = weapScript.meleeDistance;
		}

		
		// If there is an armor equiped
		if(charInvScript.armor)
		{
		    // Reference the armor script on the item
		    armScript = charInvScript.armor.GetComponent<Armor>();

		    // Set character's armor sprite to that of the armor, based on weapon
		    if(weapScript.type == "melee")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombMeleeArmSpriteSheet;
		    }
		    else if(weapScript.type == "bow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombArcherArmSpriteSheet;
		    }
		    else if(weapScript.type == "firebow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcFireArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombFireArcherArmSpriteSheet;
		    }
		}
		// No armor equiped
		else
		{
		    // Set character's armor sprite to the default clothes, based on weapon
		    if(weapScript.type == "melee")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcMeleeArmSpriteSheet;
		    else if(weapScript.type == "bow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcArcherArmSpriteSheet;
		    else if(weapScript.type == "firebow")
			animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = npcFireArcherArmSpriteSheet;		    
		}
	    }
	    if(playAttack)
	    {
		// Set player's attack stats to those of the weapon
		playAttack.attackDistance = weapScript.attackDistance;
		playAttack.damageMin = weapScript.damageMin;
		playAttack.damageMax = weapScript.damageMax;
		
		// Set player's weapon sprite to that of the weapon
		animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = weapScript.playerWeapSpriteSheet;
	    }
	}





	

	// If item is shield
	else if(toEquip.tag == "Shield")
	{
	    Shield shieldScript;
	    Stats statScript;
	    List<Animator> animators;

	    
	    // Add the Shield to the Equiped Shield slot of the character
	    charInvScript.shield = toEquip;

	    // Get reference to item's Shield script
	    shieldScript = toEquip.GetComponent<Shield>();

	    Debug.Log(toEquip.GetComponent<Shield>());
	    
	    // Get reference to charater's stats
	    statScript = character.GetComponent<Stats>();
	    
	    animators = character.GetComponent<Health>().animators;

	    // Multiply character's attack animation speed by that of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * shieldScript.attackSpeed);
		anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") * shieldScript.attackSpeed);
	    }

	    
	    // Set the block chance to that of the weapon
	    statScript.block += shieldScript.block;
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= shieldScript.walkSpeed;
	    statScript.runSpeed -= shieldScript.runSpeed;
	    // Adjust the character's power level
	    statScript.power += shieldScript.power;
		
	    // Run any special script, this weapon may have, on the character
	    shieldScript.Equip(character);

	    // Turn on weapon sprite for player or NPC based on script they have
	    Attack charAttack = character.GetComponent<Attack>();
	    PlayerAttack playAttack = character.GetComponent<PlayerAttack>();

	    if(charAttack)
		animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = shieldScript.npcShieldSprite;		
	    if(playAttack)
		animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = shieldScript.playerShieldSprite;
	}



	
	
	
	// If item is Armor
	else if(toEquip.tag == "Armor")
	{
	    Armor armScript;
	    Attack charAttack;
	    PlayerAttack playAttack;
	    Health healthScript;
	    Stats statScript;
	    List<Animator> animators;
	    Weapon weapScript;
	    
	    
	    // Add the Armor to the Equiped Armor slot of the character
	    charInvScript.armor = toEquip;


	    // Reference the armor script on the item
	    armScript = toEquip.GetComponent<Armor>();
	    
	    // Reference attack script on character (just to check if player or npc)
	    charAttack = character.GetComponent<Attack>();
	    playAttack = character.GetComponent<PlayerAttack>();

	    // Reference character's health script to change armor amount
	    healthScript = character.GetComponent<Health>();

	    // Refernce character's Stats script to change walk and run speeds
	    statScript = character.GetComponent<Stats>();
		    
	    // Reference character's animators to 
	    animators = healthScript.animators;


	    // Multiply character's attack animation speed by that of the weapon
	    foreach(Animator anim in animators)
	    {
		anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") * armScript.attackSpeedMultiplier);
		anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") * armScript.attackSpeedMultiplier);
	    }

	    // Set character's Health Armor Stat to that of this Armor
	    healthScript.armor += armScript.armorAmount;
		
	    // Set walk and run speeds less amount this weapon hinders movement
	    statScript.walkSpeed -= armScript.walkSpeedLess;
	    statScript.runSpeed -= armScript.runSpeedLess;
	    // Adjust the character's power level
	    statScript.power += armScript.power;
		
	    //Change the character's Stats to those of the armor
	    if(charAttack)
	    {		
		// If a weapon is equipped to character
		if(charInvScript.weapon)
		{
		    // Get reference to character's Weapon script
		    weapScript = charInvScript.weapon.GetComponent<Weapon>();

		    Debug.Log(weapScript.type);
		    
		    // Set character's armor sprite to that of the armor, based on weapon
		    if(weapScript.type == "melee")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombMeleeArmSpriteSheet;
		    }
		    else if(weapScript.type == "bow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombArcherArmSpriteSheet;
		    }
		    else if(weapScript.type == "firebow")
		    {
			if(character.tag != "PlayerUnit")
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcFireArcherArmSpriteSheet;
			else
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.zombFireArcherArmSpriteSheet;
		    }
		    Debug.Log(animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName);
		}
		// If character has no weapon equipped
		else
		{
		    // Use melee version of armor
		    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
		    Debug.Log(animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName);
		}
	    }
	    if(playAttack)
	    {		
		// Set player's weapon sprite to that of the weapon
		animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.playerArmSpriteSheet;
	    }
	}





	

	// If item is a Talisman
	else if(toEquip.tag == "Talisman")
	{
	    Talisman talScript;
	    
	    
	    // Add the Talisman to the Equiped Talisman slot of the character
	    charInvScript.talisman = toEquip;

	    // Get reference to item's Talisman script
	    talScript = toEquip.GetComponent<Talisman>();

	    Debug.Log(talScript);
	    
	    // Run the equip method from the Talisman Script
	    talScript.Equip(character);
	}
    }
}



// This is a data type designed to store the variables we want to save between levels and gameplay sessions
[Serializable]
class SaveData
{
    public int level;
    public int exp;
    
    public int maxWill;
    public int baseMaxWill;
    public int willRegen;
    public int baseWillRegen;
    public float maxWillAdd;
    public float maxWillMultiplier;

    public int playerWeap;
    public int playerArm;
    public int playerTal;
    public int playerShield;
    public int[] playerInv;
    
    public int area;
    public int entrance;
    
    public int[] zombies;
    public float[] zombieDistX;
    public float[] zombieDistZ;
    public int[] zombieWeap;
    public int[] zombieArm;
    public int[] zombieTal;
    public int[] zombieShield;
    public List<int>[] zombieInv;
    
    public int[] enemies;
    public float[] enemyDistX;
    public float[] enemyDistZ;
    public List<float>[] enemyTargetsX;
    public List<float>[] enemyTargetsZ;
    //public int[] enemyWeap;
    //public int[] enemyArm;
    //public int[] enemyTal;
    //public List<int>[] enemyInv;
}
