﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blood : MonoBehaviour
{
    //Speed blood is squirted at
    public Vector3 velocity = new Vector3(0f, 0f, 0f);

    //Reference to the  blood's rigidbody to effect physics upon
    private Rigidbody rb;
    
    //Reference to the player squirting the blood
    public GameObject player;

    // How long is the blood
    public float length;

    // How wide is the blood
    public float width;

    // Material to render the blood in (essentially a color)
    public Material bloodMat;

    // Countdown timer to fade out blood after a while
    public float timeToLive = 2f;

    //Timer for how often to run the code, to save cpu as this code is potentially run on many gameobjects simultaneously
    float timer = 0f;
    
    // How often to update blood after it has hit the ground
    public float howOften = 2f;

    // The number the timer starts at
    float maxTime;

    // Destroy if not on ground by this time
    public float destroyIdleTime = 3f;
    
    // A line renderer to draw the blood as it flies through the air
    LineRenderer linerenderer;


    // Prefab of mini blood objects that don't move and just sit whereever they land and fade out
    public GameObject splatPrefab;

    BoxCollider col;
    
    void Start()
    {
	// Make reference to rigidbody component
	rb = GetComponent<Rigidbody>();

	// Make reference to collider
	col = GetComponent<BoxCollider>();

	// Make reference to the actual line renderer component, and initialize the values
	linerenderer = GetComponent<LineRenderer>();
	linerenderer.material = bloodMat;
	linerenderer.startWidth = width;
	linerenderer.endWidth = width;

	maxTime = timeToLive;
    }
    void Update()
    {
	// Get the current velocity from the rigidbody
	velocity = rb.velocity;

	// Decrement idle timer
	destroyIdleTime -= Time.deltaTime;

	// If we idled too long (hasn't hit ground yet, probably fell off level geometry and just falling forever)
	if(destroyIdleTime <= 0f)
	    Destroy(gameObject);

	// Update the line renderer to the current position and angle
	DrawBlood(transform.position, transform.position - velocity * length);

	// Update timer of how often to run miscelaneous code (not very time sensitive, don't run often
	timer += Time.deltaTime;

	// If it's time to run the code again
	if(timer > howOften)
	{
	    // Increment timer to fade out
	    timeToLive -= timer;

	    // Reset timer
	    timer = 0f;

	    // Adjust width of blood line narrower as time fades
	    linerenderer.startWidth = width * timeToLive / maxTime;
	    linerenderer.endWidth = width * timeToLive / maxTime;

	    // Destroy once time to live is reached
	    if(timeToLive <= 0f)
		Destroy(gameObject);
	}
    }

    void LateUpdate()
    {
	Vector3 newPosition = transform.position + velocity * Time.deltaTime;

	transform.LookAt(Quaternion.AngleAxis(-45, Vector3.up) *  newPosition + transform.position);
    }

    void OnCollisionEnter(Collision collision)
    {	
	//Ignore gameobject that shot the arrow and triggers
	if(GameObject.ReferenceEquals(player, collision.gameObject))
	{
	    Debug.Log("hit self");
	    return;
	}
	
	//Ignore trigger colliders
	if(collision.collider.isTrigger)
	    return;

	//When impacting the ground, disable rigidbody and collider so it stays put
	if(collision.gameObject.tag == "Ground")
	{
	    // Flatten vertical velocity to draw blood along ground
	    Vector3 groundVelocity = new Vector3(velocity.x, 0f, velocity.z);
	    
	    // Update the line renderer to the current position and angle
	    DrawSplat(transform.position, transform.position - groundVelocity * length);
	}
    }
    

    //Draw blood droplets in the air, they will be erased after .05 seconds
    void DrawBlood(Vector3 start, Vector3 end, float duration = 0.05f)
    {
	linerenderer.transform.position = start;
	linerenderer.SetPosition(0, start);
	linerenderer.SetPosition(1, end);
    }

    //Draw blood droplets in the air, they will be erased after .05 seconds
    void DrawSplat(Vector3 start, Vector3 end, float duration = 0.05f)
    {
	// Create a new blood splat at this location
	GameObject splat = Instantiate(splatPrefab, transform.position, Quaternion.identity);
	LineRenderer linerender = splat.GetComponent<LineRenderer>();

	// Set the values of the linerenderer of the splat object
	linerender.material = bloodMat;
	linerender.startWidth = width;
	linerender.endWidth = width;
	linerender.transform.position = start;
	linerender.SetPosition(0, start);
	linerender.SetPosition(1, end);
    }
}
