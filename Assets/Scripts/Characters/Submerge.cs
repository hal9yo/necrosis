﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.AI;

public class Submerge : MonoBehaviour
{
    // Reference to the character's sprites that will be partially obscured by the water
    public List<GameObject> sprites;

    // Reference to character's sprite animators
    public List<Animator> animators;

    // Reference to the sprite mask that obscures the sprites
    public GameObject spriteMask;

    // Reference to the character's stats so speed can be adjusted in water
    public Stats stats;

    // The current depth the character is submerged to
    public float depth = 0;

    // Our capsule collider (make it shorter when we fall in the water
    public CapsuleCollider capCol;

    // Reference to the character's zombie script
    public Zombie zombieScript;

    // Reference to our health script
    public Health healthScript;

    // A list of water objects we are currently colliding with to get depths
    public List<Water> waterScripts;

    public List<GameObject> bridges;
    

    // Splash animation prefab
    public GameObject splashPrefab;
    
    // Audio Source to play water sound effects
    public AudioSource audioSource;

    // Audio Source to play voice sound effects (sputtering when drowning
    public AudioSource voiceSource;

    // Boolean to track if we are playing a voice sound effect or not
    bool voicePlaying;
    
    // Array of audio clips when splashing in water
    public AudioClip[] bigSplashSounds;
    public AudioClip[] smallSplashSounds;
    public AudioClip[] drowningSounds;
    public AudioClip[] drownedSounds;
    public AudioClip[] gaspingSounds;
    

    public NavMeshAgent navAgent;
    public Rigidbody rb;
    
    // Timer for how often to bob and rotate when dead
    float timer = 0f;

    // Set to true when dead and should float in water
    bool floating = false;

    float xPos;
    float yPos;
    float zPos;
    
    float xRot;
    float yRot;
    float zRot;



    
    // When we first enter a water area
    void OnTriggerEnter(Collider col)
    {
	if(col.gameObject.tag == "Bridge")
	{
	    // If we aren't already in water, put us on the bridge
	    if(bridges.Count > 0 || waterScripts.Count < 1)
	    {
		// Add to our list of bridges
		bridges.Add(col.gameObject);
	    }
	}
	else
	{
	    // Unless we are already dead and floating on the surface
	    if(!floating)
	    {
		// Add this water object to our list of Water scripts for depths
		waterScripts.Add(col.gameObject.GetComponent<Water>());

		bool waterOn = false;

		// Remove null entries from Water Script List
		waterScripts.RemoveAll(item => item == null);
		
		foreach(Water wScript in waterScripts)
		{
		    if(wScript.on)
			waterOn = true;
		}
		
		// IF we are not on a bridge
		if(waterOn && bridges.Count < 1)
		{
		    // Did we just enter the water?
		    if(waterScripts.Count < 2)
		    {
			// Play a random big splash sound at random volume as oneshot (allows multiple sounds to play at same time)
			audioSource.PlayOneShot(bigSplashSounds[Random.Range(0, bigSplashSounds.Length)], Random.Range(0.5f, 1f));

			// Create a few splash animations around the character, with random delays
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
		    }

		    // Create a few splash animations around the character, with random delays
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		    StartCoroutine(DrawSplash());
		}


	    }
	}
    }




    
    // If we are currently touching water areas
    void OnTriggerStay()
    {	
	//Set Depth here
	depth = 0f;

	// Tag to see if the water we are touching is not on
	bool waterOn = false;
	
	// If we are not on a bridge set depth via water
	if(bridges.Count < 1)
	{
	    // Remove null entries from Water Script List
	    waterScripts.RemoveAll(item => item == null);

	    // Cycle through every water script we are touching
	    foreach(Water thisScript in waterScripts)
	    {
		// Set our depth to the value on this Script
		float thisDepth = thisScript.GetDepth(transform.position);


		// If this depth is less than our current depth, set it to our depth
		if(thisDepth > depth)
		{
		    depth = thisDepth;
		}

		// If any of these scripts are on, set tag that we are touching water that is on
		if(thisScript.on)
		    waterOn = true;
	    }
	}

	// Only if we are touching water that is on
	if(waterOn)
	{
	    // Decrease our speed as we are in water
	    stats.walkSpeed = stats.origWalkSpeed - depth * .5f;
	    stats.runSpeed = stats.origRunSpeed - depth * .5f;

	    // Raise the bottom of the sprite mask to obscure bottom of sprite
	    spriteMask.transform.localPosition = new Vector3(0f, .6f, 0f);
	}
	// If we aren't touching water that is on
	else
	{
	    // Set our speed to normal
	    stats.walkSpeed = stats.origWalkSpeed;
	    stats.runSpeed = stats.origRunSpeed;
	}
	if(!floating)
	{
	    // If our sprites are above the depth under us
	    if(sprites[0].transform.localPosition.y > -depth * .07f)
	    {
		// Lower our Sprites by time since last frame
		foreach(GameObject obj in sprites)
		{
		    obj.transform.localPosition -= new Vector3(0f, Time.deltaTime * .35f, 0f);
		}
		// If we aren't touching water that is on, set our sprite mask to same height as sprites
		if(!waterOn)
		{
		    spriteMask.transform.localPosition = new Vector3(0f, sprites[0].transform.localPosition.y, 0f);
		}
		// If we are touching water that is on
		else
		{
		    if(Random.Range(0,3) == 1)
		    {
			audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));

			// Create a few splash animations around the character, with random delays
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
		    }
		}
	    }
	    else if(sprites[0].transform.localPosition.y < -depth * .07f)
	    {
		// If our head is above water now but was below before
		if(sprites[0].transform.localPosition.y < -6f * .07f && depth < 6f)
		{
		    // Stop drowning sound if playing, not drowning anymore
		    if(voicePlaying)
		    {
			// Play a random gasping sound
			voiceSource.clip = gaspingSounds[Random.Range(0, gaspingSounds.Length)];
			voiceSource.Play();
			voicePlaying = false;
		    }
		}
		// Raise our Sprites by time since last frame
		foreach(GameObject obj in sprites)
		{
		    obj.transform.localPosition += new Vector3(0f, Time.deltaTime * .35f, 0f);
		}
		// If we aren't touching water that is on, set our sprite mask to same height as sprites
		if(!waterOn)
		{
		    spriteMask.transform.localPosition = new Vector3(0f, sprites[0].transform.localPosition.y, 0f);
		}
		// If we are touching water that is on
		else
		{
		    if(Random.Range(0,3) == 1)
		    {
			audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));

			// Create a few splash animations around the character, with random delays
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
			StartCoroutine(DrawSplash());
		    }
		}
	    }
	}

	// If navAgent turned off but depth is not drownin level, turn it back on
	if(navAgent && !navAgent.enabled && (!waterOn || depth < 6f))
	{
	    navAgent.enabled = true;
	}

	// If we are in over our head and alive (and touching water that is on)
	if(waterOn && !floating && depth >= 6f)
	{
	    // If we are not a zombie
	    if(!zombieScript || !zombieScript.enabled)
	    {
		// If we have a nav mesh agent turn it off
		if(navAgent)
		{
		    navAgent.enabled = false;
		}
			
		// Move randomly
		transform.parent.transform.position += new Vector3(Random.Range(-stats.runSpeed, stats.runSpeed), 0f, Random.Range(-stats.runSpeed, stats.runSpeed)).normalized * Time.deltaTime * 5f;

	    
		// Create a splash animations around the character, with random delays
		StartCoroutine(DrawSplash());

		// Make sure we are not already playing a voice sound effect
		if(!voicePlaying)
		{
		    // Play a random drowning sound
		    voiceSource.clip = drowningSounds[Random.Range(0, drowningSounds.Length)];
		    voiceSource.Play();

		    voicePlaying = true;
		}
		


		// Subtract from character's Health
		healthScript.currHealth -= Time.deltaTime * .3f;

		// Don't let health go below 0, so we won't permadeath
		if(healthScript.currHealth < 0f)
		{		    
		    healthScript.currHealth = 0f;

		    // Play a random drowned sound
		    voiceSource.clip = drownedSounds[Random.Range(0, drownedSounds.Length)];
		    voiceSource.Play();
		}
	    }
	}

	// If we have died, float on the surface
	if(waterOn && healthScript.currHealth <= 0f)
	{
	    // Turn on floating effect for sprite
	    floating = true;

	    xPos = transform.parent.transform.position.x;
	    yPos = .5f;
	    zPos = transform.parent.transform.position.z;
	
	    xRot = sprites[0].transform.rotation.eulerAngles.x;
	    yRot = sprites[0].transform.rotation.eulerAngles.y;
	    zRot = sprites[0].transform.rotation.eulerAngles.z;
	}
    }


    


    // When we leave a water area
    void OnTriggerExit(Collider col)
    {
	if(col.gameObject.tag == "Bridge")
	{
	    // Add to our list of bridges
	    bridges.Remove(col.gameObject);
	}
	else
	{
	    // Remove this water from the object's list of depths
	    waterScripts.Remove(col.gameObject.GetComponent<Water>());
	
	    if(!floating)
	    {
		// If we weren't on a bridge
		if(bridges.Count < 0)
		{
		    // Play a random small splash sound at random volume as oneshot (allows multiple sounds to play at same time)
		    audioSource.PlayOneShot(smallSplashSounds[Random.Range(0, smallSplashSounds.Length)], Random.Range(0.5f, 1f));
		}
		
	
		// Set our depth to 0
		depth = 0f;

		// Tag to see if we are touching any water that is on
		bool waterOn = false;
		
		// If we are no longer touching water areas
		if(waterScripts.Count < 1)
		{
		    // If we have a nav mesh agent turn it on
		    if(navAgent)
			navAgent.enabled = true;
	    
		    // Make our sprites fully visible
		    foreach(GameObject obj in sprites)
		    {
			obj.transform.localPosition = Vector3.zero;
		    }

		    // Reset position of sprite mask to stop obscuring sprite
		    spriteMask.transform.localPosition = new Vector3(0f, .2f, 0f);
	    
		    // Set our speed to normal
		    stats.walkSpeed = stats.origWalkSpeed;
		    stats.runSpeed = stats.origRunSpeed;
		}
		// We are still in water
		else
		{
		    // If we are on a bridge
		    if(bridges.Count > 0)
		    {
			// Depth = 0
			depth = 0f;			
		    }
		    // If we are not on a bridge
		    else
		    {
			// Remove null entries from list
			waterScripts.RemoveAll(item => item == null);
			
			// Cycle through every water script we are touching
			foreach(Water thisScript in waterScripts)
			{
			    if(thisScript.on)
				waterOn = true;
			    
			    // Get the depth at this location from the script
			    float thisDepth = thisScript.GetDepth(transform.position);

			    // If this depth is less than our current depth, set it to our depth
			    if(thisDepth > depth)
				depth = thisDepth;
			}
		    }

		    // If we are touching water that is on
		    if(waterOn)
		    {
			// Set our speed to the current depth
			stats.walkSpeed = stats.origWalkSpeed - depth * .25f;
			stats.runSpeed = stats.origRunSpeed - depth;
		    }
		    
		    if(stats.walkSpeed < 1f)
			stats.walkSpeed = 1f;
		    if(stats.runSpeed < 1.5f)
			stats.runSpeed = 1.5f;
		}
	    }
	}
    }
    

    


    void Update()
    {
	if(voicePlaying)
	{
	    if(!voiceSource.isPlaying)
	    {
		voicePlaying = false;
	    }
	}

	// If our body is floating and we are touching water, and not on bridge
	if(floating && waterScripts.Count > 0 && bridges.Count < 1)
	{
	    // Raise the bottom of the sprite mask to obscure bottom of sprite
	    spriteMask.transform.localPosition = new Vector3(0f, .6f, 0f);
	    
	    // If our sprite is below the surface
	    if(sprites[0].transform.localPosition.y < yPos )
	    {
		// Adjust the height of our sprites upwards
		foreach(GameObject obj in sprites)
		{
		    obj.transform.localPosition = new Vector3(0f, obj.transform.localPosition.y + Time.deltaTime * .1f, 0f);
		}
	    }
	    
	    // If the sprite is above the surface, in the air
	    else if(sprites[0].transform.localPosition.y > yPos + .5f)
	    {
		
		// Adjust the height of our sprites downward
		foreach(GameObject obj in sprites)
		{
		    obj.transform.localPosition = new Vector3(0f, obj.transform.localPosition.y - Time.deltaTime * .1f, 0f);
		}
	    }
	    
	    // If it's time for the body to sway in the waves
	    if(timer <= 0f)
	    {
		// Shift the character's game object's position a random amount
		transform.parent.transform.position = new Vector3(xPos + Random.Range(-2f, 2f) * Time.deltaTime, yPos, zPos);
		
		// Tie sprite to object
		//transform.position = new Vector3(0f, -.4f, 0f);

		// Set a random amount for the body to bob on the surface
		float randomAmount = Random.Range(-1f, 1f) * Time.deltaTime;

		// Set a random amount for the body to sway on the surface
		float randomAmount2 = Random.Range(-100f, 100f) * Time.deltaTime;
		
		// Apply rotation and bobbing to sprites
		foreach(GameObject obj in sprites)
		{
		    // Bob this sprite
		    obj.transform.localPosition = new Vector3(0f, obj.transform.localPosition.y + randomAmount, 0f);
		    // Rotate this sprite
		    obj.transform.rotation = Quaternion.Euler(new Vector3(xRot, yRot, zRot + randomAmount2));
		}
		
		// Set a random time to wait before moving again
		timer = Random.Range(.01f, 1f);
	    }
	    else
	    {
		// Count down the timer
		timer -= Time.deltaTime;
	    }
	}
    }


    

    // Method that draws splash sprite animations around the character
    IEnumerator DrawSplash()
    {
	yield return new WaitForSeconds(Random.Range(0f, 1f));
					
	Instantiate(splashPrefab, new Vector3(transform.position.x + Random.Range(-1f, 1f), 0f, transform.position.z + Random.Range(-1f, 1f)), Quaternion.identity);
    }
}
