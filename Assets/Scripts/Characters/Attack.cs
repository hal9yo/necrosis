﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    // How far in front of zombie the attack takes place
    public float attackDistance;

    // How much damage each attack deals
    public int damageMax;
    public int damageMin;

    // Reference to character animators
    public List<Animator> animators;

    // Tag to make sure we only attack once per animation, public so AI script can read and awon't make character turn while they are attacking
    public bool alreadyAttacked;

    // Variable to store raycast hits
    private RaycastHit hit;
    private Ray ray;

    //Remember if player is a threat
    public bool isThreat;

    // Is the zombie rogue right now?
    public bool isRogue = false;

    // Is the zombie angry right now (about to go rogue)
    public bool angry = false;

    // Our rigidbody to read velocity
    public Rigidbody rigidbody;

    // Werewolf attacks faster and longer
    public bool werewolf = false;

    // Sound script to play attacking sound effects
    public NPCSounds npcSounds;

    // Reference to our wolf fighting where we should have the velocity
    public WolfFighting wolfFighting;
    
    public bool debugOn = false;

    // If a script is added here, damage dealt is sent to the script for processing
    public Weapon feedBackScript;
    

    
    void Update()
    {
	//Get animator state info from clothes/armor
	AnimatorStateInfo animatorInfo = animators[0].GetCurrentAnimatorStateInfo(0);

	// Is attack animation playing?
	if(animatorInfo.tagHash == Animator.StringToHash("Attacking"))
	{
	    // Make sure we did not already determine hit and apply damage
	    if(!alreadyAttacked)
	    {
		// Is it just the anger animation playing?
		if(angry)
		{
		    // Is the animation halfway through?
		    if(animatorInfo.normalizedTime >= .25f)
		    {
			// Set every animator to idle, to stop the animation (don't want to play full attack animation, just first two frames
			foreach(Animator anim in animators)
			    anim.SetTrigger("CancelAttack");

			// Turn off angry tag
			angry = false;
		    }
		}
		//Check if we got to the right point in the animation to see if it hit
		if((werewolf && animatorInfo.normalizedTime > .25f) || animatorInfo.normalizedTime > .5f)
		{
		    for(int pos = -1; pos < 2; pos++)
		    {
			// Raycast again to make sure target didn't move out of range before attack finished
			// Create a raycast out the front of the zombie
			ray = new Ray(transform.position, transform.forward + transform.right * pos);

			// Draw debug line so attack raycast visible in editor
			Debug.DrawRay(transform.position, (transform.forward + transform.right * pos) * attackDistance);
		    
			// Calculate if there was a hit, within the attack distance
			if (Physics.Raycast(ray, out hit, attackDistance))
			{				
			    // Our raycast hit something, therefor apply damage to it			
			    // Random damage
			    float damageDealt = Random.Range(damageMin, damageMax);

			    // Reference to the hit objects health script
			    Health theirHealth = hit.transform.GetComponent<Health>();
			
			    // If hit object has health component and they aren't dead already
			    if(theirHealth != null && theirHealth.currHealth > 0)
			    {
				// Keep track of exactly how much damage we dealt
				float oldHealth = theirHealth.currHealth;

				// Unless two werewolfs
				if(!(werewolf && hit.transform.tag == "WolfUnit"))
				{
				    // Send damage and relative direction to enemy
				    theirHealth.TakeDamage(damageDealt, transform);

				    // Calculate how much damage it actually did to them
				    float realDmg = oldHealth - theirHealth.currHealth;

				    // If we have a feedback script
				    if(feedBackScript)
				    {
					bool killingBlow = false;

					// If they are now dead
					if(theirHealth.currHealth < 0f)
					    killingBlow = true;

					// Send how much damage we dealt and whether or not we killed to the weapon
					feedBackScript.FeedBack(realDmg, killingBlow);
				    }
				    
				    
				    alreadyAttacked = true;
				}

				if(werewolf)
				    // Apply extra force to hit unit's rigidbody
				    hit.transform.GetComponent<Rigidbody>().velocity += transform.forward * damageDealt * 60f;
				else
				    // Apply force to hit unit's rigidbody
				    hit.transform.GetComponent<Rigidbody>().velocity += transform.forward * damageDealt * 40f;
			    }
			}
		    }
		    // Set boolean to make sure we don't attack again this attack
		    // Humans just end attack instantly
		    if(!werewolf)
			alreadyAttacked = true;
		    // werewolf attack lasts longer as they jump through air
		    else if(animatorInfo.normalizedTime > .75f)
			alreadyAttacked = true;
		}
	    }
	    
	}
	// Animation not playing so we can attack now
	else
	{
	    // Set alreadyAttacked boolean to false as we are not attacking
	    alreadyAttacked = false;

	    for(int pos = -1; pos < 2; pos++)
	    {
		// Create a raycast out the front of the zombie
		Ray ray = new Ray(transform.position, transform.forward + transform.right * pos);

		// Calculate if there was a hit, within the attack distance
		if (Physics.Raycast(ray, out hit, attackDistance))
		{
		    //Grab the tag off the hit object
		    string objectTag = hit.transform.gameObject.tag;

		    if(debugOn)
			Debug.Log(hit.transform.gameObject.name);

		    // Only target all possible enemies
		    if(objectTag != transform.gameObject.tag && (objectTag == "PlayerUnit" || objectTag == "EnemyUnit" || (objectTag == "Player" && (isThreat || isRogue)) || objectTag == "Rogue" || (!werewolf && objectTag == "WolfUnit")))
		    {
			
			//Debug.Log(objectTag + " is not " + gameObject.tag + " so attack!");
			    
			// Turn off angry tag so whole animation will play, not just first part
			angry = false;
		    
			// Our raycast hit an enemy, therefor an enemy is within melee range
			// Start attack animation
			foreach(Animator anim in animators)
			{
			    anim.SetTrigger("Attack");
			}

			// If we're a werewolf
			if(werewolf)
			{
			    // Lunge at enemy
			    rigidbody.velocity += hit.transform.position - transform.position + wolfFighting.GetEnemyVelocity();

			}
			else
			{
			    if(npcSounds)
				// Play an attacking sound effect, werewolf plays sound early
				npcSounds.PlayRandom(5);
			}
		    }
		}
		if(werewolf)
		{
		    // Create a raycast out the front of the zombie
		    Ray ray2 = new Ray(transform.position, transform.forward + transform.right * pos);

		    // Calculate if there was a hit, within the extended distance
		    if (Physics.Raycast(ray, out hit, attackDistance * 2f))
		    {
			//Grab the tag off the hit object
			string objectTag = hit.transform.gameObject.tag;
			
			// Only target all possible enemies
			if(objectTag != transform.gameObject.tag && (objectTag == "PlayerUnit" || objectTag == "EnemyUnit" || (objectTag == "Player" && (isThreat || isRogue)) || objectTag == "Rogue"))
			{
			    // Play an attacking sound effect, werewolf plays sound early
			    npcSounds.PlayRandom(5);
			}
		    }
		}
	    }

	    // if angry tag tripped, play attack animation
	    if(angry)
	    {
		// Our raycast hit an enemy, therefor an enemy is within melee range
		// Start attack animation
		foreach(Animator anim in animators)
		{
		    anim.SetTrigger("Attack");
		}
	    }
	}
    }
}
