﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpriteSwap : MonoBehaviour
{
    // The name of the sprite sheet to use
    public string spriteSheetName;

    // The name of the currently loaded sprite sheet
    private string loadedSpriteSheetName;

    // The dictionary containing all the sliced up sprites in the sprite sheet
    private Dictionary<string, Sprite> spriteSheet;

    // The Unity sprite renderer so that we don't have to get it multiple times
    public SpriteRenderer spriteRenderer;

    // Use this for initialization
    private void Start()
    {
        LoadSpriteSheet();
    }

    // Runs after the animation has done its work
    private void LateUpdate()
    {
        // Check if the sprite sheet name has changed (possibly manually in the inspector)
        if (this.loadedSpriteSheetName != this.spriteSheetName)
        {
            // Load the new sprite sheet
            this.LoadSpriteSheet();
        }

        // Swap out the sprite to be rendered by its name
        // Important: The name of the sprite must be the same!
        this.spriteRenderer.sprite = this.spriteSheet[this.spriteRenderer.sprite.name];
    }

    // Loads the sprites from a sprite sheet
    private void LoadSpriteSheet()
    {
        // Load the sprites from a sprite sheet file (png). 
        // Note: The file specified must exist in a folder named Resources
        var sprites = Resources.LoadAll<Sprite>(this.spriteSheetName);
        this.spriteSheet = sprites.ToDictionary(x => x.name, x => x);

        // Remember the name of the sprite sheet in case it is changed later
        this.loadedSpriteSheetName = this.spriteSheetName;
    }

    // Public method to force the spritesheet to update when time isn't moving
    public void ForceUpdate()
    {
	// Check if the sprite sheet name has changed (possibly manually in the inspector)
        if (this.loadedSpriteSheetName != this.spriteSheetName)
        {
            // Load the new sprite sheet
            this.LoadSpriteSheet();
        }

        // Swap out the sprite to be rendered by its name
        // Important: The name of the sprite must be the same!
        this.spriteRenderer.sprite = this.spriteSheet["sprite_224"];
    }
}
