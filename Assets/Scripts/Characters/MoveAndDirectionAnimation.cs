﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndDirectionAnimation : MonoBehaviour
{
    // Reference to the animator
    public Animator anim;

    // Minimum movement distance to be change animation to walking
    public float minMove;
    
    // Store current position to check next frame
    private Vector3 position;

    // Timer variable for checking movement
    private float timer;

    // Transform of followed object
    public Transform other;

    // The speed that is when running animation is playing full speed (1x normal speed)
    public float maxSpeed = 20f;
	
    // Start is called before the first frame update
    void Start()
    {
        position = transform.position;
	timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
	if(anim.gameObject.activeSelf)
	{
	    //Get animator state info from clothes/armor
	    AnimatorStateInfo animatorInfo = anim.GetCurrentAnimatorStateInfo(0);

	    // Is attack animation not playing?
	    if(animatorInfo.tagHash == Animator.StringToHash("Attacking"))
	    {
	    }
	    else
	    {
		// Set animation direction by angle of attached object
		anim.SetFloat("Horizontal", other.forward.x);
		anim.SetFloat("Vertical", other.forward.z);
	    
		// Update walking animation every 1/10th of a second to avoid jittering
		if (timer < .1)
		{
		    timer += Time.deltaTime;
		}
		else
		{
		    timer = 0;	    
		    // Determine if we moved further than miniscule amount
		    if (Vector3.Distance(transform.position, position) > minMove)
		    {
			anim.SetBool("Walking", true);

			anim.SetFloat("Speed", new Vector3(transform.position.x - position.x, 0f, transform.position.z - position.z).magnitude * maxSpeed * Time.deltaTime);
		    }	    
		    else
		    {
			anim.SetBool("Walking", false);
		    }
		    position = transform.position;
		}
	    }
	}
    }
}
