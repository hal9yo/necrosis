﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // Don't change this in the inspector, it's just so you can see their current health when playtesting
    public float currHealth;

    //Armor value
    public float armor;

    // Reference to spell animator
    public Animator spellAnim;

    //References to object scripts
    public PlayerMovement playerMovement;
    public PlayerManager playerManager;
    public Zombie zombie;
    public Attack attack;
    public NPC1 npc1;
    public Wolf wolf;
    public ArcherAttack archerAttack;
    public NPCSounds npcSounds;
    public NPCTargets npcTargets;
    public WolfTargets wolfTargets;

    
    // Reference to the animator associated with this gameobject's sprites
    public List<Animator> animators;

    // A time to live for the speed up effect
    private float speedTTL = 0f;
    // The time we started the speed up
    private float startTime;

    // Reference to the permadeath animator
    public Animator pDeathAnim;

    // Reference to art object
    public GameObject artObj;
    
    // Reference to this characters stats
    public Stats stats;

    // How much damage makes this character flinch (play hurt animation, breaking attack)?
    public float painTolerance;

    // How much overkill causes permadeath?
    public float permaDeathPoint;

    // Which enemy is hurting you the most?
    private Transform biggestThreat;

    // List of enemies that have attacked you
    List<Transform> enemyAtackers = new List<Transform>();
    
    // LifeMeter sprite reference
    public Transform lifeMeter;
    // LifeMeter (when zombie) sprite reference
    public Transform zombieLifeMeter;
    
    // Current life meter
    public Transform currentLifeMeter;

    // Selection bracket sprite reference
    public Transform selectionIcon;
    // Raise Icon sprite reference
    public Transform raiseIcon;

    public bool selected = false;
    public bool hoverSelect = false;
    public bool hoverRaise = false;
    public bool showLife = false;
    public bool showBanEarthIcon = false;

    // navmeshagent
    public UnityEngine.AI.NavMeshAgent navComponent;

    // Audio Source to play sound effects
    public AudioSource audioSource;

    // Array of audio clips when attack blocked
    public AudioClip[] blockSound;

    // Array of audio clips when attack hit
    public AudioClip[] hitSound;

    // Only die once
    public bool alreadyDead = false;

    // Only for zombies that start in the ground
    public bool startDead = false;
    
    // Blood prefab
    public GameObject bloodPrefab;

    // Pitch angle for blood
    private float bloodAngle;

    // How often to squirt blood when injured
    private float squirtTime = 0f;

    // Dirt sprite to grow and deparent when resurrecting dead zombie
    public GameObject dirtPile;
    public bool animateDirt = false;
    
    public bool isArcher;

    
    // Keep track of points to squirt blood from
    Vector3[] wounds = new Vector3[5];
    int woundCount = 0;

    // Reference to our capsule collider
    public CapsuleCollider capCol;

    // Reference to our box collider
    public BoxCollider boxCol;

    // Reference to our sphere collider
    public SphereCollider sphereCol;

    // When on, dead character will fade away and be destroyed after a time
    public bool decay;
    public float decayTime;
    float maxDecayTime;

    //List of the characters sprite renderers for manipulating transparency
    public List<Renderer> renderers;

    // Character's inventory
    public Inventory invScript;
    
    // The speed the animations of the character normally run at
    float animSpeed;

    public bool debugOn;

    void Start()
    {
	maxDecayTime = decayTime;

	// Randomize animation speeds so animations don't fall in lock step
	animSpeed = Random.Range(.8f, 1.2f);

	// Link references to scripts if not already done
	if(playerMovement == null && gameObject.GetComponent<PlayerMovement>() != null)
	{
	    playerMovement = gameObject.GetComponent<PlayerMovement>();
	}
	if(playerManager == null && gameObject.GetComponent<PlayerManager>() != null)
	{
	    playerManager = gameObject.GetComponent<PlayerManager>();
	}
	if(zombie == null && gameObject.GetComponent<Zombie>() != null)
	{
	    zombie = gameObject.GetComponent<Zombie>();
	}
	if(attack == null && gameObject.GetComponent<Attack>() != null)
	{
	    attack = gameObject.GetComponent<Attack>();
	}
	if(npc1 == null && gameObject.GetComponent<NPC1>() != null)
	{
	    npc1 = gameObject.GetComponent<NPC1>();
	}
	if(archerAttack == null && gameObject.GetComponent<ArcherAttack>() != null)
	{
	    archerAttack = gameObject.GetComponent<ArcherAttack>();
	}
	if(npcSounds == null && gameObject.GetComponent<NPCSounds>() != null)
	{
	    npcSounds = gameObject.GetComponent<NPCSounds>();
	}
	if(npcTargets == null && gameObject.GetComponent<NPCTargets>() != null)
	{
	    npcTargets = gameObject.GetComponent<NPCTargets>();
	}
	if(wolf == null && gameObject.GetComponent<Wolf>() != null)
	{
	    wolf = gameObject.GetComponent<Wolf>();
	}
	if(wolfTargets == null && gameObject.GetComponent<WolfTargets>() != null)
	{
	    wolfTargets = gameObject.GetComponent<WolfTargets>();
	}
	
	// If we are dead
	if(startDead)
	{
	    //Freeze all animations, stay dead
	    foreach(Animator anim in animators)
		anim.speed = 0;

	    //If this object has a playerMovement script
	    if(playerMovement != null)
	    {
		//Disable the script
		playerMovement.enabled = false;
	    }

	    //If this object has a PlayerManager script
	    if(playerManager != null)
	    {
		//Disable it
		playerManager.enabled = false;
	    }
	    
	    //If this object has a zombie script
	    if(zombie != null)
	    {
		//Disable zombie script
		zombie.enabled = false;
	    }

	    //If this object has an attack script
	    if(attack != null)
	    {
		//Disable attack script
		attack.enabled = false;
	    }
	    
	    //Check for NPC script
	    if(npc1 != null)
	    {
		//Disable NPC script
		npc1.enabled = false;
	    }

	    //Check for Wolf script
	    if(wolf !=null)
	    {
		wolf.enabled = false;
	    }

	    //Check for ArcherAttack script
	    if(archerAttack != null)
	    {
		//Disable NPC script
		archerAttack.enabled = false;
	    }

	    //Deactivate nav component
	    if(navComponent)
		navComponent.enabled = false;
	}
	else
	{
	    foreach(Animator anim in animators)
		anim.speed = animSpeed;
	}

	// If this is a zombie with a dirt pile deparent it so it doesn't follow the zombie around
	if(dirtPile)
	    dirtPile.transform.parent = null;
    }


    
    void FadeOut()
    {
	Color objectColor = renderers[0].material.color;
	float fadeColor = decayTime / maxDecayTime;

	objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeColor);
	foreach(Renderer renderer in renderers)
	{
	    renderer.material.color = objectColor;
	}
    }


    
    // Update is called once per frame
    void Update()
    {	    
	if(debugOn)
	    for(int i = 0; i<5; i++)
		Debug.Log(i + " " + wounds[i]);


	// Apply any health regeneration to our health, but not more than our max
	if(currHealth < stats.maxHealth)
	{
	    if((currHealth + stats.healthRegen) <= stats.maxHealth)
		currHealth += stats.healthRegen * Time.deltaTime;
	    else
		currHealth = stats.maxHealth;
	}

	
	// Animate the dirt pile growing if this is a zombie coming out of the ground
	if(animateDirt)
	{	    
	    if(dirtPile.transform.localScale.x < .7f)
	    {
		dirtPile.transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime * .6f, Time.deltaTime);
	    }
	    else
	    {
		dirtPile = null;
		animateDirt = false;
	    }
	}
	
	// ON DEATH
        if (!alreadyDead && currHealth <=0)
	{
	    // Call death function
	    OnDeath();
	}

	// If we are dead, and we are set to decay
	if(alreadyDead && decay)
	{
	    // Slowly fade away the sprite until it is invisible, over the time we allotted, at which point destroy the gameobject altogether
	    if(decayTime > 0)
	    {
		decayTime -= Time.deltaTime;

		FadeOut();
	    }
	    else
		Destroy(gameObject);
	}

	//If mouse hovers over unit and unit is currently selectable
	if(hoverSelect)
	{
	    if(selectionIcon)
		//Turn on selection brackets sprite to indicate selectability
		selectionIcon.gameObject.SetActive(true);
	}
	else
	{
	    if(selectionIcon)
		//Turn off selection brackets sprite
		selectionIcon.gameObject.SetActive(false);
	}

	//If mouse hovers over unit
	if(showLife)
	{
	    //Turn on life meter frame (don't turn off in else, turned off in selected else
	    currentLifeMeter.parent.gameObject.SetActive(true);
    	    //Update lifemeter
	    currentLifeMeter.localScale = new Vector3(Mathf.Max(0f, currHealth / stats.maxHealth), 1f, 1f);
	}
	else
	{
	    //Turn on life meter frame (don't turn off in else, turned off in selected else
	    currentLifeMeter.parent.gameObject.SetActive(false);	    
	}

	// If mouse hovers over unit and unit is currently raisable
	if(hoverRaise)
	{
	    if(raiseIcon)
		//Turn on raise icon sprite
		raiseIcon.transform.gameObject.SetActive(true);
	}
	else
	{
	    if(raiseIcon)
		//Turn off raise icon sprite
		raiseIcon.transform.gameObject.SetActive(false);
	}

	//Update lifemeter
	currentLifeMeter.localScale = new Vector3(Mathf.Max(0f, currHealth / stats.maxHealth), 1f, 1f);

	//If unit is selected
	if(selected)
	{
	    //Make lifemeter visible
	    currentLifeMeter.parent.gameObject.SetActive(true);

	    //Update lifemeter
	    currentLifeMeter.localScale = new Vector3(Mathf.Max(0f, currHealth / stats.maxHealth), 1f, 1f);
	}

	//If unit is neither selected nor hovered over
	if(selected == false && showLife == false)
	{
	    //Make lifemeter invisible again
	    currentLifeMeter.parent.gameObject.SetActive(false);
	}



	// Remove speed up when time runs out
	if(speedTTL > 0f)
	{
	    if(Time.time - startTime > speedTTL)
	    {
		//Return all animations to normal speed
		foreach(Animator anim in animators)
		anim.speed = animSpeed;
		speedTTL = 0f;
	    }
	}
	
	//Squirt blood when life less than max
	if(currHealth > 0f && currHealth < stats.maxHealth)
	{   	
	    if(squirtTime <= 0f)
	    {
		int squirtAmount = (int)((stats.maxHealth - currHealth * 10f));
		SquirtBlood(squirtAmount);
		squirtTime = Random.Range(.5f, currHealth + .5f);
	    }
	    else
	    {
		squirtTime -= Time.deltaTime;
	    }
	}
    }


    
    // Method to let other scripts apply damage
    public void TakeDamage(float amount, Transform attacker)
    {

	//If we have block ability
	if(stats.block > 0f)
	{
	    //Attempt block
	    //Store block ability altered by direction of incoming attack
	    float effectiveBlock = stats.block;

	    // Determine direction attack is coming
	    Vector3 direction = transform.InverseTransformPoint(attacker.position);

	    // Debug.Log(direction.z);
	    
	    //If attack is from the side decrease block chance by 1/3
	    if(direction.z < .2f && direction.z > -.2f)
	    {
		effectiveBlock = effectiveBlock * .66f;
	    }
	    //If attack is from behind, cut block chance by 2/3rd
	    else if(direction.z <= -.2f)
	    {
		effectiveBlock = effectiveBlock * .33f;
	    }

	    //If attack is an arrow, cut block chance by 10%
	    if(attacker.CompareTag("Arrow"))
		effectiveBlock = effectiveBlock * .9f;

	    
	    //Roll the dice!
	    float diceRoll = Random.Range(0f, 100f);

	    if(debugOn)
		Debug.Log("Block Chance: " + effectiveBlock + "  DiceRoll: " + diceRoll);

	    
	    //If rolled under block chance
	    if(diceRoll <= effectiveBlock)
	    {
		if(diceRoll > effectiveBlock * .5f)
		{
		    // look at attacker
		    transform.LookAt(new Vector3(attacker.position.x, 1f, attacker.position.z));
		    
		    // Trigger blocking animation
		    foreach(Animator anim in animators)
		    {
			anim.SetTrigger("Block");
			anim.SetFloat("Horizontal", transform.forward.x);
			anim.SetFloat("Vertical", transform.forward.z);
		    }
		}
		
		//Play blocking sound effect
		audioSource.clip = blockSound[Random.Range(0, blockSound.Length)];
		audioSource.Play();
		
		return;
	    }
	}
	
	//Play hit sound effect
	audioSource.clip = hitSound[Random.Range(0, hitSound.Length)];
	audioSource.Play();

	//Subtract armor and bonus from amount dealt
	amount -= armor;

	//If amount is now negative make it zero
	if(amount < 0f)
	    amount = 0f;
	    
	//Trigger hit animation if damage taken is more than their paintolerance
	if(amount > painTolerance)
	{
	    foreach(Animator anim in animators)
		anim.SetTrigger("Hit");
	}


	//If not less than zero
	if(amount > 0f)
	{
	    //Add a wound unless already at 5
	    if(woundCount < 5)
	    {
		// Make a random Vector3, as long as it's not all 0's
		while(wounds[woundCount] == Vector3.zero)
		    wounds[woundCount] = new Vector3(Random.Range(-1f, 1f), Random.Range(-.5f, .5f), Random.Range(-1f, 1f));
		// Normalize the vector3 so it always has a magnitude of 1
		wounds[woundCount] = wounds[woundCount].normalized;
		woundCount++;
	    }
		
	    //Subtract amount from health
	    currHealth -= amount;

	    //How much blood to squirt
	    int squirtAmount = (int)(amount);
		
	    //Squirt blood
	    SquirtBlood(squirtAmount);

	    // Play a hurt sound effect
	    if(npcSounds != null)
		npcSounds.PlayRandom(4);
	}
	
	transform.LookAt(new Vector3(attacker.position.x, 1f, attacker.position.z));
    }



    
    void OnDeath()
    {
	// Rotate character
	transform.rotation = Quaternion.Euler(90f, transform.rotation.y, transform.rotation.z);
	
	// Play a dying sound effect
	if(npcSounds != null)
	    npcSounds.PlayRandom(7);

	// Switch the capsule collider off, to allow characters to walk over the body on the ground
	capCol.enabled = false;

	// Disable sphere trigger collider if present (don't need FOV when dead)
	if(sphereCol)
	    sphereCol.enabled = false;

	//Update lifemeter
	currentLifeMeter.localScale = new Vector3(Mathf.Max(0f, currHealth / stats.maxHealth), 1f, 1f);

	//Random chance to permadeath increases with overkill amount
	if(Random.Range(permaDeathPoint, -0.001f) > currHealth)
	{
	    // Grab horiz and vert values from one animator
	    float horiz = animators[0].GetFloat("Horizontal");
	    float vert = animators[0].GetFloat("Vertical");
	    
	    // Disable other animators
	    foreach(Animator anim in animators)
		anim.gameObject.SetActive(false);

	    // Activate permadeath animation
	    pDeathAnim.gameObject.SetActive(true);

	    // Pause animation
	    //pDeathAnim.speed = 0;

	    // Set horiz and vert values on permadeath animator
	    pDeathAnim.SetFloat("Horizontal", horiz);
	    pDeathAnim.SetFloat("Vertical", vert);

	    // Start animation
	    //pDeathAnim.speed = 1;
	    
	    //Set tag to permadead so enemies will not keep trying to attack, and cannot be resurrected
	    gameObject.tag="PermaDead";

	    // Drop equipped items
	    if(invScript)
	    {
		if(invScript.weapon)
		{
		    // Get reference to item's Weapon script
		    Weapon weapScript = invScript.weapon.GetComponent<Weapon>();
	    
    
		    // Set the block chance to not that of the weapon
		    stats.block -= weapScript.block;
		    // Set walk and run speeds less amount this weapon hinders movement
		    stats.walkSpeed += weapScript.walkSpeedLess;
		    stats.runSpeed += weapScript.runSpeedLess;
		    // Adjust the character's power level
		    stats.power -= weapScript.power;

	
		    // Run any special script this weapon may have
		    weapScript.UnEquip(gameObject);
		
		    // Turn off character's weapon sprite
		    animators[2].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";

		    // Divide animators speed by the attackspeed of the weapon
		    foreach(Animator anim in animators)
		    {
			anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / weapScript.attackSpeed);
		    }

		    //Change the character's Stats to the defaults
		    if(attack)
		    {
			// Set player's attack stats to those of the weapon
			attack.attackDistance = 3f;
			attack.damageMin = 1;
			attack.damageMax = 2;
	    
			// Change the distances the character keeps during fights to those of the weapon
			NPCFighting fightScript = GetComponent<NPCFighting>();
			if(fightScript)
			{
			    fightScript.maxDistance = 3f;
			    fightScript.minDistance = 2.5f;
			    fightScript.meleeDistance = 3f;
			}
			Zombie zombScript = GetComponent<Zombie>();
			if(zombScript)
			{
			    zombScript.maxDistance = 3f;
			    zombScript.meleeDistance = 2.5f;
			}

			// Reference the armor script on the character			
			Armor armScript = null;
			
			if(invScript.armor)
			    armScript = invScript.armor.GetComponent<Armor>();

			// If there is an armor equiped
			if(armScript)
			{
			    // Set character's armor sprite to that of the armor, based on No weapon (melee)
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = armScript.npcMeleeArmSpriteSheet;
			}
			// No armor equiped
			else
			{
			    // Set character's armor sprite to the default clothes, based on NO Weapon (melee)
			    animators[1].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "NPC-Clothes";
			}
		    }

		    // Turn on weapon object
		    invScript.weapon.SetActive(true);

		    // Deparent the item from the character
		    invScript.weapon.transform.parent = null;
		    
		    // Put the item in front of the character
		    invScript.weapon.transform.position = transform.position + transform.forward * 1.5f;
		    
		    // Remove the item from the weapon slot
		    invScript.weapon = null;
		}

		if(invScript.armor)
		{
		    // Reference the armor script on the item
		    Armor armScript = invScript.armor.GetComponent<Armor>();

		    // Multiply character's attack animation speed by that of the weapon
		    // Set animator controller's to those of the weapon
		    foreach(Animator anim in animators)
		    {
			anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / armScript.attackSpeedMultiplier);
		    }
	    

		    // Set player's Health Armor Stat to normal
		    armor -= armScript.armorAmount;
		
		    // Set walk and run speeds less amount this weapon hinders movement
		    stats.walkSpeed += armScript.walkSpeedLess;
		    stats.runSpeed += armScript.runSpeedLess;
		    stats.power -= armScript.power;


		    invScript.armor.SetActive(true);
		    
		    // Deparent the item from the character
		    invScript.armor.transform.parent = null;
		    
		    // Put the item in front of the character
		    invScript.armor.transform.position = transform.position + transform.forward * -1.5f;
		    
		    // Remove the item from the armor slot
		    invScript.armor = null;	
		}

		if(invScript.talisman)
		{
		    // Deparent the item from the character
		    invScript.talisman.transform.parent = null;

		    // Put the item in front of the character
		    invScript.talisman.transform.position = transform.position + transform.forward * 1.5f;

		    // Turn on items rigidbody
		    invScript.talisman.GetComponent<Rigidbody>().detectCollisions = true;
		    invScript.talisman.GetComponent<Rigidbody>().useGravity = true;

		    // Get reference to item's Talisman script
		    Talisman talScript = invScript.talisman.GetComponent<Talisman>();
	    
		    // Run the unequip method from the Talisman Script
		    talScript.UnEquip(gameObject);

		    // Deparent the item from the character
		    invScript.talisman.transform.parent = null;
		    
		    // Put the item in front of the character
		    invScript.talisman.transform.position = transform.position + transform.right * 1.5f;
		    // Remove the item from the talisman slot
		    invScript.talisman = null;
		}

		if(invScript.shield)
		{
		    // Deparent the item from the character
		    invScript.shield.transform.parent = null;

		    // Put the item in front of the character
		    invScript.shield.transform.position = transform.position + transform.forward * 1.5f;

		    // Turn on items rigidbody
		    invScript.shield.GetComponent<Rigidbody>().detectCollisions = true;
		    invScript.shield.GetComponent<Rigidbody>().useGravity = true;

	
		    // Get reference to item's Talisman script
		    Shield shieldScript = invScript.talisman.GetComponent<Shield>();


		    // Multiply character's attack animation speed by that of the weapon
		    foreach(Animator anim in animators)
		    {
			anim.SetFloat("AttackSpeed", anim.GetFloat("AttackSpeed") / shieldScript.attackSpeed);
		    }

	    
		    // Set the block chance to that of the weapon
		    stats.block -= shieldScript.block;
		    // Set walk and run speeds less amount this weapon hinders movement
		    stats.walkSpeed += shieldScript.walkSpeed;
		    stats.runSpeed += shieldScript.runSpeed;
		    stats.power -= shieldScript.power;
	
		    // Run any special script, this weapon may have, on the character
		    shieldScript.UnEquip(gameObject);

		    // Deparent the item from the character
		    invScript.shield.transform.parent = null;
		    
		    // Put the item in front of the character
		    invScript.shield.transform.position = transform.position + transform.right * -1.5f;
		    invScript.shield = null;
		    
		    // Turn off shield sprite for player
		    animators[3].gameObject.GetComponent<SpriteSwap>().spriteSheetName = "none";
		}

		while(invScript.inventory.Count > 0)
		{
		    // Deparent the item from the character
		    invScript.inventory[0].transform.parent = null;
		    
		    // Put the item in front of the character
		    invScript.inventory[0].transform.position = transform.position + transform.right * -1.5f;

		    // Remove this item from the inventory list
		    invScript.inventory.RemoveAt(0);
		}
	    }
	}
	else
	{	    	
	    //Trigger death animation
	    foreach(Animator anim in animators)
	    {
		if(anim.gameObject.activeSelf)
		    anim.SetBool("Dead", true);
	    }
	
	    //Set tag to dead so enemies will not keep trying to attack
	    gameObject.tag="Dead";
	}


	//Turn off selection
	selected = false;
	


	//Disable scripts that shouldn't run when dead
	if(playerMovement != null)
	{
	    playerMovement.enabled = false;
	}
	if(playerManager != null)
	{
	    playerManager.enabled = false;
	}
	if(zombie != null)
	{
	    zombie.enabled = false;
	}
	if(attack != null)
	{
	    attack.enabled = false;
	}
	if(npc1 != null)
	{
	    npc1.enabled = false;
	}
	if(archerAttack != null)
	{
	    archerAttack.enabled = false;		
	}
	if(npcTargets != null)
	{
	    npcTargets.enabled = false;
	}
	if(wolf != null)
	{
	    wolf.enabled = false;
	}
	if(wolfTargets != null)
	{
	    wolfTargets.enabled = false;
	}

	// Turn on already dead tag to keep from running again
	alreadyDead = true;

	//Deactivate nav component
	if(navComponent)
	    navComponent.enabled = false;

	// Drop items in inventory
	if(invScript)
	    while(invScript.inventory.Count > 0)
	    {
		// Deparent the item from the character
		invScript.inventory[0].transform.parent = null;

		// Put the item in front of the character
		invScript.inventory[0].transform.position = transform.position + transform.forward * 1.5f;

		// Turn on item's rigidbody
		invScript.inventory[0].GetComponent<Rigidbody>().detectCollisions = true;
		invScript.inventory[0].GetComponent<Rigidbody>().useGravity = true;
	
		// Remove the item from the inventory list
		invScript.inventory.Remove(invScript.inventory[0]);
	    }
    }

    public void Resurrect()
    {
	// Rotate character
	transform.rotation = Quaternion.Euler(0f, transform.rotation.y, transform.rotation.z);

	// Turn on art if off
	if(artObj)
	    artObj.SetActive(true);

	// Turn on FOV sphere if present
	if(sphereCol)
	    sphereCol.enabled = true;
	
	// Turn capsule collider back on so characters can't walk through their body
	capCol.enabled = true;

	// If this zombie has a dirtpile go ahead and animate it growing
	if(dirtPile)
	{
	    animateDirt = true;
	}
	
	// Remove already dead tag
	alreadyDead = false;
	
	//Untrigger death animation
	foreach(Animator anim in animators)
	{
	    //Turn off dead bool, switching to the raise animation
	    anim.SetBool("Dead", false);
	    //If animation speed was set to 0, as in the buried zombie, return to 1 to animate again
	    if(anim.speed < animSpeed)
		anim.speed = animSpeed;
	    //Check for NPC sprites and switch to zombie colors
	    if(anim.gameObject.GetComponent<SpriteSwap>())
	    {
		if(anim.gameObject.GetComponent<SpriteSwap>().spriteSheetName == "NPC-Head1")
		    anim.gameObject.GetComponent<SpriteSwap>().spriteSheetName = "Zombie-Head1";
	    
		if(anim.gameObject.GetComponent<SpriteSwap>().spriteSheetName == "NPC-Clothes")
		    anim.gameObject.GetComponent<SpriteSwap>().spriteSheetName = "Zombie-Clothes";
	    }
	}

	//Reset alpha to full opacity, in case it was fading out
	Color objectColor = renderers[0].material.color;
	float fadeColor = 1f;

	objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeColor);
	foreach(Renderer renderer in renderers)
	{
	    renderer.material.color = objectColor;
	}

	//Make zombie lifemeter visible
	zombieLifeMeter.gameObject.SetActive(true);
	
	// Switch color of life meter
	currentLifeMeter = zombieLifeMeter;
	
	// Deactivate other life meter
	lifeMeter.gameObject.SetActive(false);

	// Regain Health
	currHealth = Random.Range(stats.maxHealth * .25f, stats.maxHealth);

	//Set tag to PlayerUnit
	gameObject.tag = "PlayerUnit";

	//Enable zombie script
	gameObject.GetComponent<Zombie>().enabled = true;

	//Enable attack script
	gameObject.GetComponent<Attack>().enabled = true;

	//Turn off attack player
	gameObject.GetComponent<Attack>().isThreat = false;

	//If archer enable archer attack script
	if(isArcher)
	{
	    //Disable NPC script
	    gameObject.GetComponent<ArcherAttack>().enabled = true;

	    // Disable archer attack player
	    gameObject.GetComponent<ArcherAttack>().isThreat = false;
	}

	// Accelerate to attack speed
	navComponent.speed = stats.runSpeed;

	//Reactivate nav component
	if(navComponent)
	    navComponent.enabled = true;

	//Add a wound if less than 1
	if(woundCount < 1)
	{
	    wounds[woundCount] = new Vector3(Random.Range(0.01f, 1f), Random.Range(0.01f, 1f), 1f);

	    wounds[woundCount] = wounds[woundCount].normalized;
	    woundCount++;
	}
    }



    public void EvilLifeMeter()
    {
	// Turn on evil life meter
	lifeMeter.gameObject.SetActive(true);
	
	// Make it current life meter as we are now evil
	currentLifeMeter = lifeMeter;

	// Deactivate other red life meter
	zombieLifeMeter.gameObject.SetActive(false);
    }

    public void GoodLifeMeter()
    {
	// Turn on good life meter
	zombieLifeMeter.gameObject.SetActive(true);
	
	// Make it current life meter as we are now good
	currentLifeMeter = zombieLifeMeter;

	// Deactivate other green life meter
	lifeMeter.gameObject.SetActive(false);
    }

    void SquirtBlood(int amount)
    {
	float bloodSpeed = 0f;
	
	// Random chance to really squirt, increases with damage
	if(Random.Range(0, currHealth + 1) < 1)
	    bloodSpeed = Random.Range(1f, 3f)  * (stats.maxHealth - currHealth);

	// Randomly choose one of our open wounds
	int whichWound = Random.Range(0, woundCount);
	
	for(int i = 0; i <= amount; i++)
	{
	    //Debug.Log(wounds[whichWound] + " + " + transform.position + " = " + (transform.position + wounds[whichWound] * 5f));
	    // Instantiate blood
	    GameObject blood = Instantiate(bloodPrefab, transform.position + wounds[whichWound] * 1.5f, Quaternion.identity);

	    Blood bloodScript = blood.GetComponent<Blood>();

	    //Randomize length, width and speed of blood droplet
	    bloodScript.length = Random.Range(.06f, .18f);
	    bloodScript.width = Random.Range(.07f, .17f);
	    bloodSpeed += Random.Range(.01f, .04f);

	    //blood.transform.forward = blood.transform.position - transform.position;
	    blood.GetComponent<Rigidbody>().velocity = new Vector3(wounds[whichWound].x + Random.Range(-.5f, .5f), wounds[whichWound].y + Random.Range(-.5f, .5f), wounds[whichWound].z + Random.Range(-.5f, .5f)).normalized * bloodSpeed;

	    //Debug.Log(-(transform.position - wounds[whichWound]) * bloodSpeed);
	    
	    //GameObject.Destroy(blood, 30f); blood script now destroys self
	}
    }

    public void SpeedUp(float speedAmount, float ttl)
    {
	//Speed up all animations, stay dead
	foreach(Animator anim in animators)
	    anim.speed = speedAmount;
	speedTTL = ttl;
	startTime = Time.time;
    }
}
