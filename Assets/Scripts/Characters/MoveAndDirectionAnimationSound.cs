﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndDirectionAnimationSound : MonoBehaviour
{
    // Reference to the animator
    public Animator anim;

    // Minimum movement distance to be change animation to walking
    public float minMove;
    
    // Store current position to check next frame
    private Vector3 position;

    // Timer variable for checking movement
    private float timer;

    // Transform of followed object
    public Transform other;

    // An audio source to play movement sounds
    public AudioSource audioSource;

    // Movement sound clips
    public AudioClip[] moveSounds;

    // How fast to be moving before playing sound
    public float minSpeedForSound;
	
    // Start is called before the first frame update
    void Start()
    {
        position = transform.position;
	timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
	if(anim.gameObject.activeSelf)
	{
	    //Get animator state info from clothes/armor
	    AnimatorStateInfo animatorInfo = anim.GetCurrentAnimatorStateInfo(0);

	    // Is attack animation not playing?
	    if(animatorInfo.tagHash == Animator.StringToHash("Attacking"))
	    {
	    }
	    else
	    {
		// Set animation direction by angle of attached object
		anim.SetFloat("Horizontal", other.forward.x);
		anim.SetFloat("Vertical", other.forward.z);
	    
		// Update walking animation every 1/10th of a second to avoid jittering
		if (timer < .1)
		{
		    timer += Time.deltaTime;
		}
		else
		{
		    timer = 0;
		    // How far did we move
		    float dist = Vector3.Distance(transform.position, position);
		    
		    // Did we move further than miniscule amount
		    if (dist > minMove)
		    {
			anim.SetBool("Walking", true);
		    }	    
		    else
		    {
			anim.SetBool("Walking", false);
		    }
		    position = transform.position;

		    // Did we move fast enough to play sound?
		    if(dist > minSpeedForSound)
		    {
			
			// If volume of movement sound is lower than 1
			if(audioSource.volume < 1f)
			    // Fade in volume of movement sound
			    audioSource.volume += Time.deltaTime * 10f;
			
			//If a sound is not already playing
			if(!audioSource.isPlaying)
			{
			    // Load a random sound from the movement sounds list
			    audioSource.clip = moveSounds[Random.Range(0, moveSounds.Length)];

			    // Make sure not a null audio.clip
			    if(GetComponent<AudioSource>().clip)
			    {
				//Play the sound
				audioSource.Play();
			    }
			    
			}
		    }
		    else
		    {
			// If volume of movement sound is above 0
			if(audioSource.volume > 0f)
			    // Fade volume of movement sound
			    audioSource.volume -= Time.deltaTime * 10f;
		    }
		}
	    }
	}
    }
}
