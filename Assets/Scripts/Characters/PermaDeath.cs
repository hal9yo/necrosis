﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermaDeath : MonoBehaviour
{
    // Reference to the animator
    public Animator anim;
    
    public void PermaDeathAnim(float horiz, float vert)
    {
	anim.SetFloat("Horizontal", horiz);
	anim.SetFloat("Vertical", vert);
    }
}
