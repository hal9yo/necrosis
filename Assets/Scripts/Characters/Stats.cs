﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{   
    public float maxHealth;

    public float healthRegen;
    
    public float block;

    public int power = 1;

    public float walkSpeed;

    public float runSpeed;

    // Variables to return speed to normal when sped up
    public float origRunSpeed;
    public float origWalkSpeed;
    //private float ttl = 0f;
    //private float startTime;


    void Start()
    {
	origRunSpeed = runSpeed;
	origWalkSpeed = walkSpeed;
    }

    /*    public void SpeedUp(float amount, float time)
    {
	runSpeed *= amount;
	walkSpeed *= amount;
	ttl = time;
	startTime = Time.time;
	}*/

    /*    void Update()
    {
	if(ttl > 0f)
	{
	    if(Time.time - startTime > ttl)
	    {
		runSpeed = origRunSpeed;
		walkSpeed = origWalkSpeed;
		ttl = 0f;
	    }
	}
    }*/
}
