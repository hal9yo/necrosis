﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherAttack : MonoBehaviour
{
    // How far in front of character can they shoot
    public float shootDistance;

    // Reference to character animators
    public List<Animator> animators;

    // Reference to zombie script
    public Zombie zombScript;

    // Tag to make sure we only attack once per animation
    private bool alreadyAttacked = true;

    // Variable to store raycast hits
    private RaycastHit hit;

    // Prefab of arrow
    public GameObject arrowPrefab;

    // Arrow speed
    public float arrowSpeed;

    // Base arrow pitch, angle to shoot it at
    float arrowPitch;

    // Amount to adjust pitch to help tweak accuracy of shots
    public float pitchAdjustment;

    // Turn this value up to make archers more accurate
    public float accuracy = 1f;

    // If the necromaner character is a threat
    public bool isThreat;

    // Reference to our targets
    public NPCTargets targets;
    
    // Remember the last place the target was, to guesstimate future position
    Vector3 oldPosition;

    // Remember which target our last place refers to
    string oldTarget;

    // Target velocity
    Vector3 targetVelocity;

    // Audio Source to play sound effects
    public AudioSource audioSource;

    // Audio clip to play while drawing the bow
    public AudioClip drawSound;

    // Audio clip to play when firing the bow
    public AudioClip fireSound;

    // Reference to Fire Master script for spawning fires if fire archer
    public FireRoot fireRoot;


    // Turn on to receive debug messages from this unit's Archer Attack script
    public bool debugOn;


    public Wind windScript;
    
    void Start()
    {
	if(!windScript)
	    windScript = GetComponent<WindPush>().windScript;
    }
    
    void LateUpdate()
    {

	//Get animator state info from clothes/armor
	AnimatorStateInfo animatorInfo = animators[1].GetCurrentAnimatorStateInfo(0);

	
	// Is attack animation playing?
	if(animatorInfo.tagHash == Animator.StringToHash("Shooting"))
	{
	    // Make sure we did not already determine hit and apply damage
	    if(!alreadyAttacked)
	    {		
		//Check if we got to the right point in the animation to see if it hit
		if(animatorInfo.normalizedTime > .5f)
		{
		    // Create a raycast out the front of the zombie
		    Ray ray = new Ray(transform.position, transform.forward);

		    // Calculate if there was a hit, within the attack distance
		    if (Physics.Raycast(ray, out hit, shootDistance))
		    {
			if(debugOn)
			    Debug.Log("raycast hit: " + hit.transform.gameObject.name + "   old target: " + oldTarget);

			// Did the raycast hit the target we wanted to shoot? Or is fire obscuring our sight? (still shoot arrow if it is fire, assume target is on other side of fire)
			if(hit.transform.gameObject.name == oldTarget || hit.transform.tag == "Fire")
			{
			    if((zombScript.enabled && zombScript.targets.Count > 0) || (!zombScript.enabled && targets.targets.Count > 0))
			    {
				// Instantiate arrow
				GameObject arrow = Instantiate(arrowPrefab, transform.position + transform.forward + transform.up * .75f, Quaternion.identity);

				arrow.GetComponent<WindPush>().windScript = windScript;

				Arrow arrowScript = arrow.GetComponent<Arrow>();
				arrowScript.player = transform.gameObject;

				FireArrow fireScript = arrow.GetComponent<FireArrow>();
				    
				// If we have a FireMaster script
				if(fireScript)
				{
				    fireScript.fireRoot = fireRoot;
				}
				
				//Play firing sound effect
				audioSource.clip = fireSound;
				audioSource.Play();
				
				// If we are a zombie
				if(zombScript.enabled)
				{
				    // Make sure we are targeting same target as we were remembering for velocity calculation
				    if(zombScript.targets[0].name == oldTarget)
				    {
					// Guesstimate targets current velocity, based on previous position
					targetVelocity = zombScript.targets[0].transform.position - oldPosition;
				
					//Look at target plus velocity times distance accounting for wind to try to lead shot
					transform.LookAt(zombScript.targets[0].transform.position + targetVelocity * zombScript.distance * accuracy - windScript.direction * windScript.strength * Random.Range(-.1f, .1f));
				    }
				    else
				    {
					// No velocity information, just aim right at them
					transform.LookAt(zombScript.targets[0].transform.position - windScript.direction * windScript.strength * windScript.strength * Random.Range(-.1f, .1f));
				    }

				    // Remember current target position to guesstimate velocity next turn
				    oldPosition = zombScript.targets[0].transform.position;

				    // Aim up more when target is farther away to arch arrow through air and adjust for gravity
				    arrowPitch = zombScript.distance * pitchAdjustment;
				}
				// If we are not a zombie
				else
				{
				    string newTarget = targets.targets[targets.closestEntry].GetComponent<RememberUnit>().unit.name;
				    //Debug.Log("oldTarget: " + oldTarget + "   oldTarget pos: " + oldPosition);
				    //Debug.Log("newTarget: " + newTarget + "   oldTarget pos: " + oldPosition);
				    // Make sure we are targeting same target as we were remembering for velocity calculation
				    if(newTarget == oldTarget)
				    {
					// Guesstimate targets current velocity, based on previous position
					targetVelocity = targets.targets[targets.closestEntry].transform.position - oldPosition;
					if(debugOn)
					    Debug.Log("target pos: " + targets.targets[targets.closestEntry].transform.position + "   target vel: " + targetVelocity);
					
					//Look at target plus velocity times distance adjusting for wind to try to lead shot
					transform.LookAt(targets.targets[targets.closestEntry].transform.position + targetVelocity * accuracy - windScript.direction * windScript.strength  * targets.closestDistance * Random.Range(0f, .01f));
				    }
				    // This target is not the one we were remembering, so just use 0 velocity as best guess
				    else
				    {
					if(debugOn)
					    Debug.Log("target pos: " + targets.targets[targets.closestEntry].transform.position + "   No Velocity");
					
					//Look at target
					transform.LookAt(targets.targets[targets.closestEntry].transform.position);
				    }
			    
				    // Remember current target position to guesstimate velocity next turn
				    oldPosition = targets.targets[targets.closestEntry].transform.position;
				    // Remember which target this refers to
				    oldTarget = targets.targets[targets.closestEntry].GetComponent<RememberUnit>().unit.name;
				    if(debugOn)
					Debug.Log(targets.targets[targets.closestEntry]);

				    // Aim up more when target is farther away to arch arrow through air and adjust for gravity
				    arrowPitch = targets.closestDistance * targets.closestDistance * pitchAdjustment * .1f;
				}

				// Flip arrow pitch if facing other direction (negative is up positive is down when backwards)
				if(transform.rotation.eulerAngles.y > 180f)
				{
				    arrowPitch = -arrowPitch;
				}
						
				//arrow.transform.LookAt(transform.forward);
				arrow.GetComponent<Rigidbody>().velocity = Quaternion.AngleAxis(arrowPitch, Vector3.forward) * transform.forward * (arrowSpeed * Random.Range(.85f, 1.15f));

				//Debug.Log(arrow.transform.rotation);

				GameObject.Destroy(arrow, 3f);
			    }
			    // Set boolean to make sure we don't attack again this attack
			    alreadyAttacked = true;
			}
			
			// Raycast hit a tree, keep firing, hoping target comes back into view
			else if(hit.transform.tag == "Tree")
			{}
			
			// Raycast did NOT hit the target we wanted to shoot, cancel firing arrow, to prevent friendly fire
			else
			{
			    // Tell our sprite animators to play the cancel shooting animation
			    foreach(Animator anim in animators)
			    {
				anim.SetTrigger("CancelAttack");
			    }
			}
		    }
		}
	    }
	}
	// Animation not playing so we can attack now
	else
	{
	    // Create a raycast out the front of the zombie
	    Ray ray = new Ray(transform.position, transform.forward);

	    if(debugOn)
		Debug.DrawRay(transform.position, transform.forward * shootDistance, Color.green, 1f);

	    // Calculate if there was a hit, within the attack distance
	    if (Physics.Raycast(ray, out hit, shootDistance))
	    {
		if(debugOn)
		{
		    Debug.Log(hit.collider.gameObject.name);
		    Debug.Log(hit.point);
		}
		
		//Grab the tag off the hit object
		string objectTag = hit.transform.gameObject.tag;
		
		// Check if the hit object does not have the same tag as us, and is tagged as a unit we would attack (attack enemies, not allies)
		if(objectTag != transform.gameObject.tag && (objectTag == "PlayerUnit" || objectTag == "EnemyUnit" || objectTag == "Rogue") || objectTag == "WolfUnit" || (isThreat && objectTag == "Player"))
		{
		    // Our raycast hit an enemy, therefor an enemy is within melee range
		    // Start attack animation
		    foreach(Animator anim in animators)
		    {
			anim.SetTrigger("Shoot");
		    }

		    // If not playing a sound already
		    if(!audioSource.isPlaying)
		    {
			//Play drawing sound effect
			audioSource.clip = drawSound;
			audioSource.Play();
		    }

		    // Set alreadyAttacked boolean to false as we just started attacking
		    alreadyAttacked = false;

		    // If we are a zombie
		    if(zombScript.enabled)
		    {
			// If we have targets
			if(zombScript.targets.Count > 0)
			{
			    // Remember position of current target to calculate velocity
			    oldPosition = zombScript.targets[0].transform.position;
			    // Remember which target this refers to
			    oldTarget = zombScript.targets[0].name;
			}
		    }
		    // If we are not a zombie
		    else
		    {
			// Remember current position to guesstimate velocity next turn
			oldPosition = targets.targets[targets.closestEntry].transform.position;

			RememberUnit targetMemory = targets.targets[targets.closestEntry].GetComponent<RememberUnit>();
			// Remember which target this refers to, if any
			if(targetMemory.unit)
			    oldTarget = targetMemory.unit.name;
		    }
		}
	    }
	}
    }
}

