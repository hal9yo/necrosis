﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    //Audio Source to play sound effects
    public AudioSource audioSource;
    
    //Array of audio clips for idling
    public AudioClip[] idleSound;

    //Array of audio clips for dying
    public AudioClip[] dieSound;

    //Array of audio clips when hurt
    public AudioClip[] hurtSound;
    
    //Array of audio clips when attacking
    public AudioClip[] attackSound;

    //How often to play sounds;
    public float soundFrequency;

    //Timer of audio clips for idling
    public float idleTimer;

    //Array of audio clips for dying
    private bool diePlayed = false;

    //Array of audio clips when hurt
    private bool hurtPlayed = false;
    
    //Array of audio clips when attacking
    private bool attackPlayed = false;


    // Distance to target
    public float distance;
    
    //The distance zombie should reach before deleting navpoint
    public float distanceDesired;


    //Health script reference
    public Health ourHealth;
    
    //A boolean to determine if in the ground still
    public bool dead = false;
    
    //A boolean to determine if the zombie is currently selected
    public bool selected = false;

    //A list to hold waypoints
    public List<GameObject> targets = new List<GameObject>();

    
    //Boolean for if the zombie went rogue
    public bool isRogue = false;
    
    //How long without selecting does it take this character to go rogue?
    public float timeToRogue;

    //Timer for going rogue
    public float timer;

    // Whether or not the unit can go rogue
    public bool canRogue;

    
    //How far away to set wandering navpoints
    public float runDist;

    //Is this an archer?
    public bool archer = false;

    // Referene to this characters stats
    public Stats stats;

    //navmeshagent
    public UnityEngine.AI.NavMeshAgent navComponent;

    //Reference to waypoint prefab
    public GameObject movePointPrefab;

    //How much will it drains to control this unit
    public float willDrain;

    //Reference to attack script
    public Attack attackScript;

    //Reference to archer attack script
    public ArcherAttack archAttackScript;

    // Reference to fighting script for archery calculations
    public NPCFighting fightScript;
    

    //Distance variables for archers
    public float maxDistance;
    public float meleeDistance;

    //Compare health to previous
    private float healthWas;

    //Reference to the line of sight sphere used to see enemies (turn on when rogue so they can find enemies to attack)
    public GameObject vision;
    

    public bool debugOn;

    
    // Start is called before the first frame update
    void Start()
    {	
	//If archer set distances
	if(archAttackScript)
	{
	    if(!fightScript)
		fightScript = transform.GetComponent<NPCFighting>();
	    archer = true;
	    maxDistance = fightScript.maxDistance;
	    meleeDistance = fightScript.meleeDistance;
	}
	
	// Randomize navigation priority
	navComponent.avoidancePriority += Random.Range(-5, 5);

	//initialize idle timer
	idleTimer = Random.Range(0f, 10f);

	//initialize roguetimer
	timer = timeToRogue;

	//Set dieplayed to false so we can play the death sound when dying
	diePlayed = false;
    }

    

    // Update is called once per frame
    void Update()
    {
	//Are we resurrected?
	if(diePlayed && ourHealth.currHealth > 0)
	{
	    // Reset dieplayed so it can play when dying again
	    diePlayed = false;
	}
	//Did we just die?
	if(ourHealth.currHealth <= 0)
	{	    
	    //play random death sound
	    PlayRandom(2);
	    
	}
	//Did we just get hurt?
	else if(ourHealth.currHealth < healthWas)
	{
	    //Reset bool to keep from playing more than once
	    hurtPlayed = false;
	    
	    //play random hurt sound effect
	    PlayRandom(3);
	}
	else
	{
	    //when not selected, play idle sounds
	    if(!selected)
	    {
		//idle timer runs out
		if(idleTimer <= 0)
		{
		    //play random idle sound
		    PlayRandom(1);

		    //make angry animation play, to show we are getting angry
		    attackScript.angry = true;
		    
		    //reset idle timer to 10% of timetorogue (increase idle sound frequency closer to rogue, 2 seconds between, closer to constant)
		    if(isRogue)
			idleTimer = Random.Range(0f, 3f);
		    else
			idleTimer = timer * .5f;
		}
		//time not run out
		else
		{
		    //decrement timer
		    idleTimer -= Time.deltaTime;
		}
	    }
	}
	
	//Remember current health to compare next frame
	healthWas = ourHealth.currHealth;
	
	//If we are selected
	if(selected)
	{
	    //Debug.Log("selected");
	    
	    // If we are rogue
	    if(isRogue)
	    {
		//Switch life meter to good red
		ourHealth.GoodLifeMeter();

		//Stop viewing Player as threat
		attackScript.isThreat = false;
		if(archAttackScript)
		    archAttackScript.isThreat = false;

		//Stop attacking as a rogue
		attackScript.isRogue = false;

		// Turn off Line of Sight sphere
		vision.SetActive(false);
		
		// Turn off rogue tag on this script
		isRogue = false;
	    }

	    // Change tag to PlayerUnit
	    gameObject.tag = "PlayerUnit";
		
	    // Reset timer
	    timer = timeToRogue;
	}
	
	//We are not selected
	else
	{
	    //Debug.Log("not selected");
	    
	    //Have we gone rogue?
	    if(isRogue)
	    {
		
		// Change tag to Rogue
		gameObject.tag = "Rogue";
		    
		//Do we need a navpoint?
		if(targets.Count < 1)
		{
		    //Randomly create a navpoint on navmesh 4 units away
		    GameObject movePoint = Instantiate(movePointPrefab, RandomNavmeshLocation(runDist), Quaternion.identity);

		    //Insert the new waypoint to the list as the top priority
		    targets.Add(movePoint);
		}
	    }
	    
	    //If not already rogue, count time
	    else
	    {
		if(canRogue)
		{
		    //If it has been too long since selected
		    if(timer < 0)
		    {
			//Clear current waypoints, start fresh
			DestroyAllNavPoints();
		    
			//Gone rogue
			isRogue = true;

			//Tell attack script to start attacking the player, and all targets including same side as this zombie
			attackScript.isRogue = true;

			// Turn on Line of Sight sphere
			vision.SetActive(true);
		    
			// Change tag to Rogue
			gameObject.tag = "Rogue";
		    }
		    else
		    {
			//Keep counting time since last selected
			timer -= Time.deltaTime;
		    }
		}
	    }
	}

	//If we have any targets
	if(targets.Count > 0)
	{
	    //Make sure it is not a null entry in the list (other npc may have destroyed waypoint upon reaching it
	    if(targets[0] == null || targets[0].transform.CompareTag("Dead") || targets[0].transform.CompareTag("PermaDead") || targets[0].transform.CompareTag("Rogue"))
	    {
		//Remove empty list entry
		targets.RemoveAt(0);
	    }
	    
	    else if(archer && targets[0].gameObject.CompareTag("EnemyUnit"))
	    {
		ArcherNavigate();
	    }
	    else
	    {
		Navigate();
	    }
	}	
    }
    

    void DestroyNavPoint()
    {
	//Check if enemy or dead
	if(targets[0].transform.CompareTag("EnemyUnit") || targets[0].transform.CompareTag("PlayerUnit") || targets[0].transform.CompareTag("Dead") || targets[0].transform.CompareTag("PermaDead") || targets[0].transform.CompareTag("Rogue"))
	{
	    //Unhighlight enemy
	    targets[0].transform.GetComponent<Health>().selected = false;
	}
	//Check if player
	else if(targets[0].transform.CompareTag("Player"))
	{}
	//If neither player, enemy or dead
	else
	{
	    //Destroy GameObject
	    Destroy(targets[0]);
	}
	//Remove entry from list
	targets.RemoveAt(0);	
    }

    
    public void DestroyAllNavPoints()
    {
	//Go through each object in the list and destroy it
	foreach(GameObject obj in targets)
	{
	    //Check if null
	    if(obj != null)
	    {
		//Check if enemy
		if(obj.transform.CompareTag("Waypoint"))
		{
		    //Destroy waypoint
		    Destroy(obj);
		}
	    }	    
	}
	targets.Clear();
    }

    
    void OnDisable()
    {
	//clear all navpoints
	DestroyAllNavPoints();	
    }

    
    void OnTriggerStay(Collider other)
    {
	//Ignore trigger colliders and arrows
	if(other.isTrigger || other.gameObject.tag == "Arrow")
	{
	    //Break method to do no more
	    return;
	}
	
	//Ignore unless we are rogue
	if(isRogue)
	{
	    if(debugOn)
		Debug.Log("rogue targetting");
	    //Make sure it is an attackable entity
	    if(other.gameObject.tag =="PlayerUnit" || other.gameObject.tag == "Player" || other.gameObject.tag == "EnemyUnit")
	    {
		//Make sure this enemy is not already targeted
		foreach(GameObject obj in targets)
		{
		    if(obj != null)
		    {
			//If the gameobject passed to us is the same as the one we are checking in our list
			if(GameObject.ReferenceEquals(obj, other.gameObject))
			{
			    //Break out of function so as not to re-add the gameobject as it is already in the list
			    return;
			}
		    }
		}
		//Not already targeted
		//Determine distance to enemy
		float distance = Vector3.Distance(transform.position, other.transform.position);

		// Is this new enemy closer than our current target if any?
		if(targets.Count > 0)
		{
		    if(distance < Vector3.Distance(transform.position, targets[0].transform.position))
		    {
			//Target the enemy priority number 1
			targets.Insert(0, other.gameObject);
		    }
		    //Not closer just add to end of targets list
		    else
		    {
			targets.Add(other.gameObject);
		    }
		}
	    }
	}
    }


    public Vector3 RandomNavmeshLocation(float radius)
    {
	Vector3 finalPosition = Vector3.zero;
	while(finalPosition == Vector3.zero)
	{
	    Vector3 randomDirection = Random.insideUnitSphere * radius;
	    randomDirection += transform.position;
	    UnityEngine.AI.NavMeshHit hit;
	    if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
	    {
		finalPosition = hit.position;            
	    }
	}
	return finalPosition;
     }


    void Navigate()
    {
	// If we have any navpoints
	if(targets.Count > 0)
	{
	    //Determine distance to navpoint and save in variable distance
	    distance = Vector3.Distance(transform.position, targets[0].transform.position);
	
	    //If navpoint reached
	    if(distance <= distanceDesired)	   
	    {
		if(debugOn)
		    Debug.Log("navpoint reached");
		
		//If not a waypoint
		if(!(targets[0].transform.CompareTag("Waypoint") || (isRogue && targets[0].transform.CompareTag("Rogue"))))
		{
		    //Make sure we are not currently attacking
		    if(!attackScript.alreadyAttacked)
		    {
			if(debugOn)
			    Debug.Log("look at enemy");
			
			//Look at enemy (so if we are already close enough not to move, we at least attack
			transform.LookAt(targets[0].transform);

			//Reset bool to keep attack sound from playing more than once per attack
			attackPlayed = false;
		    }
		    // Attacking, play sound effect
		    else
		    {
			if(debugOn)
			    Debug.Log("already attacking");
			
			//Play random attack sound
			PlayRandom(4);
		    }

		}
		//If it is a waypoint
		else
		{
		    //Destroy navpoint (removes from list, only destroys if not dead body, otherwise deselects)
		    DestroyNavPoint();
		}
	    }
	    //navpoint not reached
	    else
	    {
		//Look at enemy (so if we are already close enough not to move, we at least attack
		transform.LookAt(targets[0].transform);
			
		//Setnavmesh destination to the movepoint position
		navComponent.SetDestination(targets[0].transform.position);

		//Set speed
		navComponent.speed = stats.runSpeed + Random.Range(-.2f, .2f);
	    }
	}
    }

    

    void ArcherNavigate()
    {
	//Determine distance to navpoint and save in variable distance
	float distance = Vector3.Distance(transform.position, targets[0].transform.position);

	//If it is an enemy, player, or playerunit
	if(targets[0].transform.CompareTag("EnemyUnit") || targets[0].transform.CompareTag("PlayerUnit") || targets[0].transform.CompareTag("Player"))
	{
	    //If we are further than we want to be
	    if(distance > maxDistance)
	    {
		//Setnavmesh destination to the target position
		navComponent.SetDestination(targets[0].transform.position);
	    }
	    //Target is in range
	    else
	    {
		//Not within melee range
		if(distance > meleeDistance)
		{
		    // Raycast hit variables to contain the first object hit by the linecasts
		    RaycastHit hit1;
		    RaycastHit hit2;
		    RaycastHit hit3;

		    Debug.DrawLine(transform.position, targets[0].transform.position, Color.green, 1f);
		    Debug.DrawLine(transform.position + transform.right * 1.5f, targets[0].transform.position + targets[0].transform.right, Color.green, 1f);
		    Debug.DrawLine(transform.position - transform.right * 1.5f, targets[0].transform.position - targets[0].transform.right, Color.green, 1f);

		    // Look at the target to see if we have unobstructed line of sight
		    transform.LookAt(targets[0].transform);
		    
		    // Use linecast to check if we have direct line of sight to the object from our center
		    if(Physics.Linecast(transform.position, targets[0].transform.position, out hit1) && Physics.Linecast(transform.position + transform.right * 1.5f, targets[0].transform.position, out hit2) && Physics.Linecast(transform.position - transform.right * 1.5f, targets[0].transform.position, out hit3))
		    {
			//Did any of the linecasts hit an ally or the player?
			if(hit1.collider.tag == "PlayerUnit" || hit2.collider.tag == "PlayerUnit" || hit3.collider.tag == "PlayerUnit" || hit1.collider.tag == "Player" || hit2.collider.tag == "Player" || hit3.collider.tag == "Player")
			{
			    //Create navpoint to move left or right, depending on where the unit obscuring our line of sight is
			    Vector3 moveDirection;

			    float dir1 = 0f;
			    float dir2 = 0f;
			    float dir3 = 0f;
			    
			    if(hit1.collider.tag == "PlayerUnit" || hit1.collider.tag == "Player")
			    {
				dir1 = LeftOrRight(hit1.transform.position);

				if(debugOn)
				    Debug.Log("hit1: " + dir1);
			    }
			    if(hit2.collider.tag =="PlayerUnit" || hit2.collider.tag == "Player")
			    {
				dir2 = LeftOrRight(hit2.transform.position);

				if(debugOn)
				    Debug.Log("hit2: " + dir2);
			    }
			    if(hit3.collider.tag =="PlayerUnit" || hit3.collider.tag == "Player")
			    {
				dir3 = LeftOrRight(hit3.transform.position);

				if(debugOn)
				    Debug.Log("hit3: " + dir3);
			    }
			    
			    if(debugOn)
			    Debug.Log("hit1+hit2+hit3 = " + (dir1+dir2+dir3));

			    // hit player/playerunits are more to the right of us
			    if(dir1 + dir2 + dir3 > 0)
				// Move to the left
				moveDirection = transform.position + transform.right * -5f + transform.forward * 5f;
			    // hit player/playerunits are more to the left of us
			    else if(dir1 + dir2 + dir3 < 0)
				// Move to the right
				moveDirection = transform.position + transform.right * 5f + transform.forward * 5f;
			    // hit player/playerunits are directly in front of us
			    else
			    {
				//Randomly choose left or right to move to
				if(Random.Range(0, 2) > 0)
				    moveDirection = transform.position + transform.right * 10f + transform.forward * 5f;
				else
				    moveDirection = transform.position + transform.right * -10f + transform.forward * 5f;
			    }

			    // Make a waypoint in the direction we want to move to try to get clear line of sight without hitting our allies
			    GameObject movePoint = Instantiate(movePointPrefab, moveDirection, Quaternion.identity);

			    //Insert the new waypoint to the list as the top priority
			    targets.Insert(0, movePoint);
			}
			// Raycasts hit something, is it the target?
			else if(GameObject.ReferenceEquals(hit1.collider.gameObject, targets[0]) && GameObject.ReferenceEquals(hit2.collider.gameObject, targets[0]) && GameObject.ReferenceEquals(hit3.collider.gameObject, targets[0]))
			{			    
			    // Stop moving if moving
			    navComponent.isStopped = true;
			    navComponent.ResetPath();

			    // Look at enemy to line up a shot
			    transform.LookAt(targets[0].transform);
			}
			//Raycasts hit something else
			else
			{
			    //Move closer
			    //Setnavmesh destination to the target position
			    navComponent.SetDestination(targets[0].transform.position);
			}
		    }
		    //Raycast hit nothing...No line of sight? Move closer
		    else
		    {
			Debug.Log("Raycast hit nothing");
			//Setnavmesh destination to the target position
			navComponent.SetDestination(targets[0].transform.position);
		    }
		}
		//We are too close, just engage in melee
		else
		{
		    // Are we close enough to attack?
		    if(distance <= distanceDesired)
		    {
			//If not a waypoint
			if(!targets[0].transform.CompareTag("Waypoint"))
			{
			    //Make sure we are not currently attacking
			    if(!attackScript.alreadyAttacked)
			    {
				//Look at enemy (so if we are already close enough not to move, we at least attack
				transform.LookAt(targets[0].transform);

				//Reset bool to keep attack sound from playing more than once per attack
				attackPlayed = false;
			    }
			    // Attacking, play sound effect
			    else
			    {
				//Play random attack sound
				PlayRandom(4);
			    }

			}
			//If it is a waypoint
			else
			{
			    //Destroy navpoint (removes from list, only destroys if not dead body, otherwise deselects)
			    DestroyNavPoint();
			}
		    }
		    // Not close enough to attack, move closer
		    else
		    {
			//Setnavmesh destination to the enemy position
			navComponent.SetDestination(targets[0].transform.position);	    
		    }
		}
	    }
	}
	
	//Not an enemy, just travel to point
	else
	{
	    // Have we reached the navpoint?
	    if(distance <= distanceDesired)
	    {
		//Destroy navpoint (removes from list, only destroys if not dead body, otherwise deselects)
		DestroyNavPoint();
	    }
	    // We have not reached navpoint, set destination
	    else
	    {
		//Setnavmesh destination to the movepoint position
		navComponent.SetDestination(targets[0].transform.position);
	    }
	}
    }



    
    //Play a random audioclip from one of several groups
    void PlayRandom(int option)
    {
	//If not already playing sound effect
	if(!audioSource.isPlaying)
	{
	    //select which type of sound to play
	    switch(option)
	    {
		    //Idling
		case 1:
		    // Play the sound
		    audioSource.clip = idleSound[Random.Range(0, idleSound.Length)];

		    //End of this case
		    break;

		    
		    //Dying
		case 2:
		    // Make sure we didn't already play the sound
		    if(!diePlayed)
		    {
			// Play the sound
			audioSource.clip = dieSound[Random.Range(0, dieSound.Length)];

			// Mark the sound as played so as not to play again
			diePlayed = true;
		    }
		    // End of this case
		    break;

		    
		    //Getting hurt
		case 3:
		    if(!hurtPlayed)
		    {
			//Play the sound
			audioSource.clip = hurtSound[Random.Range(0, hurtSound.Length)];
			//Make sure we don't play sound again this hurt
			hurtPlayed = true;
		    }

		    //Break out of case switch
		    break;

		    
		    //Attacking
		case 4:
		    // If the bool to keep from playing twice is not set yet
		    if(!attackPlayed)
		    {
			//Play the sound
			audioSource.clip = attackSound[Random.Range(0, attackSound.Length)];

			// Bool to keep sound from playing more than once per attack
			attackPlayed = true;
		    }
		    //Break out of case switch (done with case)
		    break;
	    }

	
	    //Play the sound
	    audioSource.Play();
	}
    }


    
    //Return -1 if object is left of us, 1 if right of us, 0 if straight ahead
    float LeftOrRight(Vector3 targetObj)
    {
	//Get direction to the target Object
	Vector3 targetDir = targetObj - transform.position;
	
	//Get crossproduct between our forward and the direction to the object
	Vector3 perp = Vector3.Cross(transform.forward, targetDir);

	//Get dot between the crossproduct and our up
	float dir = Vector3.Dot(perp, transform.up);
		
	if (dir > 0f)
	    return 1f;
	else if (dir < 0f) 
	    return -1f;
	else 
	    return 0f;
    }
}
