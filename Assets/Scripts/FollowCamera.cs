﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Camera camera;

    public float right;
    public float down;
    public float depth;

    Vector3 position;

    // Update is called once per frame
    void Update()
    {
	position = new Vector3(right, down, depth);
        transform.position = camera.ViewportToWorldPoint(position);
    }
}
