﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WolfFighting : MonoBehaviour
{
    // Reference to our targets script
    public WolfTargets targets;

    // Reference to our navmeshagent
    public NavMeshAgent navComponent;

    // Reference to our attack script
    public Attack attackScript;

    // Reference to our NPC sound script
    public NPCSounds npcSound;

    // Reference to our Stats
    public Stats stats;

    // Reference to our search script
    public NPCSearch npcSearch;

    // Remember the last place the target was, to guesstimate future position
    Vector3 oldPosition;

    // Target velocity
    Vector3 targetVelocity;
    
    //Desired distance to keep away from targets
    public float minDistance;

    //Desired distance to stay near to targets
    public float maxDistance;

    //Bool to turn on debug messages
    public bool debugOn = false;

    // How far to run from enemies when they get to close
    public float runDist;

    // Set to true when attacking, rush enemy, when false retreat to safe distance before making attack run again
    public bool attacking;

    // Keeps track of the speed the enemy is moving at, to aim ahead of them and miss less
    Vector3 enemyVelocity;

    // Remember previous position of target to measure velocity
    Vector3 enemyPrevPos;

    // Remember which enemy we are measuring the velocity of
    GameObject enemyRemembered;

    // Amount to lead target by, multiplied to enemy velocity to aim ahead of them
    public float leadAmount = .1f;


    

    // Main method that makes the gameobject move towards the closest enemy they are aware of
    public void EngageTargets()
    {
	//Calculate enemy velocity, if possible
	enemyVelocity = GetEnemyVelocity();
	
	// Remember current position for next calculation
	enemyPrevPos = targets.targets[targets.closestEntry].transform.position;

	// Note which gameObject we are remembering the position of for next frame (so we don't add two different units together and get an exaggerated velocity
	enemyRemembered = targets.targets[targets.closestEntry];
	
	//If we are attacking and we're further than we want to be
	if(attacking && targets.closestDistance > maxDistance)
	{
	    // Look at target to try to hit them
	    transform.LookAt(targets.targets[targets.closestEntry].transform.position);

	    //Setnavmesh destination to the target position, plus their velocity
	    navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position + enemyVelocity * leadAmount);
	}
	// If we're attacking and we get to attacking range
	else if(attacking && targets.closestDistance <= maxDistance)
	{
	    // Stop attacking, to run away to prepare for next attack run
	    attacking = false;
	}
	// If we are not yet attacking, but we are at the right distane to start
	else if(!attacking && targets.closestDistance >= minDistance)
	{
	    // Start an attack run
	    attacking = true;
	}
	//Target is too close for comfort
	else if(!attacking && targets.closestDistance < minDistance)
	{
	    //Set navmesh destination in opposite direction from target
	    navComponent.SetDestination(transform.position + (transform.position - targets.targets[targets.closestEntry].transform.position).normalized * runDist);

	    //Debug.Log("Run to " + runTo);
	}
	//We are at desired running distance
	else
	{
	    if(debugOn)
		Debug.Log("look at target");

	    // Find closest target
	    targets.FindClosestFurthest();
		
	    //Look at target so we can attack
	    transform.LookAt(targets.targets[targets.closestEntry].transform.position);

	    // Raycast hit variables to contain the first object hit by the linecasts
	    RaycastHit hit1;
	    RaycastHit hit2;
	    RaycastHit hit3;
	    
	    // Draw the lines they use to check for allies in the way so we can debug this
	    Debug.DrawLine(transform.position, targets.targets[targets.closestEntry].transform.position);
	    Debug.DrawLine(transform.position + transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position);
	    Debug.DrawLine(transform.position - transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position);

	    
	    // Use linecast to check if we have direct line of sight to the object from our center
	    if(Physics.Linecast(transform.position, targets.targets[targets.closestEntry].transform.position, out hit1) && Physics.Linecast(transform.position + transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position, out hit2) && Physics.Linecast(transform.position - transform.right * 1.5f, targets.targets[targets.closestEntry].transform.position, out hit3))
	    {
		//Did any of the linecasts hit an ally?
		if(hit1.collider.tag == "EnemyUnit" || hit2.collider.tag == "EnemyUnit" || hit3.collider.tag == "EnemyUnit")
		{
		    //Create navpoint to move left or right, depending on where the unit obscuring our line of sight is
		    Vector3 moveDirection;

		    float dir1 = 0f;
		    float dir2 = 0f;
		    float dir3 = 0f;
			    
		    if(hit1.collider.tag == "EnemyUnit")
		    {
			dir1 = LeftOrRight(hit1.transform.position);

			if(debugOn)
			    Debug.Log("hit1: " + dir1);
		    }
		    if(hit2.collider.tag =="EnemyUnit")
		    {
			dir2 = LeftOrRight(hit2.transform.position);

			if(debugOn)
			    Debug.Log("hit2: " + dir2);
		    }
		    if(hit3.collider.tag =="EnemyUnit")
		    {
			dir3 = LeftOrRight(hit3.transform.position);

			if(debugOn)
			    Debug.Log("hit3: " + dir3);
		    }
			    
		    if(debugOn)
			Debug.Log("hit1+hit2+hit3 = " + (dir1+dir2+dir3));

		    // hit player/playerunits are more to the right of us
		    if(dir1 + dir2 + dir3 > 0)
			// Move to the left
			moveDirection = transform.position + transform.right * -10f + transform.forward * 5f;
		    // hit player/playerunits are more to the left of us
		    else if(dir1 + dir2 + dir3 < 0)
			// Move to the right
			moveDirection = transform.position + transform.right * 10f + transform.forward * 5f;
		    // hit player/playerunits are directly in front of us
		    else
		    {
			//Randomly choose left or right to move to
			if(Random.Range(0, 2) > 0)
			    moveDirection = transform.position + transform.right * 10f + transform.forward * 5f;
			else
			    moveDirection = transform.position + transform.right * -10f + transform.forward * 5f;
		    }

		    // Move to the strafe location
		    navComponent.SetDestination(moveDirection);
		}
		//Do we have line of sight on target?
		else
		{
		    if(debugOn)
			Debug.Log("In range, line of sight, stop moving");
		    //Stop moving
		    navComponent.isStopped = true;
		    navComponent.ResetPath();
		}
	    }

	    //No line of sight
	    else
	    {
		if(debugOn)
		    Debug.Log("In range, no line of sight, stop moving");
		//move closer
		navComponent.SetDestination(targets.targets[targets.closestEntry].transform.position);
	    }
	}
    }



    //Return -1 if object is left of us, 1 if right of us, 0 if straight ahead
    float LeftOrRight(Vector3 targetObj)
    {
	//Get direction to the target Object
	Vector3 targetDir = targetObj - transform.position;
	
	//Get crossproduct between our forward and the direction to the object
	Vector3 perp = Vector3.Cross(transform.forward, targetDir);

	//Get dot between the crossproduct and our up
	float dir = Vector3.Dot(perp, transform.up);
		
	if (dir > 0f)
	    return 1f;
	else if (dir < 0f) 
	    return -1f;
	else 
	    return 0f;
    }

    public Vector3 GetEnemyVelocity()
    {
	// A variable just to calculate it real quick
	Vector3 calcVelocity = Vector3.zero;
	
	// Run the method to calculate closest target and its distance to us
	targets.FindClosestFurthest();

	//Measure velocity of target:
	// Is this the target we've been remembering
	if(targets.targets[targets.closestEntry] && targets.targets[targets.closestEntry] == enemyRemembered)
	{
	    // velocity = current position - previous position
	    calcVelocity = targets.targets[targets.closestEntry].transform.position - enemyPrevPos;
	}
	
	// Pass the velocity back to calling script
	return calcVelocity;
    }
}
