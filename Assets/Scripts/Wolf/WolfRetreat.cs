﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfRetreat : MonoBehaviour
{
    // Reference to our health script
    public Health ourHealth;

    // Refernce to our patrol script
    public NPCPatrol patrol;

    // Reference to our targets script
    public WolfTargets targets;

    // Reference to our stats
    public Stats stats;

    // Reference to the navmeshagent
    public UnityEngine.AI.NavMeshAgent navComponent;
    
    

    // List of homebase waypoints to fall back to when outnumbered
    public List<GameObject> homeBases = new List<GameObject>();

    // A tag to tell us we are already running
    public bool running;

    // Maximum distance to set wander points from self when wandering aimlessly
    public float runDist;

    // Keep track of which homebase we have returned to, so we can go to successive bases
    int homeBase = 0;

    // Number of ally power we need to not be overpowered
    public int powerNeeded = 0;
    
    // Turn on for debugging messages
    public bool debugOn;

    
    // Distance from navpoint we need to be to consider it reached
    public float distDesired = 3f;



    public bool AssessThreat()
    {
	//boolean to return true if found to be overpowered
	bool overPowered = false;

	//number of our allies currently targeting the same group of enemies as us
	int alliesAssisting = 0;
	int enemiesSighted = 0;
	
	//include ourself, if our health better than half
	if(ourHealth.currHealth >= 2)
	    alliesAssisting = stats.power;

	//Create an array of all gameobjects in vicinity
	Collider[] colliders = Physics.OverlapSphere(transform.position, 25f);

	foreach(Collider col in colliders)
	{
	    if(col.transform.CompareTag("EnemyUnit"))
	    {
		
		// A raycast hit variable to contain the first object hit by the linecast
		RaycastHit hit;
		
		// Use linecast to check if we have direct line of sight to the object
		Physics.Linecast(transform.position, col.transform.position, out hit);
		if(hit.collider != null)
		{
		    // If the linecast hit the gameobject in question
		    if(GameObject.ReferenceEquals(hit.collider.gameObject, col.gameObject))
		    {
			// Add the power level of this NPC to our total power of allies
			alliesAssisting += col.gameObject.GetComponent<Stats>().power;
		    }
		}
	    }
	    else if(col.transform.CompareTag("PlayerUnit") || (targets.isThreat && col.transform.CompareTag("Player")))
	    {
		if(debugOn)
		    Debug.Log("Spotted " + col.transform.gameObject);
		
		// A raycast hit variable to contain the first object hit by the linecast
		RaycastHit hit;
	
		// Use linecast to check if we have direct line of sight to the object
		Physics.Linecast(transform.position, col.transform.position, out hit);

		if(hit.collider != null)
		{
		    // If the linecast hit the gameobject in question
		    if(GameObject.ReferenceEquals(hit.collider.gameObject, col.gameObject))
		    {
			// Add the power level of this player unit to total power of enemies
			enemiesSighted += col.gameObject.GetComponent<Stats>().power;

			if(debugOn)
			    Debug.Log("enemies sighted power = " + enemiesSighted);
		    }
		}
	    }
	}
	
	//If the number of enemies in sight is greater than the allies helping us fight
	if(enemiesSighted > alliesAssisting)
	{
	    overPowered = true;
	}

	// Print debug messages if told to
	if(debugOn)
	{
	    Debug.Log("enemies: " + enemiesSighted + "  allies: " + alliesAssisting);
	    Debug.Log("overpowered: " + overPowered);
	}

	if(overPowered)
	    powerNeeded = enemiesSighted;
		
	return overPowered;
    }




    public void Retreat()
    {
	// Is any enemy too close for comfort?
	if(targets.closestDistance < runDist)
	{
	    // Run opposite direction from enemy
	    navComponent.SetDestination((transform.position + transform.position - targets.targets[targets.closestEntry].transform.position) * runDist);
	}
	// Otherwise run to homebase (safe place to find allies) or if no homebases,  run randomly
	else
	{
	    // Do we have any homebase waypoints to return to, and we have not returned to them all?
	    if(homeBases.Count > 0)
	    {
	
		// Are we further than we want to be from them?
		if(Vector3.Distance(transform.position, homeBases[homeBase].transform.position) > 5f)
		{
		    if(debugOn)
			Debug.Log("Run to homebase " + homeBase);
		    navComponent.SetDestination(homeBases[homeBase].transform.position);
		}
		// We have reached the homebase in question
		else
		{
		    if(debugOn)
			Debug.Log("Homebase reached, still not enough allies, run to next homebase");
		    // Iterate homebase that we are travelling to to the next one
		    homeBase++;

		    // If out of homebases, run aimlessly
		    if(homeBase > homeBases.Count)
			RunAway();
		}
	    }
	    // We either have no homebases or we have visted them all
	    else
	    {
		if(debugOn)
		    Debug.Log("No homebases, or all have been visted, just run aimlessly");
		RunAway();
	    }
	}
    }




    public void RunAway()
    {
	if(debugOn)
	    Debug.Log("Run aimlessly!");
	// If we have just started running
	if(!running)
	{
	    // Set a random destination
	    navComponent.SetDestination(RandomNavmeshLocation(runDist));

	    // Set running tag to true
	    running = true;
	}
	// If we are already running and we have reached the current run to point
	else if(navComponent.remainingDistance < distDesired)
	{
	    // Set a random destination
	    navComponent.SetDestination(RandomNavmeshLocation(runDist));
	}
    }



    public int CheckAllies()
    {
	int alliesAssisting = 0;
	//Create an array of all gameobjects in vicinity
	Collider[] colliders = Physics.OverlapSphere(transform.position, 25f);

	//include ourself, if our health better than half
	if(ourHealth.currHealth >= 2)
	    alliesAssisting = stats.power;
	
	foreach(Collider col in colliders)
	{
	    if(col.transform.CompareTag("EnemyUnit"))
	    {
		
		// A raycast hit variable to contain the first object hit by the linecast
		RaycastHit hit;
		
		// Use linecast to check if we have direct line of sight to the object
		Physics.Linecast(transform.position, col.transform.position, out hit);
		if(hit.collider != null)
		{
		    // If the linecast hit the gameobject in question
		    if(GameObject.ReferenceEquals(hit.collider.gameObject, col.gameObject))
		    {
			// Add the power level of this NPC to our total power of allies
			alliesAssisting += col.gameObject.GetComponent<Stats>().power;
		    }
		}
	    }
	}
	return alliesAssisting;
    }

    
    
    Vector3 RandomNavmeshLocation(float radius)
    {
	UnityEngine.AI.NavMeshHit hit;
	Vector3 finalPosition = Vector3.zero;
	while(finalPosition == Vector3.zero)
	{
	    Vector3 randomDirection = Random.insideUnitSphere * radius;
	    randomDirection += transform.position;

	    if (UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
	    {
		finalPosition = hit.position;            
	    }
	}
	return finalPosition;
    }
}
