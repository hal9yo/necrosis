﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveEnvironment : MonoBehaviour
{    
    // Code to run when environment object is used
    public virtual void Use()
    {
    }
}
