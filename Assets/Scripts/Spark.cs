﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spark : MonoBehaviour
{
    //Speed blood is squirted at
    public Vector3 velocity = new Vector3(0f, 0f, 0f);

    //Reference to the arrow's rigidbody
    private Rigidbody rb;

    // How long is the spark
    public float length;

    // How wide is the spark
    public float width;

    // Material to render spark in
    public Material sparkMat;
    public Material sparkMat2;
    public Material sparkMat3;

    // How long before disabling spark
    public float ttl;
    private float createdTime;
    
    void Start()
    {
	// Reference our rigidbody so we can set a velocity
	rb = GetComponent<Rigidbody>();
    }

    void OnEnable()
    {
	// Reference our created time so we know when to disable and when to change color
	createdTime = Time.time;
    }
    void Update()
    {
	// Get our  velocity from the rigidbody
	velocity = rb.velocity;

	// Draw a line from our position to where we have just come from
	DrawSpark(transform.position, transform.position - velocity * length);

	// if ttl exceded
	if(Time.time - createdTime > ttl)
	    // disable the gameobject
	    gameObject.SetActive(false);
    }

    void LateUpdate()
    {
	Vector3 newPosition = transform.position + velocity * Time.deltaTime;

	transform.LookAt(Quaternion.AngleAxis(-45, Vector3.up) *  newPosition + transform.position);
    }

    

    //Draw blood droplets in the air, they will be erased after .05 seconds
    void DrawSpark(Vector3 start, Vector3 end, float duration = 0.05f)
    {
	GameObject myLine = new GameObject();
	myLine.transform.position = start;
	myLine.AddComponent<LineRenderer>();
	LineRenderer lr = myLine.GetComponent<LineRenderer>();
	if(Time.time - createdTime < .3f)
	    lr.material = sparkMat;
	else if(Time.time - createdTime < .6f)
	    lr.material = sparkMat2;
	else
	    lr.material = sparkMat3;
	
	//lr.SetColors(color, color);
	//lr.SetWidth(0.1f, 0.1f);
	lr.startWidth = width;
	lr.endWidth = width;
	lr.SetPosition(0, start);
	lr.SetPosition(1, end);
	lr.sortingOrder = 10;
	GameObject.Destroy(myLine, duration);
    }
}
