﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowMoveAndDirectionAnimation : MonoBehaviour
{
    // Reference to the animator
    public Animator anim;

    // Minimum movement distance to be change animation to walking
    public float minMove;
    
    // Store current position to check next frame
    private Vector3 position;

    // Timer variable for checking movement
    private float timer;

    // Transform of followed object
    public Transform other;

    // A number to set our x scale to look right (negated to look left)
    float rightScale;

    // Variable for manipulating scale of object before setting to this variable's value
    Vector3 localscale;

    Vector3 lastPosition;
	
    // Start is called before the first frame update
    void Start()
    {
	rightScale = transform.parent.transform.localScale.x;
        position = transform.position;
	lastPosition = transform.position;
	timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
	
	localscale = transform.parent.transform.localScale;

	
	// Set animation direction by movement
	if(transform.position.x - lastPosition.x < -minMove)
	{
	    localscale.x = rightScale;
	    transform.parent.transform.localScale = localscale;
	}
	if(transform.position.x - lastPosition.x > minMove)
	{
	    localscale.x = -rightScale;
	    transform.parent.transform.localScale = localscale;
	}

	// If we moved down, set gliding animation
	if(transform.position.y - lastPosition.y < -minMove)
	    anim.SetTrigger("Gliding");

	// If we moved up, set flapping animation
	if(transform.position.y - lastPosition.y > minMove)
	    anim.SetTrigger("Flapping");	

	if(transform.position.z - lastPosition.z < -minMove)
	    anim.SetFloat("Vertical", 0f);
	else if(transform.position.z - lastPosition.z > minMove)
	    anim.SetFloat("Vertical", 1f);
	else
	    anim.SetFloat("Vertical", .5f);

	lastPosition = transform.position;
    }
}

