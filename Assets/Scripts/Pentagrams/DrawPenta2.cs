﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPenta2 : MonoBehaviour
{
    
    float time = 0f;
    float length1 =  -5f;
    float length2 =  5f;
    float length3 =  -5f;
    float length4 =  -5.25f;
    float length5 = 1f;

    public GameObject sparkPrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
	
        if (Input.GetButtonDown("Fire1"))
        {
	    Time.timeScale = 1.0f;
	  
	    //transform.position = new Vector3(toFollow.position.x, 5f, toFollow.position.z);
	
	    if(length1 < 5f && length5 <= -5f)
		length1 += 2f;
	    if(length2 > -5f)
		length2 -= 2f;
	    if(length3 < 1f && length2 <= -5f)
		length3 += 1.2f;
	    if(length4 < 5.25f && length3 >= 1f)
		length4 += 2f;
	    if(length5 > -5f && length4 >= 5.25f)
		length5 -= 1.2f;
	    
	    if(length1 > 5f)
		length1 = 5f;
	    if(length2 < -5f)
		length2 = -5f;
	    if(length3 > 1f)
		length3 = 1f;
	    if(length4 > 5.25f)
		length4 = 5.25f;
	    if(length5 < -5f)
		length5 = -5f;

	    Debug.Log(length1 + " " + length2 + " " + length3 + " " + length4 + " " + length5);
	
	    for(int i = 0; i < (length1 + 5) * 20; i++)
	    {
		// choose a random x coordinate for the spark
		float x = (Random.Range(-5f, length1));
		// calculate y coordinate based on x coordinate, make it a vector3
		Vector3 location = new Vector3(x/3 - 1.66f, 0f, x);

		GameObject spark = Instantiate(sparkPrefab, location, Quaternion.identity);

		
		//spark.transform.SetParent(toFollow);
		Spark sparkScript = spark.GetComponent<Spark>();

		//Randomize length, width and speed of blood droplet
		sparkScript.length = Random.Range(.05f, .1f);
		sparkScript.width = Random.Range(.04f, .06f);

		//Randomize time to live
		sparkScript.ttl = Random.Range(.5f, 1f);

		// give it a random velocity
		spark.GetComponent<Rigidbody>().velocity = transform.forward * Random.Range(.5f, 2f);
	    
	    }
	    for(int i = 0; i < (-length2 + 5) * 20; i++)
	    {
		// choose a random x coordinate for the spark
		float x = (Random.Range(length2, 5f));
		// calculate y coordinate based on x coordinate, make it a vector3
		Vector3 location = new Vector3(-x/3 + 1.66f, 0f, x);
		GameObject spark = Instantiate(sparkPrefab, location, Quaternion.identity);

		
		//spark.transform.SetParent(toFollow);
		Spark sparkScript = spark.GetComponent<Spark>();

		//Randomize length, width and speed of blood droplet
		sparkScript.length = Random.Range(.05f, .1f);
		sparkScript.width = Random.Range(.04f, .06f);

		//Randomize time to live
		sparkScript.ttl = Random.Range(.5f, 1f);

		// give it a random velocity
		spark.GetComponent<Rigidbody>().velocity = transform.forward * Random.Range(.5f, 2f);
	    }
	    for(int i = 0; i < (length3 + 5) * 20; i++)
	    {
		// choose a random x coordinate for the spark
		float x = (Random.Range(-5f, length3));
		// calculate y coordinate based on x coordinate, make it a vector3
		Vector3 location = new Vector3((x * -1.5f) - 4.2f, 0f, x);
		GameObject spark = Instantiate(sparkPrefab, location, Quaternion.identity);

		
		//spark.transform.SetParent(toFollow);
		Spark sparkScript = spark.GetComponent<Spark>();

		//Randomize length, width and speed of blood droplet
		sparkScript.length = Random.Range(.05f, .1f);
		sparkScript.width = Random.Range(.04f, .06f);

		//Randomize time to live
		sparkScript.ttl = Random.Range(.5f, 1f);

		// give it a random velocity
		spark.GetComponent<Rigidbody>().velocity = transform.forward * Random.Range(.5f, 2f);
	    }
	    for(int i = 0; i < (length4 + 5) * 20; i++)
	    {
		// choose a random x coordinate for the spark
		float x = (Random.Range(-5.5f, length4));
		// calculate y coordinate based on x coordinate, make it a vector3
		Vector3 location = new Vector3(x, 0f, 1f);
		GameObject spark = Instantiate(sparkPrefab, location, Quaternion.identity);

		
		//spark.transform.SetParent(toFollow);
		Spark sparkScript = spark.GetComponent<Spark>();

		//Randomize length, width and speed of blood droplet
		sparkScript.length = Random.Range(.05f, .1f);
		sparkScript.width = Random.Range(.04f, .06f);

		//Randomize time to live
		sparkScript.ttl = Random.Range(.5f, 1f);

		// give it a random velocity
		spark.GetComponent<Rigidbody>().velocity = transform.forward * Random.Range(.5f, 2f);
	    }
	    for(int i = 0; i < (-length5 + 1) * 20; i++)
	    {
		// choose a random x coordinate for the spark
		float x = (Random.Range(length5, 1f));
		// calculate y coordinate based on x coordinate, make it a vector3
		Vector3 location = new Vector3((x * 1.5f) + 4.2f, 0f, x);
		GameObject spark = Instantiate(sparkPrefab, location, Quaternion.identity);

		
		//spark.transform.SetParent(toFollow);
		Spark sparkScript = spark.GetComponent<Spark>();

		//Randomize length, width and speed of blood droplet
		sparkScript.length = Random.Range(.05f, .1f);
		sparkScript.width = Random.Range(.04f, .06f);

		//Randomize time to live
		sparkScript.ttl = Random.Range(.5f, 1f);

		// give it a random velocity
		spark.GetComponent<Rigidbody>().velocity = transform.forward * Random.Range(.5f, 2f);
	    }
	}
	else if (Input.GetButtonDown("Fire3"))
	    Time.timeScale = 1.0f;
	else
	    Time.timeScale = 0.0f;
    }
}
