using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bow : Weapon
{
    // What arrow this bow shoots
    public GameObject arrowPrefab;

    // How fast this bow attacks
    public float bowSpeed = 1f;

    // How fast the arrows shoot
    public float arrowSpeed = 60f;

    // Pitch adjustment for the arrows
    public float pitchAdjustment;

    // How much to lead an enemy
    public float accuracy;

    
    // Sound played when bow fired
    public AudioClip drawSound;
    public AudioClip fireSound;

    // Wind script to attach to arrows
    //public Wind windScript;

    void Start()
    {
	//if(!windScript)
	//windScript = GameObject.Find("Wind").GetComponent<Wind>();
    }

    // Special instructions to change stats other than the usual ones effected by weapons
    public override void Equip(GameObject character)
    {
	// Reference health script
	Health healthScript = character.GetComponent<Health>();
		
	PlayerMovement playerMovement = character.GetComponent<PlayerMovement>();
	if(playerMovement)
	{
	    // Turn on archery
	    playerMovement.archery  = true;

	    PlayerAttack playerAttack = character.GetComponent<PlayerAttack>();
	    if(playerAttack)
	    {
		//playerAttack.windScript = windScript;
		playerAttack.arrowPrefab = arrowPrefab;
		playerAttack.drawSound = drawSound;
		playerAttack.fireSound = fireSound;
		playerAttack.arrowSpeed = arrowSpeed;
		playerAttack.pitchAdjustment = pitchAdjustment;
		playerAttack.maxDistance = maxDistance;
	    }
	}
	else
	{
	    // Turn on archer tag on health script
	    healthScript.isArcher = true;
	    
	    // Turn on ArcherAttack script on character
	    ArcherAttack archerScript = character.GetComponent<ArcherAttack>();

	    // Enable archer script
	    archerScript.enabled = true;
	    
	    if(archerScript)
	    {
		archerScript.enabled = true;
		//archerScript.windScript = windScript;
		archerScript.arrowPrefab = arrowPrefab;
		archerScript.drawSound = drawSound;
		archerScript.fireSound = fireSound;
		archerScript.arrowSpeed = arrowSpeed;
		archerScript.pitchAdjustment = pitchAdjustment;
		archerScript.accuracy = accuracy;
		//archerScript.maxDistance = maxDistance;		
	    }
	    
	    // Tell zombie script to start acting like an archer (keep distance and fire shots)
	    Zombie zombScript = character.GetComponent<Zombie>();
	    if(zombScript)
		zombScript.archer = true;
	}

	// Adjust speed of shoot animation

	foreach(Animator anim in healthScript.animators)
	{
	    anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") * bowSpeed);
	}
    }


    public override void UnEquip(GameObject character)
    {
	// Reference health script
	Health healthScript = character.GetComponent<Health>();
	
	// If character has player script
	PlayerMovement playerMovement = character.GetComponent<PlayerMovement>();
	if(playerMovement)
	{
	    // Turn on archery
	    playerMovement.archery  = false;
	}
	// If character has no player script (must be npc)
	else
	{
	    // Turn on archer tag on health script
	    healthScript.isArcher = true;
	    
	    // Turn on ArcherAttack script on character
	    ArcherAttack archerScript = character.GetComponent<ArcherAttack>();
	    if(archerScript)
		archerScript.enabled = false;

	    // Tell zombie script to stop acting like an archer (run in close and attack with melee)
	    Zombie zombScript = character.GetComponent<Zombie>();
	    if(zombScript)
		zombScript.archer = true;

	    // Turn off archer tag on health script
	    character.GetComponent<Health>().isArcher = false;
	}

	// Adjust speed of shoot animation
	
	foreach(Animator anim in healthScript.animators)
	{
	    anim.SetFloat("BowSpeed", anim.GetFloat("BowSpeed") / bowSpeed);
	}
    }
}
