﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wine : Consumable
{

    // Reference to the text to change the number of uses left
    public Text text;

    public int maxUses;

    public string name;
    
    void Start()
    {
	// Set name of object
	gameObject.name = name + "  " + uses + "//" + maxUses;
    }
    
    public override void Use(GameObject target)
    {
	// Reference the target's Health script
	Health healthScript = target.transform.GetComponent<Health>();
	Stats statScript = target.transform.GetComponent<Stats>();

	
	if(healthScript)
	{
	    //  If less than maxhealth set Health to maxHealth, if used when health full double health
	    if(healthScript.currHealth <= statScript.maxHealth)
		healthScript.currHealth += statScript.maxHealth;
	    else
		healthScript.currHealth -= statScript.maxHealth * Random.Range(.2f, 2.1f);

	    uses -= 1;

	    // Reset name of object
	    gameObject.name = name + "  " + uses + "//" + maxUses;

	    
	    text.text = "      ---Fine Wine---\n\nUses Left: " + uses + "\n\nUsing this potion on a character:\n increases health by maxHealth if Health is not above maxHealth, but\n decreases health by random amount if health is above maxHealth.\n\n----------------------------------\n\nWine has been the blood of the gods going back to at least Dionysus in\n Greece, where his blood was shed to literally feed the animals and the people.\n But really the idea of the harvest being the flesh of god goes back further to at\n least Egypt, and probably Babylon.\n\nIn Egypt the Virgin Mother (Earth) gave her child (the tarvest) to be sacrificed\n (harvested) to sustain everyone.  The Earth was considered a virgin\n (unmarried woman) because she submitted to no man.\n\nThe harvest of vegetation (grapes=blood, wheat=body) was the offspring\n of the mating of the sky god(sun/rain) with the earth goddess(virgin mother.)\n\nThis harvest happens in the month of Virgo, at the end of summer, which is why\n Virgo is the virgin.\n\nIn the rites of Dionysus, the ecstasy produced by consuming spirits was\n the experience of coming closer to the god by the consumption of his flesh,\nin the form of his offspring.\n\nBoth Dionysus and Osiris were associated with resurrection, and the Rites of\n Dionysus, and probably those of Osiris, promised eternal life after death, as life\n on the planet resurrects anew after each winter to sustain the people each year\n with a fresh harvest and the sun resurrects each morning.";
	}
    }
}
