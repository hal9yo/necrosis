using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Armor : MonoBehaviour
{
    // Amount of damage reduction the armor adds to the character
    public float armorAmount;

    // Amount subtracted from speed of character wearing this armor
    public float walkSpeedLess;
    public float runSpeedLess;

    // Reduction to attack speed from armor
    public float attackSpeedMultiplier;
    
    // Name of the sprite sheet of this armor when equiped to player
    public string playerArmSpriteSheet;

    // Name of the armor sprite sheet when equiped to different NPC classes
    public string npcMeleeArmSpriteSheet;
    public string npcArcherArmSpriteSheet;
    public string npcFireArcherArmSpriteSheet;

    // Name of the armor sprite sheet when equiped to different Zombie classes
    public string zombMeleeArmSpriteSheet;
    public string zombArcherArmSpriteSheet;
    public string zombFireArcherArmSpriteSheet;

    // How much power this weapon wields (effects how afraid enemies are of this character)
    public int power;

    
    // Template for methods, of inheriting classes, with special instructions to change stats other than the usual ones effected by Armor
    public virtual void Equip(GameObject character)
    {

    }


    // Template for methods, of inheriting classes, with special instructions to change stats other than the usual ones effected by Armor
    public virtual void UnEquip(GameObject character)
    {

    }
}
