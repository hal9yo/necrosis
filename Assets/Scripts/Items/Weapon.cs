using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : MonoBehaviour
{
    // Minimum and Maximum damage randomly dealt by weapon
    public int damageMin;
    public int damageMax;

    // How far from the character the weapon can hit other objects
    public float attackDistance;

    // Percent chance to block an incoming attack from the front
    public float block;

    // Amount subtracted from speed of character wielding this weapon
    public float walkSpeedLess;
    public float runSpeedLess;
    
    // Name of the sprite sheet of this weapon when equiped to player
    public string playerWeapSpriteSheet;

    // Name of the weapon sprite sheet when equiped to NPC
    public string npcWeapSpriteSheet;

    // Number multiplied to attack speed in the animators of the character
    public float attackSpeed = 1f;

    // The maximum distance this bow shoots
    public float maxDistance;
    public float minDistance;
    public float meleeDistance;

    // How much power this weapon wields (effects how afraid enemies are of this character)
    public int power;
    
    // If weapon is Melee or Bow
    public string type = "melee";

    // Special instructions to change stats other than the usual ones effected by weapons
    public virtual void Equip(GameObject character)
    {

    }


    public virtual void UnEquip(GameObject character)
    {

    }


    // A special case method that allows weapons to change characteristics when damage is dealt or an enemy is killed with them
    public virtual void FeedBack(float dmg, bool killingBlow)
    {
	
    }
}
