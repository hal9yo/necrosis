using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Talisman : MonoBehaviour
{    
    // Template for methods, of inheriting classes, with special instructions to change stats
    public virtual void Equip(GameObject character)
    {

    }


    // Template for methods, of inheriting classes, with special instructions to change stats
    public virtual void UnEquip(GameObject character)
    {

    }
}
