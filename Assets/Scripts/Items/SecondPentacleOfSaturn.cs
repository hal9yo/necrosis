using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SecondPentacleOfSaturn : Talisman
{    
    // Template for methods, of inheriting classes, with special instructions to change stats
    public override void Equip(GameObject character)
    {
	//Reference scripts we will edit
	PlayerManager playerManager = character.GetComponent<PlayerManager>();
	Stats stats = character.GetComponent<Stats>();
	
	if(playerManager)
	{
	    // Add bonus to decrease how much each zombie drains from will	    
	    playerManager.drainBonus += 2f;
	}

	if(stats)
	{
	    Debug.Log(stats.block);
	    
	    stats.block += 20f;
	    
	    Debug.Log(stats.block);
	}
    }


    // Template for methods, of inheriting classes, with special instructions to change stats
    public override void UnEquip(GameObject character)
    {
	//Reference scripts we will edit
	PlayerManager playerManager = character.GetComponent<PlayerManager>();
	Stats stats = character.GetComponent<Stats>();
	
	if(playerManager)
	{
	    // Remove bonus to decrease how much each zombie drains from will
	    playerManager.drainBonus -= 2f;
	}

	if(stats)
	    stats.block -= 20f;
    }
}
