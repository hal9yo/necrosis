﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : MonoBehaviour
{
    // How many times it can be used before it's used up
    public int uses = 1;

    // The script to run when object is used
    public virtual void Use(GameObject target)
    {

    }
}
