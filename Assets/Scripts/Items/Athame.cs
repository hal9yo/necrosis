using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Athame : Weapon
{
    float maxWillMultiplier = .3f;
    float willRegenMultiplier = .15f;

    public Text itemDescription;

    PlayerStats equipedStatScript;
    
    // Special instructions to change stats other than the usual ones effected by weapons
    public override void Equip(GameObject character)
    {
	// Reference the player's stats to adjust will
	PlayerStats statScript = character.GetComponent<PlayerStats>();

	// Remember the stat script of the player equiping this item
	equipedStatScript = statScript;
	
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(willRegenMultiplier);
	}

	// Try to grab the Attack script off the character
	Attack attScript = character.GetComponent<Attack>();
	
	if(attScript)
	{
	    attScript.feedBackScript = this;
	}

	// Try to reference the character's PlayerAttack script
	PlayerAttack playAttScript = character.GetComponent<PlayerAttack>();
	
	if(playAttScript)
	{
	    playAttScript.feedBackScript = this;
	}
    }


    public override void UnEquip(GameObject character)
    {
	// Reference the player's stats to adjust will
	PlayerStats statScript = character.GetComponent<PlayerStats>();

	// Stop remembering the stats of the player equiping this
	equipedStatScript = null;
	
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(-maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(-willRegenMultiplier);
	}

		// Try to grab the Attack script off the character
	Attack attScript = character.GetComponent<Attack>();
	
	if(attScript)
	{
	    attScript.feedBackScript = null;
	}

	// Try to reference the character's PlayerAttack script
	PlayerAttack playAttScript = character.GetComponent<PlayerAttack>();
	
	if(playAttScript)
	{
	    playAttScript.feedBackScript = null;
	}
    }



    // Run this method whenever this weapon deals damage
    public override void FeedBack(float dmg, bool killingBlow)
    {
	// First remove the Will bonuses so only the adjusted values are applied
	// Reference the player's stats to adjust will
	PlayerStats statScript = equipedStatScript;

	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(-maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(-willRegenMultiplier);
	}	

	
	// Because the weapon was just used decrement both Will bonuses
	maxWillMultiplier -= .01f;
	    
	willRegenMultiplier -= .01f;

	
	// Trim the results to two decimal places
	maxWillMultiplier *= 100f;
	willRegenMultiplier *= 100f;
	maxWillMultiplier = ((int)maxWillMultiplier) * .01f;
	willRegenMultiplier = ((int)willRegenMultiplier) * .01f;


	// Floor the values
	if(maxWillMultiplier < 0f)
	    maxWillMultiplier = 0f;

	if(willRegenMultiplier < 0f)
	    willRegenMultiplier = 0f;

	
	// Now reapply the bonuses with the new values
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(willRegenMultiplier);
	}

	// Update the description of the item to include the new values
	UpdateDescription();
    }


    
    void UpdateDescription()
    {
	itemDescription.text = "---Athame---\n\nMin Dmg: 1\nMax Dmg: 2\nAtt Spd: 1\nBlck Chnce: 15%\nWill: +" + (maxWillMultiplier * 100f) +"%\nWill Rgn: +" + (willRegenMultiplier * 100f) + "%\n\nA dagger consecrated for ceremonial use.\n\nWhen equipped to the Player:\nit slightly increases the Will\n and gives a small increase to Will Regeneration.\n\nA concecrated weapon devoted to spiritual\n purposes loses its spiritual propensities if it\n is used for mundane purposes.\n\nEach time this Weapon is used to Attack or\n Block it loses 1% of its Bonuses to Will and\n Will Regen.\n\nThat being said:\n it deals minimal damage, \n with a low chance to block incoming attacks,\n and a short attack range, \n but a fast attack speed.";
    }
}
