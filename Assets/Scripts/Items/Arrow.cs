﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    //Speed arrow is shot at
    public Vector3 velocity = new Vector3(0f, 0f, 0f);
    Vector3 oldVelocity;
    
    //Reference to the arrow's rigidbody
    private Rigidbody rb;
    
    //Reference to the player shooting the arrow
    public GameObject player;

    // How much damage each arrow deals
    public float damageMax;
    public float damageMin;

    // How long is the arrow head
    public float headLength;

    // How long is the arrow body
    public float bodyLength;

    // How wide is the arrow art
    public float width;

    // Material to render arrow body in
    public Material bodyMat;

    // Material to render arrow head in
    public Material headMat;

    // An image of an arrow to place when arrow lands
    public GameObject spentArrow;

    // Splash image to create a splash
    public GameObject splashPrefab;

    BoxCollider col;

    LineRenderer linerenderer;

    
    Vector3 oldPosition;

    
    void Start()
    {
	rb = GetComponent<Rigidbody>();

	col = GetComponent<BoxCollider>();

	linerenderer = GetComponent<LineRenderer>();
	linerenderer.material = bodyMat;
	linerenderer.startWidth = width;
	linerenderer.endWidth = width;
    }
    void Update()
    {	
	// If we are still in flight, have the velocity change our position
	if(rb)
	    velocity = rb.velocity;
	
	DrawBody(transform.position, transform.position - velocity * bodyLength);

    }


    void LateUpdate()
    {
	oldPosition = transform.position;
	
	// Remember previous velocity
	oldVelocity = velocity;
    }
    
    void OnCollisionEnter(Collision collision)
    {
	// Try to make reference to health of hit object
	Health objectHealth = collision.transform.GetComponent<Health>();
	    
	//Ignore gameobject that shot the arrow and triggers
	if(collision.collider.isTrigger || GameObject.ReferenceEquals(player, collision.gameObject))
	{
	    return;
	}

	else if(objectHealth)
	{
	    // Create a spent arrow object at the position and facing the same direction
	    GameObject arrowRemain = Instantiate(spentArrow, transform.position, Quaternion.identity);

	    // Point the arrow forward
	    arrowRemain.transform.forward = new Vector3(90f, oldVelocity.y, oldVelocity.z);
	    
	    // Parent the spent arrow to the object it hit
	    arrowRemain.transform.parent = collision.transform;
	    
	    // Calculate damage
	    float damageDealt = Random.Range(damageMin, damageMax * velocity.magnitude * .05f);

	    // Apply damage to the hit unit
	    objectHealth.TakeDamage(damageDealt, transform);

	    
	    // Apply force to hit unit's rigidbody
	    collision.transform.GetComponent<Rigidbody>().velocity += velocity * damageDealt * .5f;

	    // Debug.Log(velocity);
	    
	    // Destroy the arrow object
	    Destroy(gameObject);
	}

	else if(collision.gameObject.tag == "Water")
	{
	    // Make a splash
	    Instantiate(splashPrefab, transform.position, Quaternion.identity);

	    // Destroy the arrow
	    Destroy(gameObject);
	}
	
	else if(collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Tree" || collision.gameObject.tag == "Chasm")
	{
	    // Create a spent arrow object at the position and facing the same direction
	    GameObject arrowRemain = Instantiate(spentArrow, oldPosition, Quaternion.identity);

	    // Point the arrow forward
	    arrowRemain.transform.forward = new Vector3(90f, oldVelocity.y, oldVelocity.z);

	    // Destroy the arrow object
	    Destroy(gameObject);
	}
    }
    

    void DrawBody(Vector3 start, Vector3 end, float duration = 0.05f)
    {
	linerenderer.transform.position = start;
	linerenderer.SetPosition(0, start);
	linerenderer.SetPosition(1, end);
    }

    /*
    void DrawHead(Vector3 start, Vector3 end, float duration = 0.05f)
    {
	GameObject myLine = new GameObject();
	myLine.transform.position = start;
	myLine.AddComponent<LineRenderer>();
	LineRenderer lr = myLine.GetComponent<LineRenderer>();
	lr.material = headMat;
	//lr.SetColors(color, color);
	//lr.SetWidth(0.1f, 0.1f);
	lr.startWidth = width;
	lr.endWidth = width;
	lr.SetPosition(0, start);
	lr.SetPosition(1, end);
	GameObject.Destroy(myLine, duration);
    }
    */
}
