﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Potion : Consumable
{
    // Reference to the text to change the number of uses left
    public Text text;
    Stats statScript;
    Health healthScript;
    
    public override void Use(GameObject target)
    {
	// Reference the target's Health script
	healthScript = target.transform.GetComponent<Health>();
	statScript = target.transform.GetComponent<Stats>();


	
	if(healthScript)
	{
	    //  If less than maxhealth set Health to maxHealth, if used when health full double health
	    if(healthScript.currHealth < statScript.maxHealth)
		healthScript.currHealth = statScript.maxHealth;
	    else if(healthScript.currHealth < statScript.maxHealth * 2f)
		healthScript.currHealth += statScript.maxHealth;
	
		uses -= 1;

		text.text = "      ---Potion---/n/nUses Left: " + uses +"/n/nUsing this potion on a character:/n restores their health to full if damaged, and/n increases health to double if in full health.";
	}
    }
}
