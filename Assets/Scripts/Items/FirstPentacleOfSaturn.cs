using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FirstPentacleOfSaturn : Talisman
{    
    // Template for methods, of inheriting classes, with special instructions to change stats
    public override void Equip(GameObject character)
    {
	Debug.Log(character);
	PlayerManager playerManager = character.GetComponent<PlayerManager>();
	if(playerManager)
	{
	    Debug.Log(playerManager);
	    // Set flag that tells zombies not to go rogue when unselected for too long
	    playerManager.zombiesCantRogue = true;

	    // Add bonus to decrease how much each zombie drains from will	    
	    playerManager.drainBonus += 1f;
	}
    }


    // Template for methods, of inheriting classes, with special instructions to change stats
    public override void UnEquip(GameObject character)
    {
	PlayerManager playerManager = character.GetComponent<PlayerManager>();
	if(playerManager)
	{
	    // UnSet flag that tells zombies not to go rogue when unselected for too long
	    playerManager.zombiesCantRogue = false;

	    // Remove bonus to decrease how much each zombie drains from will
	    playerManager.drainBonus -= 1f;
	}
    }
}
