using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Robes : Armor
{
    // Amount to regenerate health every second
    public float healthRegen;

    // Amount to adjust maximum health value
    public float maxHealth;

    
    // Template for methods, of inheriting classes, with special instructions to change stats other than the usual ones effected by Armor
    public override void Equip(GameObject character)
    {
	// Get a reference to the character's health script
	Stats statScript = character.GetComponent<Stats>();

	// Adjust the maximum health by the value of this robe
	statScript.maxHealth += maxHealth;

	// Adjust the health regeneration by the value of this robe
	statScript.healthRegen += healthRegen;
    }


    // Template for methods, of inheriting classes, with special instructions to change stats other than the usual ones effected by Armor
    public override void UnEquip(GameObject character)
    {
	// Get a reference to the character's health script
	Stats statScript = character.GetComponent<Stats>();

	// Adjust the maximum health by the value of this robe
	statScript.maxHealth -= maxHealth;

	// Adjust the health regeneration by the value of this robe
	statScript.healthRegen -= healthRegen;
    }
}
