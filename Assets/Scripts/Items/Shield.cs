using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shield : MonoBehaviour
{
    // Block chance
    public float block;

    // Reduction to attack speed
    public float attackSpeed;

    // Reduction to walk and run speeds
    public float walkSpeed;
    public float runSpeed;

    // Sprite sheet to be used when player equips
    public string playerShieldSprite;

    // Sprite sheet to be used when npc/zombie equips
    public string npcShieldSprite;

    // How much power this weapon wields (effects how afraid enemies are of this character)
    public int power;

    // Special instructions to change stats other than the usual ones effected by shields
    public virtual void Equip(GameObject character)
    {

    }


    public virtual void UnEquip(GameObject character)
    {

    }
}
