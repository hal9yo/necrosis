using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsecratedSword : Weapon
{
    float maxWillMultiplier = .5f;
    float willRegenMultiplier = .25f;

    public Text itemDescription;

    PlayerStats equipedStatScript;
    
    // Special instructions to change stats other than the usual ones effected by weapons
    public override void Equip(GameObject character)
    {
	// Reference the player's stats to adjust will
	PlayerStats statScript = character.GetComponent<PlayerStats>();

	// Remember the stat script of the player equiping this item
	equipedStatScript = statScript;
	
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(willRegenMultiplier);
	}

	// Try to grab the Attack script off the character
	Attack attScript = character.GetComponent<Attack>();
	
	if(attScript)
	{
	    attScript.feedBackScript = this;
	}

	// Try to reference the character's PlayerAttack script
	PlayerAttack playAttScript = character.GetComponent<PlayerAttack>();
	
	if(playAttScript)
	{
	    playAttScript.feedBackScript = this;
	}
    }


    public override void UnEquip(GameObject character)
    {
	// Reference the player's stats to adjust will
	PlayerStats statScript = character.GetComponent<PlayerStats>();

	// Stop remembering the stats of the player equiping this
	equipedStatScript = null;
	
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(-maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(-willRegenMultiplier);
	}

		// Try to grab the Attack script off the character
	Attack attScript = character.GetComponent<Attack>();
	
	if(attScript)
	{
	    attScript.feedBackScript = null;
	}

	// Try to reference the character's PlayerAttack script
	PlayerAttack playAttScript = character.GetComponent<PlayerAttack>();
	
	if(playAttScript)
	{
	    playAttScript.feedBackScript = null;
	}
    }



    // Run this method whenever this weapon deals damage
    public override void FeedBack(float dmg, bool killingBlow)
    {
	// First remove the Will bonuses so only the adjusted values are applied
	// Reference the player's stats to adjust will
	PlayerStats statScript = equipedStatScript;

	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(-maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(-willRegenMultiplier);
	}	

	
	// Because the weapon was just used decrement both Will bonuses
	maxWillMultiplier -= .01f;
	    
	willRegenMultiplier -= .01f;

	
	// Trim the results to two decimal places
	maxWillMultiplier *= 100f;
	willRegenMultiplier *= 100f;
	maxWillMultiplier = ((int)maxWillMultiplier) * .01f;
	willRegenMultiplier = ((int)willRegenMultiplier) * .01f;

	// Floor the values
	if(maxWillMultiplier < 0f)
	    maxWillMultiplier = 0f;

	if(willRegenMultiplier < 0f)
	    willRegenMultiplier = 0f;

	
	// Now reapply the bonuses with the new values
	// If we got a stat script
	if(statScript)
	{
	    // Apply the bonuses to will and willregen
	    statScript.ChangeMaxWillMultiplier(maxWillMultiplier);

	    statScript.ChangeWillRegenMultiplier(willRegenMultiplier);
	}

	// Update the description of the item to include the new values
	UpdateDescription();
    }


    
    void UpdateDescription()
    {
	itemDescription.text = "      ---Consecrated Sword---\n\n Min Dmg: 1\n Max Dmg: 3\n Att Spd: 90%\n Blck Chnce: 25%\n WalkSpd : -.1\n RunSpd : -.2\n Will: +" + maxWillMultiplier + "%\n Will Rgn: +" + willRegenMultiplier + "%\n\nA small sword consecrated to the elements.\nWhen equiped by the Necromancer:\n it decently increases the will of the player \n and gives a small increase to Will Regeneration.\n\nA concecrated weapon devoted to spiritual purposes loses its\n spiritual propensities if it is used for mundane purposes.\n\nEach time this Weapon is used to Attack or Block it loses 1% of\n its Bonuses to Will and Regen.\n\nThat being said:\nIt deals decent damage, \nwith a good chance to block incoming attacks, \na decent attack range,\nand a decent attack speed,\nthat slightly slows movement speed.\n\n--------------------------------------------------\n\nThis is for general use in Banishing for defense against evil forces, and in certain\ninvocations.\nThe Sword should be of medium length and weight.\nPentagrams should be painted on salient portions because it is the lineal figure ofGEBURAH.\nThe Divine and Angelic Names related to GEBURAH are then to be added and also their\nsigils taken from the Rose. \n\n--Israel Regardie, The Golden Dawn";
    }
}
