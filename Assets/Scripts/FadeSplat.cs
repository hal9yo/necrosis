﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeSplat : MonoBehaviour
{
    public LineRenderer linerenderer;

    // Variable to track time since last updated
    float timer;


    public float width;
    

    // How long splat should stick around for
    public float timeToLive = 30f;
    float maxTime;
    
    // How often to update splat
    public float howOften = 2f;




    void Start()
    {
	maxTime = timeToLive;

	width = linerenderer.startWidth;
    }



    
    void Update()
    {
	// Update timer of how often to run miscelaneous code (not very time sensitive, don't run often
	timer += Time.deltaTime;

	// If it's time to run the code again
	if(timer > howOften)
	{
	    // Increment timer to fade out
	    timeToLive -= timer;

	    // Reset timer
	    timer = 0f;

	    // Adjust width of blood line narrower as time fades
	    linerenderer.startWidth = width * timeToLive / maxTime;
	    linerenderer.endWidth = width * timeToLive / maxTime;

	    // Destroy once time to live is reached
	    if(timeToLive <= 0f)
		Destroy(gameObject);
	}
    }
}
