﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ObjectFire : MonoBehaviour
{
    public bool on = false;
    bool alreadyBurnt = false;

    // How close to catching fire the object is
    public float heat;

    // How hot it needs to be to ignite
    public float howHot;
    
    // How much the heat of the object increases each second
    public float heatInc;

    // How long the object can burn before it is spent
    public float fuel = 5f;
    float maxFuel;

    // Which way the wind is blowing, causing fires it spawns in that direction
    public float windXDirection;
    public float windZDirection;

    //Timer for how often to run the code, to save cpu as this code is potentially run on many gameobjects simultaneously
    float timer = 0f;

    //How often to run the main code loop (higher number should reduce slow down in game when fire burns a lot
    public float howOften = .5f;

    // Reference to the grass and dirt art to change from grass to dirt when burnt
    public GameObject sprite;
    public GameObject burntSprite;

    // Sprite renderer for the original sprite to fade it out as it burns
    public Renderer renderer;
    
    // Reference to the fire object
    public GameObject fire;
    public CapsuleCollider fireCollider;

    // The radius to check for objects that can be set on fire if we are burning
    public float radiusToBurn;

    
    // Reference to the sway script to turn off when burnt
    public TreeSway swayScript;

    public bool burnableInWater;
    
    // Minimum and maximum time to wait before spawning more fire
    public float minTime;
    public float maxTime;
    // Timer for how often to try to spread fire
    float spreadTimer = .5f;

    // Array of burnable objects within a given radius
    public List<ObjectFire> burnable;

    // BoxCollider to disable when burnt
    public BoxCollider boxCol;

    // Nav Mesh Obstacle to turn off when burnt so nav agents can walk through
    public UnityEngine.AI.NavMeshObstacle navObstacle;

    public CapsuleCollider capCol;
    
    void Start()
    {

	maxFuel = fuel;
		
	// Initialize spreadTimer
	spreadTimer = Random.Range(minTime, maxTime);

	Vector3 windOffset = new Vector3(windXDirection, 0f, windZDirection);
	// Fill the list of nearby burnable objects
        Collider[] hitColliders =  Physics.OverlapSphere(transform.position + windOffset, radiusToBurn);
	foreach (Collider hitCollider in hitColliders)
        {
            if(hitCollider.GetComponent<ObjectFire>())
	    {
		burnable.Add(hitCollider.GetComponent<ObjectFire>());
	    }
        }
	
	// Randomize how often to run script body
	howOften += Random.Range(-.5f, .5f);

	// If the fire is on, activate everything that goes with it
	if(on)
	{
	    TurnOn();
	}
    }

    void Update()
    {
	if(on && !alreadyBurnt)
	{
	    // Increment timer
	    timer += Time.deltaTime;

	    // If it is time
	    if(timer > howOften)
	    {
		// Call the code to apply changes to the tree and spread fire, tell it how long it has been since it last run so it can update its timers
		ChangeTree(timer);

		// Reset timer
		timer = 0f;
	    }
	}

	// Increase the heat every frame
	heat += heatInc * Time.deltaTime;
	
	if(!on && heat > 0f)
	{
	    if(heat > howHot)
		TurnOn();
	    heat -= .1f * Time.deltaTime;
	}
    }

    void SpreadFire()
    {
	if(burnable.Count > 0)
	{
	    int index = Random.Range(0, burnable.Count);	
	    burnable[index].heat += .4f * Time.deltaTime;
	}
	
    }

    void TurnOn()
    {
	// Turn on the fire object (visible flame sprite
	fire.SetActive(true);

	// Turn on the flag to note that our fire is on
	on = true;

	// Turn on burnt sprite
	burntSprite.SetActive(true);
    }

    void ChangeTree(float changeTime)
    {
	if(fuel > 0f)
	{
	    // Subtract from our fuel by how long it has been
	    fuel -= changeTime;

	    // Fade out our normal sprite over time
	    FadeOut();
	}	    
	else
	{
	    // Turn off unburnt sprite
	    sprite.SetActive(false);
	    
	    //Turn off the fire
	    if(fire)
		fire.GetComponent<GrassFire>().ttl = 0f;
	    
	    //Turn off sway script to stop tree from swaying in the breeze
	    if(swayScript)
		swayScript.on = false;
	    
	    // Set tag that we are done burning so stop changing
	    alreadyBurnt = true;

	    if(boxCol)
		boxCol.enabled = false;

	    if(navObstacle)
		navObstacle.enabled = false;
	    
	    capCol.radius = .1f;
	}

	spreadTimer -= changeTime;

	if(spreadTimer < 0f)
	{
	    SpreadFire();
	    spreadTimer = Random.Range(minTime, maxTime);
	}
    }


    void OnTriggerEnter(Collider other)
    {
	// Ignore our own fire, otherwise increase the heat of the object
	if(other.gameObject.CompareTag("Fire") && other != fireCollider)
	    heatInc += .4f;
    }

    void OnTriggerStay(Collider other)
    {
	if(other.gameObject.CompareTag("Water"))
	{
	    // Put out our fire if it is burning
	    heatInc = 0f;

	    if(!burnableInWater)
		heat = 0f;
	}
    }
    
    void OnTriggerExit(Collider other)
    {
	// Ignore our own fire, otherwise decrease the heat of the object
	if(other != fireCollider)
	    heatInc -= .4f;
    }

    void FadeOut()
    {
	Color objectColor = renderer.material.color;
	float fadeColor = fuel / maxFuel;

	objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeColor);
	
	renderer.material.color = objectColor;
    }
}

