﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireArrow : MonoBehaviour
{
    // Fire object prefab, spawned when fire hits ground
    public GameObject firePrefab;

    // Reference to arrow script so we can ignore object that shot it (self)
    public Arrow arrowScript;

    // Reference made to fire script if arrow hits character
    public CharacterFire fireScript;


    // Amount to set the character on fire when it hits them
    public float burnAmount;


    bool exploded;

    
    // Audio Source to play sound effects
    public AudioSource audioSource;

    // Array of audio clips when firing
    public AudioClip[] fireSound;
    // Array of audio clips when exploding
    public AudioClip[] explodeSound;

    // Refernce to the Fire Master script for spawning fires
    public FireRoot fireRoot;


    void Start()
    {
	// Make a reference to our main arrow script
	arrowScript = GetComponent<Arrow>();

	// Randomize the pitch of the sounds played
	audioSource.pitch = Random.Range(.9f, 1.1f);
	
	// Queu up random firing sound
	audioSource.clip = fireSound[Random.Range(0, fireSound.Length)];
	// Play the sound
	audioSource.Play();
    }

    void OnCollisionEnter(Collision collision)
    {	
	// See if collided object is a character with a fire script so they can be burnt
	fireScript = collision.transform.GetComponentInChildren<CharacterFire>();
	
	//Ignore gameobject that shot the arrow and triggers
	if(exploded || collision.collider.isTrigger || GameObject.ReferenceEquals(arrowScript.player, collision.gameObject))
	{
	    //Debug.Log("return");
	    return;
	}

	// Hit a character with the ability to catch fire
	else if(fireScript)
	{
	    // Make character catch fire
	    fireScript.heat += burnAmount;

	    // Note that the arrow already exploded
	    exploded = true;
	}

	else if(collision.gameObject.tag == "Tree")
	{
	    // Get the fire object of the hit object and make it catch fire
	    collision.gameObject.GetComponentInChildren<ObjectFire>().heat += burnAmount * 15f;

	    // Note that the arrow already exploded
	    exploded = true;
	}	

	else if(collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Wall")
	{
	    // Spawn a fire at the arrow's current location
	    fireRoot.SpawnFire(transform.position, true, null);

	    // Note that the arrow already exploded
	    exploded = true;
	}

	else if(collision.gameObject.tag == "Water")
	{
	    Destroy(gameObject);
	}

	
	// Queu up random explosion sound
	audioSource.clip = explodeSound[Random.Range(0, explodeSound.Length)];
	// Play the sound
	audioSource.Play();
    }
}
