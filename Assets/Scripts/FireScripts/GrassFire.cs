﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GrassFire : MonoBehaviour
{
    // Reference to the tilemap with grass on it
    public Tilemap tileMap;
    // Reference to the grid with grass tilemap on it
    public GridLayout gridLayout;

    // The grass tiles that we can burn
    public Tile[] grassTiles;
    // The dirt tiles the grass tiles change to when burnt
    public Tile[] dirtTiles;

    // Reference to the current wind x direction
    public Wind wind;
    
    public FireRoot fireRoot;
    public AudioSource audioSource;

    // The volume we want the audioSource to play at, update will slowly raise or lower volume to match this
    float setVolume;

    Vector3Int cellPosition;

    float time;
    public float timeElapsed;
    public float timer;
    public float ttl;

    // The scale we want the fire to be at
    public Vector3 setScale;

    // 1/x chance to spawn a new fire when called by master script
    public float spreadSpeed;

    // The root fire that spawned us (if it isn't us)
    public GrassFire ourRoot;
    
    // Is this the start of a new fire? If so it will create a new rootlist for purpose of spreding the fire faster...
    public bool rootFire;
    
    // ...in which case this list will keep track of the fires spawned from the root
    public List<GrassFire>fireList;

    // Boolean that tells us not to mess with tiles
    public bool noTiles;

    // Is this the fire that burns on an object?
    public bool objectFire;

    // How far to spawn fires from this object
    public float spawnDist;

    public float minTtl;
    public float maxTtl;
    
    // Start is called before the first frame update
    void Start()
    {
	if(objectFire)
	{
	    ttl = Random.Range(minTtl, maxTtl);
	}
	// If not object fire get tilemap references for spawning fires, changing grass to dirt
	else
	{
	    if(!tileMap)
		tileMap = GameObject.Find("BaseTilemap").GetComponent<Tilemap>();

	    if(!gridLayout)
		gridLayout = GameObject.Find("BaseTiles").GetComponent<GridLayout>();
	}
	
	if(!wind)
	    wind = GameObject.Find("Wind").GetComponent<Wind>();

	if(!fireRoot)
	    fireRoot = GameObject.Find("FireRoot").GetComponent<FireRoot>();

	    
	// If we haven't hooked up the audiosource to the script yet do so
	if(!audioSource)
	    GetComponent<AudioSource>();

	// Set a random desired volume for the fire object to add variety
	setVolume = Random.Range(.8f, 1f);
    }

    public void OnEnable()
    {
	if(!objectFire)
	{
	    // Get the tilemap position of where this fireobject is
	    cellPosition = gridLayout.WorldToCell(transform.position);
		
	    // Change the tile beneath the fire to dirt so more fire can't spawn on the same spot
	    if(!noTiles)
		tileMap.SetTile(cellPosition, dirtTiles[Random.Range(0, dirtTiles.Length)]);	
	}
	
	// Always start with audio source turned off, to avoid too many sound effects drowining out other game sounds
	audioSource.enabled = false;
	
	// Initialize the time variable
	time = Time.time;
    }

    void Update()
    {	
	float newX = 0f;
	float newY = 0f;
	float newZ = 0f;
	    
	// If our X scale is not where we want it to be, adjust it:
	if(transform.localScale.x < setScale.x)
	    newX = Time.deltaTime * 2f;
	else if(transform.localScale.x > setScale.x)
	    newX = -Time.deltaTime * 2f;


	// If our Y scale is not where we want it to be, adjust it:
	if(transform.localScale.y < setScale.y)
	    newY = Time.deltaTime * 2f;
	else if(transform.localScale.y > setScale.y)
	    newY = -Time.deltaTime * 2f;

	// If our Z scale is not where we want it to be, adjust it:
	if(transform.localScale.z < setScale.z)
	    newZ = Time.deltaTime * 2f;
	else if(transform.localScale.y > setScale.z)
	    newZ = -Time.deltaTime * 2f;

	// Don't go negative
	if(transform.localScale.z + newZ < 0f)
	    newZ = -transform.localScale.z;
	if(transform.localScale.y + newY < 0f)
	    newZ = -transform.localScale.y;
	if(transform.localScale.x + newX < 0f)
	    newX = -transform.localScale.x;

	// Add the numbers we want the scale to change by to the current scale
	transform.localScale += new Vector3(newX, newY, newZ);

	// If audioSource is on and the volume is not where we want it to be
	if(audioSource.enabled && audioSource.volume != setVolume)
	{
	    // Are we turning it off (setting to zero)?
	    if(setVolume == 0f)
	    {
		// If the volume hasn't gotten to 0 yet, slowly lower volume over 1 second
		if(audioSource.volume > 0f)
		{
		    audioSource.volume -= Time.deltaTime;
		}		
		// Volume is already less than 0, so just turn off audioSource
		else
		{
		    // Remove this fireobjet from the list of current sounds playing, if it is on
		    fireRoot.soundOn.Remove(this);
		    
		    // Turn off the audioSource
		    audioSource.enabled = false;
		}
		    
	    }
	    // Not setting to zero, must be turning it up
	    else
	    {
		// If volume hasn't reached the desired setting yet, increase it slowly over 1 second
		if(audioSource.volume < setVolume)
		    audioSource.volume += Time.deltaTime;
		// volume has already reached or exceeded the level we want, set it to desired level so we stop raising it
		else
		    audioSource.volume = setVolume;
	    }
	}

	if(objectFire && Random.Range(0, 50) == 1)
	{
	    UpdateFire();
	}
	
	// If we have shrunk to nothing:
	if(setScale == Vector3.zero && transform.localScale.y < 0f)
	{
	    TurnOff();
	}

	// Increment time to live
	ttl -= Time.deltaTime;

		
	// If ttl up
	if(ttl < 0f)
	{
	    // Set the desired size to zero, initiating process of shrinking fire to nothing
	    setScale = Vector3.zero;

	    // Turn off the audio slowly
	    SoundOff();
	}
    }

    
    // Update is called by FireMaster
    public void UpdateFire()
    {
	float useSpeed;
	
	// Spread faster when it is windy
	useSpeed = spreadSpeed - wind.strength * .25f;
	
	// Take time since we last ran script (not since last frame as scripts do not always run every frame)
	timeElapsed = Time.time - time;


	// Remember the time at the point the script was run for next time it runs
	time = Time.time;


	// increment timer
	timer -= timeElapsed;

	// If time up try to spread fire and reset the timer
	if(timer <= 0f)
	{
	    if(Random.Range(0, useSpeed) < 1)
	    {
		SpreadFire();
	    }
	    timer = Random.Range(.1f, 1f);
	}
    }

    void SpreadFire()
    {
	if(!objectFire)
	{
	    // The minimum and maximum distances we will spawn fire from ourself
	    int xMin = -2;
	    int xMax = 3;
	    int zMin = -2;
	    int zMax = 3;
	    
	    // Alter these distances by the wind direction and speed
	    xMin = Mathf.RoundToInt(xMin + wind.direction.x * wind.strength * .5f);
	    xMax = Mathf.RoundToInt(xMax + wind.direction.x * wind.strength * .5f);
	    zMin = Mathf.RoundToInt(zMin - wind.direction.z * wind.strength * .5f);
	    zMax = Mathf.RoundToInt(zMax - wind.direction.z * wind.strength * .5f);
	    
	    // Cap these distances so fire can always spawn right next to us
	    if(xMin > 0)
		xMin = 0;
	    if(xMax < 1)
		xMax = 1;
	    if(zMin > 0)
		zMin = 0;
	    if(zMax < 1)
		zMax = 1;
	    
	    
	    // Store possible locations on navmesh map
	    Vector3Int location =  new Vector3Int(cellPosition.x + Random.Range(xMin, xMax), cellPosition.y + Random.Range(zMin, zMax), 0);
	    UnityEngine.AI.NavMeshHit hit;
	    	   

	    // Check the location we randomly picked to make sure it is grass, if so spawn fire there
	    foreach(Tile tile in grassTiles)
	    {
		if(tileMap.GetTile(location) == tile)
		{
		    //Debug.Log(tileMap.GetTile(location) + " = " + tile);

		    // Instantiate a new fire object at the chosen location
		    fireRoot.SpawnFire(gridLayout.CellToWorld(location), false, ourRoot);
		}
	    }
	}
	else
	{
	    // The minimum and maximum distances we will spawn fire from ourself
	    float xMin = transform.position.x - spawnDist;
	    float xMax = transform.position.x + spawnDist;
	    float zMin = transform.position.z - spawnDist;
	    float zMax = transform.position.z + spawnDist;
	    
	    // Alter these distances by the wind direction and speed
	    xMin = xMin + wind.direction.x * wind.strength * .5f;
	    xMax = xMax + wind.direction.x * wind.strength * .5f;
	    zMin = zMin - wind.direction.z * wind.strength * .5f;
	    zMax = zMax - wind.direction.z * wind.strength * .5f;


	    Vector3 location = new Vector3(Random.Range(xMin, xMax), 0f, Random.Range(zMin, zMax));
	    
	    // Tell fire root to place one of its fire objects at the chosen location, make it a root fire
	    fireRoot.SpawnFire(location, true, ourRoot);
	}
    }



    

    // Turns on the sound effects for this object
    public void SoundOn()
    {
	if(!audioSource.enabled)
	{
	    // Turn on the audio source to start playing sound
	    audioSource.enabled = true;

	    // Set a random desired volume for the fire object to add variety
	    setVolume = Random.Range(.8f, 1f);

	    // Set a random pitch shift so not all fire sounds are exactly the same (which just sounds artificial)
	    audioSource.pitch = Random.Range(.8f, 1.2f);

	    // Tell the master script this fire object is currently playing sound, to keep track of total number of fire sounds playing
	    fireRoot.soundOn.Add(this);
	}
    }



    
    
    // Turns off the sound effects for this object
    public void SoundOff()
    {
	// Set the desired volume for the fire object to 0
	setVolume = 0f;
    }
    

    // Makes this fire the root fire
    public void BecomeRoot(List<GrassFire> theList)
    {
	// Remember we are the root fire
	rootFire = true;

	// Add us to the masterscript's list
	fireRoot.grassFires.Add(this);

	// Get the list of fires that branch off this root from old root
	fireList = theList;
    }



    

    void TurnOff()
    {
	// if our audioSource is on remove from list of fireobjects with sound on
	if(audioSource.enabled)
	{
	    // If we are in the fire root script's list of fires with sound on
	    if(fireRoot && fireRoot.soundOn.Contains(this))
		//Remove from sound list
		fireRoot.soundOn.Remove(this);
	}

	// Move it really high into the sky to trigger OnTriggerExit on all objects it is currently colliding with
	// This is done because it will decrease the heat being added to other object only when triggerexit occurs
	transform.position = new Vector3(0f, 666f, 0f);

	// Remove from firelist
	fireList.Remove(this);
	    
	// If this is not the root you can safely destroy it now
	if(!rootFire)
	{
	    // Remove us from the root fire's list, if we are in it
	    if(ourRoot && ourRoot.fireList.Contains(this))
		ourRoot.fireList.Remove(this);
		
	    // Turn off this gameobject
	    gameObject.SetActive(false);
	}
	// If this was the rootfire, make another fire root
	else
	{
	    // If the root fire list of fires contains this fire script
	    if(fireRoot && fireRoot.grassFires.Contains(this))
		// Remove this object from the root fire list
		fireRoot.grassFires.Remove(this);
		
	    // Remove from firelist
	    fireList.Remove(this);

	    if(fireList.Count > 1)
		fireList[Random.Range(0, fireList.Count - 1)].BecomeRoot(fireList);

	    // Turn off this gameobject
	    gameObject.SetActive(false);
	}
    }




    
    void OnCollisionEnter(Collision other)
    {
	Debug.Log(other.collider.gameObject);
	
	if(other.collider.gameObject.CompareTag("Water"))
	    TurnOff();
    }
}
