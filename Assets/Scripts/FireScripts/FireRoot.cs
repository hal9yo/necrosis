﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FireRoot : MonoBehaviour
{
    
    // A list of all the root fires
    public List<GrassFire> grassFires;

    // An array of all the fire objects with sound on
    public List<GrassFire> soundOn;

    // The index of the next fireobject to update
    int index = 0;

    // A list of all the fire objects in the level (to spawn new fires with as they are reused)
    public List<GameObject> fires;

    // A list of all the fire scripts so we can manipulate them without having to get them every time
    public List<GrassFire> fireScripts;

    // Shortest and max times fires should live
    public float minTTL;
    public float maxTTL;

    // Smallest and max sizes fires should grow to
    public float minSize;
    public float maxSize;
    
    void Update()
    {	
	if(grassFires.Count > 0)
	{
	    // Choose a random index
	    index = Random.Range(0, grassFires.Count - 1);

	    // If we don't have enough fire sounds running
	    if(soundOn.Count < 30)
	    {
		// Pick a random fire object
		GrassFire thisFire = grassFires[index].fireList[Random.Range(0, grassFires[index].fireList.Count - 1)];
		
		// If it is not already in our sound list
		if(!soundOn.Contains(thisFire));
		{
		    // Turn on random fire object's sound effect
		    thisFire.SoundOn();
		}
	    }
	    // We have enough sounds running, randomly choose one to turn off, for variety
	    /*	    else
		    grassFires[Random.Range(0, grassFires.Count)].fireList[Random.Range(0, grassFires[index].fireList.Count - 1)].SoundOff();	*/	


	    // Run the update script for the fireobject with the current index
	    grassFires[index].fireList[Random.Range(0, grassFires[index].fireList.Count - 1)].UpdateFire();
	}
    }

    public void SpawnFire(Vector3 location, bool isRoot, GrassFire ourRoot)
    {
	// Which fire object should be used next?
	int spawnIndex = 0;

	while(spawnIndex < 100 && fires[spawnIndex].activeSelf)
	{
	    // Increment spawn index and keep searching
	    spawnIndex++;
	}
	// Did we not find an unused index?
	if(spawnIndex == 100)
	{
	   if(!isRoot)
	   {
	       return;
	   }
	   else
	   {
	       spawnIndex = Random.Range(0, 99);
	   }
	}
	
	// Set its location
	fires[spawnIndex].transform.position = location;

	// Set a random size for the fire, around the normal size of a fire tile
	fireScripts[spawnIndex].setScale = new Vector3(Random.Range (minSize, maxSize), Random.Range(minSize + 1f, maxSize + 1f), Random.Range(minSize, maxSize));
	
	// Reset time variables on fire
	fireScripts[spawnIndex].timeElapsed = 0f;
	fireScripts[spawnIndex].timer = Random.Range(.1f, 1f);
	fireScripts[spawnIndex].ttl = Random.Range(minTTL, maxTTL);

	// Turn on the fire object
	fires[spawnIndex].SetActive(true);

	    
	// If it is a root fire
	if(isRoot)
	{
	    // Make the fire as big as it wants to be instantly
	    fires[spawnIndex].transform.localScale = fireScripts[spawnIndex].setScale;
	    
	    // Add the new fire to our list
	    grassFires.Add(fireScripts[spawnIndex]);

	    // Turn on the root flag
	    fireScripts[spawnIndex].rootFire = true;

	    // Set the root to itself
	    fireScripts[spawnIndex].ourRoot = fireScripts[spawnIndex];

	    // Add itself to its fireList
	    fireScripts[spawnIndex].fireList.Add(fireScripts[spawnIndex]);
	}
	// If it is not a root fire
	else
	{
	    // Pass the root fire object we were given on to the new spawn
	    fireScripts[spawnIndex].ourRoot = ourRoot;
	    
	    // Add the new fire to the root firelist
	    ourRoot.fireList.Add(fireScripts[spawnIndex]);
	}

    }
}
