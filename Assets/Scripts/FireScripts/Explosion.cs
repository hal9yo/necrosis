﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Explosion : MonoBehaviour
{
    public GameObject firePrefab;

    public Tilemap tileMap;
    public GridLayout gridLayout;

    public int radius = 1;
    public List<int> radius1 = new List<int> {1, 2, 3, 4, 5, 6, 7, 8};
    public List<int> radius2 = new List<int> {9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
    public List<int> radius3 = new List<int> {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48};

    Vector3Int cellPosition;
	
    
    // Start is called before the first frame update
    void Start()
    {
	gridLayout = GameObject.Find("BaseTiles").GetComponent<GridLayout>();
	tileMap = GameObject.Find("BaseTilemap").GetComponent<Tilemap>();

	cellPosition = gridLayout.WorldToCell(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
	Vector3Int location = Vector3Int.zero;
        if(radius1.Count > 3)
	{
	    int chosen = Random.Range(0, radius1.Count);
	    if(radius1[chosen] == 1)
	    {
		location = new Vector3Int(cellPosition.x - 1, cellPosition.y - 1, 0);
	    }
	    if(radius1[chosen] == 2)
	    {
		location = new Vector3Int(cellPosition.x, cellPosition.y - 1, 0);
	    }
	    if(radius1[chosen] == 3)
	    {
		location = new Vector3Int(cellPosition.x + 1, cellPosition.y - 1, 0);
	    }
	    if(radius1[chosen] == 4)
	    {
		location = new Vector3Int(cellPosition.x + 1, cellPosition.y, 0);
	    }
	    if(radius1[chosen] == 5)
	    {
		location = new Vector3Int(cellPosition.x + 1, cellPosition.y + 1, 0);
	    }
	    if(radius1[chosen] == 6)
	    {
		location = new Vector3Int(cellPosition.x, cellPosition.y + 1, 0);
	    }
	    if(radius1[chosen] == 7)
	    {
		location = new Vector3Int(cellPosition.x - 1, cellPosition.y + 1, 0);
	    }
	    if(radius1[chosen] == 8)
	    {
		location = new Vector3Int(cellPosition.x - 1, cellPosition.y, 0);
	    }
	    radius1.Remove(radius1[chosen]);

	    Instantiate(firePrefab, gridLayout.CellToWorld(location), transform.rotation);
	}
	else
	{
	    if(radius > 1)
	    {
		if(radius2.Count > 6)
		{
		    int chosen = Random.Range(0, radius2.Count);
		    if(radius2[chosen] == 9)
		    {
			location = new Vector3Int(cellPosition.x - 2, cellPosition.y - 2, 0);
		    }
		    if(radius2[chosen] == 10)
		    {
			location = new Vector3Int(cellPosition.x - 1, cellPosition.y - 2, 0);
		    }
		    if(radius2[chosen] == 11)
		    {
			location = new Vector3Int(cellPosition.x, cellPosition.y - 2, 0);
		    }
		    if(radius2[chosen] == 12)
		    {
			location = new Vector3Int(cellPosition.x + 1, cellPosition.y - 2, 0);
		    }
		    if(radius2[chosen] == 13)
		    {
			location = new Vector3Int(cellPosition.x + 2, cellPosition.y - 2, 0);
		    }
		    if(radius2[chosen] == 14)
		    {
			location = new Vector3Int(cellPosition.x + 2, cellPosition.y - 1, 0);
		    }
		    if(radius2[chosen] == 15)
		    {
			location = new Vector3Int(cellPosition.x + 2, cellPosition.y, 0);
		    }
		    if(radius2[chosen] == 16)
		    {
			location = new Vector3Int(cellPosition.x + 2, cellPosition.y + 1, 0);
		    }
		    if(radius2[chosen] == 17)
		    {
			location = new Vector3Int(cellPosition.x + 2, cellPosition.y + 2, 0);
		    }
		    if(radius2[chosen] == 18)
		    {
			location = new Vector3Int(cellPosition.x + 1, cellPosition.y + 2, 0);
		    }
		    if(radius2[chosen] == 19)
		    {
			location = new Vector3Int(cellPosition.x, cellPosition.y + 2, 0);
		    }
		    if(radius2[chosen] == 20)
		    {
			location = new Vector3Int(cellPosition.x - 1, cellPosition.y + 2, 0);
		    }
		    if(radius2[chosen] == 21)
		    {
			location = new Vector3Int(cellPosition.x - 2, cellPosition.y + 2, 0);
		    }
		    if(radius2[chosen] == 22)
		    {
			location = new Vector3Int(cellPosition.x - 2, cellPosition.y + 1, 0);
		    }
		    if(radius2[chosen] == 23)
		    {
			location = new Vector3Int(cellPosition.x - 2, cellPosition.y, 0);
		    }
		    if(radius2[chosen] == 24)
		    {
			location = new Vector3Int(cellPosition.x - 2, cellPosition.y - 1, 0);
		    }
		    radius2.Remove(radius2[chosen]);

		    Instantiate(firePrefab, gridLayout.CellToWorld(location), transform.rotation);
		}
		else
		{
		    if(radius > 2)
		    {
			if(radius3.Count > 8)
			{
			    int chosen = Random.Range(0, radius3.Count);
			    if(radius3[chosen] == 25)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 26)
			    {
				location = new Vector3Int(cellPosition.x - 2, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 27)
			    {
				location = new Vector3Int(cellPosition.x - 1, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 28)
			    {
				location = new Vector3Int(cellPosition.x, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 29)
			    {
				location = new Vector3Int(cellPosition.x + 1, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 30)
			    {
				location = new Vector3Int(cellPosition.x + 2, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 31)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y - 3, 0);
			    }
			    if(radius3[chosen] == 32)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y - 2, 0);
			    }
			    if(radius3[chosen] == 33)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y - 1, 0);
			    }
			    if(radius3[chosen] == 34)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y, 0);
			    }
			    if(radius3[chosen] == 35)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y + 1, 0);
			    }
			    if(radius3[chosen] == 36)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y + 2, 0);
			    }
			    if(radius3[chosen] == 37)
			    {
				location = new Vector3Int(cellPosition.x + 3, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 38)
			    {
				location = new Vector3Int(cellPosition.x + 2, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 39)
			    {
				location = new Vector3Int(cellPosition.x + 1, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 40)
			    {
				location = new Vector3Int(cellPosition.x, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 41)
			    {
				location = new Vector3Int(cellPosition.x - 1, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 42)
			    {
				location = new Vector3Int(cellPosition.x - 2, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 43)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y + 3, 0);
			    }
			    if(radius3[chosen] == 44)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y + 2, 0);
			    }
			    if(radius3[chosen] == 45)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y + 1, 0);
			    }
			    if(radius3[chosen] == 46)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y, 0);
			    }
			    if(radius3[chosen] == 47)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y - 1, 0);
			    }
			    if(radius3[chosen] == 48)
			    {
				location = new Vector3Int(cellPosition.x - 3, cellPosition.y - 2, 0);
			    }
			    radius3.Remove(radius3[chosen]);

			    Instantiate(firePrefab, gridLayout.CellToWorld(location), transform.rotation);
			}
			else
			{
			    Destroy(gameObject);
			}
		    }
		    else
		    {
			Destroy(gameObject);
		    }
		}
	    }
	    else
	    {
		Destroy(gameObject);
	    }
	}
    }
}
