﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFire : MonoBehaviour
{
    // Reference to our fire object (so we don't burn ourselves)
    public GameObject ourFire;
    
    // Reference to our health script so fire can hurt us
    public Health healthScript;

    // How hot the character has gotten, increasing the size of flames and damage taken per second
    public float heat;

    // How much the heat is increasing each second
    float heatInc;

    // The flame sprite attached to this character to turn on make grow as they catch on fire more
    public GameObject flameSprite;

    // Reference to our sprites so we can make it turn black when burnt
    public SpriteRenderer[] spriteRenders;

    // Reference to our NPCnp sound script
    public NPCSounds npcSound;
    
    // A color that goes from 1 to 0 to darken the sprite
    Color charColor;

    // A vector that holds the current size of the flame sprite
    Vector3 flameSize;


    // Variable to tell our NPC script where the fire burning us is, so we can move away from it
    public Vector3 sourceOfFire;


    
    // Start is called before the first frame update
    void Start()
    {
	// Set the starting color to the starting color
	charColor = spriteRenders[0].color;
    }

    // Update is called once per frame
    void Update()
    {

	// Increase heat by increase amount each frame, until we are too burnt (don't keep burning when health < -4)
	if(healthScript.currHealth > -4f)
	    heat += heatInc * Time.deltaTime;
	
	// Don't let heat grow beyond a certain amount
	if(heat > 1f)
	    heat = 1f;
	
	// If we are catching fire
	if(heat > 0f)
	{
	    // If we have a sound script attached
	    if(npcSound)
	    {
		// Scream if we are on fire too much, but only while we are alive
		if(healthScript.currHealth > -.1f)
		{
		    npcSound.PlayRandom(6);
		}
		// Otherwise fade out screaming sound (if it's palying)
		else
		{
		    npcSound.FadeScream();
		}
	    }
	    
	    // Take damage equal to how much we are on fire
	    healthScript.currHealth -= heat * Time.deltaTime;

	    // If the flames sprite is not currently on, turn it on
	    if(!flameSprite.activeSelf)
		flameSprite.SetActive(true);

	    // Make the flames sprite grow with the heat
	    flameSize = new Vector3(heat, heat * 2f, heat);
	    flameSprite.transform.localScale = flameSize;

	    // Make the sprite darken as it is burnt so it looks charred
	    if(healthScript.currHealth < 1f && charColor.r > 0)
	    {
		charColor = new Color(charColor.r - Time.deltaTime, charColor.g - Time.deltaTime, charColor.b - Time.deltaTime, 1f);
		foreach(SpriteRenderer render in spriteRenders)
		    render.color = charColor;
	    } 
	    
	    // Have the flames subside over time
	    heat -= .2f * Time.deltaTime;
	}
	else
	{
	    // If the flames sprite is on turn it off
	    if(flameSprite.activeSelf)
		flameSprite.SetActive(false);
	    // If we have an npcsound script
	    if(npcSound)
	    {
		// If we are screaming fade it out
		if(npcSound.burningPlaying)
		    npcSound.FadeScream();
	    }
	}
    }

    void OnTriggerEnter(Collider other)
    {
	if(other.gameObject.CompareTag("Fire") && other.gameObject != ourFire)
	{
	    // Make our heat increase since we are touching fire
	    heatInc += .4f;

	    // Note the location of the fire so we can avoid it if we are burning too much
	    sourceOfFire = other.transform.position;
	}
    }

    void OnTriggerStay(Collider other)
    {
	if(other.gameObject.CompareTag("Water"))
	{
	    // Put out our fire if it is burning
	    heatInc = 0f;

	    heat = 0f;
	}
    }

    void OnTriggerExit(Collider other)
    {
	if(other.gameObject.CompareTag("Fire"))
	{
	    heatInc -= .4f;

	    if(heatInc < 0f)
		heatInc = 0f;
	}
    }
}
