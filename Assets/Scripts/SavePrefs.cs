﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavePrefs : MonoBehaviour
{
    public Slider music;
    public Slider volume;
    public Slider brightness;
    public CameraFollow cameraScript;

    public void OnDisable()
    {
	PlayerPrefs.SetFloat("music", music.normalizedValue);
	PlayerPrefs.SetFloat("volume", volume.normalizedValue);
	PlayerPrefs.SetFloat("brightness", brightness.normalizedValue);
	PlayerPrefs.SetFloat("camYHeight", cameraScript.camYHeight);
    }

    public void OnEnable()
    {
	music.normalizedValue = PlayerPrefs.GetFloat("music");
	volume.normalizedValue = PlayerPrefs.GetFloat("volume");
	brightness.normalizedValue = PlayerPrefs.GetFloat("brightness");
	cameraScript.camYHeight = PlayerPrefs.GetFloat("camYHeight");
    }
}
