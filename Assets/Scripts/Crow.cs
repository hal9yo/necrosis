﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crow : MonoBehaviour
{
    // timer to track when to move
    float timer;

    // Min/Max values of time to wait before doing something in idle
    public float waitMin = .1f;
    public float waitMax = 6f;


    // Height to rise to when flying
    public float flyHeight;

    // Height to rest on the ground
    public float groundHeight;
    

    // Minimum distance to be from waypoint
    public float distDesired;

    // Distance from waypoint to start gliding descent
    public float glideDist;

    // The height we currently want to be, based on our distance to waypoint
    float heightDesired;
    

    // Current up/down speed
    float ySpeed;
    public float maxYSpeed;
    // Current left/right speed
    float xSpeed;
    public float maxXSpeed;
    // Current forward/backward speed
    float zSpeed;
    public float maxZSpeed;

    // Acceleration speed on y axis
    public float yAccel;
    // Acceleration speed on x and z axis
    public float accel;

    
    // The animator for this crow
    public Animator anim;
    
    // The animator for the shadow
    public Animator shadowAnim;

    // Audio source to play caw
    public AudioSource audioSource;

    // Waypoint to move to
    public GameObject waypoint;
    
    // Start is called before the first frame update
    void Start()
    {
	// Randomly initialize timer
        timer = Random.Range(waitMin, waitMax);

	// Link audio source if not already
	if(!audioSource)
	    audioSource = GetComponent<AudioSource>();

	audioSource.pitch = Random.Range(.95f, 1.05f);
    }

    // Update is called once per frame
    void Update()
    {
	// Get our ground position
	Vector3 groundPosition = new Vector3(transform.position.x, 0.5f, transform.position.z);
	
	// Get distance to waypoint
	float distance = Vector3.Distance(groundPosition, waypoint.transform.position);

	//Debug.Log(groundPosition + " - " + waypoint.transform.position + " = " + distance);

	
	// Are we close enough to our waypoint?
	if(distance <= distDesired)
	{
	    // Are we grounded?
	    if(transform.position.y <= groundHeight)
	    {
		// Increment timer
		timer -= Time.deltaTime;

		// If time to do something
		if (timer < 0f)
		{
		    // Perform random idle action
		    DoSomething();
		}
	    }
	    // Not grounded, decelerate
	    else
	    {
		xSpeed = 0f;
		zSpeed = 0f;
	    }
	}
	// We are not close enough to waypoint
	else
	{
	    
	    Debug.Log(heightDesired);
	    
	 // Adjust height for flying/landing:

	    // Are we close enough to way point to glide downwards?
	    if(distance <= glideDist)
	    {
		heightDesired = groundHeight;
	    }
	    // We are not close enough to glide, stay in air
	    else
	    {
		heightDesired = flyHeight;
	    }
	    

	    
	    // Are we lower than the height we want to be?
	    if(transform.position.y < heightDesired)
	    {
		ySpeed += yAccel;
		if(ySpeed > maxYSpeed)
		    ySpeed = maxYSpeed;
	    }	    
	    // Are we higher than we want to be?
	    else if(transform.position.y > heightDesired)
	    {
		ySpeed -= yAccel;
		if(ySpeed < -maxYSpeed)
		    ySpeed = -maxYSpeed;
	    }
	    // We are at the height we want to be
	    else
	    {
		//if(ySpeed > 0)
		//    ySpeed -= yAccel;
		//else if(ySpeed < 0)
		//    ySpeed += yAccel;
		ySpeed = 0f;
	    }
         // Adjust speed for moving:

	    // Are we to the left of where we want to be?
	    if(transform.position.x < waypoint.transform.position.x)
	    {
		//xSpeed += accel;
		//if(xSpeed > maxXSpeed)
		    xSpeed = maxXSpeed;
	    }
	    // Are we to the right of where we want to be?
	    else if(transform.position.x > waypoint.transform.position.x)
	    {
		//xSpeed -= accel;
		//if(xSpeed < -maxXSpeed)
		    xSpeed = -maxXSpeed;
	    }

	    
	    // Are we to in behind of where we want to be?
	    if(transform.position.z < waypoint.transform.position.z)
	    {
		//zSpeed += accel;
		//if(zSpeed > maxZSpeed)
		    zSpeed = maxZSpeed;
	    }
	    // Are we in front of where we want to be?
	    else if(transform.position.z > waypoint.transform.position.z)
	    {
		//zSpeed -= accel;
		//if(zSpeed < -maxZSpeed)
		    zSpeed = -maxZSpeed;
	    }
	}

	// Apply speed to current position
	Vector3 movement = new Vector3(xSpeed, ySpeed, zSpeed);
	transform.position += movement;
    }

    void DoSomething()
    {
	// Random chance to look or caw
	if(Random.Range(0, 2) <1)
	{
	    // Random chance to either look or stop looking over shoulder
	    if(Random.Range(0, 2) <1)
	    {
		anim.SetBool("Look", true);
		shadowAnim.SetBool("Look", true);
	    }
	    else
	    {
		anim.SetBool("Look", false);
		shadowAnim.SetBool("Look", false);
	    }
	}
	else
	{
	    // Random chance to caw
	    if(Random.Range(0, 2) <1)
	    {
		anim.SetTrigger("Caw");
		shadowAnim.SetTrigger("Caw");
		audioSource.Play();
	    }
	}

	// Set timer randomly to wait to do something again
	timer = Random.Range(waitMin, waitMax);
    }
}
