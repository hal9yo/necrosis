﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour
{
    // Reference to our animator
    public Animator anim;
    
    // Start is called before the first frame update
    void OnEnable()
    {
	// Make the splash a random size
        transform.localScale = new Vector3(Random.Range(.2f, 2f), Random.Range(.2f, 2f), 1f);

	// Make the animation speed random
	anim.speed = Random.Range(.8f, 1.2f);
    }

    // When the splash animation is done playing
    public void AnimationDone()
    {
	// Destroy this game object
	Destroy(gameObject);
    }
}
