_Necrosis_


Necrosis is a Real Time Strategy videogame with Action Role Playing Game elements.


Built in Unity 2019.4.20f1

All code original and written by myself, including some examples more or less exactly from public coding boards.




_Credits_

licenses:
CC-BY 3.0
 https://creativecommons.org/licenses/by/3.0/

CC0 1.0
 https://creativecommons.org/publicdomain/zero/1.0/

CC-BY-SA 3.0
 https://creativecommons.org/licenses/by-sa/3.0/

CC-BY-SA 4.0
 https://creativecommons.org/licenses/by-sa/4.0/deed.en

CC-BY 4.0
 https://creativecommons.org/licenses/by/4.0/

CC BY-NC 3.0
 https://creativecommons.org/licenses/by-nc/3.0/

GPL 3.0
 http://www.gnu.org/licenses/gpl-3.0.html

GPL 2.0
 http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

OGA-BY 3.0
 http://opengameart.org/content/oga-by-30-faq

FFC
 https://www.1001fonts.com/licenses/ffc.html

Public Domain
 https://en.wikipedia.org/wiki/public_domain

PD Mark 1.0
 https://creativecommons.org/publicdomain/mark/1.0/

Open Content Program
 http://www.getty.edu/about/opencontent.html



--Art By:

Clint Bellanger (CC-BY 3.0)
 https://opengameart.org/users/clint-bellanger

mart (CC0 1.0)
 https://opengameart.org/users/mart

Galun (CC0 1.0)
 https://opengameart.org/content/wooden-barrel

Clint Bellanger, Blarumyrran, crowline, Justin Nichol (CC0)
 https://opengameart.org/users/clint-bellanger

Stagnation (CC-BY 3.0)
 https://opengameart.org/users/stagnation

Revangale (CC-BY-SA 3.0)
 https://opengameart.org/users/revangale

0x0077BE (CC0 1.0)
 https://commons.wikimedia.org/wiki/File:Dirt_and_Mud_002_-_Loose_Dirt_with_Plants.jpg

Mikodrak (CC0 1.0)
 https://opengameart.org/users/mikodrak

VWolfdog (CC0 1.0)
 https://opengameart.org/users/vwolfdog
 
cron (CC0 1.0)
 https://opengameart.org/users/cron

Jess McCarthy (CC-BY 3.0)
 https://opengameart.org/users/jesse-mccarthy

William.Thompsonj, Hugh Spectrum, Sharm
 CC-BY 4.0, CC-BY 3.0, GPL 3.0, GPL 2.0, OGA-BY 3.0
 https://opengameart.org/users/williamthompsonj
 https://opengameart.org/users/hughspectrum
 https://opengameart.org/users/sharm

OwlishMedia (CC0 1.0)
 https://opengameart.org/users/owlishmedia

Proxy Games (CC0 1.0)
 https://opengameart.org/users/proxy-games

*Unknown Author* (Public Domain)
 https://commons.wikimedia.org/wiki/File:Broadswords_of_Miskolc_02.jpg

Pearson Scott Foresman (Public Domain)
 https://en.wikipedia.org/wiki/Pearson_Scott_Foresman

Quandtum (CC-BY-SA 3.0)
 https://opengameart.org/users/quandtum

Vincent LaClair (CC0 1.0)
 https://opengameart.org/users/vincent-laclair

J. Paul Getty Museum (Open Content Program)
 http://www.getty.edu/art/gettyguide/artObjectDetails?artobj=1481

Macesito (CC BY-SA 4.0)
 https://commons.wikimedia.org/wiki/User:Macesito
 
15th century scribe, Internet Archive scanner; colorization by AnonMoos (PD-OLD-100; PD-ART; PD-SELF)
 https://archive.org/details/twotudorbooksofa00fostuoft

Lucian Pavel (CC0 1.0)
 https://opengameart.org/users/lucian-pavel

Rubber Duck (CC0 1.0)
 https://opengameart.org/users/rubberduck

feudalwars (CC0 1.0)
 https://opengameart.org/users/feudalwars

Bleed (CC BY 3.0)
 https://opengameart.org/users/bleed

Daniel Anderson (CC0 1.0)
 https://twitter.com/silveralv

Scribe (CC BY-SA 3.0)
 https://opengameart.org/users/scribe



--Fonts by:

Dieter Steffmann (FFC)
 https://www.1001fonts.com/users/steffmann/


-- Voicelines by:
Thomas Baumgardner (Public Domain)


-- Screams by:
Halie Symmons (Public Domain)
 https://gitlab.com/hal9yo


--Sounds by:

flag2 (CC BY 3.0)
 https://freesound.org/people/flag2/sounds/63318/

drummy (CC0 1.0)
 https://freesound.org/people/drummy/sounds/458887/

macohibs (CC0 1.0)
 https://freesound.org/people/macohibs/sounds/461645/

Stephan (Public Domain)
 https://soundbible.com/1717-Creaking-Door-Spooky.html

Mike Koenig (CC BY 3.0)
 https://soundbible.com/2106-Draw-Bow-and-Fire.html
 https://soundbible.com/764-Whine.html
 https://soundbible.com/1476-Dog-Growl-Then-Bark.html

_nuel (CC0 1.0)
 https://freesound.org/people/_nuel/sounds/54973/

InspectorJ (CC BY 4.0)
 https://freesound.org/people/InspectorJ/sounds/418262/
 https://freesound.org/people/InspectorJ/sounds/352099/
 https://freesound.org/people/InspectorJ/sounds/416710/
 https://freesound.org/people/InspectorJ/sounds/398702/

christophe1138 (CC0 1.0)
 https://freesound.org/people/christophe1138/sounds/554071/

anton (Noncommercial 3.0)
 https://soundbible.com/1408-Man-Choking.html

florianreichelt (CC0 1.0)
 https://freesound.org/people/florianreichelt/sounds/460509/

soundscalpel.com (CC BY 3.0)
 https://freesound.org/people/soundscalpel.com/sounds/110393/

Ploor (CC0 1.0)
 https://soundbible.com/2100-Splash-Rock-In-Lake.html

SoundsForHim (CC0 1.0)
 https://freesound.org/people/SoundsForHim/sounds/399616/

FunWithSound (CC BY 4.0)
 https://freesound.org/people/FunWithSound/sounds/361485/

Reitanna (CC0 1.0)
 https://freesound.org/people/Reitanna/sounds/332661/

Mark DiAngelo (CC BY 3.0)
 https://soundbible.com/1810-Wind.html

Spleencast (CC BY-NC 3.0)
 https://freesound.org/people/Spleencast/sounds/87289/

Daniel Simon (CC BY 3.0)
 https://soundbible.com/2149-Dogs-Howling.html

Sabotovat (CC BY 3.0)
 https://freesound.org/people/sabotovat/sounds/414350/

EminYILDIRIM (CC BY 4.0)
 https://freesound.org/people/EminYILDIRIM/sounds/547368/

deleted_user_3424813 (CC0 1.0)
 https://freesound.org/people/deleted_user_3424813/sounds/190595/

Paresh (CC0 1.0)
 https://freesound.org/people/Paresh/sounds/158780/




--Music:

Mass No. 2 In E Minor
 by Anton Bruckner; Wiener Kammerchor; Orchester Der Wiener Staatsoper; Hans Gillesberger
 https://archive.org/details/lp_mass-no-2-in-e-minor_anton-bruckner-wiener-kammerchor-orchester/disc1/01.01.+Kyrie-Festive.mp3

Nocturne in C minor, Op. 48 no. 1
 by Frédéric Chopin; performed by Luke Faulkner (PD Mark 1.0)
 https://musopen.org/music/111-nocturnes-op-48/

Preludes, Op. 28 no. 4
Preludes, Op. 28 no. 15
 by Frédéric Chopin; performed by Ivan Ilic (CC BY 3.0)
 https://musopen.org/music/82-preludes-op-28/

Sposa Son Disprezzata
 by Geminiano Giacomelli; performed by La ruta de Ace
 https://www.youtube.com/watch?v=8idaptJf0dE

The Requiem Mass in D Minor (K. 626)
 https://archive.org/details/TheRequiemMassInDMinorK.626/The+Requiem+Mass+in+D+minor+(K.+626)+-+I.+Introitus+Requiem+aeternam.mp3
